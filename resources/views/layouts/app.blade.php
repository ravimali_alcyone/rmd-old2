<!DOCTYPE html>
<html lang="en">
    <!-- BEGIN: Head -->
    <head>
        <meta charset="utf-8">
        <link href="{{asset('dist/images/fevicon.svg')}}" rel="shortcut icon">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="{{ config('app.name') }} admin ">
        <meta name="keywords" content="{{ config('app.name') }} admin">
        <meta name="author" content="Alcyone">
        <title>{{ config('app.name') }} Admin</title>
		<meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="stylesheet" href="{{asset('dist/css/app.css')}}" />
        <link rel="stylesheet" href="{{asset('css/custom.css')}}" />
        <link rel="stylesheet" href="{{asset('plugins/sweetalert/sweetalert.css')}}" />
        <link rel="stylesheet" href="{{asset('assets/css/lightslider.css')}}" />
		<link rel="stylesheet" href="{{asset('dashboard/css/lightbox.min.css') }}" >
		<link href="{{asset('assets/css/jquery-toaster.css')}}" rel="stylesheet" />

        <script src="{{ asset('js/jquery.min.js')}}"></script>	
		<script src="{{asset('assets/js/admin-custom.js')}}"></script>
		<script src="{{asset('assets/js/jquery-toaster.min.js')}}"></script>
        <script src="{{asset('plugins/ckeditor/ckeditor.js')}}"></script>
		
        <!-- Moment -->
        <script src="{{ asset('dashboard/plugins/fullcalendar/moment.min.js') }}"></script>
				
		<!--Datetimepicker-->
    	<link href="{{ asset('assets/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" />
    	<script src="{{ asset('assets/js/bootstrap-datetimepicker.min.js') }}"></script>
		
    </head>
    <!-- Loader -->
    <div class="loader" style="display:none;">
        <img src="{{asset('assets/images/loading.gif')}}" alt="loading">
    </div>	
    <!-- END: Head -->
		<?php
            if(auth() && auth()->user()->menu_permissions != ''){
                $privileges = json_decode(auth()->user()->menu_permissions,true);
            }else{
                $privileges = array();
            }

            $online_visit_new = App\Http\Controllers\admin\OnlineVisitController::getVisitCount('online');
            $concierge_visit_new = App\Http\Controllers\admin\OnlineVisitController::getVisitCount('concierge');
			
			#script for getting notification and count..
			use App\Notifications;
			use App\Library\Services\CommonService;
			$common = new CommonService();
			
			$user_id = auth()->user()->id;
			$notifications = Notifications::select('notifications.*','payment_logs.amount')
			->join('payment_logs', function ($join) {
				$join->on('notifications.post_id', '=', 'payment_logs.online_visit_id');
			})
			->where('notification_type','admin')->where('status',1)
			->orderBy('id','DESC')
			->get();
			
			$notifications_count = Notifications::select('notifications.*')->where('notification_type','admin')->where('status',1)->orderBy('id','DESC')->count();
			
		?>

    <body class="app">
        <!-- BEGIN: Mobile Menu -->
        <div class="mobile-menu md:hidden">
            <div class="mobile-menu-bar">
                <a href="{{ route('admin.home') }}" class="flex mr-auto">
                    <img alt="{{ config('app.name') }} Admin" class="w-6" src="{{asset('dist/images/logo.svg')}}">
                </a>
                <a href="javascript:;" id="mobile-menu-toggler"> <i data-feather="bar-chart-2" class="w-8 h-8 text-white transform -rotate-90"></i> </a>
            </div>
            <ul class="border-t border-theme-24 py-5 hidden">
                <li>
                    <a href="{{ route('admin.home') }}" class="menu" <?php if(isset($page) && $page == 'dashboard'){ echo 'menu--active';}?>>
                        <div class="menu__icon"> <i data-feather="home"></i> </div>
                        <div class="menu__title"> Dashboard </div>
                    </a>
                </li>
			<?php if(($privileges && in_array('visit_pool',$privileges)) || auth()->user()->user_role == 1){?>
                <li>
                    <a href="javascript:;" class="menu">
                        <div class="menu__icon">
                            <img src="{{ asset('dist/images/peoples_white.svg') }}" alt="">
                        </div>
                        <div class="menu__title"> Visit Pool <i data-feather="chevron-down" class="menu__sub-icon"></i> </div>
                    </a>
                    <ul class="">
                        <li>
                            <a href="{{ url('/admin/asynchronous_visits') }}" class="menu online_visits <?php if(isset($page) && $page == 'asynchronous_visits'){ echo 'menu--active';}?>">
                                <div class="menu__icon"> <i data-feather="monitor"></i> </div>
								<div class="menu__title"> Asynchronous  @if($online_visit_new > 0)<span class="custom_badge text-xs px-1 rounded-full bg-theme-1 text-white ml-5"><?php echo $online_visit_new;?></span>@endif</div>
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('/admin/synchronous_visits') }}" class="menu online_visits <?php if(isset($page) && $page == 'synchronous_visits'){ echo 'menu--active';}?>">
                                <div class="menu__icon"> <i data-feather="monitor"></i> </div>
								<div class="menu__title"> Synchronous  @if($online_visit_new > 0)<span class="custom_badge text-xs px-1 rounded-full bg-theme-1 text-white ml-5"><?php echo $online_visit_new;?></span>@endif</div>
                            </a>
                        </li>						
                        <li>
                            <a href="{{ url('/admin/concierge_visits') }}" class="menu concierge_visits <?php if(isset($page) && $page == 'concierge_visits'){ echo 'menu--active';}?>">
                                <div class="menu__icon"> <i data-feather="user"></i> </div>
                                <div class="menu__title"> Concierge @if($concierge_visit_new > 0)<span class="custom_badge text-xs px-1 rounded-full bg-theme-1 text-white ml-5"><?php echo $concierge_visit_new;?></span>@endif</div>
                            </a>
                        </li>
                    </ul>
                </li>
		<?php } ?>
		<?php if(($privileges && in_array('visit_forms',$privileges)) || auth()->user()->user_role == 1){?>
                <li>
                    <a href="{{ url('/admin/visit_forms') }}" class="menu service_pricing" <?php if(isset($page) && $page == 'visit_forms'){ echo 'menu--active';}?>>
                        <div class="menu__icon"> <i data-feather="file-text"></i> </div>
                        <div class="menu__title"> Visit Forms </div>
                    </a>
                </li>
		<?php } ?>
		<?php if(($privileges && in_array('manage_orders',$privileges)) || auth()->user()->user_role == 1){?>
                <li>
                    <a href="#" class="menu">
                        <div class="menu__icon"> <i data-feather="shopping-cart"></i> </div>
                        <div class="menu__title"> Orders </div>
                    </a>
                </li>
		<?php } ?>
		<?php //if(($privileges && in_array('manage_products',$privileges)) || auth()->user()->user_role == 1){?>
                <!--li>
                    <a href="{{ url('admin/products') }}" class="menu <?php if(isset($page) && $page == 'products'){ echo 'menu--active';}?>">
                        <div class="menu__icon"> <i data-feather="tag"></i> </div>
                        <div class="menu__title"> Products </div>
                    </a>
                </li-->
		<?php //} ?>
		<?php if(($privileges && in_array('manage_providers',$privileges)) || auth()->user()->user_role == 1){?>
                <li>
                    <a href="{{ url('admin/providers') }}" class="menu doctors <?php if(isset($page) && $page == 'providers'){ echo 'menu--active';}?>">
                        <div class="menu__icon"> <i data-feather="user"></i> </div>
                        <div class="menu__title"> Providers </div>
                    </a>
                </li>
		<?php } ?>
		<?php if(($privileges && in_array('manage_patients',$privileges)) || auth()->user()->user_role == 1){?>
                <li>
                    <a href="{{ url('admin/patients') }}" class="menu patients" <?php if(isset($page) && $page == 'patients'){ echo 'menu--active';}?>>
                        <div class="menu__icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="26.83" height="22.009" viewBox="0 0 26.83 22.009">
                                <g id="hospital_real" transform="translate(0 -45.999)">
                                    <path id="Path_19" data-name="Path 19" d="M25.258,50.191H20.227v-2.62A1.574,1.574,0,0,0,18.655,46H8.175A1.574,1.574,0,0,0,6.6,47.571v2.62H1.572A1.574,1.574,0,0,0,0,51.763V66.436a1.574,1.574,0,0,0,1.572,1.572H25.258a1.574,1.574,0,0,0,1.572-1.572V51.763A1.574,1.574,0,0,0,25.258,50.191Zm-5.031,1.048H21.8v1.572a.525.525,0,0,1-.524.524h-.524a.525.525,0,0,1-.524-.524ZM8.175,47.047h10.48a.525.525,0,0,1,.524.524v2.62H18.131V48.619a.524.524,0,0,0-.524-.524H9.223a.524.524,0,0,0-.524.524v1.572H7.651v-2.62A.525.525,0,0,1,8.175,47.047Zm8.908,3.668a.525.525,0,0,1-.524.524H10.271a.525.525,0,0,1-.524-.524V49.143h7.336ZM6.6,51.239v1.572a.525.525,0,0,1-.524.524H5.555a.525.525,0,0,1-.524-.524V51.239Zm-5.031,0h2.41v1.572a1.574,1.574,0,0,0,1.572,1.572h.524a1.574,1.574,0,0,0,1.572-1.572V51.239H8.788a1.575,1.575,0,0,0,1.482,1.048h6.288a1.575,1.575,0,0,0,1.482-1.048h1.138v1.572a1.574,1.574,0,0,0,1.572,1.572h.524a1.574,1.574,0,0,0,1.572-1.572V51.239h2.41a.525.525,0,0,1,.524.524v13.1H1.048v-13.1a.525.525,0,0,1,.524-.524Zm24.21,15.2a.525.525,0,0,1-.524.524H1.572a.525.525,0,0,1-.524-.524v-.524H25.782Z" fill="<?php if(isset($page) && $page == 'patients'){ echo '#ff776d';} else { echo '#fff'; }?>"/>
                                    <path id="Path_20" data-name="Path 20" d="M197.965,232.289h2.358v-1.965h1.965v-2.358h-1.965V226h-2.358v1.965H196v2.358h1.965Z" transform="translate(-185.729 -170.57)" fill="<?php if(isset($page) && $page == 'patients'){ echo '#ff776d';} else { echo '#fff'; }?>"/>
                                </g>
                            </svg>
                        </div>
                        <div class="menu__title"> Patients </div>
                    </a>
                </li>
		<?php } ?>
		<?php if(($privileges && in_array('manage_services',$privileges)) || auth()->user()->user_role == 1){?>
                <li>
                    <a href="{{ url('/admin/services') }}" class="menu services" <?php if(isset($page) && $page == 'services'){ echo 'menu--active';}?>>
                        <div class="menu__icon"> <i data-feather="briefcase"></i> </div>
                        <div class="menu__title"> Services/Treatments </div>
                    </a>
                </li>
		<?php } ?>
		<?php if(($privileges && in_array('manage_treatment_medicines',$privileges)) || auth()->user()->user_role == 1){?>
                <li>
                    <a href="{{ url('/admin/treatment_medicines') }}" class="menu treatment_medicines" <?php if(isset($page) && $page == 'treatment_medicines'){ echo 'menu--active';}?>>
                        <div class="menu__icon"> <i data-feather="tag"></i> </div>
                        <div class="menu__title"> Prescription Products</div>
                    </a>
                </li>
		<?php } ?>
		<?php if(($privileges && in_array('manage_contact_us',$privileges)) || auth()->user()->user_role == 1){?>
                <li>
                    <a href="{{ url('/admin/contact_us') }}" class="menu contact_us" <?php if(isset($page) && $page == 'contact_us'){ echo 'menu--active';}?>>
                        <div class="menu__icon"> <i data-feather="phone"></i> </div>
                        <div class="menu__title"> Contact Us </div>
                    </a>
                </li>
		<?php } ?>
		<?php if(($privileges && in_array('manage_finances',$privileges)) || auth()->user()->user_role == 1){?>
                <li>
                    <a href="#" class="menu">
                        <div class="menu__icon"> <i data-feather="dollar-sign"></i> </div>
                        <div class="menu__title"> Finances </div>
                    </a>
                </li>
		<?php } ?>
		<?php if(($privileges && in_array('manage_reports',$privileges)) || auth()->user()->user_role == 1){?>
                <li>
                    <a href="javascript:;" class="menu">
                        <div class="menu__icon"> <i data-feather="file-text"></i> </div>
                        <div class="menu__title"> Reports <i data-feather="chevron-down" class="menu__sub-icon"></i> </div>
                    </a>
                    <ul class="">
                        <li>
                            <a href="{{ route('admin.reports.prescription_products') }}" class="menu">
                                <div class="menu__icon"> <i data-feather="activity"></i> </div>
                                <div class="menu__title"> Product Data </div>
                            </a>
                        </li>					
                        
                        <li>
                            <a href="{{ route('admin.reports.providers') }}" class="menu">
                                <div class="menu__icon"> <i data-feather="activity"></i> </div>
                                <div class="menu__title"> Provider Data </div>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('admin.reports.patients') }}" class="menu">
                                <div class="menu__icon"> <i data-feather="activity"></i> </div>
                                <div class="menu__title"> Patient Data </div>
                            </a>
                        </li>
                    </ul>
                </li>
		<?php } ?>		
		<?php if(($privileges && in_array('manage_website_content',$privileges)) || auth()->user()->user_role == 1){?>
                <li>
                    <a href="javascript:;" class="menu">
                        <div class="menu__icon"> <i data-feather="layout"></i> </div>
                        <div class="menu__title"> Website Content <i data-feather="chevron-down" class="menu__sub-icon"></i> </div>
                    </a>
                    <ul class="">
                        <li>
                            <a href="{{ url('/admin/content/home') }}" class="menu">
                                <div class="menu__icon"> <i data-feather="activity"></i> </div>
                                <div class="menu__title"> Home </div>
                            </a>
                        </li>					
                        <li>
                            <a href="#" class="menu">
                                <div class="menu__icon"> <i data-feather="activity"></i> </div>
                                <div class="menu__title"> About </div>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="menu">
                                <div class="menu__icon"> <i data-feather="activity"></i> </div>
                                <div class="menu__title"> Terms </div>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="menu">
                                <div class="menu__icon"> <i data-feather="activity"></i> </div>
                                <div class="menu__title"> Privacy Policy </div>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="menu">
                                <div class="menu__icon"> <i data-feather="activity"></i> </div>
                                <div class="menu__title"> Contact Us </div>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="menu">
                                <div class="menu__icon"> <i data-feather="activity"></i> </div>
                                <div class="menu__title"> How it works </div>
                            </a>
                        </li>
                    </ul>
                </li>
		<?php } ?>	
		<?php if(($privileges && in_array('sub_admins',$privileges)) || auth()->user()->user_role == 1){?>
                <li>
                    <a href="{{ route('admin.subadmins') }}" class="menu sub_admins" <?php if(isset($page) && $page == 'sub_admins'){ echo 'menu--active';}?>>
                        <div class="menu__icon"> <i data-feather="command"></i> </div>
                        <div class="menu__title"> Sub Admins </div>
                    </a>
                </li>
		<?php } ?>
		<?php if(($privileges && in_array('manage_blogs',$privileges)) || auth()->user()->user_role == 1){?>
                <li>
                    <a href="{{ url('admin/blogs') }}" class="menu <?php if(isset($page) && $page == 'blogs'){ echo 'menu--active';}?>">
                        <div class="menu__icon"> <i data-feather="pen-tool"></i> </div>
                        <div class="menu__title"> Blogs </div>
                    </a>
                </li>
		<?php } ?>
		<?php if(($privileges && in_array('manage_faq',$privileges)) || auth()->user()->user_role == 1){?>
                <li>
                    <a href="{{ url('/admin/faqs') }}" class="menu faq" <?php if(isset($page) && $page == 'faq'){ echo 'menu--active';}?>>
                        <div class="menu__icon"> <i data-feather="message-square"></i> </div>
                        <div class="menu__title"> FAQ </div>
                    </a>
                </li>
		<?php } ?>
		<?php if(($privileges && in_array('manage_service_pricing',$privileges)) || auth()->user()->user_role == 1){?>
                <li>
                    <a href="{{ url('/admin/service_pricing') }}" class="menu service_pricing" <?php if(isset($page) && $page == 'service_pricing'){ echo 'menu--active';}?>>
                        <div class="menu__icon"> <i data-feather="shopping-bag"></i> </div>
                        <div class="menu__title"> Service Pricing </div>
                    </a>
                </li>
		<?php } ?>
		<?php if(($privileges && in_array('manage_plan_features',$privileges)) || auth()->user()->user_role == 1){?>
                <li>
                    <a href="{{ url('/admin/plan_features') }}" class="menu plan_features" <?php if(isset($page) && $page == 'plan_features'){ echo 'menu--active';}?>>
                        <div class="menu__icon"> <i data-feather="star"></i> </div>
                        <div class="menu__title"> Plan Features </div>
                    </a>
                </li>
		<?php } ?>		
            </ul>
        </div>
        <!-- END: Mobile Menu -->
        
		
		<div class="flex">
            <!-- BEGIN: Side Menu -->
            <nav class="side-nav">
                <a href="{{ route('admin.home') }}" class="intro-x flex items-center pl-5 pt-4">
                    <img alt="{{ config('app.name') }} Admin" class="w-6" src="{{asset('dist/images/logo.svg')}}">
                    <span class="hidden xl:block text-white text-lg ml-3"> Replenish<span class="font-medium">MD</span> </span>
                </a>
                <div class="side-nav__devider my-6"></div>
                <ul>
                    <li>
                        <a href="{{ route('admin.home') }}" class="side-menu <?php if(isset($page) && $page == 'dashboard'){ echo 'side-menu--active';}?>">
                            <div class="side-menu__icon"> <i data-feather="home"></i> </div>
                            <div class="side-menu__title"> Dashboard </div>
                        </a>
                    </li>
                    <?php if(($privileges && in_array('visit_pool',$privileges)) || auth()->user()->user_role == 1){?>
                        <li>
                            <a href="javascript:;" class="side-menu <?php if(isset($page) && ( $page == 'asynchronous_visits' || $page == 'synchronous_visits' || $page == 'concierge_visits' )){ echo 'side-menu--active';}?>">
                                <div class="side-menu__icon">
                                    @if(isset($page) && $page == 'asynchronous_visits' || $page == 'synchronous_visits' || $page == 'concierge_visits')
                                        <img src="{{ asset('dist/images/peoples_red.svg') }}" alt="">
                                    @else
                                        <img src="{{ asset('dist/images/peoples_white.svg') }}" alt="">
                                    @endif
                                </div>
                                <div class="side-menu__title"> Visit Pool <i data-feather="chevron-down" class="side-menu__sub-icon"></i> </div>
                            </a>
                            <ul class="<?php if(isset($page) && ( $page == 'asynchronous_visits' || $page == 'synchronous_visits' || $page == 'concierge_visits' )){ echo 'side-menu__sub-open';}?>">
                                <li>
                                    <a href="{{ url('/admin/asynchronous_visits') }}" class="side-menu <?php if(isset($page) && $page == 'asynchronous_visits'){ echo 'side-menu--active';}?>">
                                        <div class="side-menu__icon"> <i data-feather="monitor"></i> </div>
                                        <div class="side-menu__title"> Asynchronous  @if($online_visit_new > 0)<span class="custom_badge text-xs px-1 rounded-full bg-theme-1 text-white ml-5"><?php echo @$online_visit_new;?></span>@endif</div>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('/admin/synchronous_visits') }}" class="side-menu <?php if(isset($page) && $page == 'synchronous_visits'){ echo 'side-menu--active';}?>">
                                        <div class="side-menu__icon"> <i data-feather="monitor"></i> </div>
                                        <div class="side-menu__title"> Synchronous  @if($online_visit_new > 0)<span class="custom_badge text-xs px-1 rounded-full bg-theme-1 text-white ml-5"><?php echo @$online_visit_new;?></span>@endif</div>
                                    </a>
                                </li>								
                                <li>
                                    <a href="{{ url('/admin/concierge_visits') }}" class="side-menu  <?php if(isset($page) && $page == 'concierge_visits'){ echo 'side-menu--active';}?>">
                                        <div class="side-menu__icon"> <i data-feather="user"></i> </div>
                                        <div class="side-menu__title"> Concierge  @if($concierge_visit_new > 0)<span class="custom_badge text-xs px-1 rounded-full bg-theme-1 text-white ml-5"><?php echo @$concierge_visit_new;?></span>@endif</div>
                                    </a>
                                </li>
                            </ul>					
                        </li>
            <?php } ?>
		<?php if(($privileges && in_array('visit_forms',$privileges)) || auth()->user()->user_role == 1){?>
                    <li>
                        <a href="{{ url('/admin/visit_forms') }}" class="side-menu <?php if(isset($page) && $page == 'visit_forms'){ echo 'side-menu--active';}?>">
                            <div class="side-menu__icon"> <i data-feather="file-text"></i> </div>
                            <div class="side-menu__title"> Visit Forms </div>
                        </a>
                    </li>
		<?php } ?>
		<?php if(($privileges && in_array('manage_orders',$privileges)) || auth()->user()->user_role == 1){?>
                    <li>
                        <a href="#" class="side-menu">
                            <div class="side-menu__icon"> <i data-feather="shopping-cart"></i> </div>
                            <div class="side-menu__title"> Orders </div>
                        </a>
                    </li>
		<?php } ?>
		<?php //if(($privileges && in_array('manage_products',$privileges)) || auth()->user()->user_role == 1){?>
                    <!--li>
                        <a href="{{ url('admin/products') }}" class="side-menu <?php if(isset($page) && $page == 'products'){ echo 'side-menu--active';}?>">
                            <div class="side-menu__icon"> <i data-feather="tag"></i> </div>
                            <div class="side-menu__title"> Products </div>
                        </a>
                    </li-->
		<?php //} ?>
		<?php if(($privileges && in_array('manage_providers',$privileges)) || auth()->user()->user_role == 1){?>
                    <li>
                        <a href="{{ url('admin/providers') }}" class="side-menu <?php if(isset($page) && $page == 'providers'){ echo 'side-menu--active';}?>">
                            <div class="side-menu__icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="26.83" height="22.009" viewBox="0 0 26.83 22.009">
                                    <g id="hospital_real" transform="translate(0 -45.999)">
                                        <path id="Path_19" data-name="Path 19" d="M25.258,50.191H20.227v-2.62A1.574,1.574,0,0,0,18.655,46H8.175A1.574,1.574,0,0,0,6.6,47.571v2.62H1.572A1.574,1.574,0,0,0,0,51.763V66.436a1.574,1.574,0,0,0,1.572,1.572H25.258a1.574,1.574,0,0,0,1.572-1.572V51.763A1.574,1.574,0,0,0,25.258,50.191Zm-5.031,1.048H21.8v1.572a.525.525,0,0,1-.524.524h-.524a.525.525,0,0,1-.524-.524ZM8.175,47.047h10.48a.525.525,0,0,1,.524.524v2.62H18.131V48.619a.524.524,0,0,0-.524-.524H9.223a.524.524,0,0,0-.524.524v1.572H7.651v-2.62A.525.525,0,0,1,8.175,47.047Zm8.908,3.668a.525.525,0,0,1-.524.524H10.271a.525.525,0,0,1-.524-.524V49.143h7.336ZM6.6,51.239v1.572a.525.525,0,0,1-.524.524H5.555a.525.525,0,0,1-.524-.524V51.239Zm-5.031,0h2.41v1.572a1.574,1.574,0,0,0,1.572,1.572h.524a1.574,1.574,0,0,0,1.572-1.572V51.239H8.788a1.575,1.575,0,0,0,1.482,1.048h6.288a1.575,1.575,0,0,0,1.482-1.048h1.138v1.572a1.574,1.574,0,0,0,1.572,1.572h.524a1.574,1.574,0,0,0,1.572-1.572V51.239h2.41a.525.525,0,0,1,.524.524v13.1H1.048v-13.1a.525.525,0,0,1,.524-.524Zm24.21,15.2a.525.525,0,0,1-.524.524H1.572a.525.525,0,0,1-.524-.524v-.524H25.782Z" fill="<?php if(isset($page) && $page == 'providers'){ echo '#ff776d';} else { echo '#fff'; }?>"/>
                                        <path id="Path_20" data-name="Path 20" d="M197.965,232.289h2.358v-1.965h1.965v-2.358h-1.965V226h-2.358v1.965H196v2.358h1.965Z" transform="translate(-185.729 -170.57)" fill="<?php if(isset($page) && $page == 'providers'){ echo '#ff776d';} else { echo '#fff'; }?>"/>
                                    </g>
                                </svg>
                            </div>
                            <div class="side-menu__title"> Providers </div>
                        </a>
                    </li>
		<?php } ?>
		<?php if(($privileges && in_array('manage_patients',$privileges)) || auth()->user()->user_role == 1){?>
                    <li>
                        <a href="{{ url('admin/patients') }}" class="side-menu <?php if(isset($page) && $page == 'patients'){ echo 'side-menu--active';}?>">
                            <div class="side-menu__icon"> <i data-feather="users"></i> </div>
                            <div class="side-menu__title"> Patients </div>
                        </a>
                    </li>
		<?php } ?>
		<?php if(($privileges && in_array('manage_services',$privileges)) || auth()->user()->user_role == 1){?>
                    <li>
                        <a href="{{ url('/admin/services') }}" class="side-menu <?php if(isset($page) && $page == 'services'){ echo 'side-menu--active';}?>">
                            <div class="side-menu__icon"> <i data-feather="briefcase"></i> </div>
                            <div class="side-menu__title"> Services/Treatments </div>
                        </a>
                    </li>
		<?php } ?>
		<?php if(($privileges && in_array('manage_treatment_medicines',$privileges)) || auth()->user()->user_role == 1){?>
                    <li>
                        <a href="{{ url('/admin/treatment_medicines') }}" class="side-menu <?php if(isset($page) && $page == 'treatment_medicines'){ echo 'side-menu--active';}?>">
                            <div class="side-menu__icon"> <i data-feather="tag"></i> </div>
                            <div class="side-menu__title"> Prescription Products</div>
                        </a>
                    </li>
		<?php } ?>
		<?php if(($privileges && in_array('manage_contact_us',$privileges)) || auth()->user()->user_role == 1){?>
                    <li>
                        <a href="{{ url('/admin/contact_us') }}" class="side-menu <?php if(isset($page) && $page == 'contact_us'){ echo 'side-menu--active';}?>">
                            <div class="side-menu__icon"> <i data-feather="phone"></i> </div>
                            <div class="side-menu__title"> Contact Us </div>
                        </a>
                    </li>
		<?php } ?>
		<?php if(($privileges && in_array('manage_finances',$privileges)) || auth()->user()->user_role == 1){?>
                    <li>
                        <a href="#" class="side-menu">
                            <div class="side-menu__icon"> <i data-feather="dollar-sign"></i> </div>
                            <div class="side-menu__title"> Finances </div>
                        </a>
                    </li>
		<?php } ?>
		<?php if(($privileges && in_array('manage_reports',$privileges)) || auth()->user()->user_role == 1){?>
                    <li>
                        <a href="javascript:;" class="side-menu">
                            <div class="side-menu__icon"> <i data-feather="file-text"></i> </div>
                            <div class="side-menu__title"> Reports <i data-feather="chevron-down" class="side-menu__sub-icon"></i> </div>
                        </a>
                        <ul class="<?php if(isset($parent_page) && ( $parent_page == 'reports' )){ echo 'side-menu__sub-open';}?>">
	
                            <li>
                                <a href="{{ route('admin.reports.prescription_products') }}" class="side-menu <?php if(isset($page) && $page == 'report_prescription_products'){ echo 'side-menu--active';}?>">
                                    <div class="side-menu__icon"> <i data-feather="activity"></i> </div>
                                    <div class="side-menu__title"> Product Data </div>
                                </a>
                            </li>					
                            <!--li>
                                <a href="#" class="side-menu">
                                    <div class="side-menu__icon"> <i data-feather="activity"></i> </div>
                                    <div class="side-menu__title"> Product Sales </div>
                                </a>
                            </li-->
                            <li>
                                <a href="{{ route('admin.reports.providers') }}" class="side-menu <?php if(isset($page) && $page == 'report_providers'){ echo 'side-menu--active';}?>">
                                    <div class="side-menu__icon"> <i data-feather="activity"></i> </div>
                                    <div class="side-menu__title"> Provider Data </div>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('admin.reports.patients') }}" class="side-menu <?php if(isset($page) && $page == 'report_patients'){ echo 'side-menu--active';}?>">
                                    <div class="side-menu__icon"> <i data-feather="activity"></i> </div>
                                    <div class="side-menu__title"> Patient Data </div>
                                </a>
                            </li>
                        </ul>
                    </li>
		<?php } ?>		
		<?php if(($privileges && in_array('manage_website_content',$privileges)) || auth()->user()->user_role == 1){?>
                    <li>
                        <a href="javascript:;" class="side-menu">
                            <div class="side-menu__icon"> <i data-feather="layout"></i> </div>
                            <div class="side-menu__title"> Website Content <i data-feather="chevron-down" class="side-menu__sub-icon"></i> </div>
                        </a>
                        <ul class="<?php if(isset($parent_page) && ( $parent_page == 'website_content' )){ echo 'side-menu__sub-open';}?>">
	
                            <li>
                                <a href="{{ url('/admin/content/home') }}" class="side-menu <?php if(isset($page) && $page == 'home'){ echo 'side-menu--active';}?>">
                                    <div class="side-menu__icon"> <i data-feather="activity"></i> </div>
                                    <div class="side-menu__title"> Home </div>
                                </a>
                            </li>					
                            <li>
                                <a href="#" class="side-menu">
                                    <div class="side-menu__icon"> <i data-feather="activity"></i> </div>
                                    <div class="side-menu__title"> About </div>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="side-menu">
                                    <div class="side-menu__icon"> <i data-feather="activity"></i> </div>
                                    <div class="side-menu__title"> Terms </div>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="side-menu">
                                    <div class="side-menu__icon"> <i data-feather="activity"></i> </div>
                                    <div class="side-menu__title"> Privacy Policy </div>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="side-menu">
                                    <div class="side-menu__icon"> <i data-feather="activity"></i> </div>
                                    <div class="side-menu__title"> Contact Us </div>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="side-menu">
                                    <div class="side-menu__icon"> <i data-feather="activity"></i> </div>
                                    <div class="side-menu__title"> How it works </div>
                                </a>
                            </li>
                            <li>
                                <a href="{{ url('/admin/testimonials') }}" class="side-menu <?php if(isset($page) && $page == 'testimonials'){ echo 'side-menu--active';}?>">
                                    <div class="side-menu__icon"> <i data-feather="activity"></i> </div>
                                    <div class="side-menu__title"> Testimonials </div>
                                </a>
                            </li>
                            <li>
                                <a href="{{ url('/admin/sponsors') }}" class="side-menu <?php if(isset($page) && $page == 'sponsors'){ echo 'side-menu--active';}?>">
                                    <div class="side-menu__icon"> <i data-feather="activity"></i> </div>
                                    <div class="side-menu__title"> Sponsors </div>
                                </a>
                            </li>
                        </ul>
                    </li>
		<?php } ?>	
		
		<?php if(($privileges && in_array('manage_sub_admin',$privileges)) || auth()->user()->user_role == 1){?>
                    <li>
                        <a href="{{ route('admin.subadmins') }}" class="side-menu <?php if(isset($page) && $page == 'sub_admins'){ echo 'side-menu--active';}?>">
                            <div class="side-menu__icon"> <i data-feather="command"></i> </div>
                            <div class="side-menu__title"> Sub Admins </div>
                        </a>
                    </li>
		<?php } ?>
		<?php if(($privileges && in_array('manage_blogs',$privileges)) || auth()->user()->user_role == 1){ ?>
				<li>
					<a href="javascript:;" class="side-menu">
						<div class="side-menu__icon"> <i data-feather="pen-tool"></i> </div>
						<div class="side-menu__title"> Blog <i data-feather="chevron-down" class="side-menu__sub-icon"></i> </div>
					</a>
					<ul class="<?php if(isset($parent_page) && ( $parent_page == 'blogs' )){ echo 'side-menu__sub-open';}?>">
						<li>
							<a href="{{ route('admin.blogs.blog_category') }}" class="side-menu" <?php if(isset($page) && $page == 'blog_category'){ echo 'side-menu--active';}?>>
								<div class="side-menu__icon"> <i data-feather="activity"></i> </div>
								<div class="side-menu__title"> Blog Category </div>
							</a>
						</li>
                        <li>
							<a href="{{ route('admin.blogs.category_topic') }}" class="side-menu" <?php if(isset($page) && $page == 'category_topic'){ echo 'side-menu--active';}?>>
								<div class="side-menu__icon"> <i data-feather="activity"></i> </div>
								<div class="side-menu__title"> Category Topic </div>
							</a>
						</li>
                        <li>
							<a href="{{ route('admin.blogs.blog_post_type') }}" class="side-menu" <?php if(isset($page) && $page == 'blog_post_type'){ echo 'side-menu--active';}?>>
								<div class="side-menu__icon"> <i data-feather="activity"></i> </div>
								<div class="side-menu__title"> Blog Post Type </div>
							</a>
						</li>
						<li>
							<a href="{{ url('admin/blogs') }}" class="side-menu <?php if(isset($page) && $page == 'blogs'){ echo 'side-menu--active';}?>">
                            <div class="side-menu__icon"> <i data-feather="activity"></i> </div>
								<div class="side-menu__title"> Blogs List </div>
							</a>
                        </li>
					</ul>
				</li>
		<?php } ?>
		
		
		<?php if(($privileges && in_array('manage_forum',$privileges)) || auth()->user()->user_role == 1){?>
                    <li>
                        <a href="javascript:;" class="side-menu">
                            <div class="side-menu__icon"> <i data-feather="layout"></i> </div>
                            <div class="side-menu__title"> Forums <i data-feather="chevron-down" class="side-menu__sub-icon"></i> </div>
                        </a>
                        <ul class="<?php if(isset($parent_page) && ( $parent_page == 'forums' )){ echo 'side-menu__sub-open';}?>">
							<li>
                                <a href="{{ url('/admin/forums') }}" class="side-menu <?php if(isset($page) && $page == 'forums'){ echo 'side-menu--active';}?>">
                                    <div class="side-menu__icon"> <i data-feather="activity"></i> </div>
                                    <div class="side-menu__title"> Forum List </div>
                                </a>
                            </li>
                            <li>
                                <a href="{{ url('/admin/forums/posts') }}" class="side-menu <?php if(isset($page) && $page == 'forum_topics'){ echo 'side-menu--active';}?>">
                                    <div class="side-menu__icon"> <i data-feather="activity"></i> </div>
                                    <div class="side-menu__title"> Forum Post </div>
                                </a>
                            </li>
							
                            
							
						</ul>
					</li>
		<?php } ?>
							
		<?php if(($privileges && in_array('manage_faq',$privileges)) || auth()->user()->user_role == 1){?>
                    <li>
                        <a href="{{ url('/admin/faqs') }}" class="side-menu <?php if(isset($page) && $page == 'faq'){ echo 'side-menu--active';}?>">
                            <div class="side-menu__icon"> <i data-feather="message-square"></i> </div>
                            <div class="side-menu__title"> FAQ </div>
                        </a>
                    </li>
		<?php } ?>
		<?php if(($privileges && in_array('manage_service_pricing',$privileges)) || auth()->user()->user_role == 1){?>
                    <li>
                        <a href="{{ url('/admin/service_pricing') }}" class="side-menu <?php if(isset($page) && $page == 'service_pricing'){ echo 'side-menu--active';}?>">
                            <div class="side-menu__icon"> <i data-feather="shopping-bag"></i> </div>
                            <div class="side-menu__title"> Service Pricing </div>
                        </a>
                    </li>
		<?php } ?>
		<?php if(($privileges && in_array('manage_plan_features',$privileges)) || auth()->user()->user_role == 1){?>
                    <li>
                        <a href="{{ url('/admin/plan_features') }}" class="side-menu <?php if(isset($page) && $page == 'plan_features'){ echo 'side-menu--active';}?>">
                            <div class="side-menu__icon"> <i data-feather="star"></i> </div>
                            <div class="side-menu__title"> Plan Features </div>
                        </a>
                    </li>
		<?php } ?>			
                </ul>
            </nav>
            <!-- END: Side Menu -->
            
			<!-- BEGIN: Content -->
            <div class="content">
                <!-- BEGIN: Top Bar -->
                <div class="top-bar">
                    <!-- BEGIN: Breadcrumb -->
                    <div class="-intro-x breadcrumb mr-auto hidden sm:flex">
						<a href="{{ env('APP_URL') }}" class="">Application</a> 
						<i data-feather="chevron-right" class="breadcrumb__icon"></i> 
						<a href="{{ env('APP_URL') }}/admin" class="">Dashboard</a> 
						
						@if(isset($parent_page) && isset($parent_page_heading))
						<i data-feather="chevron-right" class="breadcrumb__icon"></i> 						
						<a href="#" class="">{{ $parent_page_heading }}</a> 
						@endif
						
						@if(isset($page) && isset($page_heading))
						<i data-feather="chevron-right" class="breadcrumb__icon"></i> 						
						<a href="{{ url()->full() }}" class="breadcrumb--active">{{ $page_heading }}</a> 
						@endif
						
					</div>
                    <!-- END: Breadcrumb -->
                    <!-- BEGIN: Search -->
                    <div class="intro-x relative mr-3 sm:mr-6">
                        <div class="search hidden sm:block">
                            <input type="text" class="search__input input placeholder-theme-13" placeholder="Search...">
                            <i data-feather="search" class="search__icon dark:text-gray-300"></i>
                        </div>
                        <a class="notification sm:hidden" href="#"> <i data-feather="search" class="notification__icon dark:text-gray-300"></i> </a>
                        <div class="search-result">
                            <div class="search-result__content">
                                <div class="search-result__content__title">Pages</div>
                                <div class="mb-5">
                                    <a href="#" class="flex items-center">
                                        <div class="w-8 h-8 bg-theme-18 text-theme-9 flex items-center justify-center rounded-full"> <i class="w-4 h-4" data-feather="inbox"></i> </div>
                                        <div class="ml-3">Mail Settings</div>
                                    </a>
                                    <a href="#" class="flex items-center mt-2">
                                        <div class="w-8 h-8 bg-theme-17 text-theme-11 flex items-center justify-center rounded-full"> <i class="w-4 h-4" data-feather="users"></i> </div>
                                        <div class="ml-3">Users & Permissions</div>
                                    </a>
                                    <a href="#" class="flex items-center mt-2">
                                        <div class="w-8 h-8 bg-theme-14 text-theme-10 flex items-center justify-center rounded-full"> <i class="w-4 h-4" data-feather="credit-card"></i> </div>
                                        <div class="ml-3">Transactions Report</div>
                                    </a>
                                </div>
                                <div class="search-result__content__title">Users</div>
                                <div class="mb-5">
                                    <a href="#" class="flex items-center mt-2">
                                        <div class="w-8 h-8 image-fit">
                                            <img alt="{{ config('app.name') }}" class="rounded-full" src="{{asset('dist/images/profile-5.jpg')}}">
                                        </div>
                                        <div class="ml-3">Nicolas Cage</div>
                                        <div class="ml-auto w-48 truncate text-gray-600 text-xs text-right">nicolascage@left4code.com</div>
                                    </a>
                                    <a href="#" class="flex items-center mt-2">
                                        <div class="w-8 h-8 image-fit">
                                            <img alt="{{ config('app.name') }}" class="rounded-full" src="{{asset('dist/images/profile-7.jpg')}}">
                                        </div>
                                        <div class="ml-3">Al Pacino</div>
                                        <div class="ml-auto w-48 truncate text-gray-600 text-xs text-right">alpacino@left4code.com</div>
                                    </a>
                                    <a href="#" class="flex items-center mt-2">
                                        <div class="w-8 h-8 image-fit">
                                            <img alt="{{ config('app.name') }}" class="rounded-full" src="{{asset('dist/images/profile-5.jpg')}}">
                                        </div>
                                        <div class="ml-3">Leonardo DiCaprio</div>
                                        <div class="ml-auto w-48 truncate text-gray-600 text-xs text-right">leonardodicaprio@left4code.com</div>
                                    </a>
                                    <a href="#" class="flex items-center mt-2">
                                        <div class="w-8 h-8 image-fit">
                                            <img alt="{{ config('app.name') }}" class="rounded-full" src="{{asset('dist/images/profile-10.jpg')}}">
                                        </div>
                                        <div class="ml-3">Robert De Niro</div>
                                        <div class="ml-auto w-48 truncate text-gray-600 text-xs text-right">robertdeniro@left4code.com</div>
                                    </a>
                                </div>
                                <div class="search-result__content__title">Products</div>
                                <a href="#" class="flex items-center mt-2">
                                    <div class="w-8 h-8 image-fit">
                                        <img alt="{{ config('app.name') }}" class="rounded-full" src="{{asset('dist/images/preview-6.jpg')}}">
                                    </div>
                                    <div class="ml-3">Dell XPS 13</div>
                                    <div class="ml-auto w-48 truncate text-gray-600 text-xs text-right">PC &amp; Laptop</div>
                                </a>
                                <a href="#" class="flex items-center mt-2">
                                    <div class="w-8 h-8 image-fit">
                                        <img alt="{{ config('app.name') }}" class="rounded-full" src="{{asset('dist/images/preview-14.jpg')}}">
                                    </div>
                                    <div class="ml-3">Samsung Galaxy S20 Ultra</div>
                                    <div class="ml-auto w-48 truncate text-gray-600 text-xs text-right">Smartphone &amp; Tablet</div>
                                </a>
                                <a href="#" class="flex items-center mt-2">
                                    <div class="w-8 h-8 image-fit">
                                        <img alt="{{ config('app.name') }}" class="rounded-full" src="{{asset('dist/images/preview-8.jpg')}}">
                                    </div>
                                    <div class="ml-3">Samsung Galaxy S20 Ultra</div>
                                    <div class="ml-auto w-48 truncate text-gray-600 text-xs text-right">Smartphone &amp; Tablet</div>
                                </a>
                                <a href="#" class="flex items-center mt-2">
                                    <div class="w-8 h-8 image-fit">
                                        <img alt="{{ config('app.name') }}" class="rounded-full" src="{{asset('dist/images/preview-4.jpg')}}">
                                    </div>
                                    <div class="ml-3">Sony A7 III</div>
                                    <div class="ml-auto w-48 truncate text-gray-600 text-xs text-right">Photography</div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- END: Search -->
                    <!-- BEGIN: Notifications -->
                    <div class="intro-x dropdown relative mr-auto sm:mr-6">
                        <div class="dropdown-toggle notification notification--bullet cursor-pointer">
							<span class="notif-count n_t"> {{$notifications_count}} </span>
							<i data-feather="bell" class="notification__icon dark:text-gray-300"></i> 
						</div>
                        <div class="notification-content dropdown-box mt-8 absolute top-0 left-0 sm:left-auto sm:right-0 z-20 -ml-10 sm:ml-0" style="height: 400px;overflow: auto;">
                            <div class="notification-content__box dropdown-box__content box dark:bg-dark-6" id="pdNotify_n">
                                <div class="notification-content__title notice_count" data-count="{{$notifications_count}}">Notifications (
									<span class="notif-count"> {{$notifications_count}} </span>)
								</div>
								@if(!empty($notifications))
								@foreach($notifications as $val)
								<?php
								$message = ($val->notice_type=='asynchronous_visit_payment') ? 'Payment amount '.$val->amount.'$ paid by '.$val->created_name.' for Asynchronous Visit.' : '';
								?>
                                <div class="notif_main_row">
									<div class="cursor-pointer relative flex items-center mt-5">
										<div class="w-12 h-12 flex-none image-fit mr-1">
											<img alt="{{ $val->created_name }}" class="rounded-full" src="/{{ $val->created_image }}">
											<div class="w-3 h-3 bg-theme-9 absolute right-0 bottom-0 rounded-full border-2 border-white"></div>
										</div>
										<div class="ml-2 overflow-hidden">
											<div class="flex items-center">
												<a href="javascript:;" class="font-medium truncate mr-5">{{ $val->created_name }}</a>
												<div class="text-xs text-gray-500 ml-auto whitespace-no-wrap">{{ date(env('DATE_FORMAT_PHP_H'), strtotime($val->created_at)) }}</div>
											</div>
											<div class="w-full truncate text-gray-600">{{ $message }}</div>
										</div>
									</div>
								</div>
								@endforeach
								@endif
                            </div>
                        </div>
                    </div>
                    <!-- END: Notifications -->
                    <!-- BEGIN: Account Menu -->
                    <div class="intro-x dropdown w-8 h-8 relative">
                        <div class="dropdown-toggle w-8 h-8 rounded-full overflow-hidden shadow-lg image-fit zoom-in">
                        @if(auth()->user()->image != null)
                            <img alt="{{auth()->user()->name}}" src="{{env('APP_URL')}}/{{auth()->user()->image}}">
                        @else
                            <img alt="{{auth()->user()->name}}" src="{{env('APP_URL')}}/dist/images/user_icon.png">
                        @endif
                        </div>
                        <div class="dropdown-box mt-10 absolute w-56 top-0 right-0 z-20">
                            <div class="dropdown-box__content box bg-theme-38 dark:bg-dark-6 text-white">
                                <div class="p-4 border-b border-theme-40 dark:border-dark-3">
								<?php if(auth()->user()){?>
								<div class="font-medium">{{auth()->user()->name}}</div>
                                    <div class="text-xs text-theme-41 dark:text-gray-600"><?php
									if(auth()->user()->category == '1'){
										echo 'Head Manager';
									}elseif(auth()->user()->category == '2'){
										echo 'Backend Engineer';
									}elseif(auth()->user()->category == '3'){
										echo 'Supervisor';
									}elseif(auth()->user()->category == '4'){
										echo 'Branch Manager';
									}
									?></div>
                                </div>
								<?php } ?>
                                <div class="p-2">
                                    <a href="{{ route('admin.profile') }}" class="flex items-center block p-2 transition duration-300 ease-in-out hover:bg-theme-1 dark:hover:bg-dark-3 rounded-md"> <i data-feather="user" class="w-4 h-4 mr-2"></i> Profile </a>
									<a href="#" class="flex items-center block p-2 transition duration-300 ease-in-out hover:bg-theme-1 dark:hover:bg-dark-3 rounded-md"> <i data-feather="settings" class="w-4 h-4 mr-2"></i> Settings </a>
                                    <a href="#" class="flex items-center block p-2 transition duration-300 ease-in-out hover:bg-theme-1 dark:hover:bg-dark-3 rounded-md"> <i data-feather="user-plus" class="w-4 h-4 mr-2"></i> Membership </a>
									 <a href="#" class="flex items-center block p-2 transition duration-300 ease-in-out hover:bg-theme-1 dark:hover:bg-dark-3 rounded-md"> <i data-feather="tablet" class="w-4 h-4 mr-2"></i> Advertising </a>

                                    <a href="#" class="flex items-center block p-2 transition duration-300 ease-in-out hover:bg-theme-1 dark:hover:bg-dark-3 rounded-md"> <i data-feather="help-circle" class="w-4 h-4 mr-2"></i> Help </a>
                                </div>
                                <div class="p-2 border-t border-theme-40 dark:border-dark-3">
                                    <a href="{{ route('logout') }}" class="flex items-center block p-2 transition duration-300 ease-in-out hover:bg-theme-1 dark:hover:bg-dark-3 rounded-md" onclick="event.preventDefault();document.getElementById('logout-form').submit();"> <i data-feather="toggle-right" class="w-4 h-4 mr-2"></i> Logout </a>
									<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END: Account Menu -->
                </div>
                <!-- END: Top Bar -->

				@yield('content')

            </div>
            <!-- END: Content -->
        </div>
        <!-- BEGIN: Dark Mode Switcher-->
        <div style="display:none;" class="dark-mode-switcher shadow-md fixed bottom-0 right-0 box dark:bg-dark-2 border rounded-full w-40 h-12 flex items-center justify-center z-50 mb-10 mr-10">
            <div class="mr-4 text-gray-700 dark:text-gray-300">Dark Mode</div>
            <input class="input input--switch border" type="checkbox" value="1">
        </div>
        <!-- END: Dark Mode Switcher-->
		
		<!--Pusher access key--->
		<input type="hidden" id="user_id" value="{{ auth()->user()->id }}">
		<input type="hidden" id="user_role" value="{{ auth()->user()->user_role }}">
		<input type="hidden" id="pusher_api_key" value="{{ env('PUSHER_APP_KEY') }}">
		<input type="hidden" id="pusher_channel" value="{{ env('PUSHER_APP_CHANNEL') }}">
		<input type="hidden" id="pusher_cluster" value="{{ env('PUSHER_APP_CLUSTER') }}">	
        <!-- BEGIN: JS Assets-->
        <script src="{{asset('dist/js/app.js')}}"></script>
	
		
	@if(isset($parent_page) && $parent_page == 'reports')
		<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
		<script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
		<script src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
		<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js"></script>
		<script src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>

		<link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css" />
		<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.dataTables.min.css" />
	@endif
		
        <script src="{{asset('plugins/sweetalert/sweetalert.min.js')}}"></script>
        <!-- END: JS Assets-->
		<script type="text/javascript" src="{{asset('dashboard/js/lightbox.min.js') }}" ></script>
		
		<!---Pusher page for notification---->
		<script src="//js.pusher.com/3.1/pusher.min.js"></script>
		<script src="{{ asset('dashboard/js/admin_pusher_notification.js') }}"></script>
		
        @yield('script')
    </body>
</html>
