@extends('layouts.app')

@section('content')
<div class="intro-y flex flex-col sm:flex-row items-center mt-8">
	<h2 class="text-lg font-medium mr-auto">Forum Topic List</h2>
	<div class="w-full sm:w-auto flex mt-4 sm:mt-0">
		<div class="intro-y col-span-12 flex flex-wrap sm:flex-no-wrap items-center mt-2">
			<a href="{{ url('admin/forums/add_topic/0') }}" class="button w-32 mr-2 mb-2 flex items-center justify-center btn_white">Add New</a>
		</div>
	</div>
</div>

<div class="grid grid-cols-12 gap-6 mt-5">
	<div class="intro-y datatable-wrapper box p-5 col-span-12 overflow-auto lg:overflow-hidden" id="patients_table">
		<table id="myTable" class="table table-report table-report--bordered display w-full sub_admin_table dataTable mt-0">
			<thead>
				<tr class="intro-x">
					<th class="border-b-2">S.No.</th>
					<th class="border-b-2">Forum</th>
					<th class="border-b-2">Topic</th>
					<th class="border-b-2">Create by</th>
					<th class="border-b-2">Approved</th>
					<th class="border-b-2">Status</th>
					<th class="border-b-2">Action</th>
				</tr>
			</thead>
			<tbody>
			
				@if($forum_topic)
					@php $i=1; @endphp
					@foreach($forum_topic as $value)
						<tr class="intro-x">
							<td class="border-b-2">{{$i}}</td>
							<td class="border-b-2">{{$value->forum_title}}</td>
							<td class="border-b-2">{{$value->topic}}</td>
							<td class="border-b-2">{{$value->create_by}}</td>
							<td class="border-b-2"><input type="checkbox" name="approved" class="input input--switch border" title="Approved" @if($value->is_approved==1) checked @endif onchange="changeApproved({{$value->id}})"></td>
							<td class="border-b-2"><input type="checkbox" name="status" class="input input--switch border" title="Status" @if($value->status==1) checked @endif onchange="changeStatus({{$value->id}})"></td>
							<td class="border-b-2 flex">
								<div class="flex">
									<a href="{{ url('admin/forums/add_topic/'.$value->id.'') }}" class="button button--sm w-16 shadow-md mr-1 mb-2 btn_blue text-white editUser" data-id="{{$value->id}}" title="Edit"> Edit </a>
								</div>
								
								<div class="flex">
									<a href="javascript:void(0)" onclick="deleteRow({{$value->id}})" class="button button--sm w-16 shadow-md mr-1 mb-2 btn_red text-white deleteUser" data-id="{{$value->id}}" title="Delete"> Delete </a>
								</div>	
							</td>
						</tr>
					@php $i+=1; @endphp	
					@endforeach
				@endif
			</tbody>
		</table>
	</div>
</div>

<script type="text/javascript">
 function deleteRow(id) {
		swal({
			title: "Are you sure to delete?",
			text: "",
			icon: 'warning',
			buttons: {
			  cancel: true,
			  delete: 'Yes, Delete It'
			}
		  }).then((isConfirm) => {
			if (!isConfirm) {
				return false;
			} else {
				$.ajax({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
					type: "DELETE",
					url: "{{ env('APP_URL').'/admin/forums/topic_destroy' }}"+'/'+id,
					success: function (response) {
						if(response['status'] == 'success'){
						swal({
							title: response['message'],
							icon: 'success'
						});
						setTimeout(function(){
						swal.close();
						location.reload();
						}, 1000);
						}
						
					},
					error: function (data) {
						console.log('Error:', data);
						swal({
							title: 'Error Occured',
							icon: 'error'
						});
					}
				});
			}
        });
    }

	function changeStatus(id) {
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			url: "{{ url('admin/forums/change_topic_status') }}",
			type: "POST",
			data: {'id': id},
			success: function(response) {
				swal({
					title: 'Update successfully.',
					icon: 'success'
				});
				setTimeout(function(){
				swal.close();
				
				}, 1000);
			}
		});
	}
	function changeApproved(id) {
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			url: "{{ url('admin/forums/change_topic_approved') }}",
			type: "POST",
			data: {'id': id},
			success: function(response) {
				swal({
					title: 'Update successfully.',
					icon: 'success'
				});
				setTimeout(function(){
				swal.close();
				
				}, 1000);
			}
		});
	}
</script>
</html>
@endsection