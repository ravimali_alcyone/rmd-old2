@extends('layouts.app')
@section('content')
    @php
    	$i=1;
	@endphp
	<div class="intro-y box mt-5">
		<div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
			<h2 class="font-medium text-base mr-auto">Home > Info Section</h2>
		</div>
		<div class="intro-y datatable-wrapper box p-5 col-span-12 overflow-auto lg:overflow-hidden">
			<form id="info_sectionForm">
                
                <div class="intro-y col-span-12 lg:col-span-12 mt-5">
					<h2 class="text-lg font-medium mr-auto">Top Page Section</h2>
                </div>
				
                    <div class="menu_section mt-3">
                        <div class="intro-y col-span-12 lg:col-span-6">
                            <div>
                                <label>Main Heading</label>
                                <input type="text" name="top_main_name" value="{{ isset($section_content->top_main_name) ? $section_content->top_main_name : '' }}" class="input w-full border mt-2" placeholder="Main Heading">
                            </div>
                        </div>
                        <div class="intro-y col-span-12 lg:col-span-6 mt-5">
                            <div>
                                <label>Content</label>
                                <input type="text" name="top_main_content" value="{{ isset($section_content->top_main_content) ? $section_content->top_main_content : '' }}" class="input w-full border mt-2" placeholder="Content">
                            </div>
                        </div>						
                    </div>

				<div class="intro-y col-span-12 lg:col-span-12 mt-5">
					<h2 class="text-lg font-medium mr-auto">Info Section Detail</h2>
                </div>
				
                    <div class="menu_section mt-3">
                        <div class="intro-y col-span-12 lg:col-span-6">
                            <div>
                                <label>Main Heading</label>
                                <input type="text" name="main_name" value="{{ isset($section_content->main_name) ? $section_content->main_name : '' }}" class="input w-full border mt-2" placeholder="Main Heading">
                            </div>
                        </div>
                        <div class="intro-y col-span-12 lg:col-span-6 mt-5">
                            <div>
                                <label>Sub Heading</label>
                                <input type="text" name="sub_name" value="{{ isset($section_content->sub_name) ? $section_content->sub_name : '' }}" class="input w-full border mt-2" placeholder="Sub Heading">
                            </div>
                        </div>						
                    </div>
					
				<div class="intro-y col-span-12 lg:col-span-12 mt-5">
					<h2 class="text-lg font-medium mr-auto">Info Section Items</h2>
                </div>

                @if(empty($section_content->images))
                    <div class="menu_section mt-3">
                        <div class="intro-y col-span-12 lg:col-span-6">
                            <div>
                                <label>Title</label>
                                <input type="text" name="headings[]" value="" class="input w-full border mt-2" placeholder="Title">
                            </div>
                        </div>
                        <div class="intro-y col-span-12 lg:col-span-6 mt-5">
                            <div>
                                <label>Description</label>
                                <input type="text" name="descriptions[]" value="" class="input w-full border mt-2" placeholder="Description">
                            </div>
                        </div>

                        <div class="intro-y col-span-12 lg:col-span-6 mt-5 img_wrap">
                            <label>Main Image:</label>
                            <input type="file" name="images[]" class="background_img slider_images input w-full border mt-2" onchange="imageReader(event, 'preview_<?php echo $i;?>')">
                            <input type="hidden" name="old_image[]" value="">
                            <small>The file size must be less than 1 MB.</small>
                        </div>
                        <div class="images_preview mypreview preview_<?php echo $i;?> mb-4"></div>
						
                    </div>
				@endif

                <div id="slides_wrapper">
                    @if(!empty($section_content->images))
                        @foreach($section_content->images as $key => $value)
                            <div class="menu_section mt-3">
                                <div class="text-right"><a href="javascript:;" class="remove_section_btn">X</a></div>
                                <div class="intro-y col-span-12 lg:col-span-6">
                                    <div>
                                        <label>Title</label>
                                        <input type="text" name="headings[]" value="{{ $value->heading }}" class="input w-full border mt-2" placeholder="Title">
                                    </div>
                                </div>
                                <div class="intro-y col-span-12 lg:col-span-6 mt-5">
                                    <div>
                                        <label>Description</label>
                                        <input type="text" name="descriptions[]" value="{{ $value->description }}" class="input w-full border mt-2" placeholder="Description">
                                    </div>
                                </div>

                                <div class="intro-y col-span-12 lg:col-span-6 mt-5 img_wrap">
                                    <label>Main Image:</label>
                                    <input type="file" name="images[]" class="background_img slider_images input w-full border mt-2" onchange="imageReader(event, 'preview_<?php echo $i;?>', this)" data-max-size="1000000">
                                    <input type="hidden" name="old_image[]" value="{{ !empty($value->image) ? $value->image : '' }}">
                                    <small>The file size must be less than 1 MB.</small>
                                </div>
                                <div class="images_preview mypreview preview_<?php echo $i;?> mb-4">
                                    @if(!empty($value->image))
                                        <img src="/{{ $value->image }}" alt="" class="imageThumb">
                                    @endif
                                </div>
                            </div>
                            @php
                                $i++;
                            @endphp
                        @endforeach
                    @endif
                </div>

                <div class="intro-y col-span-12 lg:col-span-12 mt-5">
					<button type="button" class="button button--sm w-24 mr-1 mb-2 bg-theme-1 text-white float-right add_new_menu_btn add_new_slide_btn">Add More</button>
                </div>

				<div class="grid grid-cols-12 gap-6 mt-12">
					<div class="intro-y col-span-12 lg:col-span-12 pt-5 border-t border-gray-200 dark:border-dark-5 form_btn_wrapper">
						<input type="hidden" name="sectionName" value="info_section" class="sectionName">
						<input type="hidden" name="pageName" value="{{ $page_name }}" class="pageName">
						<button type="submit" id="saveBtn" class="button button--md w-24 mr-1 mb-2 bg-theme-9 text-white">Save</button>
						<button type="reset" class="button button--md w-24 mr-1 mb-2 bg-theme-6 text-white" onclick="location.href = '{{ env('APP_URL') }}/admin/content/{{$page_name}}';">Cancel</button>
					</div>
				</div>
			</form>
		</div>
	</div>

	<script type="text/javascript">
	 var x = parseInt(<?php echo $i;?>);
        // Image Reader
        function imageReader(e, preview_class) {
            let old_img;
            if (window.File && window.FileList && window.FileReader) {
                var files = e.target.files,
                filesLength = files.length;
                for (var i = 0; i < filesLength; i++) {
                    var f = files[i]
                    if (files[i].size <= 1000000) {
                        var fileReader = new FileReader();
                        fileReader.onload = (function(e) {
                            var file = e.target;
                            let html = "<span class=\"pip\">" +
                            "<img class=\"imageThumb\" src=\"" + e.target.result + "\" title=\"" + file.name + "\"/>" +
                            "</span>";
                            $('.' + preview_class).empty().append(html);
                        });
                        fileReader.readAsDataURL(f);
                    } else {
                        alert('The file size must be less than 1 MB.');
                        $("." + preview_class).closest(".menu_section").find(".img_wrap input[type=file]").val('');
                        old_img = $("." + preview_class).closest(".menu_section").find(".img_wrap input[type=hidden]").val();
                        if (old_img != null && old_img != '') {
                            $("." + preview_class).empty().append("<img src='/"+ old_img +"' class='imageThumb'>");
                        } else {
                            $("." + preview_class).empty();
                        }
                        return false;
                    }
                }
            } else {
                alert("Your browser doesn't support to File API")
            }
        }

		$(document).ready(function() {
			// Add new menu
			$(".add_new_slide_btn").click(function() {
         
                var rand = x++;
				console.log('rand',rand);
                var cl = "'preview_"+rand+"'";
				let html = '<div class="menu_section mt-3">'+
                                '<div class="text-right"><a href="javascript:;" class="remove_section_btn">X</a></div>'+
                                '<div class="intro-y col-span-12 lg:col-span-6">'+
                                    '<div>'+
                                        '<label>Title</label>'+
                                        '<input type="text" name="headings[]" value="" class="input w-full border mt-2" placeholder="Title">'+
                                    '</div>'+
                                '</div>'+
                                '<div class="intro-y col-span-12 lg:col-span-6 mt-5">'+
                                    '<div>'+
                                        '<label>Description</label>'+
                                        '<input type="text" name="descriptions[]" value="" class="input w-full border mt-2" placeholder="Description">'+
                                    '</div>'+
                                '</div>'+
                                '<div class="intro-y col-span-12 lg:col-span-6 mt-5 img_wrap">'+
                                    '<label>Main Image:</label>'+
                                    '<input type="file" name="images[]" class="background_img slider_images input w-full border mt-2" onchange="imageReader(event, '+cl+')">'+
                                    '<input type="hidden" name="old_image[]" value="">'+
                                    '<small>The file size must be less than 1 MB.</small>'+
                                '</div>'+
                                '<div class="images_preview mypreview preview_'+rand+' mb-4"></div>'+
                            '</div>';
				$("#slides_wrapper").append(html);
                $(".select2").select2();
			});

			// Remove menu section
			$(document).on("click", ".remove_section_btn", function() {
				if (confirm('Are you sure?')) {
					$(this).closest('.menu_section').remove();
				}
			});

			// Submission process
			$('#info_sectionForm').submit(function(e) {
				e.preventDefault();
                var top_main_name = $('[name="top_main_name"]').val();
                var top_main_content = $('[name="top_main_content"]').val();
				var main_name = $('[name="main_name"]').val();
				var sub_name = $('[name="sub_name"]').val();
				var headings = $('[name="headings[]"]').map(function(){return $(this).val();}).get();
                var descriptions = $('[name="descriptions[]"]').map(function(){return $(this).val();}).get();
                var old_images = $('[name="old_image[]"]').map(function(){return $(this).val();}).get();
				var images = [];

                $.ajax({
                    url: "{{ url('/admin/multiSectionImageUpload') }}",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'POST',
                    data: new FormData(this),
                    contentType: false,
                    processData:false,
                    beforeSend: function() {
                        $("#saveBtn .loader").show();
                        $("#saveBtn").attr("disabled", "disabled").css('cursor', 'no-drop');
                    },
                    success: function(result) {
						
						$("#saveBtn").prop("disabled", false).css('cursor', '');
										
                        $.each(headings, function(key,value){
                            var sec_data = {
                                heading : headings[key],
                                description : descriptions[key],
                                image : result[key]
                            };
                            images.push(sec_data);
                        });

                        var content = { top_main_name: top_main_name, top_main_content: top_main_content, main_name: main_name, sub_name: sub_name, images: images };
                        $.ajax({
                            url: "{{ url('admin/insertAndUpdateSection') }}",
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            type: 'POST',
                            datatype: 'json',
                            data: {
                                _token: $('input[name=_token]').val(),
                                section_name: $('.sectionName').val(),
                                page_name: $('.pageName').val(),
                                section_content : content,
                            },
                            success: function(result) {
                                if(result.msg == true) {
									swal({
										title: 'Data saved successfully!',
										icon: 'success'
									});	

									setTimeout(function(){										
										swal.close();
									}, 1000);									
								}
                            }
                        });
                    }
                });
			});

            $(".select2").select2();
		});
	</script>
@endsection