@extends('front.layouts.app')

@section('content')

    <section class="inner_banner hiw_banner">
        <div class="container">
            <div class="content_wrapper">
                <div class="b_text">
                    <h1>How It Works</h1>
                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et.</p>
                </div>
            </div>
        </div>
        <img src="/assets/images/bottom_curve.svg" alt="bottom_curve">
    </section>

    <div class="why_rmd">
        <div class="title">How ReplenishMD Works?
            <p>This is the reason why you should have join us.</p>
        </div>
        <div class="step_wrap">
            <div class="step_block">
                <div class="step_img">
                    <div class="mob_img"><img class="frt_img" src="/assets/images/orange.png" alt=""><img class="phone_mock" src="/assets/images/step1.png" alt=""></div>
                </div>
                <div class="step_details">
                    <div class="stp_box">
                        <div class="step_number"><span>Step 01</span></div>
                        <div class="stp_head">Get started By Choosing a level of Service</div>
                        <div class="stp_info">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et sadipscing elitr, sed diam nonumy eirmod.</div>
                    </div>
                </div>
            </div>
            <div class="step_block">
                <div class="step_img">
                    <div class="mob_img"><img class="frt_img" src="/assets/images/apples.png" alt=""><img class="phone_mock" src="/assets/images/step2.png" alt=""></div>
                </div>
                <div class="step_details">
                    <div class="stp_box">
                        <div class="step_number"><span>Step 02</span></div>
                        <div class="stp_head">An Expert Practitioner Reviews Your Case</div>
                        <div class="stp_info">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et sadipscing elitr, sed diam nonumy eirmod.</div>
                    </div>
                </div>
            </div>
            <div class="step_block">
                <div class="step_img">
                    <div class="mob_img"><img class="frt_img" src="/assets/images/straberry.png" alt=""><img class="phone_mock" src="/assets/images/step3.png" alt=""></div>
                </div>
                <div class="step_details">
                    <div class="stp_box">
                        <div class="step_number"><span>Step 03</span></div>
                        <div class="stp_head">Get Free Delivery if Medications are Prescribed</div>
                        <div class="stp_info">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et sadipscing elitr, sed diam nonumy eirmod.</div>
                    </div>
                </div>
            </div>
            <div class="step_block">
                <div class="step_img">
                    <div class="mob_img"><img class="frt_img" src="/assets/images/blueberry.png" alt=""><img class="phone_mock" src="/assets/images/step4.png" alt=""></div>
                </div>
                <div class="step_details">
                    <div class="stp_box">
                        <div class="step_number"><span>Step 04</span></div>
                        <div class="stp_head"> Receive ongoing support from our community of providers and patrons</div>
                        <div class="stp_info">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et sadipscing elitr, sed diam nonumy eirmod.</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="work_join_sec">
        <div class="data_wrap">
            <div class="container">
                <div class="content_wrapper">
                    <div class="title">Join the ReplenishMD
                        <p>Lorem ipsum dolor sit amet, consetetur</p>
                    </div>
                    <div class="boxes_wrapper d_flex_j_center">
                        <div class="box">
                            <span class="fa fa-user icon"></span>
                            <h6 class="title">I'm a Replenisher.</h6>
                             <p>Happy to have you on board, please click below button to register your account.</p>
                            <a href="{{URL('signup')}}" class="blue_btn">Register Now!</a>
                        </div>
                        <div class="box">
                            <span class="fa fa-user-md icon"></span>
                            <h6 class="title">I'm a Provider.</h6>
                            <p>Happy to have you on board, please click below button to register your account.</p>
                            <a href="{{URL('providersignup')}}" class="blue_btn">Register Now!</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection