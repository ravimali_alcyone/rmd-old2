<div class="col-xl-12 col-lg-12 col-md-12">				
@if($visits && $visits->count() > 0)	
	@foreach($visits as $key => $value)

	<div class="myvisits status-box mt-15 bg-white round-crn pd-20-30 hover-effect-box">	
        <div class="pv_title">
            <div class="pv_info">
			@if(isset($value->provider_id) && ($value->visit_status == 'ACCEPTED' || $value->visit_status == 'REVIEWED' || $value->visit_status == 'COMPLETE') )
                <div class="pv_info_box">
					<div class="pv_img"><img src="{{ env('APP_URL') }}/{{ $value->provider_image }}" alt="provider" class="thumb" /></div>
					<span class="value_username">{{ $value->provider_name }}</span>
				</div>
			@endif
			<div class="pv_t">
				<h4 class="font-weight-bold">{{ $value->service_name }} <small>(@if($value->service_visit_type == 1) Asynchronous @elseif($value->service_visit_type == 2) Synchronous @elseif($value->service_visit_type == 3) Concierge @endif)</small></h4>
				<div class="pv_date">Created : {{ date(env('DATE_FORMAT_PHP_HA'),strtotime($value->created_at)) }}</div>
			</div>
            </div>
            
            <div class="label-mute text-right mt-1">VISIT STATUS - <span class="text-info status"> {{ $value->visit_status}}</span></div>
        </div>	
		
		<div class="pv_treat_box">
			@if(isset($value->order_id) && isset($value->subscription_id))
			<div class="provider">
				<h6 class="label-mute">PLAN</h6>
				<div class="provider-details mt-15">
				@if(isset($value->medicine_id))
					@if(isset($value->medicine_images) && $value->medicine_images != '')
						@php 
							$medicine_images =  json_decode($value->medicine_images,true);
							$medicine_image  = env('APP_URL').'/'.$medicine_images[0];
						@endphp
					@else
							$medicine_image = '';
					@endif
						<img src="{{ $medicine_image }}" alt="prescription" class="thumb" />
						<span class="value_username">{{$value->medicine_name}}, {{$value->variant_name}}</span>
				@endif
				</div>
			</div>
			@endif
					
			@if(isset($value->event_start) && $value->event_start != '' && $value->event_status == 1)
			<div class="provider appointment_area">
				<h6 class="label-mute">APPOINTMENT</h6>
				<div class="provider-details mt-15">					
					<span class="value_username appointment_time">{{ date(env('DATE_FORMAT_PHP_HA'),strtotime($value->event_start)) }}</span>						
				</div>
			</div>	
			@endif
			<div class="action_btn">		
				@if($value->visit_status == 'ACCEPTED' || $value->visit_status == 'REVIEWED' || $value->visit_status == 'COMPLETE')
					
					@if($value->is_feedback_done != 1 && $value->visit_status == 'COMPLETE')
						<button type="button" class="btn btn-outline-primary btn-sm" title="Feedback" onclick="feedback_modal(this,{{ $value->provider_id }}, {{ $value->my_service_id }}, {{ $value->id }} );">Feedback</button>
					@endif
					
						<button 
							type="button" 
							class="btn @if($value->event_chat==1) btn-outline-success @else btn-outline-primary @endif btn-sm" @if($value->service_visit_type==1) @if($value->event_chat==1) onclick="chat_box('{{ $value->provider_id }}','{{$value->id}}',1);" @else 
							onclick="video_call(this,'{{$value->id}}','{{$value->provider_id}}','chat_feature')" @endif @else onclick="chat_box('{{ $value->provider_id }}','{{$value->id}}',1);" @endif 
							title="Chat"
						>Chat</button>
					
		
						<button 
							type="button" 
							class="btn @if(isset($value->event_status) && $value->event_status == 1) btn-outline-success @else btn-outline-danger @endif btn-sm" 
							title="@if(isset($value->event_status) && $value->event_status == 1) Video Call @else Upgrade to Video Call @endif" 
							onclick="video_call(this,'{{$value->id}}','{{$value->provider_id}}','video_call')"
						>Video Call</button>										
				@endif
				
				@if($value->visit_status == 'RESCHEDULED')
					<button type="button" class="btn btn-outline-primary btn-sm" title="Appointment" onclick="window.location.href = '{{env('APP_URL')}}/user/select_appointment/{{$value->id}}';">Appointment</button>
				@endif		
			</div>
		</div>
	</div>
	@endforeach
@endif	
</div>
<div class="col-xl-12 col-lg-12 col-md-12">	
@if($visits && $visits->count() > 0)
	<div class="custom-pagination mt-15">
		<nav aria-label="Page navigation example">
			{{ $visits->links() }}
		</nav>
	</div>	
@endif	
</div>