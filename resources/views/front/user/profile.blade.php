@extends('front.dashboard_layout.app')

@section('content')

@push('header_scripts')
<style>
	.pac-container {
        z-index: 10000 !important;
    }
	.pac-container:after {
		background-image: none !important;
		height: 0px;
	}
	#profileModal .modal-content {
		min-width: 700px;
	}
	.iframe{
		width: 100%;
		height:500px;
	}
</style>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key={{ env('Google_Map_Keys')}}" async></script>
<script>
	$(function(){
		$("#dob").datepicker({
			dateFormat: "mm/dd/yy",
			maxDate: -1,
			changeMonth: true,
			changeYear: true,
			yearRange: "-150:+0",
		});
		
	});
</script>
@endpush
	<div class="row">
		<div class="mid_wrap col-xl-9 col-lg-12 col-md-12 col-sm-12">
			<div class="row">
				<div class="col-md-12">
					<div class="main-center-data user_pro_edit_area">
						<h3 class="display-username onpage w-100">Profile Setup</h3>
						<div class="informative-block bg-white round-crn pd-20-30 mt-15 hover-effect-box">
							<span class="edit-details">
								<form id="changeProfileForm">
									<div class="image-upload">
										<label for="file_input">
											<i class="fa fa-camera" aria-hidden="true"></i>
										</label>
										<input id="file_input" name="file_input" type="file" onchange="changeProfile(event)"/>
									</div>
								</form>
							</span>
							<div class="edit_user_pic">
								@if(auth()->user()->image)			  
								<img id="profileData" src="{{env('APP_URL')}}/{{auth()->user()->image}}" alt="{{auth()->user()->name}}"/> 
								@else
									<img id="profileData" src="{{env('APP_URL')}}/dist/images/user_icon.png" alt="{{auth()->user()->name}}"/> 
								@endif
								
							</div>
						</div>

						<div class="informative-block bg-white round-crn pd-20-30 mt-15 hover-effect-box">
							<h4 class="font-weight-bold">User Info</h4>
							<span class="edit-details" onclick="modal_box('profile');"><img src="{{asset('dashboard/img/edit.svg')}}" alt="edit" /></span>
							<table class="table-responsive mt-15 user_profile">
								<tbody>
									<tr>
										<td class="label-mute">Name</td>
										<td>{{auth()->user()->name}}</td>
									</tr>
									<tr>
										<td class="label-mute">Email</td>
										<td>{{auth()->user()->email}}</td>
									</tr>
									<tr>
										<td class="label-mute">Phone</td>
										<td>{{auth()->user()->phone}}</td>
									</tr>
									<tr>
										<td class="label-mute">DOB</td>
										<td><?php if(auth()->user()->dob != '0000-00-00'){ echo date(env('DATE_FORMAT_PHP'),strtotime(auth()->user()->dob)); }else{ echo 'mm/dd/yyyy'; } ?> </td>
									</tr>
									<tr>
										<td class="label-mute">Gender</td>
										<td>{{ucfirst(auth()->user()->gender)}}</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="informative-block bg-white round-crn pd-20-30 mt-15 hover-effect-box w-100">
							<h4 class="font-weight-bold">About</h4>
							<table class="table-responsive mt-15 user_shiping">
								<tbody>
									<tr>
										<td class="label-mute" id="short_about_info">{!! auth()->user()->short_info !!}</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="informative-block bg-white round-crn pd-20-30 mt-15 hover-effect-box w-100">
							<h4 class="font-weight-bold">Merchant Account</h4>
							<span class="edit-details" onclick="modal_box('merchant_account');"><img src="{{asset('dashboard/img/edit.svg')}}" alt="edit" /></span>
							<table class="table-responsive mt-15 user_shiping">
								<tbody>
									<tr>
										<td class="label-mute">Login Id</td>
										<td id="text_login_id">@if(auth()->user()->MERCHANT_LOGIN_ID) {{auth()->user()->MERCHANT_LOGIN_ID}} @endif</td>
									</tr>
									<tr>
										<td class="label-mute">Transaction Key</td>
										<td id="text_transaction_key">@if(auth()->user()->MERCHANT_TRANSACTION_KEY) {{auth()->user()->MERCHANT_TRANSACTION_KEY}} @endif</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="informative-block bg-white round-crn pd-20-30 mt-15 hover-effect-box w-100">
							<h4 class="font-weight-bold">Address</h4>
							<span class="edit-details" onclick="modal_box('address');"><img src="{{asset('dashboard/img/edit.svg')}}" alt="edit" /></span>
							<table class="table-responsive mt-15 user_shiping">
								<tbody>
									<tr>
										<td class="label-mute">Living</td>
										<td>@if($living_data) {{$living_data->address}} @endif</td>
									</tr>
									<tr>
										<td class="label-mute">Shipping</td>
										<?php
											if($shipping_data) { 
												$address = $shipping_data->address_line1.', '.$shipping_data->address_line2.', '.$shipping_data->city.', '.$shipping_data->state.', '.$shipping_data->country.', '.$shipping_data->postal_code;
												$address = str_replace(", ,",",",$address);	
											}
										?>
										<td>@if($shipping_data) {{$address}} @endif</td>
									</tr>
								</tbody>
							</table>
						</div>
						
						<div class="informative-block bg-white round-crn pd-20-30 mt-15 hover-effect-box w-100">
							<h4 class="font-weight-bold">Password</h4>
							<span class="edit-details" onclick="modal_box('pswrd');"><img src="{{asset('dashboard/img/edit.svg')}}" alt="edit" /></span>
							<table class="table-responsive mt-15">
								<tbody>
									<tr>
										<td class="label-mute">Password</td>
										<td>*********</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="rigt_sidebar col-xl-3 col-lg-12 col-md-12 col-sm-12">
			<div class="informative-block bg-white round-crn pd-20-30 settings hover-effect-box">
				<h4 class="font-weight-bold">Settings</h4>
				<div class="stwich-toggle">
					<div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="customSwitch1">
					  <label class="custom-control-label" for="customSwitch1"></label>
					</div>
				</div>
				<h6 class="label-mute mt-15">SMS Notifications</h6>
				<p class="note m-0">By turning on this toggle, I agree to receive texts from RMD and/or {{auth()->user()->name}} to {{auth()->user()->phone}} that might be considered marketing and that may be sent using an auto dialer. I understand that agreeing is not required to purchase.</p>
			</div>
		</div>
	</div>

	<div class="chat_pop edit_pop petient chat_pop_pswrd">
		<div class="edit_pop_title">
			<h3>Update Password</h3>
			<a class="cp_close normal"><i class="fa fa-times"></i></a>
		</div>
        <div class="chat_block">
			<div class="container pt-3">
				<div class="row">
					<form id="changePassForm" class="col-md-12">
						<div class="form-group col-sm-12">
							<label for="old_password">Old Password <span class="text-danger">*</span></label>
							<input type="password" name="old_password" class="form-control" id="old_password" placeholder="">
							<span class="text-danger">
								<strong class="error" id="old_password-error"></strong>
							</span>
						</div>

						<div class="form-group col-sm-12">
							<label for="new_password">New Password <span class="text-danger">*</span></label>
							<input type="password" name="password" class="form-control" id="new_password" placeholder="">
							<span class="text-danger">
								<strong class="error" id="new_password-error"></strong>
							</span>
						</div>

						<div class="form-group col-sm-12">
							<label for="confirm_password">Confirm New Password <span class="text-danger">*</span></label>
							<input type="password" name="password_confirmation" class="form-control" id="confirm_password" placeholder="">
							<span class="text-danger">
								<strong class="error" id="confirm_password-error"></strong>
							</span>
						</div>
						
						<div class="form-group col-sm-12">
							<button type="submit" id="saveBtn" class="accept_btn btn btn-outline-primary btn-sm" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing" >Submit</button>
							<button type="button" class="btn btn-outline-danger btn-sm" onclick="$('.cp_close').trigger('click');">Cancel</button>
						</div>
					</form>
				</div>							
			</div>
    	</div>
	</div>

	<div class="chat_pop edit_pop petient chat_pop_merchant_account">
		<div class="edit_pop_title">
			<h3>Merchant Account Credential</h3>
			<a class="cp_close normal"><i class="fa fa-times"></i></a>
		</div>
        <div class="chat_block">
			<div class="container pt-3">
				<div class="row">
					<form id="merchantAccountForm" class="col-md-12">
						<div class="form-group col-sm-12">
							<label for="old_password">Login Id <span class="text-danger">*</span></label>
							<input type="text" name="login_id" class="form-control" id="login_id" placeholder="">
							<span class="text-danger">
								<strong class="error" id="old_password-error"></strong>
							</span>
						</div>

						<div class="form-group col-sm-12">
							<label for="new_password">Transaction Key <span class="text-danger">*</span></label>
							<input type="text" name="transaction_key" class="form-control" id="transaction_key" placeholder="">
							<span class="text-danger">
								<strong class="error" id="new_password-error"></strong>
							</span>
						</div>

						<div class="form-group col-sm-12">
							<button type="submit" id="saveBtn" class="accept_btn btn btn-outline-primary btn-sm" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing" >Submit</button>
							<button type="button" class="btn btn-outline-danger btn-sm" onclick="$('.cp_close').trigger('click');">Cancel</button>
						</div>
					</form>
				</div>							
			</div>
    	</div>
	</div>

	<div class="chat_pop edit_pop petient chat_pop_profile">
		<div class="edit_pop_title">
			<h3>Edit Profile</h3>
			<a class="cp_close normal"><i class="fa fa-times"></i></a>
		</div>
        <div class="chat_block">
			<div class="container pt-3">
				<div class="row">
					<form id="profileForm" class="col-md-12">
						<div class="form-row">
							<div class="form-group col-md-6">
								<label for="first_name">First Name <span class="text-danger">*</span></label>
								<input type="text" name="first_name" class="form-control" id="first_name" value="{{auth()->user()->first_name}}">
								<span class="text-danger">
									<strong class="error" id="first_name-error"></strong>
								</span>
							</div>
							<div class="form-group col-md-6">
								<label for="last_name">Last Name <span class="text-danger">*</span></label>
								<input type="text" name="last_name" class="form-control" id="last_name" value="{{auth()->user()->last_name}}">
								<span class="text-danger">
									<strong class="error" id="last_name-error"></strong>
								</span>
							</div>
						</div>
						<div class="form-row">
							<div class="form-group col-md-6">
								<label for="email">Email</label>
								<input type="email" name="email" class="form-control" id="email" value="{{auth()->user()->email}}" disabled>
								<span class="text-danger">
									<strong class="error" id="email-error"></strong>
								</span>
							</div>
							<div class="form-group col-md-6">
								<label for="phone">Phone <span class="text-danger">*</span></label>
								<input type="number" name="phone" class="form-control" id="phone" value="{{auth()->user()->phone}}">
								<span class="text-danger">
									<strong class="error" id="phone-error"></strong>
								</span>
							</div>
						</div>
						<div class="form-row">
							<div class="form-group col-md-6">
								<label for="dob">DOB <span class="text-danger">*</span></label>
								<input type="text" name="dob" class="form-control" id="dob" value="<?php if(auth()->user()->dob != '0000-00-00'){ echo date(env('DATE_FORMAT_PHP'),strtotime(auth()->user()->dob)); } ?>" max="{{ date('Y-m-d', strtotime('-1 day')) }}" placeholder="mm/dd/yyyy" >
								<span class="text-danger">
									<strong class="error" id="dob-error"></strong>
								</span>
							</div>
							<div class="form-group col-md-6">
								<label for="gender">Gender <span class="text-danger">*</span></label>
								<div class="form-check-inline" style="display:flex !important;">
									<label class="form-check-label mr-2">
										<input type="radio" class="form-check-input" name="gender" value="male" @if(auth()->user()->gender == 'male' && auth()->user()->gender != '') checked @else checked @endif >Male</label>
										<label class="form-check-label">
										<input type="radio" class="form-check-input" name="gender" value="female" @if(auth()->user()->gender == 'female') checked @endif >Female
									</label>
								</div>
								<span class="text-danger">
									<strong class="error" id="gender-error"></strong>
								</span>
							</div>
						</div>
						<div class="form-row">
							<div class="form-group col-md-6 has-feedback">
								<label for="phone">Education </label>
								<input type="text" name="eduction" class="form-control" id="eduction" value="{{auth()->user()->education}}">
								<span class="text-danger">
									<strong class="error" id="eduction-error"></strong>
								</span>
							</div>
							<div class="form-group col-md-6 has-feedback">
								<label for="dob">Goal </label>
								<input type="text" name="goal" class="form-control" id="goal" value="{{auth()->user()->goal}}">
								<span class="text-danger">
									<strong class="error" id="goal-error"></strong>
								</span>
							</div>
						</div>
						<div class="form-row">
							<div class="form-group col-md-6 has-feedback">
								<label for="phone">Interest </label>
								<input type="text" name="interest" class="form-control" id="interest" value="{{auth()->user()->interest}}">
								<span class="text-danger">
									<strong class="error" id="interest-error"></strong>
								</span>
							</div>
							<div class="form-group col-md-6 has-feedback">
								<label for="dob">Relationship Status </label>
								<input type="text" name="rel_status" class="form-control" id="rel_status" value="{{auth()->user()->relation_status}}">
								<span class="text-danger">
									<strong class="error" id="rel_status-error"></strong>
								</span>
							</div>
						</div>
						<div class="form-row">
							<div class="form-group col-md-12 has-feedback">
								<label for="first_name">About yourself </label>
								<textarea name="profile_summary" id="profile_summary" class="input w-full border mt-2" cols="30" rows="4"><?php echo auth()->user()->short_info;?></textarea>
								<span class="text-danger">
									<strong class="error" id="profile_summary-error"></strong>
								</span>	
							</div>
						</div>
						<div class="form-group col-sm-12">
							<button type="submit" id="saveBtn2" class="accept_btn btn btn-outline-primary btn-sm" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing">Update</button>
							<button type="button" class="btn btn-outline-danger btn-sm" onclick="$('.cp_close').trigger('click');" >Cancel</button>
						</div>
					</form>
				</div>							
			</div>
    	</div>
	</div>

	<div class="chat_pop edit_pop petient chat_pop_address">
		<div class="edit_pop_title">
			<h3>Address</h3>
			<a class="cp_close normal"><i class="fa fa-times"></i></a>
		</div>
        <div class="chat_block">
			<div class="container pt-3">
				<div class="row">
					<form id="shippingForm" class="col-md-12">
						<div class="form-row">
							<div class="form-group col-md-6 has-feedback">
									<label for="address_1" style="color:#ff6c66;">Living Address</label>
								</div>
						</div>
						<div class="form-row">				
							<div class="form-group col-md-6">
								<label for="address_1">Address-1<span class="text-danger">*</span></label>
								<input type="text" name="address_1" class="form-control" id="address_1" value="@if($living_data) {{$living_data->address_line1}} @endif" required>
								<span class="text-danger">
									<strong class="error" id="address_1-error"></strong>
								</span>
							</div>
							<div class="form-group col-md-6">
								<label for="address_2">Address-2</label>
								<input type="text" name="address_2" class="form-control" id="address_2" value="@if($living_data) {{$living_data->address_line2}} @endif">
								<span class="text-danger">
									<strong class="error" id="address_2-error"></strong>
								</span>
							</div>
						</div>
						<div class="form-row">
							<div class="form-group col-md-4 has-feedback">
								<label for="city">City<span class="text-danger">*</span></label>
								<input type="text" name="city" class="form-control" id="city" value="@if($living_data) {{$living_data->city}} @endif" required>
								<span class="text-danger">
									<strong class="error" id="city-error"></strong>
								</span>
							</div>
							<div class="form-group col-md-4 has-feedback">
								<label for="state">State<span class="text-danger">*</span></label>
								<select name="state" id="state"  class="custom-select select2" required>
									<option value="">Please Select State</option>
									@if($states)
										@foreach($states as $key => $value)
											<option value="{{ $value->code }}" @if($living_data && $living_data->state == $value->code) selected @endif > {{ $value->name }}</option>
										@endforeach
									@endif
								</select>
								<span class="text-danger">
									<strong class="error" id="state-error"></strong>
								</span>
							</div>
							<div class="form-group col-md-4 has-feedback">
								<label for="postal_code">Postal Code<span class="text-danger">*</span></label>
								<input type="text" name="postal_code" id="postal_code" value="@if($living_data) {{$living_data->postal_code}} @endif" maxlength="5"  onkeypress="return event.charCode >= 48 && event.charCode <= 57" value="" data-toggle="tooltip" data-placement="right" title="Only Texas zipcodes are allowable." class="form-control" required>
								<span class="text-danger">
									<strong class="error" id="phone-error"></strong>
								</span>
							</div>
						</div>
						<div class="form-row">
							<div class="form-group col-md-6 has-feedback">
								<input type ="checkbox" name="copy_text" id="copy_text" value="1" onclick="copy_text_other()">&nbsp;&nbsp; <span style="color:#ff6c66;">Check it for same as living address. 
							</div>	 
						</div>
						<div class="form-row">
							<div class="form-group col-md-6 has-feedback">
								<label for="address_1" style="color:#ff6c66;">Shipping Address</label>
							</div>
						</div>
						<div class="form-row">
							<div class="form-group col-md-6 has-feedback">
								<label for="address_1">Address-1</label>
								<input type="text" name="copy_address_1" class="form-control" id="copy_address_1" value="@if($shipping_data) {{$shipping_data->address_line1}} @endif" required>
								<span class="text-danger">
									<strong class="error" id="copy_address_1-error"></strong>
								</span>
							</div>
							<div class="form-group col-md-6 has-feedback">
								<label for="address_2">Address-2</label>
								<input type="text" name="copy_address_2" class="form-control" id="copy_address_2" value="@if($shipping_data) {{$shipping_data->address_line2}} @endif">
								<span class="text-danger">
									<strong class="error" id="copy_address_2-error"></strong>
								</span>
							</div>
						</div>

						<div class="form-row">
							<div class="form-group col-md-4 has-feedback">
								<label for="city">City</label>
								<input type="text" name="copy_city" class="form-control" id="copy_city" value="@if($shipping_data) {{$shipping_data->city}} @endif" required>
								<span class="text-danger">
									<strong class="error" id="copy_city-error"></strong>
								</span>
							</div>
							<div class="form-group col-md-4 has-feedback">
								<label for="state">State</label>
								<select name="copy_state" id="copy_state"  class="custom-select select2" required>
									<option value="">Please Select State</option>
									@if($states)
										@foreach($states as $key => $value)
											<option value="{{ $value->code }}" @if($shipping_data && $shipping_data->state == $value->code) selected @endif > {{ $value->name }}</option>
										@endforeach
									@endif
								</select>
								<span class="text-danger">
									<strong class="error" id="copy_state-error"></strong>
								</span>
							</div>
							<div class="form-group col-md-4 has-feedback">
								<label for="postal_code">Postal Code</label>
								<input type="text" name="copy_postal_code" id="copy_postal_code" value="@if($shipping_data) {{$shipping_data->postal_code}} @endif" maxlength="5"  onkeypress="return event.charCode >= 48 && event.charCode <= 57" value="" data-toggle="tooltip" data-placement="right" title="Only Texas zipcodes are allowable." class="form-control" required>
								<span class="text-danger">
									<strong class="error" id="copy_postal_code-error"></strong>
								</span>
							</div>
						</div>
						<div class="form-group col-sm-12">
							<button type="submit" id="saveBtn3" class="accept_btn btn btn-outline-primary btn-sm" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing">Add</button>
							<button type="button" class="btn btn-outline-danger btn-sm" onclick="$('.cp_close').trigger('click');" >Cancel</button>
						</div>
					</form>
				</div							
			</div>
    	</div>
	</div>
@push('scripts')	
<!--**********--->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>
	<!-- CKEditor -->
	<script src="{{ asset('plugins/ckeditor/ckeditor.js') }}"></script>
	<script>
	
		CKEDITOR.replace('profile_summary');
		CKEDITOR.add;
		
		function copy_text_other()
		{
			if($('#copy_text').is(":checked"))
			{
				$("#copy_address_1").val($("#address_1").val());
				$("#copy_address_2").val($("#address_2").val());
				$("#copy_city").val($("#city").val());
				$("#copy_state").val($("#state").val());
				$("#copy_postal_code").val($("#postal_code").val());
			}
			else
			{
				$("#copy_address_1").val('');
				$("#copy_address_2").val('');
				$("#copy_city").val('');
				$("#copy_state").val('');
				$("#copy_postal_code").val('');
			}
			
		}

		$("#changePassForm").submit(function(e) {
			e.preventDefault();

			//loader start
			$('#saveBtn').html($('#saveBtn').attr('data-loading-text'));

			$("#changePassForm span.text-danger .error").html('');

			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: $('#changePassForm').serialize(),
				url: "{{ route('user.change_password') }}",
				type: "POST",
				success: function (response) {
					$('#saveBtn').html('Submit');
					if(response['status'] == 'success'){
						$(".cp_close").trigger('click');
						callProgressBar();
					}
				},
				error: function (data) {
					$('#saveBtn').html('Submit');

					if(data.responseJSON.errors) {
						let errors = data.responseJSON.errors;
						if(errors.old_password){
							$( '#old_password-error' ).html( errors.old_password[0] );
						}

						if(errors.password){
							$( '#new_password-error' ).html( errors.password[0] );
						}

						if(errors.password_confirmation){
							$( '#confirm_password-error' ).html( errors.password_confirmation[0] );
						}

					}

				}
			});
		});

		$("#merchantAccountForm").submit(function(e) {
			e.preventDefault();

			//loader start
			$(".loader").css('display', 'flex');
			$('#saveBtn').html($('#saveBtn').attr('data-loading-text'));

			$("#merchantAccountForm span.text-danger .error").html('');

			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: $('#merchantAccountForm').serialize(),
				url: "{{ route('user.set_merchant_account') }}",
				type: "POST",
				success: function (response) {
					$('#saveBtn').html('Submit');
					if(response['status'] == 'success'){
						$(".cp_close").trigger('click');
						callProgressBar();
						$("#text_login_id").text(response['MERCHANT_LOGIN_ID']);
						$("#text_transaction_key").text(response['MERCHANT_TRANSACTION_KEY']);
						$("input[type=text]").val('');
						$(".loader").css('display', 'none');
					}
				},
				error: function (data) {
					$('#saveBtn').html('Submit');
					$(".loader").css('display', 'none');
					if(data.responseJSON.errors) {
						let errors = data.responseJSON.errors;
						if(errors.review_text){
							$( '#review_text-error' ).html( errors.review_text[0] );
						}
						
						if(errors.rating){
							$( '#rating-error' ).html( errors.rating[0] );
						}
					}
				}
			});
		});


		
		$("#profileForm").submit(function(e) {
			e.preventDefault();

			//loader start
			$('#saveBtn2').html($('#saveBtn2').attr('data-loading-text'));
			$("#profileForm span.text-danger .error").html('');
			
			var profile_summary = CKEDITOR.instances.profile_summary.getData();
			var formData = new FormData(this);
			formData.append('profile_summary', profile_summary);

			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: formData,
				url: "{{ route('user.update_profile') }}",
				type: "POST",
				cache:false,
				contentType: false,
				processData: false,
				success: function (response) {
					$('#saveBtn2').html('Update');
					if(response['status'] == 'success'){
						var updatedata = response['user_data'];
						//var hidden = $('.chat_pop');
						//hidden.animate({"right":"-100%"}, "slow").removeClass('visible');
						$(".cp_close").trigger('click');
						callProgressBar();
						$('.info_box .user_name').text(updatedata['name']);
						$('.user_profile tr:eq(0) td:eq(1)').html(updatedata['name']);
						$('.user_profile tr:eq(2) td:eq(1)').html(updatedata['phone']);
						$('.user_profile tr:eq(3) td:eq(1)').html(updatedata['dob']);
						$('.user_profile tr:eq(4) td:eq(1)').html(updatedata['gender']);
						$('.pdh_user_name').html('Hi, '+updatedata['first_name']);
						$('#short_about_info').html(updatedata['short_info']);
					}
				},
				error: function (data) {
					$('#saveBtn2').html('Update');

					if(data.responseJSON.errors) {
						let errors = data.responseJSON.errors;

						if(errors.first_name){ $('#first_name-error').html(errors.first_name[0]); }
						if(errors.last_name){ $('#last_name-error').html(errors.last_name[0]); }
						if(errors.phone){ $('#phone-error').html(errors.phone[0]); }
						if(errors.dob){ $('#dob-error').html(errors.dob[0]); }
					}

				}
			});
		});


		$("#shippingForm").submit(function(e) {
			e.preventDefault();

			//loader start
			$('#saveBtn3').html($('#saveBtn3').attr('data-loading-text'));
			$("#shippingForm span.text-danger .error").html('');

			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: $('#shippingForm').serialize(),
				url: "{{ route('user.shipping') }}",
				type: "POST",
				success: function (response) {
					$('#saveBtn3').html('Add');

					if(response['status'] == 'success'){
						//var hidden = $('.chat_pop');
						//hidden.animate({"right":"-100%"}, "slow").removeClass('visible');
						$(".cp_close").trigger('click');
						callProgressBar();
						var updatedata = response['ship_data'];
						var updatedata2 = response['living_data'];
						
						var shipping =updatedata['address'];
						$('.user_shiping tr:eq(0) td:eq(1)').html(updatedata2['address']);
						$('.user_shiping tr:eq(1) td:eq(1)').html(response['ship_data']);
					}
				},
				error: function (data) {
					$('#saveBtn3').html('Add');
					if(data.responseJSON.errors) {
						let errors = data.responseJSON.errors;
						if(errors.address_1){ $('#address_1-error').html(errors.address_1[0]); }
						if(errors.city){ $('#city-error').html(errors.city[0]); }
						if(errors.state){ $('#state-error').html(errors.state[0]); }
						if(errors.postal_code){ $('#postal_code-error').html(errors.postal_code[0]); }
					}

				}
			});
		});
		
		function changeProfile(e)
		{     
			var saida = document.getElementById('file_input');
			var quantos = saida.files.length;
			for(i = 0; i < quantos; i++){
				var urls = URL.createObjectURL(e.target.files[i]);
				document.getElementById('profileData').src=urls;
				document.getElementById('sortProfileData').src=urls;
				document.getElementById('left_side_profile').src=urls;
			}
			var data = new FormData(document.getElementById("changeProfileForm"));
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				type: "POST",
				url: "{{ route('user.change_profile_pic') }}",
				data:  data,
				enctype: 'multipart/form-data',
				processData: false,  // tell jQuery not to process the data
				contentType: false,   // tell jQuery not to set contentType
				dataType: "json",
				success: function(response)
				{
					//successAlert(response['message'],2000,'top-right');
					callProgressBar();
				},
				beforeSend: function()
				{
					// some code before request send if required like LOADING....
				}
			});
		}
		
		$(document).ready(function() {
			var serchinput = "address_1";
			var autocomplete;
			
			autocomplete = new google.maps.places.Autocomplete((document.getElementById(serchinput)), {
				types : ['geocode'],
				//componentRestrictions : {
				//	country: "USA"
				//}
			});
			
			google.maps.event.addListener(autocomplete, 'places_changed', function (){
				var near_place = autocomplete.getPlace();
			});
			
		});
		
		$(document).ready(function() {
			// var serchinput = "search_input";
			var serchinput = "address_2";
			var autocomplete;
			
			autocomplete = new google.maps.places.Autocomplete((document.getElementById(serchinput)), {
				types : ['geocode'],
				//componentRestrictions : {
				//	country: "USA"
				//}
			});
			
			google.maps.event.addListener(autocomplete, 'places_changed', function (){
				var near_place = autocomplete.getPlace();
			});
			
		});


	</script>
@endpush
@endsection