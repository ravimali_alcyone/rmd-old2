<div class="rigt_sidebar">
	<div class="blog-stories sponsored">
		<h4 class="blog-heading">Sponsored</h4>
		<ul class="m-0 p-0">
			<li class="mt-15">
				<a href="#">
					<div class="blog-feature-image">
						<img src="{{ asset('dashboard/img/sponsor-1.png') }}" alt="sponsor-1" />
					</div>
					<div class="blog-extract short-description">
						<p class="mb-0">Diet Plan according to your BMI</p>
						<span href="#">eathealthy.com</span>
					</div>
				</a>
			</li>
			<li class="mt-15">
				<a href="#">
					<div class="blog-feature-image">
						<img src="{{ asset('dashboard/img/sponsor-2.png') }}" alt="sponsor-2" />
					</div>
					<div class="blog-extract short-description">
						<p class="mb-0">Best Hospital for cancer treatment</p>
						<span href="#">eathealthy.com</span>
					</div>
				</a>
			</li>
			<li class="mt-15">
				<a href="#">
					<div class="blog-feature-image">
						<img src="{{ asset('dashboard/img/sponsor-1.png') }}" alt="sponsor-1" />
					</div>
					<div class="blog-extract short-description">
						<p class="mb-0">Diet Plan according to your BMI</p>
						<span href="#">eathealthy.com</span>
					</div>
				</a>
			</li>
			<li class="mt-15">
				<a href="#">
					<div class="blog-feature-image">
						<img src="{{ asset('dashboard/img/sponsor-2.png') }}" alt="sponsor-2" />
					</div>
					<div class="blog-extract short-description">
						<p class="mb-0">Best Hospital for cancer treatment</p>
						<span href="#">eathealthy.com</span>
					</div>
				</a>
			</li>
		</ul>
	</div>

	<div class="blog-stories mt-15">
		<h4 class="blog-heading">Blog stories</h4>
		<ul class="m-0 p-0">
			<li class="mt-15">
				<a href="#">
					<div class="blog-feature-image">
						<img src="{{ asset('dashboard/img/blog-1.svg') }}" alt="blog" />
					</div>
					<div class="blog-extract short-description">
						Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia.
					</div>
				</a>
			</li>

			<li class="mt-15">
				<a href="#">
					<div class="blog-feature-image">
						<img src="{{ asset('dashboard/img/blog-2.svg') }}" alt="blog" />
					</div>
					<div class="blog-extract short-description">
						Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia.
					</div>
				</a>
			</li>

			<li class="mt-15">
				<a href="#">
					<div class="blog-feature-image">
						<img src="{{ asset('dashboard/img/blog-3.svg') }}" alt="blog" />
					</div>
					<div class="blog-extract short-description">
						Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia.
					</div>
				</a>
			</li>

			<li class="mt-15">
				<a href="#">
					<div class="blog-feature-image">
						<img src="{{ asset('dashboard/img/blog-4.svg') }}" alt="blog" />
					</div>
					<div class="blog-extract short-description">
						Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia.
					</div>
				</a>
			</li>
		</ul>
		<div class="see-all mt-30">
			<a href="#">See all Stories <img src="{{ asset('dashboard/img/arrow-forward.svg') }}" alt="arrow-forward"/></a>
		</div>
	</div>

	<div class="advertisment mt-15">
		<img src="{{ asset('dashboard/img/ads.svg') }}" alt="ads" class="img-fluid" />
	</div>
</div>