@extends('front.online_visit_layout.app')

@section('css')

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

@section('content')

<style>
.modal-backdrop.show {
    opacity: 0.5;
    z-index: 0;
}

.modal-open .modal{
    z-index: 1;
}

.payment_image{

	width:33%;

	float:left;

}
li.saved_card {
    border: 1px solid #dedede;
    padding:10px 15px;
    display: flex;
    flex-wrap: wrap;
    justify-content: space-between;
    align-items: center;
    border-radius: 4px;
	box-shadow: 0px 0px 20px 0px rgba(50, 56, 66, 0.1); margin-bottom:10px;
}
.card_info {
    display: flex;
    width: calc(100% - 40px);
    justify-content: space-between;
    align-items: center;
    flex-wrap: wrap;
}
.cc_cvv_input input {
    width: 70px;
    text-align: center;
    letter-spacing: 5px;
}
.cc_card_info {
    display: flex;flex-wrap: wrap;font-size: 13px;
}
.cc_card_info .cc_holder_name { width:100%;
    text-transform: uppercase;
    font-size: 14px;
    font-weight: 600;
    margin-bottom: 5px;
}
.cc_card_info .cc_number{ margin-right:8px;}
.new_cc_add{
    background-color: #f5f5f5;
    border-radius: 4px;
    padding: 15px;
    border: 1px solid #eee;
}
</style>

    <div class="flex items-center mt-5">

        <div class="md:w-1/2 md:mx-auto">



            @if (session('status'))

                <div class="text-sm border border-t-8 rounded text-green-700 border-green-600 bg-green-100 px-3 py-4 mb-4" role="alert">

                    {{ session('status') }}

                </div>

            @endif



            <div class="flex flex-col break-words bg-white border border-2 rounded shadow-md">



				<div class="font-semibold bg-gray-200 text-gray-700 py-3 px-6 mb-0 text-right">

					<span class="px-6">Payment Amount</span>

					<span class="font-bold">${{ session('final_amount') }}</span>

				</div>



                <div class="w-full p-6">

                        @if(session('error_message'))

                            <div role="alert" class="mb-4">

                                <div class="bg-red-500 text-white font-bold rounded-t px-4 py-2">

                                    Payment Failed

                                </div>

                                <div class="border border-t-0 border-red-400 rounded-b bg-red-100 px-4 py-3 text-red-700">

                                    <p>{{ session('error_message') }}</p>

                                </div>

                            </div>

                        @endif



                        <div class="mx-auto clearfix">

							@if(!empty($intent->card_info) && count($intent->card_info)>0)


							<ul class="saved_cards mb-4">
								<li class="mb-1">Saved Cards</li>
							@foreach($intent->card_info as $value)
								<li class="saved_card">
									<div class="cc_checked"><input type="radio" name="cardSelect" @php if($value->is_primary==1){ echo "checked"; } @endphp class="" value="{{ $value->card_id }}"></div>
									<div class="card_info">
										<div class="cc_card_info">
											<div class="cc_holder_name">{{ $value->card_name }}</div>
											<div class="cc_number">@php $number =  $value->card_number;
												$masked =  str_pad(substr($number, -4), strlen($number), '*', STR_PAD_LEFT);
												@endphp
												{{ $masked }} <input type="hidden" value="{{ $number }}" >
											</div>
											
											<div class="cc_exp_date">{{ $value->expire_date }}</div>
										</div>
										<div class="cc_cvv_input"><input type="password" class="appearance-none form-control" name="card_cvv" value="" maxlength="3" placeholder="CVV"></div>
									</div>
								</li>
								@endforeach
							</ul>
							<hr/>
							<div class="owner new_card_action">
								<input type="radio" name="cardSelect" class="mr-3" value="new"><label for="owner">New Card</label>
							</div>
							@endif

                            <form id="paymentForm" class="new_cc_add">

                                            @csrf

                                            <div class="row">

                                                <div class="form-group owner col-md-8">

                                                    <label for="owner">Name</label>

                                                    <input type="text" class=" appearance-none border rounded w-full py-3 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="card_owner" name="card_owner" value="{{ old('card_owner') }}" placeholder="Paul Hayden">

                                                    <span id="owner-error" class="error text-red"></span>

                                                </div>

                                                <div class="form-group CVV col-md-4">

                                                    <label for="cvv">Security Code/CVV</label>

                                                    <input type="password" class=" appearance-none border rounded w-full py-3 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="card_cvv" name="card_cvv" value="{{ old('card_cvv') }}" maxlength="3" placeholder="***">

                                                    <span id="cvv-error" class="error text-red"></span>

                                                </div>

                                            </div>    

                                            <div class="row">

                                                <div class="form-group col-md-8" id="card-number-field">

                                                    <label for="cardNumber">Card Number</label>

                                                    <input type="text" class=" appearance-none border rounded w-full py-3 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="card_number" name="card_number" value="{{ old('card_number') }}" placeholder="**** **** **** ****">

                                                    <span id="card-error" class="error text-red"></span>

                                                </div>

                                                

												<div class="form-group col-md-4" id="expiration-date">

                                                    <label>Expiration (MM/YYYY)</label><br/>

                                                    <input type="text" class=" appearance-none border rounded w-full py-3 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="expirationdate" name="expirationdate" value="" maxlength="7" placeholder="MM/YYYY">

                                                </div>

                                            </div>    

                                            <div class="row">

                                                                                                

                                                <div class="form-group col-md-8" id="credit_cards" >

                                                    <div class="d-flex">

														<i class="fab fa-cc-visa" style="color:navy;font-size:40px !important;"></i>

														<i class="fab fa-cc-amex " style="color:blue;font-size:40px !important;padding-left:10px;"></i>

														<i class="fab fa-cc-mastercard" style="color:red;font-size:40px !important;padding-left:10px;"></i>

														<i class="fab fa-cc-discover" style="color:orange;font-size:40px !important;padding-left:10px;"></i>

													</div>

                                                </div>

												<div class="form-group col-md-4" id="pay-now">

													<input type="hidden" class="" id="amount" name="amount"  value="{{ session('final_amount') }}" >

													<input type="hidden" class="cardSelect" id="cardSelect" name="cardSelect"  value="{{ $intent->card_primary }}" >

													

													<button type="submit" class="inline-block align-middle text-center select-none border font-bold whitespace-no-wrap py-2 px-4 rounded text-base leading-normal no-underline text-gray-100 bg-red-500 hover:bg-red-600 float-right mr-6 mt-3

													" id="card-button">Confirm Payment</button>

												</div>

                                            </div>

                                        </form>

							

							

                        </div>

                </div>

            </div>

            

        </div>

    </div>

	

	<div id="paymentPinModal" class="modal" tabindex="-1" role="dialog">

	  <div class="modal-dialog modal-dialog-centered" role="document">

		<div class="modal-content">

		  <div class="modal-header">

			<h5 class="modal-title">Payment PIN</h5>

			<!--<button type="button" class="close" data-dismiss="modal" aria-label="Close">

			  <span aria-hidden="true">&times;</span>

			</button>-->

		  </div>

		  <form id="paymentPinForm">

		  <div class="modal-body">

				<span>Please enter your 4 digit payment verification pin.</span>

				<div>&nbsp;</div>

				<div class="form-group has-feedback">

					<input type="password" class="form-control w-50" name="payment_pin" id="payment_pin" value="" maxlength="4" style="text-align:center;" placeholder="Payment pin">	

				</div>

		  </div>

		  <div class="modal-footer">

			<button type="submit" id="verifyBtn" class="accept_btn btn btn-outline-primary btn-sm" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing">Submit</button>

		  </div>

		  </form>

		</div>

	  </div>

	</div>



<script src="{{asset('assets/js/jquery.mask.js')}}"></script>

<script>

	$('#card_number').mask('0000 0000 0000 0000');
	$('#expirationdate').mask('00/0000');
	<!--Open 4 digit Payment verification pin-->

	$("#paymentForm").submit(function(e) {
		e.preventDefault();
		$(".loader").css('display', 'flex');
		$("#card_number").unmask();
		$("#expirationdate").unmask();

		var formData = new FormData(this);

		if($('#cardSelect').val()!='new'){

			var card_cvv = $('input[name=cardSelect]:checked').closest('.saved_card').find('.card_info').find('.cc_cvv_input').find('input').val();
			formData.append('card_cvv', card_cvv);

		}
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			data: formData,
			url: "{{ route('payment_card_verification') }}",
			type: "POST",
			cache:false,
			contentType: false,
			processData: false,
			success: function (response) {

				$('#card_number').mask('0000 0000 0000 0000');
				$('#expirationdate').mask('00/0000');

				if(response['status'] == 'success'){
					$(".loader").css('display', 'none');
					$("#paymentPinModal").modal({
						backdrop: 'static',
						keyboard: false
					});
				}			

			},
			error: function (data) {
				$(".loader").css('display', 'none');
				$('#card_number').mask('0000 0000 0000 0000');
				$('#expirationdate').mask('00/0000');

				let errors = data.responseJSON.errors;

				$.each(errors, function(key, value) {
					errorAlert(value[0],3000,'top-right');
				});	
			}
		});		

	});


	$("#paymentPinForm").submit(function(e) {
		$('#verifyBtn').html($('#verifyBtn').attr('data-loading-text'));
	    $('#verifyBtn').addClass('disabled');
	    $(".loader").css('display', 'flex');

		//$("#paymentPinModal").modal('hide');
		e.preventDefault();

		var card_owner = $("#card_owner").val();
		var card_cvv = $("#card_cvv").val();
		var amount = $("#amount").val();
		var expirationdate = $("#expirationdate").val();
		var card_number = $("#card_number").unmask().val();
		var cardSelect = $("#cardSelect").val();
	

		if($('#cardSelect').val()!='new'){

			var card_owner = $('input[name=cardSelect]:checked').closest('.saved_card').find('.card_info').find('.cc_card_info').find('.cc_holder_name').text();
			
			var card_cvv = $('input[name=cardSelect]:checked').closest('.saved_card').find('.card_info').find('.cc_cvv_input').find('input').val();

			var expirationdate = $('input[name=cardSelect]:checked').closest('.saved_card').find('.card_info').find('.cc_card_info').find('.cc_exp_date').text();

			var card_number = $('input[name=cardSelect]:checked').closest('.saved_card').find('.card_info').find('.cc_card_info').find('.cc_number').find('input').val();

		}

		

		var formData = new FormData(this);
		formData.append('card_owner', card_owner);
		formData.append('card_cvv', card_cvv);
		formData.append('amount', amount);
		formData.append('expirationdate', expirationdate);
		formData.append('card_number', card_number);
		formData.append('cardSelect', cardSelect);

		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			data: formData,
			url: "{{ route('dopay.online') }}",
			type: "POST",
			cache:false,
			contentType: false,
			processData: false,
			success: function (response) {
				if(response['status'] == 'success'){
					$("#paymentPinModal").modal('hide');
					$("#paymentPinForm").trigger('reset');

					$("#paymentPinForm span.text-danger .error").html('');		

					//successAlert(response['message'],3000,'top-right');
					callProgressBar();
					setTimeout(function(){
					$(".loader").css('display', 'none');
					    if(response['paywall_feature']=='paywall_feature'){
							parent.box_close();
						}else{	
							window.location = "{{ route('success_page') }}";
						}

					}, 3000);					

				}else{
					$(".loader").css('display', 'none');
					errorAlert('Error occured.',3000,'top-right');						

				}
                    $('#verifyBtn').removeClass('disabled');
					$('#verifyBtn').html('Submit');
					$(".loader").css('display', 'none');
			},

			error: function (data) {

				$('#verifyBtn').removeClass('disabled');
				$('#verifyBtn').html('Submit');
				$(".loader").css('display', 'none');

				let errors = data.responseJSON.errors;

				$.each(errors, function(key, value) {

					errorAlert(value[0],3000,'top-right');

				});	

			}

		});
	});

	

	$(document).on('change', 'input[name="cardSelect"]', function(e){
		$('#cardSelect').val($(this).val());
	});

</script>

@endsection



