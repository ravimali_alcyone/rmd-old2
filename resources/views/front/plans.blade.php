@extends('front.layouts.app')

@section('content')

    <section class="inner_banner banner_with_spike plans_banner">
        <div class="blue_bg_overlay">
            <div class="container">
                <div class="content_wrapper">
                    <div class="b_text text-center">
                        <h1>Pricing & Plans</h1>
                        <p>As a ReplenishMD member, you receive a highly trained care team who use holistic medicine to transform your health.</p>
                    </div>
                </div>
            </div>
            <img src="/assets/images/bottom_curve.svg" alt="bottom_curve">
        </div>
    </section>

    <section class="plans_sec">
        <div class="container">
            <h1 class="title">Select Right Plan For You!</h1>
            <div class="plans_wrapper">
		@if($plans)
			@foreach($plans as $key => $value)
                <div class="plan shadow">
                    <div class="head">
                        <h4 class="title">{{ $value['name'] }}</h4>
                        <p class="amount special"><span class="fa fa-"></span> {{ $value['monthly_price'] }}</p>
                        <p class="amount no_special" style="display:none;"><span class="fa fa-"></span> {{ $value['total_price'] }}</p>
                        <p class="short_des special mb-1">{{ $value['monthly_plan_text'] }}</p>
                        <p class="short_des no_special mb-1" style="display:none;">{{ $value['total_plan_text'] }}</p>
						@if($value['is_total'] == 1)
							<a href="javascript:void(0);" onclick="setPlanType(this);" class="plan_criteria special">{{ $value['special'] }}</a>
							<a href="javascript:void(0);" onclick="setPlanType(this);" class="plan_criteria no_special" style="display:none;">I'd rather pay monthly</a>
						@endif
                    </div>
                    <div class="body">
                        <p>{{ $value['detail'] }}</p>
					@if(isset($value['features']) && $value['features'])
                        <div class="points_wrapper">
						@foreach($value['features'] as $k => $val)	
                            <div class="point"><span class="fa fas fa-check"></span>{{ $val['feature_name'] }}</div>
						@endforeach
                        </div>
					@endif
						@if(auth()->user() && auth()->user()->user_role == 4)
							<button class="btn btn-primary">Select</button>
						@else
							<button class="btn btn-primary" onclick="location.href = '{{ route('signup') }}/plans';">Join Now</button>
						@endif
                    </div>
					@if($value['is_most_popular'] == 1)<p class="popular_tag">MOST POPULAR</p>@endif
                </div>
			@endforeach
		@endif
		
            </div>
        </div>
    </section>

    <section class="about_sec">
        <div class="container">
            <div class="row expertise_row">
                <div class="col-sm-12 col-md-6">
                    <div class="img_wrapper">
                        <img src="/assets/images/care_1.png" alt="Care_1" class="w-auto">
                    </div>
                </div>
                <div class="col-sm-12 col-md-6">
                    <div class="content_wrapper">
                        <h1 class="title">Covering your care.</h1>
                        <p class="p_md">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna</p>
                        <ul>
                            <li>
                                <span class="fa fas fa-check"></span>
                                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna...</p>
                            </li>
                            <li>
                                <span class="fa fas fa-check"></span>
                                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna...</p>
                            </li>
                            <li>
                                <span class="fa fas fa-check"></span>
                                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna...</p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="oac plans_oac">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="title">All membership plans include</div>
                    <div class="oac_list">
                        <div class="oac_box">
                            <div class="oacb_icon"><img src="/assets/images/icon1.svg" class="img-fluid" alt=""></div>
                            <div class="oac_detail">
                                <h4>Concierge services</h4>
                                <p>Whether it is a Botox party, a weight loss consultation or testosterone replacement therapy consult, our providers can come to you where ever you are.</p>
                            </div>
                        </div>
                        <div class="oac_box">
                            <div class="oacb_icon"><img src="/assets/images/icon2.svg" class="img-fluid" alt=""></div>
                            <div class="oac_detail">
                                <h4>Get quick and Easy Treatment Options</h4>
                                <p>We've created a host of treatments that members and soon to be members desire. We've made the process to get treated streamlined and more convenient - just for you.</p>
                            </div>
                        </div>
                        <div class="oac_box">
                            <div class="oacb_icon"><img src="/assets/images/icon3.svg" class="img-fluid" alt=""></div>
                            <div class="oac_detail">
                                <h4>Get Support from Our Community</h4>
                                <p>We have a q&a section where you can ask licensed providers questions about treatments and conditions. We also have a forum for added support.</p>
                            </div>
                        </div>
                        <div class="oac_box">
                            <div class="oacb_icon"><img src="/assets/images/icon1.svg" class="img-fluid" alt=""></div>
                            <div class="oac_detail">
                                <h4>Contests/ Challenges</h4>
                                <p>We have a q&a section where you can ask licensed providers questions about treatments and conditions. We also have a forum for added support.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

	<script>
	
		function setPlanType(e){

			if($(e).hasClass('special')){
				$(e).hide();
				$(e).parent().find('.plan_criteria.no_special').show();				
				
				$(e).parent().find('.short_des.special').hide();				
				$(e).parent().find('.short_des.no_special').show();				
				
				$(e).parent().find('.amount.special').hide();				
				$(e).parent().find('.amount.no_special').show();				
								
			}else{
				$(e).hide();
				$(e).parent().find('.plan_criteria.special').show();
				
				$(e).parent().find('.short_des.no_special').hide();						
				$(e).parent().find('.short_des.special').show();

				$(e).parent().find('.amount.no_special').hide();						
				$(e).parent().find('.amount.special').show();				
			}

		}
	</script>
@endsection