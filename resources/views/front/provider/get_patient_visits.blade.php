<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
@if($patient_visits && $patient_visits->count() > 0)
<?php //echo '<pre>'; print_r($patient_visits); ?>
	@foreach($patient_visits as $key => $value)
	<div class="myvisits pv_box status-box bg-white round-crn hover-effect-box">
        <div class="pv_title">
            <div class="pv_info">
                <div class="pv_info_box">
					<div class="pv_img"><img src="{{ env('APP_URL') }}/{{ $value->patient_image }}" alt="patient" class="thumb" /></div>
					<span class="value_username">{{ $value->patient_name }}</span>
				</div>
            
            <div class="pv_t"><h4 class="font-weight-bold">{{ $value->service_name }} <small>(@if($value->service_visit_type == 1) Asynchronous @elseif($value->service_visit_type == 2) Synchronous @elseif($value->service_visit_type == 3) Concierge @endif)</small></h4>
            
            <div class="pv_date">Created : {{ date(env('DATE_FORMAT_PHP_HA'),strtotime($value->visit_time)) }}</div>
            </div>
            </div>
            <div class="label-mute text-right mt-1"><span class="@if($value->is_imp == 1)imp_visit @endif pv_signal fa fa-heart" onclick="setImp(this, {{ $value->online_visit_id }});"></span>VISIT STATUS - <span class="text-info status"> {{ $value->visit_status}}</span></div>
        </div>
        
		<div class="pv_treat_box">
					
		@if(isset($value->order_id) && isset($value->subscription_id))
			<div class="provider">
				<h6 class="label-mute">PRESCRIPTION</h6>
				<div class="provider-details mt-15">
			@if(isset($value->medicine_id))
				@if(isset($value->medicine_images) && $value->medicine_images != '')
					@php 
						$medicine_images =  json_decode($value->medicine_images,true);
						$medicine_image  = env('APP_URL').'/'.$medicine_images[0];
					@endphp
				@else
						$medicine_image = '';
				@endif
					<img src="{{ $medicine_image }}" alt="prescription" class="thumb" />
					<span class="value_username">{{$value->medicine_name}}, {{$value->variant_name}}</span>
			@endif
				</div>
			</div>
		@endif
		@if(isset($value->event_start) && $value->event_start != '' && $value->visit_status != 'PENDING ACCEPTANCE')
			<div class="provider appointment_area">
				<h6 class="label-mute">APPOINTMENT</h6>
				<div class="provider-details mt-15">
				@if($value->event_status != 2)						
					<span class="value_username appointment_time">{{ date(env('DATE_FORMAT_PHP_HA'),strtotime($value->event_start)) }}</span>
				@endif
				@if($value->event_status == 0)						
					<span class="value_username appointment_status">
						<button type="button" class="btn btn-outline-success btn-small" title="Accept" onclick="change_appointment_status(this,{{$value->event_id}},'accept');">Accept</button>
						<button type="button" class="btn btn-outline-danger btn-small" title="Decline" onclick="change_appointment_status(this,{{$value->event_id}},'decline');">Decline</button>
					</span>
				@endif						
				</div>
			</div>	
		@endif				
			<div class="action_btn">		

				@if($value->visit_status == 'PENDING ACCEPTANCE')
					<button type="button" class="btn btn-outline-success btn-sm" title="Accept" onclick="change_status(this,{{$value->online_visit_id}},'accept');">Accept</button>
				@endif			
					
				<div class="action_div @if($value->visit_status == 'ACCEPTED' || $value->visit_status == 'REVIEWED' || $value->visit_status == 'COMPLETE') action_show @else action_hide @endif">
					<button type="button" class="btn btn-outline-primary btn-sm" title="View" onclick="window.location.href = '{{env('APP_URL')}}/provider/visit_detail/{{$value->online_visit_id}}';">View</button>
					
					{{-- @if($value->patient_chat_id) --}}
					
					 @if($value->service_visit_type == 1)
						 @if($value->chat_feature==1)
							<button type="button" class="btn btn-outline-success btn-sm chat_btn" onclick="chat_box({{ $value->patient_id }});" title="Chat">Chat</button>
						 @endif
						 @if($value->video_call_feature==1)
							<button type="button" class="btn btn-outline-success btn-sm" onclick="video_box({{ $value->patient_id }});" title="Video Call">Video Call</button>
						 @endif
					  @else
						<button type="button" class="btn btn-outline-success btn-sm chat_btn" onclick="chat_box({{ $value->patient_id }});" title="Chat">Chat</button>
					
						<button type="button" class="btn btn-outline-success btn-sm" onclick="video_box({{ $value->patient_id }});" title="Video Call">Video Call</button>
					  @endif
						<!--button type="button" class="btn btn-outline-primary btn-sm" onclick="window.open('{{env('APP_URL')}}/myvideo/{{$value->patient_id}}','_blank');" title="Video Call">Video Call</button-->
					{{-- @endif --}}
					
					@if(isset($value->event_start) && $value->event_start !='')
						<!--button type="button" class="btn @if($value->event_status == 1) btn-outline-success @else btn-outline-primary @endif btn-sm video_call_btn" title="Video Call">Video Call</button-->
					@endif
				</div>
									
			</div>
		</div>
	</div>
	@endforeach
@endif
</div>
<div class="col-xl-12 col-lg-12 col-md-12">
@if($patient_visits && $patient_visits->count() > 0)
	<div class="custom-pagination mt-15">
		<nav aria-label="Page navigation example">
			{{ $patient_visits->links() }}
		</nav>
	</div>
@endif
</div>