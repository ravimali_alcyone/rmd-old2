@extends('front.provider_layout.app')

@section('content')

@push('header_scripts')

<style>
.chat_pop{width:100%;max-width:600px;z-index:2;position:fixed;background:#f1f8f9;color:#000;right:-100%;bottom:0;height:100vh;box-shadow:0 10px 30px 0 rgba(0,0,0,.16);z-index: 10;}
.cp_close{width:40px;height:48px;font-size:14px;position:absolute;left:0;top:0;display:flex;justify-content:center;align-items:center;cursor:pointer;border-right:1px solid #dedede}

iframe.chat_frame {
    height: calc(100vh - 70px);
    width: 100%;
    border: none;
}
.chat_pop {
    z-index: 1031;
	top:70px;
}
@media screen and (max-width: 962px){
.chat_pop {
    z-index: 1031;
	top :0;
}
iframe.chat_frame {
    height: 100vh ;
    width: 100%;
    border: none;
}
}
</style>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
@endpush
	
	<!-- Chat Box -->
    <div class="chat_pop patient">
		<a class="cp_close" onclick="chat_close();"><i class="fa fa-times"></i></a>
        <div class="chat_block chatBox">
			
        </div>
    </div>	
	
	<!-- Video Box -->
    <div class="chat_pop video">
		<div class="edit_pop_title">
			<h3>Audio/Video Call</h3>
			<a class="cp_close" onclick="video_close();"><i class="fa fa-times"></i></a>
		</div>
        <div class="chat_block videoBox">
			
        </div>
    </div>		
		
	<div class="col-xl-10 col-lg-9 col-md-9 col-sm-12">
		<div class="main-center-data">
			<h3 class="display-username">Patient Visits</h3>
			<div class="row mt-15">
				<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
					<h5>Filter</h5>
					<div class="filters_wrapper row">
					
						<div class="data-list-filters">
							<span class="filter_heading">By Date</span>
							<div class="dates">
								<input type="text" placeholder="Date : From" id="start_date" value="" onchange="getData();">
								<input type="test" placeholder="Date : To" id="end_date" value="" onchange="getData();">
							</div>
							
						</div>
					
						<div class="data-list-filters">
							<span class="filter_heading">By Visit Type</span>
							<select id="by_visit_type" class="custom-select select2" onchange="getData();">
								<option value="">All</option>
								<option value="1">Asynchronous Telemedicine</option>
								<option value="2">Synchronous Telemedicine</option>
								<option value="3">Concierge</option>
							</select>
						</div>

						<div class="data-list-filters">
							<span class="filter_heading">By Service</span>
							<select id="by_service" class="custom-select select2" onchange="getData();">
								<option value="">All</option>
							@if($services && $services->count() > 0)
								@foreach($services as $key => $value)
									<option value="{{ $value->id }}">{{ $value->name }}</option>
								@endforeach
							@endif
							</select>																
						</div>	

						<div class="data-list-filters">
							<span class="filter_heading">By Visit Status</span>
							<select id="by_visit_status" class="custom-select select2" onchange="getData();">
								<option value="">All</option>
								<option value="IMPORTANT">IMPORTANT</option>
								<option value="PENDING ACCEPTANCE">PENDING</option>
								<option value="ACCEPTED">ACCEPTED</option>
								<option value="REVIEWED">REVIEWED</option>
								<option value="SCHEDULED">SCHEDULED</option>
								<option value="COMPLETE">COMPLETE</option>
								<option value="CANCELED">CANCELED</option>
								<option value="REFUND">REFUND</option>
								<option value="RESCHEDULED">RESCHEDULED</option>
							</select>
						</div>					
					</div>
				</div>
			</div>
			<div id="patients_data" class="row">
			</div>
		</div>
	</div>

@push('scripts')
<script>

	$(function(){
		$("#start_date").datepicker({
			dateFormat: "{{ env('DATE_FORMAT_JQ') }}",
			changeMonth: true,
			changeYear: true,
			yearRange: "-150:+0",
		});
		
		$("#end_date").datepicker({
			dateFormat: "{{ env('DATE_FORMAT_JQ') }}",
			changeMonth: true,
			changeYear: true,
			yearRange: "-150:+0",
		});
		
	});
	

	$("#by_visit_type, #by_service, #by_visit_status").select2({
		 minimumResultsForSearch: -1
	});
	
	$(function() {
	  	getData();
	});
	
	function getData(page)
	{
		
		if($("#start_date").val() !='' &&  $("#end_date").val() != ''){
			
			if($("#start_date").val() > $("#end_date").val()){
				errorAlert('Start date must not be greater than End date.',2000,'top-right');
				return false;
			}
		
		}
		
		var start_date = $("#start_date").val();
		var end_date = $("#end_date").val();
	
		var new_start_date = moment(start_date, "MM/DD/YYYY").format("YYYY-MM-DD");
		var new_end_date = moment(end_date, "MM/DD/YYYY").format("YYYY-MM-DD");
		
		if(start_date==''){
			new_start_date='';
		}
		if(end_date==''){
			new_end_date='';
		}
		
		var req_data = {
			'start_date': new_start_date,
			'end_date': new_end_date,
			'visit_type': $("#by_visit_type").val(),
			'visit_status': $("#by_visit_status").val(),
			'service': $("#by_service").val()
		};
		
		$(".loader").css('display', 'flex');
		
		var url;
		
		if(page){
			url = "{{ route('provider.patient_visits') }}?page="+page;
		}else{
			url = "{{ route('provider.patient_visits') }}";
		}
		
		
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			data: req_data,
			url: url,
			type: "POST",
			// dataType: 'json',
			success: function (response) {
				$(".loader").css('display', 'none');
				if(response['status'] == 'success'){
					$('#patients_data').html(response['data']);
				}else{
					errorAlert(response['message'],2000,'top-right');
					$('#patients_data').html(response['data']);
				}
			},
			error: function (data) {
				$(".loader").css('display', 'none');
				let errors = data.responseJSON.errors;
				
				$.each(errors, function(key, value) {
					errorAlert('Error Occured.',3000,'top-right');					
				});					
	
			}
		});		
	}
	
	function change_status(e,id,status){
		$(".loader").css('display', 'flex');
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			data: {'id':id,'status':status},
			url: "{{ route('provider.change_visit_status') }}",
			type: "POST",
			// dataType: 'json',
			success: function (response) {
				$(".loader").css('display', 'none');
				if(response['status'] == 'success'){
					//successAlert(response['message'],2000,'top-right');
					callProgressBar();
					setTimeout(function(){
						location.reload();							
					}, 2000);
				}else{
					errorAlert(response['message'],2000,'top-right');						
				}
			},
			error: function (data) {
				$(".loader").css('display', 'none');
				let errors = data.responseJSON.errors;
				
				$.each(errors, function(key, value) {
					errorAlert('Error Occured.',3000,'top-right');
				});					
			}
		});
	}
	
	function change_appointment_status (e,event_id,status){
		
		$(".loader").css('display', 'flex');
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			data: {'event_id':event_id,'status':status},
			url: "{{ route('provider.change_appointment_status') }}",
			type: "POST",
			// dataType: 'json',
			success: function (response) {
				$(".loader").css('display', 'none');
				if(response['status'] == 'success'){
					//successAlert(response['message'],2000,'top-right');
					callProgressBar();
					setTimeout(function(){
						if(status == 'accept'){
							$(e).parent().parent().parent().parent().find('.action_btn button.video_call_btn').removeClass('btn-outline-primary');
							$(e).parent().parent().parent().parent().find('.action_btn button.video_call_btn').addClass('btn-outline-success');
							$(e).parent().parent().parent().parent().parent().find('.visit_status_info span.status').html('ACCEPTED');
							$(e).parent().parent().parent().parent().find('.action_btn .action_div').addClass('action_show');
							$(e).parent().parent().parent().parent().find('.action_btn .action_div').removeClass('action_hide');
							$(e).parent('.appointment_status').remove();
						}
						else{
							$(e).parent().parent().parent().parent().find('.appointment_time').remove();
							$(e).parent().parent().parent().parent().find('.action_btn button.video_call_btn').remove();
							$(e).parent().parent().parent().parent().parent().find('.visit_status_info span.status').html('RESCHEDULED');
							$(e).parent().parent().parent().parent().find('.action_btn .action_div').addClass('action_hide');
							$(e).parent().parent().parent().parent().find('.action_btn .action_div').removeClass('action_show');		
							$(e).closest('.appointment_area').remove();
						}
						location.reload();
					}, 2000);
				}else{
					errorAlert(response['message'],2000,'top-right');						
				}
			},
			error: function (data) {
				$(".loader").css('display', 'none');
				let errors = data.responseJSON.errors;
				
				$.each(errors, function(key, value) {
					errorAlert('Error Occured.',3000,'top-right');
				});					
			}
		});		
	}
	
	//Pagination
	$(document).on('click', '.custom-pagination ul.pagination li.page-item a.page-link', function(event){
		event.preventDefault(); 
		var page = $(this).attr('href').split('page=')[1];
	  
		getData(page);
	 });

	 
	function setImp(e, id){

		var is_imp;
		var msg;
		
		if($(e).hasClass('imp_visit')){
			is_imp = 0; //new status
			msg = "Are you sure to remove this visit from Important?";
		}else{
			is_imp = 1; //new status
			msg = "Are you sure to make this visit Important?";
		}
				
		swal({
			title: msg,
			text: "",
			//icon: 'warning',
			buttons: {
			cancel: true,
			delete: 'Yes'
			}
		}).then((isConfirm) => {
			if (!isConfirm) {
				return false;
			} else {
				
				$(".loader").css('display', 'flex');
				$.ajax({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
					data: {'id':id, 'is_imp':is_imp},
					url: "{{ route('provider.set_imp_visit') }}",
					type: "POST",
					// dataType: 'json',
					success: function (response) {
						$(".loader").css('display', 'none');
						if(response['status'] == 'success'){
											
							//successAlert(response['message'],2000,'top-right');
							callProgressBar();
							if($(e).hasClass('imp_visit')){
								$(e).removeClass('imp_visit');
							}else{
								$(e).addClass('imp_visit');
							}
							
						}else{
							errorAlert('Error occured.',3000,'top-right');								
						}
					},
					error: function (data) {
						$(".loader").css('display', 'none');
						errorAlert('Error occured.',3000,'top-right');													
					}
				});
			}
		});
	}
	
	//Chat Box
	$(document).ready(function(){
		$('.cp_close').click(function(){
			var hidden = $('.chat_pop');
			hidden.animate({"right":"-100%"}, "slow").removeClass('visible');
		});
	});

	
	function chat_close(){
		$('.chat_block.chatBox').html('');
		$('.chat_block.videoBox').html('');
	}
	
	function video_close(){
		$('.chat_block.videoBox').html('');
		$('.chat_block.chatBox').html('');
	}	
	
	function box_close(){
		$('.cp_close').trigger('click');
	}
	
	function chat_box(userid){
		let url = "{{ env('APP_URL') }}/mychat/"+userid;
		let html_val = "<iframe src='"+url+"' class='chat_frame'></iframe>";
		
		$('.chat_block.chatBox').html(html_val);
		
		var hidden = $('.chat_pop.patient');
		if (hidden.hasClass('visible')){
			hidden.animate({"right":"-100%"}, "slow").removeClass('visible');
		} else {
			hidden.animate({"right":"0"}, "slow").addClass('visible');
		}
	}

	function video_box(userid){
		let url = "{{ env('APP_URL') }}/myvideo/"+userid;
		let html_val = "<iframe src='"+url+"' style='width:100%;height:100vh; border:none;'></iframe>";
		
		$(".edit_pop_title h3").text("Audio/Video Call");
			
		$('.chat_block.videoBox').html(html_val);
		
		var hidden = $('.chat_pop.video');
		if (hidden.hasClass('visible')){
			hidden.animate({"right":"-100%"}, "slow").removeClass('visible');
		} else {
			hidden.animate({"right":"0"}, "slow").addClass('visible');
		}		
	}	
	
</script>
@endpush
	
@endsection