@extends('front.provider_layout.app')

@section('content')

	<div class="col-xl-9 col-lg-9 col-md-12">
		<div class="main-center-data visit_overview">
			<h4 class="display-username">Visits Overview - {{ $patient_visit->service_name }} </h4>
			<div class="cleint-box bg-white round-crn pd-20-30 mt-15 hover-effect-box">
				<div class="m-0 p-0 pateint_style">
					<span>Patient Info</span>
				</div>
				<div class="pateint_details">
					<div class="user_left">
						<div class="user_pic">
							<img src="{{ env('APP_URL') }}/{{ $patient_visit->patient_image }}" alt="User Profile">
						</div>
						<div class="user_name">
							<h4 class="mt-15">{{ $patient_visit->patient_name }}</h4>
							<ul class="gender_list pl-0">
								@php
								$bday = new DateTime($patient_visit->dob); // Your date of birth
								$today = new Datetime(date('Y-m-d'));
								$diff = $today->diff($bday);
								@endphp
								<li>{{ $diff->y }} Years</li>
								<li>{{ ucfirst($patient_visit->gender) }}</li>
							</ul>
						</div>

					</div>
					<div class="contact_details mt-15 ">
						<div class="con_no">
							<i class="fa fa-phone" aria-hidden="true"></i>
							<span>{{ $patient_visit->phone }}</span>

						</div>
						<div class="con_no">
							<i class="fa fa-envelope" aria-hidden="true"></i>
							<span>{{ $patient_visit->email }}</span>

						</div>
					</div>

				</div>
			</div>
			<div class="cleint-box bg-white round-crn pd-20-30 mt-15 hover-effect-box">
				<div class="main-center-data visit_overview">
					<div class="m-0 p-0 pateint_style">
						<span>Prescription included with this treatment</span>
					</div>
					<div class="prescription_name">
						<div class="treatment_details mt-2">
							<span>Prescription for :</span>
							<h4 class="mt-2">Erectile dysfunction treatment</h4>
							<p>This Treatment was taken on {{ date(env('DATE_FORMAT_PHP'),strtotime($patient_visit->visit_time)) }}</p>

						</div>
					
						<div class="treatment_details prescription_user mt-2">
							<span>Prescription</span>
							<div class="provider_img mt-3">
							@if(isset($patient_visit->medicine_id))
								@if(isset($patient_visit->medicine_images) && $patient_visit->medicine_images != '')
									@php
										$medicine_images = json_decode($patient_visit->medicine_images,true);
										$medicine_image = env('APP_URL').'/'.$medicine_images[0];
									@endphp
								@else
									$medicine_image = '';
								@endif
								<div class="user_pic ">
									<img class="d-inline" src="{{ $medicine_image }}" alt="User Profile">
								</div>
								<div class="user_name ml-3">
									<p>{{$patient_visit->medicine_name}}, {{$patient_visit->variant_name}}</p>

								</div>
							@else
								<div>
									<p>No prescription</p>
								</div>
							@endif
							</div>
						</div>
					</div>
				</div>



			</div>

		@if($question_answers)
			<div class="cleint-box rmd_dashboard_ques bg-white round-crn pd-20-30 mt-15 hover-effect-box">
				<div class="main-center-data visit_overview">
					<div class="mb-2 p-0 pateint_style">
						<span>Patient Visit Details</span>
					</div>
					<div class="question_ans">
						<div class="ques_head rmd_head">
							<div class="ques_sub">
								<span>Questions</span>
							</div>
							<div class="ans_right">
								<span>Answers</span>
							</div>
						</div>
					@php $q= 1; @endphp
					@foreach($question_answers as $key => $val)

						@php
							$ans = '';

							if($val['answer_type'] == 'option'){
								$ans = $val['option_text'];
							}else{
								$ans = $val['answer_text'];
							}
						@endphp
						
						<div class="ques_head rmd_ques">
							<div class="ques_sub">
								<h4>{{$q}}. {{ $val['question_text'] }} </h4>
							</div>
							<div class="ans_right">
								<p> {{ $ans }} </p>
							</div>
						</div>
						@php $q++; @endphp
					@endforeach
					</div>
				</div>
			</div>
		@endif

		@if($last_visit && $last_visit->count() > 0)	
			<div class="cleint-box bg-white round-crn pd-20-30 mt-15 hover-effect-box">
				<div class="main-center-data visit_overview">
					<div class="m-0 p-0 pateint_style">
						<span>Last Visit Details</span>
					</div>
					<div class="prescription_name">
						<div class="treatment_details mt-2">
							<span>Prescription for :</span>
							<h4 class="mt-2">Erectile dysfunction treatment</h4>
							<p>Last Treatment was taken on {{ date(env('DATE_FORMAT_PHP'),strtotime($last_visit->visit_time)) }}</p>
						</div>
						<div class="treatment_details prescription_user mt-2">
							<span>Provider</span>
						@if(isset($last_visit->provider_id) && $last_visit->provider_id != '')
							<div class="provider_img mt-3">
								<div class="user_pic">
									<img class="d-inline" src="{{ env('APP_URL') }}/{{ $last_visit->provider_image }}" alt="Provider Profile">
								</div>
								<div class="user_name ml-3">
									<p>{{ $last_visit->provider_name }}</p>
								</div>
							</div>
							@else
								<div>
									<p>No provider assinged.</p>
								</div>						
						@endif
						</div>
					
						<div class="treatment_details prescription_user mt-2">
							<span>Prescription</span>					
							<div class="provider_img mt-3">
							@if(isset($last_visit->medicine_id))
								@if(isset($last_visit->medicine_images) && $last_visit->medicine_images != '')
									@php
										$medicine_images = json_decode($last_visit->medicine_images,true);
										$medicine_image = env('APP_URL').'/'.$medicine_images[0];
									@endphp
								@else
									$medicine_image = '';
								@endif
								<div class="user_pic ">
									<img class="d-inline" src="{{ $medicine_image }}" alt="User Profile">
								</div>
								<div class="user_name ml-3">
									<p>{{$last_visit->medicine_name}}, {{$last_visit->variant_name}}</p>
								</div>
							@else
								<div>
									<p>No prescription</p>
								</div>
							@endif
							</div>
						</div>
					</div>
				</div>
			</div>
		@endif

			<div class="cleint-box bg-white round-crn pd-20-30 mt-15 hover-effect-box">
				<div class="main-center-data visit_overview">
					<div class="m-0 p-0 pateint_style">
						<span>Medical Note</span>
					</div>
					<form id="addNoteFrom">
						<div class="add_note_area">
							<textarea id="add_note" placeholder="Type Here..." cols="50" rows="4">{{ $patient_visit->provider_note }}</textarea>
						</div>
						<div class="add_note_btn">
							<input type="hidden" name="id" value="{{ $patient_visit->online_visit_id }}"/>
							<button type="submit" class="btn btn-outline-primary btn-sm">Save Note</button>
						</div>
					</form>
				</div>
			</div>
			
			<div class="cleint-box round-crn pd-20-30 mt-15 hover-effect-box">
			
				<!--a href="javascript:void(0);" class="rmd_blue_button mt-3" onclick="window.open('{{env('APP_URL')}}/mychat/{{$patient_visit->patient_id}}','_blank');">Chat</a-->
				
			@if($patient_visit->visit_type == 1)
				@if($patient_visit->visit_status != 'REVIEWED' && $patient_visit->visit_status != 'COMPLETE' )
					<a href="javascript:void(0);" class="rmd_blue_button mt-3 ml-2" onclick="change_status(this,{{$patient_visit->online_visit_id}},'review');">Reviewed</a>					
				@endif
				@if($patient_visit->visit_status == 'REVIEWED')	
					<a href="javascript:void(0);" class="rmd_blue_button mt-3 ml-2" onclick="change_status(this,{{$patient_visit->online_visit_id}},'complete');" >Complete</a>
				@endif					
			@else
				@if($patient_visit->visit_status != 'COMPLETE')	
					<a href="javascript:void(0);" class="rmd_blue_button mt-3 ml-2" onclick="change_status(this,{{$patient_visit->online_visit_id}},'complete');" >Complete</a>
				@endif				
			@endif
				

	
				<a href="javascript:void(0);" class="rmd_blue_button mt-3 ml-2" onclick="window.location.href = '{{ route('provider.patient_visits') }}';">Back</a>
			</div>
		</div>
	</div>

	<script>
		//CKEDITOR.replace('add_note');
		
			CKEDITOR.replace( 'add_note',
			{
				toolbar :
				[
					{ name: 'document', items : [ 'NewPage','Preview' ] },
					{ name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
					{ name: 'editing', items : [ 'Find','Replace','-','SelectAll','-','Scayt' ] },
					{ name: 'insert', items : [ 'Table','HorizontalRule','Smiley','SpecialChar','PageBreak' ] },
					{ name: 'styles', items : [ 'Styles','Format' ] },
					{ name: 'basicstyles', items : [ 'Bold','Italic','Strike','-','RemoveFormat' ] },
					{ name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote' ] },
					{ name: 'links', items : [ 'Link','Unlink','Anchor' ] },
					{ name: 'tools', items : [ 'Maximize','-','About' ] }
				]
			}); 

		$("#addNoteFrom").submit(function(e) {
			e.preventDefault();
			var add_note = CKEDITOR.instances.add_note.getData();		
			var formData = new FormData(this);
			formData.append('add_note', add_note);
			
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				url: "{{ route('provider.visit_add_note') }}",
				data: formData,
				type: "POST",
				cache:false,
				contentType: false,
				processData: false,
				// dataType: 'json',
				success: function (response) {
					if(response['status'] == 'success'){
						swal({
							title: response['message'],
							icon: 'success'
						});
						setTimeout(function(){
							swal.close();
						}, 1000);
					}
				}
			});
		});
	
		function change_status(e,id,status){
			$(".loader").css('display', 'flex');
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: {'id':id,'status':status},
				url: "{{ route('provider.change_visit_status') }}",
				type: "POST",
				// dataType: 'json',
				success: function (response) {
					$(".loader").css('display', 'none');
					if(response['status'] == 'success'){
						swal({
							title: response['message'],
							icon: 'success'
						});
						setTimeout(function(){
							swal.close();
							
							if(status == 'complete'){
								window.location = "{{ route('provider.patient_visits') }}";
							}else{
								location.reload();		
							}
												
						}, 1000);
					}else{
						swal({
							title: response['message'],
							icon: 'error'
						});
						setTimeout(function(){
							swal.close();						
						}, 1000);						
					}
				},
				error: function (data) {
					$(".loader").css('display', 'none');
					let errors = data.responseJSON.errors;
					
					$.each(errors, function(key, value) {
						errorAlert(value[0],5000,'bottom-left');
					});					
					swal({
						title: 'Error Occured.',
						icon: 'error'
					})
				}
			});
		}
		
	</script>
@endsection