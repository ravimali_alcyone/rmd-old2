@extends('front.common_layout.app')

@section('content')
	
@push('header_scripts')

	<style>
		.pac-container {
			z-index: 10000 !important;
		}
		.pac-container:after {
			background-image: none !important;
			height: 0px;
		}
		.width_2{
			width:49%;
		}
		
		.mid_wrap {
			margin: 0 auto;
			width: 100%;
			max-width: 735px;
		}		
	</style>

	@if(auth()->user()->user_role==4)
		<link rel="stylesheet" href="{{ asset('dashboard/css/lightbox.min.css') }}">
		<link rel="stylesheet" href="{{ asset('dashboard/css/zuck.min.css') }}">
		<link rel="stylesheet" href="{{ asset('dashboard/css/snapssenger.css') }}">
		<script src="{{ asset('dashboard/js/lightbox.min.js') }}"></script>
	@endif
	
	@if(auth()->user()->user_role==3)
		<link rel="stylesheet" href="{{ asset('dashboard/css/lightbox.min.css') }}">
		<script src="{{ asset('dashboard/js/lightbox.min.js') }}"></script>
	@endif

@endpush

	@if(auth()->user()->user_role==3)
		<div class="col-xl-10 col-lg-8 col-md-9 col-sm-12">
	@endif
			<div class="row">
				<div class="mid_wrap col-xl-9 col-lg-9 col-md-9 col-sm-12">
					<div class="row">
						<div class="col-md-12">
							<div class="main-center-data user_pro_edit_area">
								<h3 class="display-username onpage w-100">Profile View</h3>
								<div class="informative-block bg-white round-crn pd-20-30 mt-15 hover-effect-box">
									<div class="edit_user_pic new_gal">
										@if($user->image)
											<a href="/{{$user->image}}" data-lightbox="lightbox-set-10">	
												<img id="profileData" src="/{{$user->image}}" alt="{{$user->name}}" class="avatar-post"/> 
											</a>
										@else
											<a href="/dist/images/user_icon.png" data-lightbox="lightbox-set-10">
												<img id="profileData" src="/dist/images/user_icon.png" alt="{{$user->name}}" class="avatar-post"/>
											</a>
										@endif
										
									</div>
									
									<div class="text-center" style="margin-top: 15px;">
									@php $flag = 0; @endphp
									@if($supporting)
										@php $flag = 1; @endphp
										@if($supporting->status == 1)
											@php $flag2 = 1; @endphp
											<a href="javascript:void(0)" class="following" data-toggle="modal" data-target=	"#unFollowModal" data="{{$id}}"><span class="fa fas fas fa-check"></span> Supporting</a>
										@else
											<a href="javascript:void(0)">Pending..</a>
										@endif
									@endif
									

									
									@if($supporter)
										@php $flag = 1; @endphp
										@if($supporter->status == 1)
											<a href="javascript:void(0)" class="following" data-toggle="modal" data-target=	"#unFollowModal" data="{{$id}}"><span class="fa fas fas fa-check"></span> Supporter</a>
										@else
											<a href="javascript:void(0)"></span><span class="fa fas fa-check"></span> Confirm</a>
											<a href="javascript:void(0)"></span><span class="fa fas fa-remove"></span> Ignore</a>
											<a href="javascript:void(0)" class="follow" onclick="javascript: follow({{$id}}, this)"><span class="fa fas fa-plus"></span> Support Back</a>
										@endif	
									@endif

									@if($flag == 0)
										<a href="javascript:void(0)" class="follow" onclick="javascript: follow({{$id}}, this)"><span class="fa fas fa-plus"></span> Support</a>
									@endif
								
									
									</div>
								</div>
								<div class="informative-block bg-white round-crn pd-20-30 mt-15 hover-effect-box">
									<h4 class="font-weight-bold">User Info</h4>
									<table class="table-responsive mt-15 user_profile">
										<tbody>
											<tr>
												<td class="label-mute">Name</td>
												<td>{{$user->name}}</td>
											</tr>
											<tr>
												<td class="label-mute">Email</td>
												<td>{{$user->email}}</td>
											</tr>
											<tr>
												<td class="label-mute">Phone</td>
												<td>{{$user->phone}}</td>
											</tr>
											<tr>
												<td class="label-mute">DOB</td>
												<td><?php if($user->dob != '0000-00-00'){ echo date(env('DATE_FORMAT_PHP'),strtotime($user->dob)); }else{ echo 'mm/dd/yyyy'; } ?> </td>
											</tr>
											<tr>
												<td class="label-mute">Gender</td>
												<td>{{ucfirst($user->gender)}}</td>
											</tr>
											<tr>
												<td class="label-mute">Education</td>
												<td>{{ucfirst($user->education)}}</td>
											</tr>
											<tr>
												<td class="label-mute">Goal</td>
												<td>{{ucfirst($user->goal)}}</td>
											</tr>
											<tr>
												<td class="label-mute">Interest</td>
												<td>{{ucfirst($user->interest)}}</td>
											</tr>
											<tr>
												<td class="label-mute">Relationship Status</td>
												<td>{{ucfirst($user->relation_status)}}</td>
											</tr>
										</tbody>
									</table>
								</div>
								<div class="informative-block bg-white round-crn pd-20-30 mt-15 hover-effect-box w-100">
									<h4 class="font-weight-bold">Short Info</h4>
									<table class="table-responsive mt-15 user_shiping">
										<tbody>
											<tr>
												<td>@if($user->short_info) {!! $user->short_info !!} @endif</td>
											</tr>
										</tbody>
									</table>
								</div>
								<div class="informative-block bg-white round-crn pd-20-30 mt-15 hover-effect-box width_2">
									@if($user->user_role==4)
									<h4 class="font-weight-bold"></h4>
									<table class="table-responsive mt-15 user_shiping">
										<tbody>
											<!--<tr>
												<td class="label-mute">Address</td>
												<td><?php echo ($user->id==auth()->user()->id) ? $user->address : '********'; ?></td>
											</tr>-->
											<tr>
												<td class="label-mute">Location</td>
												<!--<td><?php echo ($user->id==auth()->user()->id) ? $user->city : '********'; ?></td>-->
												<td><?php echo $user->city; ?></td>
											</tr>
										</tbody>
									</table>
									@else
									<h4 class="font-weight-bold"></h4>
									<table class="table-responsive mt-15 user_shiping">
										<tbody>
											<!--<tr>
												<td class="label-mute">Address</td>
												<td><?php echo ($user->id==auth()->user()->id) ? $user->address : '********'; ?></td>
											</tr>-->
											<tr>
												<td class="label-mute">Location</td>
												<!--<td><?php echo ($user->id==auth()->user()->id) ? $user->city : '********'; ?></td>-->
												<td><?php echo $user->city; ?></td>
											</tr>
										</tbody>
									</table>
									@endif
									
								</div>
								<div class="informative-block bg-white round-crn pd-20-30 mt-15 hover-effect-box width_2">
									<h4 class="font-weight-bold">Supporter’s</h4>
									<table class="table-responsive mt-15">
										<tbody>
											<tr>
												<td class="label-mute">Supporter’s</td>
												<td>{{$followers_count}}</td>
											</tr>
										</tbody>
									</table>
								</div>
								@if(!empty($user->clinical_expertise))
								<div class="informative-block bg-white round-crn pd-20-30 mt-15 hover-effect-box w-100">
									<h4 class="font-weight-bold">Short info</h4>
									<table class="table-responsive mt-15">
										<tbody>
											<tr>
												<td class="label-mute">Info</td>
												<td>{{$user->clinical_expertise}}</td>
											</tr>
										</tbody>
									</table>
								</div>
								@endif
							</div>
						</div>
					</div>
					@if(auth()->user()->user_role==4)
						<div class="row mt-15" id="all_dashboard_data">
							
						</div>
					@endif
				</div>
				@include('front.supporters.right_sidebar')
			</div>
	@if(auth()->user()->user_role==3)
		</div>
	@endif


    <!-- Modal -->
    <div class="modal fade" id="unFollowModal" tabindex="-1" role="dialog" aria-labelledby="unFollowModal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Confirm to Unsupporting</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to Unsupporting <span class="username font-weight-bold"></span>?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="javascript: unfollow(this)">Unupporting</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
	
@endsection

@push("scripts")
<script src="{{ asset('dashboard/js/lightbox.min.js') }}"></script>

@if(auth()->user()->user_role==4)
<script src="https://cdnjs.cloudflare.com/ajax/libs/emojionearea/3.4.2/emojionearea.min.js" integrity="sha512-hkvXFLlESjeYENO4CNi69z3A1puvONQV5Uh+G4TUDayZxSLyic5Kba9hhuiNLbHqdnKNMk2PxXKm0v7KDnWkYA==" crossorigin="anonymous"></script>
<script src="{{asset('assets/js/owl.carousel.js')}}" type="text/javascript"></script>
<script src="{{ asset('dashboard/js/zuck.min.js') }}"></script>
<script src="{{ asset('dashboard/js/script.js') }}"></script>

<script>
$(window).on('load', function() {
	load_page_data();
});
/****Upload dashboard data******/
function load_page_data()
{
	//$(".loader").css('display', 'block');
    var user_id = '<?php echo $user->id; ?>';
	$.ajax({

		headers: {

			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

		},

		data: {"user_id":user_id},

		url: "{{ route('load_data') }}",

		type: "GET",

		success: function (response) {

			//$(".loader").css('display', 'none');

			if(response['status'] == 'success'){

				$("#all_dashboard_data").html(response['data']);

				

				/********Bind the click event on html response*******/

				$(".reactions-box li").bind("click",function(){

					var flagType = $(this).data("reaction");
					
					flagType = (flagType=="Like") ? "like_flag" : ((flagType=="Love") ? "love_flag" : ((flagType=="Sad") ? "sad_flag" : "angary_flag" ));

					var postId   = $(this).data("postid");

					var reactionFlag = $(this).data("likeflag");

					reactionFlag = (reactionFlag == 1) ? 0 : 1;  //1= Post alreday liked,0= post need to unliked

					$.ajax({

						headers: {

							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

						},

						data: {"post_id":postId, "likeFlag": reactionFlag, "flagType":flagType},

						url: "{{ route('add_post_like') }}",

						type: "POST",

						success: function (response) {

							//$(".loader").css('display', 'none');

							if(response['status'] == 'success'){

								callProgressBar();
								if($(".post_like_"+postId).hasClass('like')){
									
								}else if($(".post_like_"+postId).hasClass('no_like')){
									$(".post_like_"+postId).append(",You");
								}else{
									$(".like_user_name_"+postId).append('<span> <a href="javascript:void(0)" class="like post_like_'+postId+'" onclick="getLikeDetail('+postId+')">You</a><span>');
									
								}

								/*successAlert(response['message'],2000,'top-right');

								setTimeout(function(){

									window.location = response['redirect'];

								}, 2000);*/

							}

							else{

								errorAlert(response['message'],2000,'top-right');

							}

						},

						error: function (data) {

							//$(".loader").css('display', 'none');

							console.log(data);

							let errors = data.responseJSON.errors;

							$.each(errors, function(key, value) {

								errorAlert(value[0],3000,'top-right');

							});

						}

					});	

				});

				/**************End Here*****************/

			}

			else{

				errorAlert(data.status,2000,'top-right');

			}

		},

		error: function (data) {

			$(".loader").css('display', 'none');

			let errors = data.responseJSON.errors;

			$.each(errors, function(key, value) {

				errorAlert(value[0],3000,'top-right');

			});

		}

	});
}
	

/**********Load more post on dashborad**********/

function loadMorePost(offset1,offset2)

{

	//$(".loader").css('display', 'block');

	$.ajax({

		headers: {

			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

		},

		data: {"offset1":offset1,"offset2":offset2},

		url: "{{ env('APP_URL').'/post' }}"+'/0/1',

		type: "GET",

		success: function (response) {

			//$(".loader").css('display', 'none');

			if(response['status'] == 'success'){

				$("#allPost").append(response['data']);

				$("#offset1").val(response['nxtOffset']);

				$("#offset2").val(offset2);

				callProgressBar();

				/********Bind the click event on html response*******/

				$(".reactions-box li").bind("click",function(){

					var flagType = $(this).data("reaction");
					//alert('FDFDFDF');
					flagType = (flagType=="Like") ? "like_flag" : ((flagType=="Love") ? "love_flag" : ((flagType=="Sad") ? "sad_flag" : "angary_flag" ));

					var postId   = $(this).data("postid");

					var reactionFlag = $(this).data("likeflag");

					reactionFlag = (reactionFlag == 1) ? 0 : 1;  //1= Post alreday liked,0= post need to unliked

					$.ajax({

						headers: {

							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

						},

						data: {"post_id":postId, "likeFlag": reactionFlag, "flagType":flagType},

						url: "{{ route('add_post_like') }}",

						type: "POST",

						success: function (response) {

							//$(".loader").css('display', 'none');

							if(response['status'] == 'success'){
								callProgressBar();
								if($(".post_like_"+postId).hasClass('like')){
									
								}else if($(".post_like_"+postId).hasClass('no_like')){
									$(".post_like_"+postId).append(",You");
								}else{
									$(".like_user_name_"+postId).append('<span> <a href="javascript:void(0)" class="like post_like_'+postId+'" onclick="getLikeDetail('+postId+')">You</a><span>');
									
								}

								/*successAlert(response['message'],2000,'top-right');

								setTimeout(function(){

									window.location = response['redirect'];

								}, 2000);*/

							}

							else{

								errorAlert(response['message'],2000,'top-right');

							}

						},

						error: function (data) {

							//$(".loader").css('display', 'none');

							//console.log(data);

							let errors = data.responseJSON.errors;

							$.each(errors, function(key, value) {

								errorAlert(value[0],3000,'top-right');

							});

						}

					});	

				});

				/**************End Here*****************/

			}

			else{

				errorAlert(data.status,2000,'top-right');

			}

		},

		error: function (data) {

			//$(".loader").css('display', 'none');
			//alert('fdfdf');
			let errors = data.responseJSON.errors;
		
			errorAlert('No Record Found.',2000,'top-right');
			
			$.each(errors, function(key, value) {

				errorAlert(value[0],3000,'top-right');

			});

		}

	});

}	
		
</script>	

@endif	

@endpush