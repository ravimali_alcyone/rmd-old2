@extends('front.layouts.app')

@section('content')

    <section class="inner_banner about_banner">
        <div class="container">
            <div class="content_wrapper">
                <div class="b_text">
                    <h1>The future of health care.</h1>
                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et.</p>
                </div>
            </div>
        </div>
        <img src="/assets/images/bottom_curve.svg" alt="bottom_curve">
    </section>

    <section class="client_feedback_sec">
        <div class="container">
            <div class="feedback_wrapper">
                <div class="feedback">
                    <span class="fas fa-quote-left"></span>
                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren,
                        no sea takimata sanctus est Lorem ipsum dolor sit.</p>
                    <img src="/assets/images/feedback_separator.png" alt="feedback_separator">
                    <div class="client">
                        <img src="/dist/images/user_icon.png" alt="client_img">
                        <span>Douglas Johnson, Founder & CEO</span>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="about_sec">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-6 order-md-1 order-2">
                    <div class="content_wrapper">
                        <p class="title_sm_red text-uppercase">About US</p>
                        <h1 class="title">Dedicate Yourself to the in things that really matter a good health</h1>
                        <p class="p_md">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna</p>
                        <ul>
                            <li>
                                <span class="fa fas fa-check"></span>
                                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna...</p>
                            </li>
                            <li>
                                <span class="fa fas fa-check"></span>
                                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna...</p>
                            </li>
                            <li>
                                <span class="fa fas fa-check"></span>
                                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna...</p>
                            </li>
                        </ul>
                        <button type="button" class="btn btn-primary">Get Started Now!</button>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 order-md-2 order-1">
                    <div class="img_wrapper">
                        <img src="/assets/images/about_img.svg" alt="about_img" class="w-auto">
                    </div>
                </div>
            </div>
            <div class="row expertise_row">
                <div class="col-sm-12 col-md-6">
                    <div class="img_wrapper">
                        <img src="/assets/images/about_expertise.png" alt="about_expertise" class="w-auto">
                    </div>
                </div>
                <div class="col-sm-12 col-md-6">
                    <div class="content_wrapper">
                        <p class="title_sm_red text-uppercase">Our Expertise</p>
                        <h1 class="title">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy</h1>
                        <p class="p_md">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna</p>
                        <ul>
                            <li>
                                <span class="fa fas fa-check"></span>
                                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna...</p>
                            </li>
                            <li>
                                <span class="fa fas fa-check"></span>
                                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna...</p>
                            </li>
                            <li>
                                <span class="fa fas fa-check"></span>
                                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna...</p>
                            </li>
                        </ul>
                        <button type="button" class="btn btn-primary">Get Started Now!</button>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="work_join_sec about">
        <div class="data_wrap">
            <div class="container">
                <div class="content_wrapper">
                    <div class="title">Categories Will Focus on
                        <p>Lorem ipsum dolor sit amet, consetetur</p>
                    </div>
                    <div class="boxes_wrapper d_flex_j_center">
                        <div class="box">
                            <span class="fas fa-dollar-sign icon"></span>
                            <h6 class="title">Transparent pricing</h6>
                            <p>Lorem ipsum dolor sit amet, consetetur sadiping elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam.</p>
                        </div>
                        <div class="box">
                            <span class="fas fa-hands-helping icon"></span>
                            <h6 class="title">Best-in-class care</h6>
                            <p>Lorem ipsum dolor sit amet, consetetur sadiping elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam.</p>
                        </div>
                        <div class="box">
                            <span class="icon"><img src="/assets/images/metro_chart_icon.png" alt="metro_chart_icon"></span>
                            <h6 class="title">Unlimited access</h6>
                            <p>Lorem ipsum dolor sit amet, consetetur sadiping elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection