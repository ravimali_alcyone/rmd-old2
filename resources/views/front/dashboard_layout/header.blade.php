<div class="loader" style="display:none;">
	<img src="{{asset('assets/images/loading.gif')}}" alt="loading">
</div>
<?php 
	use App\Notifications;
	use App\Library\Services\CommonService;
	$common = new CommonService();
	
	$user_id = auth()->user()->id;
	$notifications = Notifications::select('notifications.*')->where('notification_type','!=', 'supporter_requests')->where('followers_id',$user_id)->orderBy('id','DESC')->get();
	
	  
	$notice_count = Notifications::select('notifications.*')->where('notification_type','!=', 'supporter_requests')->where('followers_id',$user_id)->where('status',1)->orderBy('id','DESC')->get();
	  
	  
	$notifications_sp = Notifications::select('notifications.*')->where('notification_type', 'supporter_requests')->where('followers_id',$user_id)->orderBy('id','DESC')->get();
	
	$notice_count_sp = Notifications::select('notifications.*')->where('notification_type','supporter_requests')->where('followers_id',$user_id)->where('status',1)->orderBy('id','DESC')->get();
	
	$message_notice = Notifications::select('notifications.*')->where('followers_id',$user_id)->where('notification_type', 'unread_messages')->orderBy('id','DESC')->get();
	  
	$notice_count_img = Notifications::select('notifications.*')->where('followers_id',$user_id)->where('status',1)->where('notification_type', 'unread_messages')->orderBy('id','DESC')->get();
    
?>

<header class="pd_header bg-white border-bottom fixed-top">
    <div class="head_wrap">
        <div class="head_lft">
            <div class="rmd_logo">
                <a href="{{ env('APP_URL') }}"><img src="{{ asset('dashboard/img/logo.svg') }}" alt="logo"></a>
            </div>
            <div class="head_serach">
                <button class="mbl_search" type="button" data-toggle="collapse" data-target="#headSearch" aria-controls="headSearch" aria-expanded="true" aria-label="Toggle navigation">
                    <span class=""><i class="fa fa-search" aria-hidden="true"></i></span>
                </button>
                <div class="collapse in" id="headSearch">
                    <form class="form-inline search_form" action="{{env('APP_URL')}}/user/all_known_persons" method="GET">
                        <input class="form-control mr-sm-2 search_text" value="@if(!empty($_GET['search'])){{$_GET['search']}}@endif" name="search" type="text" placeholder="Search here..." aria-label="Search">
                    </form>
                </div>
            </div>
        </div>
        <div class="head_mid">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="{{ route('user.dashboard') }}">
                        <span class="menu_icon">
                            <svg id="Home_icon" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20">
                                <rect id="Rectangle_320" data-name="Rectangle 320" width="20" height="20" fill="#999" opacity="0"/>
                                <g id="Group_879" data-name="Group 879" transform="translate(0.557 1.425)">
                                    <path id="Icon_awesome-home" data-name="Icon awesome-home" d="M9.12,6.034l-6,4.94v7.239a.52.52,0,0,0,.52.52l3.645-.009a.52.52,0,0,0,.518-.52V13.182a.52.52,0,0,1,.52-.52h2.082a.52.52,0,0,1,.52.52V18.2a.52.52,0,0,0,.52.522l3.644.01a.52.52,0,0,0,.52-.52V10.97l-6-4.936A.4.4,0,0,0,9.12,6.034Zm9.474,3.357L15.874,7.15V2.644a.39.39,0,0,0-.39-.39H13.662a.39.39,0,0,0-.39.39V5.006l-2.912-2.4a1.561,1.561,0,0,0-1.984,0L.141,9.391a.39.39,0,0,0-.052.55l.83,1.008a.39.39,0,0,0,.55.053L9.12,4.7a.4.4,0,0,1,.5,0L17.269,11a.39.39,0,0,0,.55-.052l.83-1.008a.39.39,0,0,0-.055-.551Z" transform="translate(0.001 -2.254)" fill="#999"/>
                                </g>
                            </svg>
                        </span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">
                        <span class="menu_icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20">
                                <g id="Watch_icon" transform="translate(0.274)">
                                    <rect id="Rectangle_322" data-name="Rectangle 322" width="20" height="20" transform="translate(-0.274)" fill="#fff" opacity="0"/>
                                    <g id="Group_880" data-name="Group 880" transform="translate(1.001 1)">
                                        <path id="Exclusion_1" data-name="Exclusion 1" d="M13.263,18H3.79a.947.947,0,1,1,0-1.895h9.473a.947.947,0,0,1,0,1.895ZM14,15.158H4a4,4,0,0,1-4-4V4A4,4,0,0,1,4,0H14a4,4,0,0,1,4,4v7.159A4,4,0,0,1,14,15.158ZM7.083,4.029a1,1,0,0,0-1,1v4.9a1,1,0,0,0,1,1,.989.989,0,0,0,.455-.111l4.791-2.453a1,1,0,0,0,0-1.78L7.538,4.141A.989.989,0,0,0,7.083,4.029Z" transform="translate(-0.275 0)" fill="#999"/>
                                    </g>
                                </g>
                            </svg>
                        </span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">
                        <span class="menu_icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="20.17" height="20" viewBox="0 0 20.17 20">
                                <g id="CancieargeService_icon" transform="translate(-0.282)">
                                    <rect id="Rectangle_323" data-name="Rectangle 323" width="20" height="20" transform="translate(0.452)" fill="#fff" opacity="0"/>
                                    <path id="Icon_awesome-concierge-bell" data-name="Icon awesome-concierge-bell" d="M10.932,6.276v-.7h.607a.607.607,0,0,0,.607-.607V4.357a.607.607,0,0,0-.607-.607H7.9a.607.607,0,0,0-.607.607v.607a.607.607,0,0,0,.607.607H8.5v.7a8.5,8.5,0,0,0-7.288,8.407H18.221a8.5,8.5,0,0,0-7.288-8.407Zm7.9,9.621H.607A.607.607,0,0,0,0,16.5v1.215a.607.607,0,0,0,.607.607H18.828a.607.607,0,0,0,.607-.607V16.5A.607.607,0,0,0,18.828,15.9Z" transform="translate(0.282 -1.4)" fill="#999"/>
                                </g>
                            </svg>
                        </span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">
                        <span class="menu_icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20">
                                <g id="Telemedi_icon" transform="translate(0.163)">
                                <rect id="Rectangle_324" data-name="Rectangle 324" width="20" height="20" transform="translate(-0.163)" fill="#fff" opacity="0"/>
                                <path id="Icon_awesome-notes-medical" data-name="Icon awesome-notes-medical" d="M13.439,2.389h-3.2A2.481,2.481,0,0,0,7.679,0,2.481,2.481,0,0,0,5.12,2.389H1.92A1.86,1.86,0,0,0,0,4.181v13.14a1.86,1.86,0,0,0,1.92,1.792H13.439a1.86,1.86,0,0,0,1.92-1.792V4.181A1.86,1.86,0,0,0,13.439,2.389Zm-5.76-.9a.927.927,0,0,1,.96.9.962.962,0,0,1-1.92,0A.927.927,0,0,1,7.679,1.493Zm3.84,11.349a.311.311,0,0,1-.32.3H8.959v2.091a.311.311,0,0,1-.32.3H6.72a.311.311,0,0,1-.32-.3V13.14H4.16a.311.311,0,0,1-.32-.3V11.05a.311.311,0,0,1,.32-.3H6.4V8.661a.311.311,0,0,1,.32-.3h1.92a.311.311,0,0,1,.32.3v2.091H11.2a.311.311,0,0,1,.32.3Zm0-7.167a.311.311,0,0,1-.32.3H4.16a.311.311,0,0,1-.32-.3v-.6a.311.311,0,0,1,.32-.3H11.2a.311.311,0,0,1,.32.3Z" transform="translate(2.321 0.443)" fill="#999"/>
                                </g>
                            </svg>
                        </span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">
                        <span class="menu_icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20">
                                <g id="Ask_icon" transform="translate(0.437)">
                                <rect id="Rectangle_325" data-name="Rectangle 325" width="20" height="20" transform="translate(-0.437)" fill="#fff" opacity="0"/>
                                <path id="Icon_awesome-question-circle" data-name="Icon awesome-question-circle" d="M19.573,10.068A9.505,9.505,0,1,1,10.068.562,9.5,9.5,0,0,1,19.573,10.068Zm-9.25-6.362A4.964,4.964,0,0,0,5.856,6.149a.461.461,0,0,0,.1.623L7.29,7.781A.46.46,0,0,0,7.929,7.7c.685-.868,1.154-1.372,2.2-1.372.783,0,1.752.5,1.752,1.263,0,.574-.474.869-1.247,1.3-.9.505-2.095,1.135-2.095,2.708v.153a.46.46,0,0,0,.46.46h2.146a.46.46,0,0,0,.46-.46V11.7c0-1.091,3.188-1.136,3.188-4.088C14.789,5.392,12.483,3.705,10.323,3.705Zm-.255,9.505a1.763,1.763,0,1,0,1.763,1.763A1.765,1.765,0,0,0,10.068,13.211Z" transform="translate(-0.068 -0.068)" fill="#999"/>
                                </g>
                            </svg>
                        </span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">
                        <span class="menu_icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20">
                                <g id="Q_A_icon" data-name="Q&amp;A_icon" transform="translate(-0.289)">
                                    <g id="Group_885" data-name="Group 885">
                                        <rect id="Rectangle_326" data-name="Rectangle 326" width="20" height="20" transform="translate(0.289)" fill="#fff" opacity="0"/>
                                        <path id="Icon_material-chat" data-name="Icon material-chat" d="M19.887,3H4.876A1.874,1.874,0,0,0,3.009,4.876L3,21.764l3.753-3.753H19.887a1.882,1.882,0,0,0,1.876-1.876V4.876A1.882,1.882,0,0,0,19.887,3ZM6.753,9.567H18.011v1.876H6.753Zm7.505,4.691H6.753V12.382h7.505Zm3.753-5.629H6.753V6.753H18.011Z" transform="translate(-2.322 -2.103)" fill="#999"/>
                                    </g>
                                </g>
                            </svg>
                        </span>
						</a>
                </li>
                <li class="nav-item side_menu_mobile">
                    <a class="nav-link" data-toggle="collapse" data-target="#sidebarCollapse" aria-controls="sidebarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="14" viewBox="0 0 20 14">
                          <g id="Icon_feather-menu" data-name="Icon feather-menu" transform="translate(-3.5 -8)">
                            <path id="Path_335" data-name="Path 335" d="M4.5,18h18" transform="translate(0 -3)" fill="none" stroke="#999" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
                            <path id="Path_336" data-name="Path 336" d="M4.5,9h18" fill="none" stroke="#999" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
                            <path id="Path_337" data-name="Path 337" d="M4.5,27h18" transform="translate(0 -6)" fill="none" stroke="#999" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
                          </g>
                        </svg>
                    </a>
                </li>
            </ul>
        </div>
        <div class="head_rgt">
            <div class="pd_head_profile">
                <a class="profile-icon-dropdown" data-toggle="collapse" href="#showprofiledw" role="button" aria-expanded="false" aria-controls="showprofiledw">
                    <div class="pdh_profile_pic">
                        @if(auth()->user()->image)
                            <img id="sortProfileData" src="{{ env('APP_URL')}}/{{auth()->user()->image }}" alt="{{ auth()->user()->name }}"/>
                        @else
                            <img id="sortProfileData" src="{{ asset('dist/images/user_icon.png') }}" alt="{{ auth()->user()->name }}"/>
                        @endif
                    </div>
					<?php 
						$name = (strpos(auth()->user()->name, ' ') !== false) ? explode(' ',auth()->user()->name)[0] : auth()->user()->name;
					?>
                    <div class="pdh_user_name">Hi, <?php echo $name;?> <span class="dropdown-toggle"></span></div>
                </a>
                <div class="collapse drop_collapse" id="showprofiledw">
                    <ul class="m-0 p-0">
                        <li>
                            <a href="{{route('user.profile')}}">
                                <img src="{{ asset('dashboard/img/draw.svg') }}" alt="left-nav-icons"> Edit profile
                            </a>
                        </li>
                        <li>
                            <a href="{{route('user.setting')}}">
                                <img src="{{ asset('dashboard/img/settings.svg') }}" alt="left-nav-icons"> Settings
                            </a>
                        </li>
                        <li>
                            <a href="{{route('logout')}}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                <img src="{{ asset('dashboard/img/logout.svg') }}" alt="left-nav-icons"> Logout
                            </a>
							<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="pd_rgt_menu">
                <ul class="pdrm">
                    <li>
						<a data-toggle="collapse" href="#pdNotify" aria-expanded="false" aria-controls="pdNotify">
						
							<span class="notice_count_su notice_text @if(count($notice_count_sp)>0) badge-danger @endif ">
								<?php if(count($notice_count_sp)>0){ echo count($notice_count_sp); }?> </span>
						
                            <svg xmlns="http://www.w3.org/2000/svg" width="23.571" height="15" viewBox="0 0 23.571 15">
                                <path id="users" d="M17.571,13.929a3.214,3.214,0,1,0-3.214-3.214A3.2,3.2,0,0,0,17.571,13.929ZM9,13.929a3.214,3.214,0,1,0-3.214-3.214A3.2,3.2,0,0,0,9,13.929Zm0,2.143c-2.5,0-7.5,1.254-7.5,3.75V22.5h15V19.821C16.5,17.325,11.5,16.071,9,16.071Zm8.571,0c-.311,0-.664.021-1.039.054a4.521,4.521,0,0,1,2.111,3.7V22.5h6.429V19.821C25.071,17.325,20.068,16.071,17.571,16.071Z" transform="translate(-1.5 -7.5)" fill="#2c99cb"/>
                            </svg>
                        </a>
						<div class="collapse drop_collapse" id="pdNotify">
                            <ul class="notify_drop">
                                <div class="notify-drop-title">
                                    <h6 class="mb-0 notice_count_sp" data-count="{{ count($notice_count_sp) }}">Notifications (<span class="notif-count-sp"> {{ count($notice_count_sp) }} </span>)</h6>
                                </div>
                                <div class="drop-content">
                                    <div class="list-group notif_main_row_sp">
                                        
									@if(count($notifications_sp)>0 && !empty($notifications_sp))
										
									    @foreach($notifications_sp as $key=>$value)
											<a href="javascript:void(0)" class="<?php if($value->status ==1){ echo 'dot pusher_notification_row ';} ?> list-group-item list-group-item-action" data-id="{{$value->id}}" data-viewid="{{$value->id}}" data-viewtype="{{$value->notice_type}}" @if($value->status ==1) onclick="javascript: pusher_notification_row({{$value->id}},'{{$value->notice_type}}', this)" @endif>
												<div class="d-flex w-100 justify-content-start align-items-start">
													<div class="noti_person_img"><img src="{{ asset($value->created_image) }}" alt=""></div>
													<div>
														@if($value->notification_type=='supporter_requests')
															<p class="mb-1"> <b>{{$value->created_name}}</b>
															@if($value->notice_type=='supporter_accept')
																accepted your request
															@endif
															
															@if($value->notice_type=='supporter_request')
																has been sent a friend request
															@endif
															
														</p>
														@endif
														
														<small class="badge badge-info">{{ date(env('DATE_FORMAT_PHP_H'), strtotime($value->created_at)) }}</small>
													</div>
												</div>
											</a>
										@endforeach
									@endif
                                    </div>
                                </div>
                            </ul>
                        </div>
                    </li>
                    <li>
                        <a data-toggle="collapse" href="#pdNotify_img" aria-expanded="false" aria-controls="pdNotify_img">
						
							<span class="notice_count_spp notice_text @if(count($notice_count_img)>0) badge-danger @endif ">
								<?php if(count($notice_count_img)>0){ echo count($notice_count_img); }?> </span>
                            <svg xmlns="http://www.w3.org/2000/svg" width="15.141" height="15.141" viewBox="0 0 15.141 15.141">
                                <g id="chat" transform="translate(-3.375 -3.375)">
                                <path id="Path_450" data-name="Path 450" d="M6.461,18.82A1.9,1.9,0,0,1,4.831,17.19V11.25H4.481a1.111,1.111,0,0,0-1.106,1.106v9.958L5.5,20.2h7.829a1.124,1.124,0,0,0,1.106-1.125V18.82Z" transform="translate(0 -3.799)" fill="#2c99cb"/>
                                <path id="Path_451" data-name="Path 451" d="M19.518,3.375H9.356a1.274,1.274,0,0,0-1.27,1.27v8.229a1.277,1.277,0,0,0,1.27,1.274h8.669l2.762,1.929V4.645A1.274,1.274,0,0,0,19.518,3.375Z" transform="translate(-2.272)" fill="#2c99cb"/>
                                </g>
                            </svg>
                        </a>
						<div class="collapse drop_collapse" id="pdNotify_img">
						<ul class="notify_drop">
							<div class="notify-drop-title">
								<h6 class="mb-0 notice_count" data-count="{{ count($notice_count_img) }}">Notifications (<span class="notif-count"> {{ count($notice_count_img) }} </span>)</h6>
							</div>
							<div class="drop-content">
								<div class="list-group notif_main_row">
									
								@if(count($message_notice)>0 && !empty($message_notice))
									
									@foreach($message_notice as $key=>$value)
										<a href="javascript:void(0);" class="<?php if($value->status ==1){ echo 'dot pusher_notification_row ';} ?> list-group-item list-group-item-action" data-id="{{$value->id}}" data-viewid="{{$value->id}}" data-viewtype="{{$value->notification_type}}" @if($value->status ==1) onclick="javascript: pusher_notification_row({{$value->id}},'{{$value->notification_type}}', this)" @endif>
											<div class="d-flex w-100 justify-content-start align-items-start">
												<div class="noti_person_img"><img src="{{ asset($value->created_image) }}" alt=""></div>
												<div>
													@if($value->notification_type=='unread_messages')
														<p class="mb-1"> <b>{{$value->created_name}}</b> {{ $value->notice_type }} message unread</p>
													@endif
													
													
													<small class="badge badge-info">{{ date(env('DATE_FORMAT_PHP_H'), strtotime($value->created_at)) }}</small>
												</div>
											</div>
										</a>
									@endforeach
								@endif
								</div>
							</div>
						</ul>
					</div>
                    </li>
                    <li>
                        <a data-toggle="collapse" href="#pdNotify_n" role="button" aria-expanded="false" aria-controls="pdNotify_n">
						
							<span class="notif-count n_t notice_text @if(count($notice_count)>0) badge-danger @endif"> <?php if(count($notice_count)>0){ echo count($notice_count); }?> </span>
						
                            <svg xmlns="http://www.w3.org/2000/svg" width="16.002" height="18.224" viewBox="0 0 16.002 18.224">
                                <g id="notification" transform="translate(-8259.998 -10998)">
                                <g id="Icon_feather-bell" data-name="Icon feather-bell" transform="translate(8260.998 11000.419)">
                                    <path id="Path_447" data-name="Path 447" d="M15.606,7.442a4.442,4.442,0,1,0-8.885,0c0,5.183-2.221,6.664-2.221,6.664H17.827s-2.221-1.481-2.221-6.664" transform="translate(-4.5 -3)" fill="#2c99cb" stroke="#2c99cb" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
                                    <path id="Path_448" data-name="Path 448" d="M17.967,31.5a1.481,1.481,0,0,1-2.562,0" transform="translate(-10.022 -17.433)" fill="#2c99cb" stroke="#2c99cb" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
                                </g>
                                </g>
                            </svg>
                        </a>
                        <div class="collapse drop_collapse" id="pdNotify_n">
                            <ul class="notify_drop">
                                <div class="notify-drop-title">
                                    <h6 class="mb-0 notice_count" data-count="{{ count($notice_count) }}">Notifications (<span class="notif-count"> {{ count($notice_count) }} </span>)</h6>
                                </div>
                                <div class="drop-content">
                                    <div class="list-group notif_main_row">
                                        
									@if(count($notifications)>0 && !empty($notifications))
										
									    @foreach($notifications as $key=>$value)
											<a href="@if($value->notification_type=='visit_all_status' || $value->notification_type=='add_appointment' || $value->notification_type=='decline_appointment' || $value->notification_type=='visit_all_status') {{ URL('user/treatments') }} @else #post_wrap_{{$value->post_id}} @endif" class="<?php if($value->status ==1){ echo 'dot pusher_notification_row ';} ?> list-group-item list-group-item-action" data-id="{{$value->id}}" data-viewid="{{$value->id}}" data-viewtype="{{$value->notification_type}}" @if($value->status ==1) onclick="javascript: pusher_notification_row({{$value->id}},'{{$value->notification_type}}', this)" @endif onclick="redirect_post('post_wrap_{{$value->post_id}}')">
												<div class="d-flex w-100 justify-content-start align-items-start">
													<div class="noti_person_img"><img src="{{ asset($value->created_image) }}" alt=""></div>
													<div>
														@if($value->notification_type=='post_comment')
															<p class="mb-1"> <b>{{$value->created_name}}</b> commented on your post</p>
														@endif
														
														@if($value->notification_type=='post_create')
															<p class="mb-1"> <b>{{$value->created_name}}</b> has created a new post</p>
														@endif
														
														@if($value->notification_type=='post_like')
															<p class="mb-1"> <b>{{$value->created_name}}</b> likes your post</p>
														@endif
														
														@if($value->notification_type=='decline_appointment')
															<p class="mb-1"> <b>{{$value->created_name}}</b> declined your appointment</p>
														@endif
														
														@if($value->notification_type=='accept_appointment')
															<p class="mb-1"> <b>{{$value->created_name}}</b> accepted your appointment</p>
														@endif
														
														@if($value->notification_type=='visit_all_status')
															<p class="mb-1"> <b>{{$value->created_name}}</b>
															@if($value->notice_type=='accept')
																accepted your appointment
															@endif
															
															@if($value->notice_type=='review')
																reviewed your appointment
															@endif
															
															@if($value->notice_type=='complete')
																completed your appointment
															@endif
															
															@if($value->notice_type=='pending')
																pending your appointment
															@endif
														</p>
														@endif
														
														<small class="badge badge-info">{{ date(env('DATE_FORMAT_PHP_H'), strtotime($value->created_at)) }}</small>
													</div>
												</div>
											</a>
										@endforeach
									@endif
                                    </div>
                                </div>
								<?php 
								//if(count($notifications)>=5){ ?>
                                <!-- <div class="notify-drop-footer text-center">
                                    <a href="">Show all of them</a>
                                </div> -->
								<?php //} ?>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</header>
<!-- Header Ends -->

