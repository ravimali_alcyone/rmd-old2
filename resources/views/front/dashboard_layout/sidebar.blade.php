<?php
use App\SocialUserFollowConnection;
use App\User;

$followers = SocialUserFollowConnection::where('user_id', auth()->user()->id)->first();
if(!empty($followers)){
	$followers_ids = explode(',', $followers['follower_id']);
	$followers_count = count($followers_ids);
}else{
	$followers_count=0;
}

#Latest user detail
$users = User::where('status', 1)->where('is_admin',0)->orderBy('id','DESC')->limit(16)->get();

?>
<div class="left_side_menu collapse in"  id="sidebarCollapse">
    <div class="left-nav pt-0">
        <div class="profile_sec">
            <div class="texture">
                <!-- <div class="profile_pic"><img src="{{ asset('dashboard/img/profile-pic.png') }}" alt="profile-pic" class="mw-100">
				</div>-->
				<div class="profile_pic">
					<img id="left_side_profile" src="@if(!empty(auth()->user()->image)) {{ asset('/').auth()->user()->image }} @else {{ asset('/dist/images/user_icon.png')}} @endif" alt="profile-pic" class="mw-100">
				</div>
            </div>
			
            <div class="options">
                <div class="followers text-center">
                    <p>{{ $followers_count }}</p>
                    <span>Supporter’s</span>
                </div>
                <div><a href="{{ url('user/profile') }}"><img src="{{ asset('dashboard/img/edit-icon.svg') }}" alt="edit-icon"></a></div>
            </div>
            <div class="info_box text-center">
                <p class="user_name name mb-0">{{ auth()->user()->name }}</p>
                <p class="designation mb-0"><?php echo implode(' ', array_slice(explode(' ', auth()->user()->clinical_expertise), 0, 5)); ?></p>
            </div>
        </div>

        <ul class="p-0 m-0">
            <li>
                <a href="{{ url('user/dashboard') }}" class="user_name {{ isset($page) && $page == 'user_dashboard' ? 'active' : '' }} ml-0">
                    <img src="{{ asset('dashboard/img/home-2.svg') }}" alt="left-nav-icons" /> Dashboard
                </a>
            </li>
        </ul>
        <h4>Profile</h4>
        <ul class="p-0 m-0">
            <li>
                <a href="{{ url('user/profile') }}" class="{{ isset($page) && $page == 'user_profile' ? 'active' : '' }}">
                    <img src="{{ asset('dashboard/img/user2.svg') }}" alt="left-nav-icons" /> My Profile
                </a>
            </li>
            <li>
                <a href="{{ url('user/treatments') }}" class="{{ isset($page) && $page == 'user_treatments' ? 'active' : '' }}">
                    <img src="{{ asset('dashboard/img/bag.svg') }}" alt="left-nav-icons" /> My Treatments
                </a>
            </li>
            <li>
                <a href="{{ url('user/subscriptions') }}" class="{{ isset($page) && $page == 'user_subscriptions' ? 'active' : '' }}">
                    <img src="{{ asset('dashboard/img/send.svg') }}" alt="left-nav-icons" /> My Subscriptions
                </a>
            </li>
            <li>
                <a href="{{ url('user/appointments') }}" class="{{ isset($page) && $page == 'user_appointments' ? 'active' : '' }}">
                    <img src="{{ asset('dashboard/img/calender.svg') }}" alt="left-nav-icons" /> Appointments
                </a>
            </li>
            <li>
                <a href="#">
                    <img src="{{ asset('dashboard/img/chat2.svg') }}" alt="left-nav-icons" /> Messages <span class="notification-no">2</span>
                </a>
            </li>
        </ul>
        <h4>Community</h4>
        <ul class="p-0 m-0">
            <li>
                <a href="{{ url('user/supporters') }}" class="{{ isset($page) && $page == 'supporters' ? 'active' : '' }}">
                    <img src="{{ asset('dashboard/img/user2.svg') }}" alt="left-nav-icons" /> Supporters
                </a>
            </li>
            <li>
                <a href="#">
                    <img src="{{ asset('dashboard/img/user2.svg') }}" alt="left-nav-icons" /> Groups
                </a>
            </li>
            <li>
                <a href="{{ url('user/forums') }}">
                    <img src="{{ asset('dashboard/img/bag.svg') }}" alt="left-nav-icons" /> Forum
                </a>
            </li>
            <li>
                <a href="{{ url('user/blogs') }}" class="{{ isset($page) && $page == 'blogs' ? 'active' : '' }}">
                    <img src="{{ asset('dashboard/img/chat2.svg') }}" alt="left-nav-icons" /> Blogs
                </a>
            </li>
            <li>
                <a href="#">
                    <img src="{{ asset('dashboard/img/chat2.svg') }}" alt="left-nav-icons" /> Recommandations
                </a>
            </li>
            <li>
                <a href="#">
                    <img src="{{ asset('dashboard/img/doctor.svg') }}" alt="left-nav-icons" /> Challenges
                </a>
            </li>
        </ul>
        <h4>Professional Services</h4>
        <ul class="p-0 m-0">
            <li>
                <a href="#">
                    <img src="{{ asset('dashboard/img/ask.svg') }}" alt="left-nav-icons" /> Professional Q & A
                </a>
            </li>
            <li>
                <a href="#">
                    <img src="{{ asset('dashboard/img/bag.svg') }}" alt="left-nav-icons" /> Telemedicine Services
                </a>
            </li>
            <li>
                <a href="#">
                    <img src="{{ asset('dashboard/img/doctor.svg') }}" alt="left-nav-icons" /> Concierge Services
                </a>
            </li>
        </ul>
    </div>
    <div style="margin-top:5px;"></div>
    <div class="left-nav pt-0">
        <h4>Latest Members</h4>
        <div class="widget-signups__list lastest_mem">
            @isset($users)
                @foreach($users as $val)
                    <?php $cls = ($val->is_available==1) ? "online" : "offline"; ?>
                    <a href="{{ url('user/view') }}/{{$val->id }}" class="{{ $cls }}" ><img class="avatar-img" src="/{{$val->image}}" alt="{{$val->name}}" title="{{$val->name}}"></a>
                @endforeach
            @endif        
        </div>
    </div>
</div>