<!DOCTYPE HTML>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	@if(isset($meta_description))<meta name="description" content="{{$meta_description}}">@endif
    <title>{{ config('app.name') }} @if(isset($meta_title)) - {{$meta_title}} @endif</title>
	<meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{asset('assets/images/favicon.png')}}" rel="shortcut icon" type="image/x-icon">
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" type="text/css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Lato:wght@400;700&display=swap" rel="stylesheet">
    <link href="{{asset('assets/css/main.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/owl.carousel.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/responsive.css')}}" rel="stylesheet" />
	<link href="{{asset('assets/css/blog.css')}}" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="{{asset('plugins/sweetalert/sweetalert.css')}}" />
	<link href="{{asset('assets/css/select2.min.css')}}" rel="stylesheet" />
	<link href="{{asset('assets/css/jquery-toaster.css')}}" rel="stylesheet" />
	
	<script src="{{asset('assets/js/jquery.min.js')}}"></script>
	<script src="{{asset('assets/js/custom.js')}}"></script>
	<script src="{{asset('plugins/sweetalert/sweetalert.min.js')}}"></script>
	<script src="{{asset('assets/js/select2.min.js')}}"></script>	
	<script src="{{asset('assets/js/jquery-toaster.min.js')}}"></script>
	<style>
		.dash_top_process {   position: fixed;    top: 0;    width: 100%;    z-index: 1050;    left: 0;    right: 0;}
		.dash_top_process .progress{height: 5px;border-radius:0px;overflow: visible;}
		.dash_top_process .progress-bar{background-color: #ff6c66;box-shadow: 0 3px 6px rgba(255, 108, 102, 0.3);overflow: visible;}
	</style>
	@stack('header_scripts')
</head>

<body id="page-top">
	<!-- progress bar -->
	<div class="container-fluid">
		<div class="row">
			<div class="col">
				<div class="dash_top_process" id="pbar" style="display: none;">
					<div class="progress">
					  <div class="progress-bar" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
    <header>
        <!-- Navigation -->
        <div id="mainNav" class="<?php if($page == 'home'){echo 'fixed-top';}else{echo 'head_bg';}?>">
            <nav class="navbar navbar-expand-lg ">
                <div class="container">
                    <a class="navbar-brand js-scroll-trigger" href="{{env('APP_URL')}}#page-top"><img src="{{asset('assets/images/rmd_logo_red.svg')}}" class="logo logo_red" alt="ReplenishMD"/><img src="{{asset('assets/images/rmd_logo_white.svg')}}" class="logo logo_white" alt="ReplenishMD"/></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                      	<span class="fa fa-bars"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarResponsive">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item">
                                <a class="nav-link js-scroll-trigger {{ isset($page) && $page == 'plans' ? 'active' : '' }}" href="{{env('APP_URL')}}/plans">Plans</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link js-scroll-trigger {{ isset($page) && $page == 'how_it_works' ? 'active' : '' }}" href="{{env('APP_URL')}}/how_it_works">How it works</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link js-scroll-trigger {{ isset($page) && $page == 'about' ? 'active' : '' }}" href="{{env('APP_URL')}}/about">About us</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link js-scroll-trigger {{ isset($page) && $page == 'blogs' ? 'active' : '' }}" href="{{env('APP_URL')}}/blogs">Good Stuff</a>
                            </li>
						@if(@auth()->user()->user_role != 3)
                            <li class="nav-item">
                                <a class="nav-link js-scroll-trigger" href="{{ route('provider_signup') }}">Become a Provider</a>
                            </li>
						@endif
							
						@if(auth()->user())
                            <li class="nav-item">
							<?php
								if((auth()->user()->user_role)==1){
									?>
									<a class="js-scroll-trigger blue_btn" href="{{route('admin.home')}}">Dashboard</a>
								<?php
								}else if((auth()->user()->user_role)==2){
									?>
									<a class="js-scroll-trigger blue_btn" href="{{route('admin.home')}}">Dashboard</a>
								<?php
								}else if((auth()->user()->user_role)==3){
									?>
									<a class="js-scroll-trigger blue_btn" href="{{route('provider.dashboard')}}">Dashboard</a>
								<?php
								}else if((auth()->user()->user_role)==4){
									?>
									<a class="js-scroll-trigger blue_btn" href="{{route('user.dashboard')}}">Dashboard</a>
									<?php	
								}
								?>
                                
                            </li>								
                            <li class="nav-item">
                                <a class="js-scroll-trigger btn-primary" href="{{route('logout')}}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a>
								<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
                            </li>
													
						@else		
								
                            <li class="nav-item">
                                <a class="js-scroll-trigger blue_btn" href="{{ route('sign_in') }}">Sign In</a>
                            </li>
                            <li class="nav-item">
                                <a class="js-scroll-trigger btn-primary" href="{{ route('joinnow') }}">Join Now</a>
                            </li>		
							
						@endif
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    </header>