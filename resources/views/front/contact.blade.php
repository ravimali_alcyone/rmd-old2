@extends('front.layouts.app')

@section('content')

    <section class="inner_banner contact_banner">
        <div class="container">
            <div class="content_wrapper">
                <div class="b_text">
                    <h1>Contact Us</h1>
                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et.</p>
                </div>
            </div>
        </div>
        <img src="/assets/images/bottom_curve.svg" alt="bottom_curve">
    </section>

    <section class="contact_head_sec">
        <div class="container">
            <div class="title">Want to learn more about ReplenishMD?
                <p>Schedule a call with one of our membership advisors.</p>
            </div>
            <button type="button" class="btn btn-primary">Schedule a call</button>
        </div>
    </section>

    <section class="contact_info_sec">
        <div class="contact_info shadow">
            <div class="c_form">
                <div class="heading">
                    <span class="fas far fa-envelope"></span>
                    <h1 class="title mb-0">Send us a message</h1>
                </div>
                <form id="ContactForm">
                    <div class="input_wrapper">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Your Name" id="name" name="name">
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" placeholder="Email Address" id="email" name="email">
                        </div>
                    </div>
                    <div class="input_wrapper">
                        <div class="form-group">
                            <input type="number" class="form-control" placeholder="Phone" id="phone" name="phone">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Subject" id="subject" name="subject">
                        </div>
                    </div>
                    <textarea name="message" id="message" class="form-control" cols="30" rows="10" placeholder="Message"></textarea>
                    <button type="submit" class="btn btn-primary border-0">Send a  Message</button>
                </form>
            </div>
            <div class="c_info">
                <h1 class="title ml-0 text-white text-left">Contact Information</h1>

                <h6 class="title_sm title_sm_1">Email Id:</h6>
                <p><a href="mailto:team.sales@rmd.com" class="text-white">team.sales@rmd.com</a></p>

                <h6 class="title_sm">Phone:</h6>
                <p>Monday - Saturday 10am-10pm <a href="tel:600-00-00800" class="text-white">600-00-00800</a></p>

                <h6 class="title_sm">Location:</h6>
                <p>3820 N, 8th Street, <br>Newyork, AZ 85012</p>

                <div class="social_icon">
                    <a href="javascript:;"><span class="fab fa-facebook-f ml-0"></span></a>
                    <a href="javascript:;"><span class="fab fa-twitter"></span></a>
                    <a href="javascript:;"><span class="fab fa-instagram"></span></a>
                    <a href="javascript:;"><span class="fab fa-youtube"></span></a>
                </div>
            </div>
        </div>
        <div class="map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3023.993197586167!2d-73.95895228428746!3d40.71816624517459!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c2595db44ef3fd%3A0x538fa68da7be9842!2sN%208th%20St%2C%20Brooklyn%2C%20NY%2C%20USA!5e0!3m2!1sen!2sin!4v1599112984218!5m2!1sen!2sin"
                width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
            <!-- <img src="/assets/images/contact_map.png" alt="contact_map"> -->
        </div>
    </section>

    <section class="member_support">
        <div class="container">
            <div class="content_wrapper">
                <div class="title">Member Support
                    <p>Lorem ipsum dolor sit amet, consetetur</p>
                </div>
                <div class="boxes_wrapper d_flex_j_center">
                    <div class="box">
                        <span class="fas fa-envelope-open-text icon"></span>
                        <h6 class="title">Transparent pricing</h6>
                        <p>Lorem ipsum dolor sit amet, consetetur sadiping elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam.</p>
                        <a href="javascript:;" class="link">Message us</a>
                    </div>
                    <div class="box">
                        <span class="icon"><img src="/assets/images/phone_call_icon.png" alt="phone_call_icon"></span>
                        <h6 class="title">Urgent contact</h6>
                        <p>Lorem ipsum dolor sit amet, consetetur sadiping elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam.</p>
                        <a href="javascript:;" class="link">Say Hi RMD!</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

<script>
	$("#ContactForm").submit(function(e) {
		e.preventDefault();
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			data: $('#ContactForm').serialize(),
			url: "{{ route('front.contactSubmit') }}",
			type: "POST",
			// dataType: 'json',
			success: function (response) {
				//response = JSON.parse(res);
				if(response['status'] == 'success'){
					swal({
						title: response['message'],
						icon: 'success'
					});
					$('#ContactForm').trigger("reset");
					setTimeout(function(){
						swal.close();
					}, 1000);					
				}
			},
			error: function (data) {
				let errors = data.responseJSON.errors;
				
				var errMsg = 'Error Occured';
				var x = 0;
				
				$.each(errors, function(key, value) {
					if(x == 0){
						errMsg = value[0];
					}
					x++;
				}); 
				
				swal({
					title: errMsg,
					icon: 'error'
				});				
				setTimeout(function(){
					swal.close();
				}, 1000);					

			}
		});
	});
</script>


@endsection