@php
	use App\Library\Services\CommonService;
	$common = new CommonService();
@endphp

@extends('front.layouts.app')

@section('content')
@push('header_scripts')
 <link href="{{asset('assets/css/forum.css')}}" rel="stylesheet" type="text/css" />
@endpush
<style>
    .chat_pop{width:100%;max-width:600px;z-index:2;position:fixed;background:#f1f8f9;color:#000;right:-100%;bottom:0;height:calc(100vh - 80px);box-shadow:0 10px 30px 0 rgba(0,0,0,.16);z-index: 10;}
.cp_close{width:40px;height:48px;font-size:14px;position:absolute;left:0;top:0;display:flex;justify-content:center;align-items:center;cursor:pointer;border-right:1px solid #dedede}
.edit_pop_title{background-color: #ff6c66;height: 48px; padding-left: 40px;display:flex;justify-content:center;align-items:center;}
.edit_pop_title .cp_close{border-right:1px solid rgba(255, 255, 255,0.1); color: white;}
.edit_pop_title h3{ margin-bottom: 0; font-size: 18px; font-weight: 600; color: white;}
.edit_pop{ background-color: #f3f7fa;}
.edit_pop .form-group .form-control{ border-color: transparent;}
.chat_block{ height: calc(100vh - 130px);overflow-y: scroll;}
iframe.chat_frame {
    height: calc(100vh - 80px);
    width: 100%;
    border: none;
}
.chat_pop {
    z-index: 1031;
	top:80px;
}
</style>
   <section class="forum_page_name">
        <h1>Forum</h1>
    </section>
    <div class="forum_wrap">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!--<ul class="nav forum_tabs mb-3" id="pills-tab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <a class="nav-link active" id="pills-category-tab" data-toggle="pill" href="#pills-category" role="tab" aria-controls="pills-category" aria-selected="true">Categories</a>
                        </li>
                        <li class="nav-item" role="presentation">
                            <a class="nav-link" id="pills-latest-forum-tab" data-toggle="pill" href="#pills-latest-forum" role="tab" aria-controls="pills-latest-forum" aria-selected="false">Latest</a>
                        </li>
                        <li class="nav-item" role="presentation">
                            <a class="nav-link" id="pills-popular-tab" data-toggle="pill" href="#pills-popular-forum" role="tab" aria-controls="pills-popular" aria-selected="false">Popular</a>
                        </li>
                        <li class="nav-item ml-auto" role="presentation">	
                            <a href="{{ route('joinnow') }}" class="btn-primary">Create Your Own Forum</a>	
                        </li>
                    </ul>-->
                    <div class="tab-content pb-4" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-category" role="tabpanel" aria-labelledby="pills-category-tab">
                            <div class="row forum_cat_list">
                            @if($categories && $categories->count() > 0)
                                @foreach($categories as $key => $value)
                                    <div class="col-md-4">
                                        <div class="forum_cat_box" style="">
                                            <div class="num_topics post_topics">{{ $value->topic_count }}</div>
                                            <div class="forum_cat_name">
                                                <h5>{{ $value->forum_subject }}</h5>
                                                <small>{{ substr($value->short_info, 0, 40).'...' }}</small>
                                                @if($value->is_paid == 1)
                                                    <a href="<?php echo route('forum.posts',[$value->id,0]); ?>" class="link d-block"><small>Get started</small></a>
                                                @else
                                                    <a href="<?php echo route('forum.posts',[$value->id,1]); ?>" class="link d-block"><small>Get started</small></a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                            </div>
                        </div>					
                    </div>
                </div>
            </div>
        </div>
        
        @if($paywall_forum_id !="")
            <div class="chat_pop edit_pop petient chat_pop_pswrd">
                <div class="edit_pop_title">
                    <h3>Paywall Payment</h3>
                    <a class="cp_close"><i class="fa fa-times"></i></a>
                </div>
                <div class="chat_block">
                    {!! $paywall !!}	
                </div>
            </div>
            <script>
                setTimeout(function(){
                    chat_box('pswrd');	
                }, 1000);
            </script>
        @endif 

    </div>

    
    
    <script type="text/javascript">
	
	$(document).ready(function(){
		
        $('.cp_close').click(function(flag){
			var hidden = $('.chat_pop');
			hidden.animate({"right":"-100%"}, "slow").removeClass('visible');
		});
	});
    
    function chat_box(flag)
    {  
        var hidden = $('.chat_pop_'+flag);
        if (hidden.hasClass('visible')){
            hidden.animate({"right":"-100%"}, "slow").removeClass('visible');
        } else {
            hidden.animate({"right":"0"}, "slow").addClass('visible');
        }
    }
	
    </script>


@endsection