<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	@if(isset($meta_description))<meta name="description" content="{{$meta_description}}">@endif
    <title>{{ config('app.name') }} @if(isset($meta_title)) - {{$meta_title}} @endif</title>
    <link href="{{asset('assets/images/favicon.png')}}" rel="shortcut icon" type="image/x-icon">
	<meta name="csrf-token" content="{{ csrf_token() }}">	
    <!-- Google Font -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Lato:wght@400;700&display=swap" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- Bootstrap CSS -->
	<link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="{{asset('dashboard/css/style.css')}}">
	<link href="{{asset('assets/css/owl.carousel.css')}}" rel="stylesheet" />
	@yield('stylesheet')
	
    <!-- Main Dashbord Content Ends-->
	<script src="{{asset('assets/js/jquery.min.js')}}"></script>	
    <script src="{{asset('assets/js/bootstrap.bundle.min.js')}}"></script>
		<!-- Blog listing page -->
		<script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
		<link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css" />		
	
	<!-- Sweet Alert -->
    <link rel="stylesheet" href="{{asset('plugins/sweetalert/sweetalert.css')}}" />
	<script src="{{asset('plugins/sweetalert/sweetalert.min.js')}}"></script>
		
	<!-- Select2 -->
	<link href="{{ asset('assets/css/select2.min.css') }}" rel="stylesheet" />
	<script src="{{ asset('assets/js/select2.min.js') }}"></script>
	
	<!-- Full Calendar -->
    <script src="{{ asset('dashboard/plugins/fullcalendar/moment.min.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('dashboard/plugins/fullcalendar/fullcalendar.css') }}" />	
    <script src="{{ asset('dashboard/plugins/fullcalendar/fullcalendar.js') }}"></script>	
	<script src="{{ asset('dashboard/plugins/fullcalendar/popper.min.js') }}"></script>

	<!-- CKEditor -->
	<script src="{{ asset('plugins/ckeditor/ckeditor.js') }}"></script>
	
	<!-- Timepicker -->
	<link href="{{ asset('dashboard/css/jquery.timepicker.min.css') }}" rel="stylesheet" />
	<script src="{{ asset('dashboard/js/jquery.timepicker.min.js') }}"></script>

	<!--Datetimepicker-->
	<link href="{{ asset('assets/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" />
	<script src="{{ asset('assets/js/bootstrap-datetimepicker.min.js') }}"></script>	
	
	<!-- Toaster css -->
    <link href="{{asset('assets/css/jquery-toaster.css')}}" rel="stylesheet" />
	<script src="{{asset('assets/js/jquery-toaster.min.js')}}"></script>
	<script src="{{asset('assets/js/owl.carousel.js')}}" type="text/javascript"></script>	
	<script src="{{asset('assets/js/custom.js')}}"></script>		
    <style>
        .pdrm li>a {
            width: 40px;
            height: 40px;
            border-radius: 50%;
            color: #2c99cb;
            display: flex;
            position: relative;
            justify-content: center;
            align-items: center;
            background-color: rgba(44,153,203,.16);
            transition: all ease-in-out .3s;
        }
        div#pdNotify,
		div#pdNotifySu{
            box-shadow: 0 10px 20px rgb(0 0 0 / 16%);
            position: absolute;
            font-size: 12px;
            line-height: 20px;
            width: 300px;
            background: #ffff;
            border: solid 1px #f5f5f5;
            padding: 0;
            border-radius: 4px;
            top: 55px;
            z-index: 99;
            right: 0;
        }
        .notify_drop {
            margin: 0;
            padding: 0;
        }
        .notify_drop .notify-drop-title {
            border-bottom: 1px solid #e2e2e2;
            padding: 5px 15px 10px 15px;
        }
        .notify_drop .drop-content {
            min-height: 280px;
            max-height: 280px;
            overflow-y: scroll;
        }
        .notify_drop .list-group {
            border-radius: 0;
        }
        .noti_person_img {
            margin-right: 15px;
        }
        .noti_person_img img {
            width: 40px;
            height: 40px;
            border-radius: 50%;
            object-position: center;
            object-fit: cover;
        }
        .notify_drop .list-group-item {
            border-left: 0;
            border-right: 0;
            border-top: 0;
        }
        
        .notice_text{
			position: absolute;
			top: -1px;
			border-radius: 50%;
			font-size: 10px;
			height: 15px;
			width: 15px;
			text-align: center;
			padding-top: 0;
			display: flex;
			justify-content: center;
			align-items: center;
			left: 10px;
        }
        .notif_main_row a.dot::before {
            width: 6px;
            height: 6px;
            position: absolute;
            top: 8px;
            right: 8px;
            background-color: #ff6c66;
            content: '';
            border-radius: 50%;
            box-shadow: 0 2px 6px rgb(0 0 0 / 30%);
        }
		div#pdNotify_n{box-shadow:0 10px 20px rgba(0,0,0,.16);position:absolute;font-size:12px;line-height:20px;width:300px;background:#ffff;border:solid 1px #f5f5f5;padding:0;border-radius:4px;top:55px;z-index:99;right:0}
		.dash_top_process {   position: fixed;    top: 0;    width: 100%;    z-index: 1050;    left: 0;    right: 0;}
        .dash_top_process .progress{height: 5px;border-radius:0px;overflow: visible;}
        .dash_top_process .progress-bar{background-color: #ff6c66;box-shadow: 0 3px 6px rgba(255, 108, 102, 0.3);overflow: visible;}
		
		.dot::before {
			width: 6px;
			height: 6px;
			position: absolute;
			top: 8px;
			right: 8px;
			background-color: #ff6c66;
			content: '';
			border-radius: 50%;
			box-shadow: 0 2px 6px rgb(0 0 0 / 30%);
		}
    </style>
	
@stack('header_scripts')

<script>
// search box 
	$(".search_form").submit(function(e){
		event.preventDefault();
		var search_text =$('.search_text').val();
		if(search_text!=''){
			location.href = "{{ url('provider/all_known_persons')}}?search="+search_text;
		}
	});
</script>
	
  </head>
  <body class="pb-70">
    <!-- Loader -->
    <div class="loader" style="display:none;">
        <img src="{{asset('assets/images/loading.gif')}}" alt="loading">
    </div>    
    <!-- Navbar -->
    
    <?php 
	use App\Notifications;
	use App\Library\Services\CommonService;
	$common = new CommonService();
	
	$user_id = auth()->user()->id;
	
	$notifications = Notifications::select('notifications.*')->where('followers_id',$user_id)->whereIn('notification_type', ['visit_all_status','add_appointment'])->orderBy('id','DESC')->get();
	  
	$notice_count = Notifications::select('notifications.*')->where('followers_id',$user_id)->whereIn('notification_type', ['visit_all_status','add_appointment'])->orderBy('id','DESC')->get();
	
	$message_notice = Notifications::select('notifications.*')->where('followers_id',$user_id)->where('notification_type', 'unread_messages')->orderBy('id','DESC')->get();
	  
	$notice_count_img = Notifications::select('notifications.*')->where('followers_id',$user_id)->where('status',1)->where('notification_type', 'unread_messages')->orderBy('id','DESC')->get();
	
	$notifications_sp = Notifications::select('notifications.*')->where('notification_type', 'supporter_requests')->where('followers_id',$user_id)->orderBy('id','DESC')->get();
	
	$notice_count_sp = Notifications::select('notifications.*')->where('notification_type','supporter_requests')->where('followers_id',$user_id)->where('status',1)->orderBy('id','DESC')->get();
	 
    
    ?>
	<section class="main-dashboard">
		<div class="container-fluid">
			<div class="row">
				<div class="col">
					<div class="dash_top_process" id="pbar" style="display: none;">
						<div class="progress">
						  <div class="progress-bar" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
    <nav class="navbar navbar-expand-lg custom-navbar fixed-top">
        <div class="container-fluid">
          <a class="navbar-brand" href="{{env('APP_URL')}}"><img src="{{asset('dashboard/img/logo.svg')}}" alt="logo"></a>
          <div class="show-user-mobile">
              <a class="profile-icon-dropdown" data-toggle=" collapse" href="#showprofiledw" role="button" aria-expanded="false" aria-controls="showprofiledw">
			@if(auth()->user()->image)			  
			  <img id="sortProfileData_pro" src="{{env('APP_URL')}}/{{auth()->user()->image}}" alt="{{auth()->user()->name}}"/> 
			@else
				<img id="sortProfileData_pro" src="{{asset('dist/images/user_icon.png')}}" alt="{{auth()->user()->name}}"/> 
			@endif
			  <span class="pdh_user_name">{{auth()->user()->name}} </span>
			  </a>
                <div class="drop_collapse collapse" id="showprofiledw">
                    <ul class="m-0 p-0">
                        <li>
                            <a href="#">
                                <img src="{{asset('dashboard/img/draw.svg')}}" alt="left-nav-icons">
                                Edit profile
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('provider.setting') }}">
                                <img src="{{asset('dashboard/img/settings.svg')}}" alt="left-nav-icons">
                                Settings
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                <img src="{{asset('dashboard/img/logout.svg')}}" alt="left-nav-icons">
                                Logout
                            </a>
                        </li>
                    </ul>
                </div>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"><i class="fa fa-bars" aria-hidden="true"></i></span>
          </button>

          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <form class="form-inline search_form my-2 my-lg-0" action="{{env('APP_URL')}}/provider/all_known_persons" method="GET">
              <input class="form-control mr-sm-2 search_text" type="text" value="@if(!empty($_GET['search'])){{$_GET['search']}}@endif" name="search" placeholder="Search" aria-label="Search">
              <button class="btn btn-outline-success my-2 my-sm-0" type="submit"><img src="{{asset('dashboard/img/search.svg')}}" alt="search-icon"/></button>
            </form>
            <ul class="navbar-nav ml-auto">
              <li class="nav-item active">
                <a class="nav-link" href="{{route('provider.dashboard')}}"><img src="{{asset('dashboard/img/home.svg')}}" alt="header-nav-icon"> Home</a>
              </li>
              {{--<li class="nav-item">
                <a class="nav-link" href="#"><img src="{{asset('dashboard/img/users.svg')}}" alt="header-nav-icon"> Supporters</a>
              </li>--}}
			  <li class="nav-item">
					<a class="nav-link" data-toggle="collapse" href="#pdNotifySu" role="button" aria-expanded="false" aria-controls="pdNotifySu">
						<span class="notice_count_su notice_text @if(count($notice_count_sp)>0) badge-danger @endif ">
							<?php if(count($notice_count_sp)>0){ echo count($notice_count_sp); }?> </span><img src="{{asset('dashboard/img/users.svg')}}" alt="header-nav-icon"> Supporters
					</a>
					<div class="collapse drop_collapse" id="pdNotifySu">
						<ul class="notify_drop">
							<div class="notify-drop-title">
								<h6 class="mb-0 notice_count_sp" data-count="{{ count($notice_count_sp) }}">Notifications (<span class="notif-count-sp"> {{ count($notice_count_sp) }} </span>)</h6>
							</div>
							<div class="drop-content">
								<div class="list-group notif_main_row_sp">
									
								@if(count($notifications_sp)>0 && !empty($notifications_sp))
									
									@foreach($notifications_sp as $key=>$value)
										<a href="javascript:void(0)" class="<?php if($value->status ==1){ echo 'dot pusher_notification_row';} ?> list-group-item list-group-item-action" data-id="{{$value->id}}" data-viewid="{{$value->id}}" data-viewtype="{{$value->notice_type}}" @if($value->status ==1) onclick="javascript: pusher_notification_row({{$value->id}},'{{$value->notification_type}}', this)" @endif >
											<div class="d-flex w-100 justify-content-start align-items-start">
												<div class="noti_person_img"><img src="{{ asset($value->created_image) }}" alt=""></div>
												<div>
													@if($value->notification_type=='supporter_requests')
														<p class="mb-1"> <b>{{$value->created_name}}</b>
														@if($value->notice_type=='supporter_accept')
															accepted your request
														@endif
														
														@if($value->notice_type=='supporter_request')
															has been sent a friend request
														@endif
														
													</p>
													@endif
													
													<small class="badge badge-info">{{ date(env('DATE_FORMAT_PHP_H'), strtotime($value->created_at)) }}</small>
												</div>
											</div>
										</a>
									@endforeach
								@endif
								</div>
							</div>
						</ul>
					</div>
				</li>
              <li class="nav-item">
				<a class="nav-link" data-toggle="collapse" href="#pdNotify_n" role="button" aria-expanded="false" aria-controls="pdNotify_n">
						
					<span class="notif-count n_t notice_text @if(count($notice_count_img)>0) badge-danger @endif"> <?php if(count($notice_count_img)>0){ echo count($notice_count_img); }?> </span>
				
					<svg xmlns="http://www.w3.org/2000/svg" width="15.141" height="15.141" viewBox="0 0 15.141 15.141">
                                <g id="chat" transform="translate(-3.375 -3.375)">
                                <path id="Path_450" data-name="Path 450" d="M6.461,18.82A1.9,1.9,0,0,1,4.831,17.19V11.25H4.481a1.111,1.111,0,0,0-1.106,1.106v9.958L5.5,20.2h7.829a1.124,1.124,0,0,0,1.106-1.125V18.82Z" transform="translate(0 -3.799)" fill="#2c99cb"/>
                                <path id="Path_451" data-name="Path 451" d="M19.518,3.375H9.356a1.274,1.274,0,0,0-1.27,1.27v8.229a1.277,1.277,0,0,0,1.27,1.274h8.669l2.762,1.929V4.645A1.274,1.274,0,0,0,19.518,3.375Z" transform="translate(-2.272)" fill="#2c99cb"/>
                                </g>
                            </svg>
							Messaging
				</a>
				<div class="collapse drop_collapse" id="pdNotify_n">
					<ul class="notify_drop">
						<div class="notify-drop-title">
							<h6 class="mb-0 notice_count" data-count="{{ count($notice_count_img) }}">Notifications (<span class="notif-count"> {{ count($notice_count_img) }} </span>)</h6>
						</div>
						<div class="drop-content">
							<div class="list-group notif_main_row">
								
							@if(count($message_notice)>0 && !empty($message_notice))
								
								@foreach($message_notice as $key=>$value)
									<a href="@if($value->status ==1)javascript:void(0);@else{{URL('provider/patient_visits')}}@endif" class="<?php if($value->status ==1){ echo 'dot pusher_notification_row';} ?> list-group-item list-group-item-action" data-id="{{$value->id}}" data-viewid="{{$value->id}}" data-viewtype="{{$value->notification_type}}" @if($value->status ==1) onclick="javascript: pusher_notification_row({{$value->id}},'{{$value->notification_type}}', this)" @endif>
										<div class="d-flex w-100 justify-content-start align-items-start">
											<div class="noti_person_img"><img src="{{ asset($value->created_image) }}" alt=""></div>
											<div>
												@if($value->notification_type=='unread_messages')
													<p class="mb-1"> <b>{{$value->created_name}}</b> {{ $value->notice_type }} message unread</p>
												@endif
												
												
												<small class="badge badge-info">{{ date(env('DATE_FORMAT_PHP_H'), strtotime($value->created_at)) }}</small>
											</div>
										</div>
									</a>
								@endforeach
							@endif
							</div>
						</div>
					</ul>
				</div>
              </li>
              <li class="nav-item pdrm">
                <a class="nav-link" data-toggle="collapse" href="#pdNotify" role="button" aria-expanded="false" aria-controls="pdNotify">
                    <span class="notice_count_spp notice_text @if(count($notice_count)>0) badge-danger @endif ">
								<?php if(count($notice_count)>0){ echo count($notice_count); }?> </span>
				    <svg xmlns="http://www.w3.org/2000/svg" width="16.002" height="18.224" viewBox="0 0 16.002 18.224">
                        <g id="notification" transform="translate(-8259.998 -10998)">
                        <g id="Icon_feather-bell" data-name="Icon feather-bell" transform="translate(8260.998 11000.419)">
                            <path id="Path_447" data-name="Path 447" d="M15.606,7.442a4.442,4.442,0,1,0-8.885,0c0,5.183-2.221,6.664-2.221,6.664H17.827s-2.221-1.481-2.221-6.664" transform="translate(-4.5 -3)" fill="#2c99cb" stroke="#2c99cb" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
                            <path id="Path_448" data-name="Path 448" d="M17.967,31.5a1.481,1.481,0,0,1-2.562,0" transform="translate(-10.022 -17.433)" fill="#2c99cb" stroke="#2c99cb" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
                        </g>
                        </g>
                    </svg>				
                    Notifications</a>
                    <div class="collapse drop_collapse" id="pdNotify">
                        <ul class="notify_drop">
                            <div class="notify-drop-title">
                                <h6 class="mb-0 notice_count" data-count="{{ count($notice_count) }}">Notifications (<span class="notif-count"> {{ count($notice_count) }} </span>)</h6>
                            </div>
                            <div class="drop-content">
                                <div class="list-group notif_main_row">
                                    
								@if(count($notifications)>0 && !empty($notifications))
									
								    @foreach($notifications as $key=>$value)
								    
										<a href="<?php if($value->status ==1){ echo 'javascript:void(0)';}else{
										 URL('provider/patient_visits');} ?>" class="<?php if($value->status ==1){ echo 'dot pusher_notification_row ';} ?> list-group-item list-group-item-action" data-id="{{$value->id}}" data-viewid="{{$value->id}}" data-viewtype="{{$value->notification_type}}" @if($value->status ==1) onclick="javascript: pusher_notification_row({{$value->id}},'{{$value->notification_type}}', this)" @endif>
											<div class="d-flex w-100 justify-content-start align-items-start">
												<div class="noti_person_img"><img src="{{ asset($value->created_image) }}" alt=""></div>
												<div>
													
													@if($value->notification_type=='add_appointment')
														<p class="mb-1"> <b>{{$value->created_name}}</b> new appointment </p>
													@endif
													
													@if($value->notification_type=='visit_all_status')
														@if($value->notice_type=='paywall_feature_video_call')
														<p class="mb-1"> <b>{{$value->created_name}}</b> has paid for the video call feature.</p>
													@endif 
													@if($value->notice_type=='paywall_feature_chat')
														<p class="mb-1"> <b>{{$value->created_name}}</b> has paid for the chat feature.</p>
													@endif 
													@endif
													
													<small class="badge badge-info">{{ date(env('DATE_FORMAT_PHP_H'), strtotime($value->created_at)) }}</small>
												</div>
											</div>
										</a>
									@endforeach
								@endif
                                </div>
                            </div>
							
                        </ul>
                    </div>
              </li>
              <li class="nav-item">				
              <a class="profile-icon-dropdown" data-toggle="collapse" href="#showprofiledw" role="button" aria-expanded="false" aria-controls="showprofiledw">
			@if(auth()->user()->image)			  
			  <img id="sortProfileData" src="{{env('APP_URL')}}/{{auth()->user()->image}}" alt="{{auth()->user()->name}}"/> 
			@else
				<img id="sortProfileData" src="{{env('APP_URL')}}/dist/images/user_icon.png" alt="{{auth()->user()->name}}"/> 
			@endif
			  <span class="pdh_user_name">{{auth()->user()->name}}</span>
			  </a>

			  
                <div class="collapse drop_collapse" id="showprofiledw">
                    <ul class="m-0 p-0">
                    	<li>
	                		<a href="{{route('provider.profile')}}">
                                <img src="{{asset('dashboard/img/draw.svg')}}" alt="left-nav-icons">
                                Edit profile
                            </a>
                    	</li>
                    	<li>
                    		<a href="{{ route('provider.setting') }}">
                                <img src="{{asset('dashboard/img/settings.svg')}}" alt="left-nav-icons">
                                Settings
                            </a>
                    	</li>
                    	<li>
                    		<a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                <img src="{{asset('dashboard/img/logout.svg')}}" alt="left-nav-icons">
                                Logout
                            </a>
							<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
                    	</li>
                    </ul>
                </div>
              </li>
            </ul>
          </div>
      </div>
    </nav>
	
    <!-- Navbar Ends -->
	
	<!-- Main Section Side Bar -->
	
    <section class="main-dashboard mt-5 pt-5">
        <div class="container-fluid">
            <div class="row">
			@if(auth()->user()->is_first_login == 0)
				<div class="col-xl-2 col-lg-4 col-md-3 col-sm-12">
                    <div class="left-nav">
                        <ul class="p-0 m-0">
                            <li>
                                <a href="{{route('provider.dashboard')}}" <?php if(isset($page) && $page == 'provider_dashboard'){ echo 'class="active"';}?> >
                                    <img src="{{asset('dashboard/img/home-2.svg')}}" alt="left-nav-icons"/>
                                    Dashboard
                                </a>
                            </li>
                            <li>
                                <a href="{{route('provider.profile')}}" <?php if(isset($page) && $page == 'provider_profile'){ echo 'class="active"';}?> >
                                    <img src="{{asset('dashboard/img/user2.svg')}}" alt="left-nav-icons"/>
                                    My Profile
                                </a>
                            </li>
							<li>
                                <a href="{{route('provider.patient_visits')}}" <?php if(isset($page) && $page == 'provider_patient_visits'){ echo 'class="active"';}?>>
                                    <img src="{{asset('dashboard/img/doctor.svg')}}" alt="left-nav-icons"/>
                                    Patient Visits
                                </a>
                            </li>
                            <li>
                                <a href="{{route('provider.appointments')}}" <?php if(isset($page) && $page == 'provider_appointments'){ echo 'class="active"';}?>>
                                    <img src="{{asset('dashboard/img/calender.svg')}}" alt="left-nav-icons"/>
                                    Appointments
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img src="{{asset('dashboard/img/chat2.svg')}}" alt="left-nav-icons"/>
                                    Messages <span class="notification-no">2</span>
                                </a>
                            </li>
                        </ul>
                        <h4>Community</h4>
                        <ul class="p-0 m-0">
                            <li>
                                <a href="#">
                                    <img src="{{asset('dashboard/img/doctor.svg')}}" alt="left-nav-icons"/>
                                    Doctor Consult
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img src="{{asset('dashboard/img/ask.svg')}}" alt="left-nav-icons"/>
                                    Quick Q & A
                                </a>
                            </li>
							<li>
                                <a href="{{URL('provider/supporters')}}">
                                    <img src="{{asset('dashboard/img/user2.svg')}}" alt="left-nav-icons"/>
                                    Supporters
                                </a>
                            </li>
							<li>
                                <a href="{{URL('provider/blogs')}}">
                                    <img src="{{asset('dashboard/img/ask.svg')}}" alt="left-nav-icons"/>
                                    Blogs
                                </a>
                            </li>
                            <li>
                                <a href="{{URL('provider/forums')}}">
                                    <img src="{{asset('dashboard/img/bag.svg')}}" alt="left-nav-icons"/>
                                    Forums                                </a>
                            </li>							
                        </ul>
                    </div>
                </div>	
				@endif

