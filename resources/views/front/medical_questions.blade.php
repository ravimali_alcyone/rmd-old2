@extends('front.online_visit_layout.app')

@section('content')
    <div class="tabs_wrapper welcome basics m_qs">
        <div>
            <a href="javascript:void(0);" class="how_it_works">Medical Questions</a>
            <div class="steps">
                <span class="active ml-0"></span>
                <span class="active"></span>
                <span></span>
                <span></span>
            </div>
        </div>
        <span id="pageBackBtn" class="back_arrow fa fas fa-arrow-left" onclick="location.href = '{{ route('online_visit_basics') }}';"></span>
        <span id="blockBackBtn" class="back_arrow fa fas fa-arrow-left" onclick="goPrev()" style="display:none;"></span>
    </div>

    <div class="main_welcome_wrapper">
		<form id="onlineVisitForm">
        <section class="welcome_content_wrapper medical_qs">
            <div class="medical_qs_welcome">
                <h3 class="heading">Medical Questions</h3>
                <p class="description text-center">Tell your doctor about your symptoms and overall health. Your doctor needs this information to determine the most appropriate treatment for you. It's important that you are honest and respond as accurately as possible.</p>
                <div class="img_wrapper text-center">
                    <img src="{{asset('assets/images/medical_qs.svg')}}" alt="medical_qs" class="img-fluid">
                </div>
                <button type="button" class="btn btn-primary">Continue</button>
            </div>
		
		<?php 
			if($medical_questions){
				$button_text = 'Next';
				$is_submit = 0;
				$firstQ = array_key_first($medical_questions);
				$lastQ = array_key_last($medical_questions);

				foreach($medical_questions as $key => $val){			
					
					if($lastQ == $key){ 
						$button_text = 'Save and Continue';
						$is_submit = 1;
					}
					
					if($val['question']['question_type'] == 'single'){
		?>
            <div class="questions medical_qs_<?php echo $key;?> animate__animated animate__fadeInRight" style="display:none;" data-id="<?php echo $val['question']['question_order'];?>" prev-id="<?php if($val['question']['prev_id']){echo $val['question']['prev_id'];}?>">
                <h5 class="question"><?php echo $val['question']['question_text'];?></h5>
				<input type="hidden" value="<?php echo $val['question']['question_id'];?>" name="question[]"/>
                <div class="answers_box">
			<?php 
				if($val['options']){
					$y=1;
					foreach($val['options'] as $key2 =>

					$op){
			?>
                    <div>
                        <input type="radio" name="answers[option][<?php echo $val['question']['question_id'];?>][0]" value="<?php echo $op['option_id'];?>" id="q<?php echo $key;?>_a<?php echo $y;?>" class="<?php if($op['is_conditional'] == 1){ echo 'is_conditional';}?>" next-question="<?php echo $op['next_question_id'];?>">
                        <label for="q<?php echo $key;?>_a<?php echo $y;?>"><?php echo $op['option_text'];?></label>
                    </div>
			<?php 
				$y++;
					}
				}
			?>
                    <div class="btn_wrapper">
                        <a href="javascript:void(0);" class="btn btn-primary medical_qs_<?php echo $key;?>_btn disabled" <?php if($is_submit == 1){ echo 'onclick="goSubmit(this);"';}?> ><?php echo $button_text;?></a>
                    </div>
                </div>
            </div>
			<?php }elseif($val['question']['question_type'] == 'multiple'){?>
			
            <div class="questions medical_qs_<?php echo $key;?> animate__animated animate__fadeInRight" style="display:none;" data-id="<?php echo $val['question']['question_order'];?>" prev-id="<?php if($val['question']['prev_id']){echo $val['question']['prev_id'];}?>">
                <h5 class="question"><?php echo $val['question']['question_text'];?></h5>
				<input type="hidden" value="<?php echo $val['question']['question_id'];?>" name="question[]"/>
                <div class="answers_box">
			<?php 
				if($val['options']){
					$y=1;
					$m=0;
					foreach($val['options'] as $key2 => $op){
			?>				
                    <div class="form-group">
                        <input type="checkbox" name="answers[option][<?php echo $val['question']['question_id'];?>][<?php echo $m;?>]" value="<?php echo $op['option_id'];?>" id="q<?php echo $key;?>_a<?php echo $y;?>" class="checkbox <?php if($op['is_conditional'] == 1){ echo 'is_conditional';}?>" next-question="<?php echo $op['next_question_id'];?>">
                        <label for="q<?php echo $key;?>_a<?php echo $y;?>" class="checkbox_label"><?php echo $op['option_text'];?></label>
                    </div>
			<?php 
				$m++;
				$y++;
					}
				}
			?>					
                    <div class="btn_wrapper">
                        <a href="javascript:void(0);" class="btn btn-primary medical_qs_<?php echo $key;?>_btn disabled" <?php if($is_submit == 1){ echo 'onclick="goSubmit(this);"';}?> ><?php echo $button_text;?></a>
                    </div>
                </div>
            </div>
		<?php }elseif($val['question']['question_type'] == 'long_text'){?>	
		
            <div class="questions medical_qs_<?php echo $key;?> animate__animated animate__fadeInRight" style="display:none;" data-id="<?php echo $val['question']['question_order'];?>" prev-id="<?php if($val['question']['prev_id']){echo $val['question']['prev_id'];}?>">
                <h5 class="question mb-0"><?php echo $val['question']['question_text'];?></h5>
				<input type="hidden" value="<?php echo $val['question']['question_id'];?>" name="question[]"/>
                <div class="answers_box">
                    <div class="comment_box">
                        <textarea name="answers[text][<?php echo $val['question']['question_id'];?>]" cols="30" rows="4" placeholder="Please type your answer here..." maxlength="800"></textarea>
                    </div>
                    <div class="btn_wrapper">
                        <a href="javascript:void(0);" class="btn btn-primary medical_qs_<?php echo $key;?>_btn disabled" <?php if($is_submit == 1){ echo 'onclick="goSubmit(this);"';}?> ><?php echo $button_text;?></a>
                    </div>
                </div>
            </div>
		<?php }elseif($val['question']['question_type'] == 'short_text'){?>	
		
            <div class="questions medical_qs_<?php echo $key;?> animate__animated animate__fadeInRight" style="display:none;" data-id="<?php echo $val['question']['question_order'];?>" prev-id="<?php if($val['question']['prev_id']){echo $val['question']['prev_id'];}?>">
                <h5 class="question mb-0"><?php echo $val['question']['question_text'];?></h5>
				<input type="hidden" value="<?php echo $val['question']['question_id'];?>" name="question[]"/>
                <div class="answers_box">
                    <div class="comment_box">
                        <input type="text" name="answers[text][<?php echo $val['question']['question_id'];?>]" placeholder="Please type your answer here..." maxlength="60">
                    </div>
                    <div class="btn_wrapper">
                        <a href="javascript:void(0);" class="btn btn-primary medical_qs_<?php echo $key;?>_btn disabled" <?php if($is_submit == 1){ echo 'onclick="goSubmit(this);"';}?> ><?php echo $button_text;?></a>
                    </div>
                </div>
            </div>			
		<?php }elseif($val['question']['question_type'] == 'yes_no'){?>			
		
            <div class="questions medical_qs_<?php echo $key;?> animate__animated animate__fadeInRight" style="display:none;" data-id="<?php echo $val['question']['question_order'];?>" prev-id="<?php if($val['question']['prev_id']){echo $val['question']['prev_id'];}?>">
                <h5 class="question"><?php echo $val['question']['question_text'];?></h5>
				<input type="hidden" value="<?php echo $val['question']['question_id'];?>" name="question[]"/>
                <div class="answers_box">
			<?php 
				if($val['options']){
					$y=1;
					foreach($val['options'] as $key2 => $op){
			?>
                    <div>
                        <input type="radio" name="answers[option][<?php echo $val['question']['question_id'];?>][0]" value="<?php echo $op['option_id'];?>" id="q<?php echo $key;?>_a<?php echo $y;?>" class="<?php if($op['is_conditional'] == 1){ echo 'is_conditional';}?>" next-question="<?php echo $op['next_question_id'];?>">
                        <label for="q<?php echo $key;?>_a<?php echo $y;?>"><?php echo $op['option_text'];?></label>
                    </div>
			<?php 
				$y++;
					}
				}
			?>
                    <div class="btn_wrapper">
                        <a href="javascript:void(0);" class="btn btn-primary medical_qs_<?php echo $key;?>_btn disabled" <?php if($is_submit == 1){ echo 'onclick="goSubmit(this);"';}?> ><?php echo $button_text;?></a>
                    </div>
                </div>
            </div>		
		<?php 
				}

				}
			}
		?>
        </section>
		<button type="submit" id="SaveBtn" style="display:none;" />
		</form>		
    </div>
	
    <script>
	
		var total = <?php echo count($medical_questions);?>;
        $(document).ready(function() {
            $(".the_basics_sec .biological_sex span").click(function() {
                $(".the_basics_sec .biological_sex span").removeClass("active");
                $(this).addClass("active");
            });

            $(".medical_qs_welcome button").click(function() {
                $(".medical_qs_welcome").hide();
                $("#pageBackBtn").hide();
                $("#blockBackBtn").show();
                $(".medical_qs_<?php echo array_key_first($medical_questions);?>").show();
            });

		<?php 
			if($medical_questions){
				
				foreach($medical_questions as $key => $val){
		?>
		
            $(".medical_qs_<?php echo $key;?>_btn").click(function() {
				var count = <?php echo $key;?>;
				if($(this).hasClass('next_question_id')){
					let qid = $(this).attr('nq-id');
					//console.log('qid',qid);
					$(".medical_qs_"+count).hide();
					$(".medical_qs_"+qid).show();
				}
				else{
					$(".medical_qs_"+count).hide();
					
					$('.welcome_content_wrapper.medical_qs .questions').each(function() {
						//console.log('data-id',$(this).attr('data-id'));
						//console.log('count',count);
						if($(this).attr('prev-id') == '' && $(this).attr('data-id') > count){
							$(this).show();
							return false;
						}
					});
										
				}

            });
		<?php 
				}
			}
		?>			
			
			//Radio
			$(".answers_box input[type=radio]").click(function(){
				$(this).parent().parent().find('.btn_wrapper a').removeClass('disabled');
				
				if($(this).hasClass('is_conditional')){
					var next_question_id = $(this).attr('next-question');
					$(this).parent().parent().find('.btn_wrapper a').addClass('next_question_id');
					$(this).parent().parent().find('.btn_wrapper a').attr('nq-id', next_question_id);					
				}else{
					let x = $(this).parent().parent().parent().attr('data-id');
				
					console.log('x',x);
					console.log('total',total);
						//if($(this).attr('prev-id') == '' && $(this).attr('data-id') > count){					
					if((total - x)  == 1){
						$(this).parent().parent().find('.btn_wrapper a').text('Save and Continue');
						$(this).parent().parent().find('.btn_wrapper a').attr('onclick','goSubmit(this);');
					}
				}				
			});
			
			//Checkbox
			$(".answers_box input[type=checkbox]").click(function(){
				
				var atLeastOneIsChecked = false;
				var next_question_id = '';
				$(this).parent().parent().find('input:checkbox').each(function() {
					if ($(this).is(':checked')) {
						if($(this).hasClass('is_conditional')){
							next_question_id = $(this).attr('next-question');
						}
					  atLeastOneIsChecked = true;
					  return false;
					}
				});

				if(atLeastOneIsChecked == true){
					$(this).parent().parent().find('.btn_wrapper a').removeClass('disabled');
					
					if(next_question_id != ''){
						$(this).parent().parent().find('.btn_wrapper a').addClass('next_question_id');
						$(this).parent().parent().find('.btn_wrapper a').attr('nq-id', next_question_id);
						//let prev_id = $(this).parent().parent().parent().attr('data-id');
						
						//$('.welcome_content_wrapper.medical_qs .questions.medical_qs_'+next_question_id).attr('prev-id',prev_id);
					}
				}else{
					$(this).parent().parent().find('.btn_wrapper a').addClass('disabled');
				}
				
			});	
			
			//Text
			$('.answers_box .comment_box input,textarea').on('input',function(e){
				
				if($(this).val() == ''){
					$(this).parent().parent().find('.btn_wrapper a').addClass('disabled');
				}else{
					$(this).parent().parent().find('.btn_wrapper a').removeClass('disabled');
				}
			});			
		
        });
		
		
		function goPrev(){
			$('.welcome_content_wrapper.medical_qs .questions').each(function() {
				
				if( $(this).css('display')== 'block' ) {
					let q_id = $(this).attr('data-id');
					let prev_id = $(this).attr('prev-id');
					if(q_id == 1){
						$(this).hide();					
						$('.medical_qs_welcome').show();
						$("#pageBackBtn").show();
						$("#blockBackBtn").hide();					
					}else{
						let pid;
						
						if(prev_id != ''){
							pid = prev_id;
							$(".welcome_content_wrapper.medical_qs [data-id="+pid+"]").show();
						}else{
							//pid = q_id - 1;
							
							var dtArr = [];
							$('.welcome_content_wrapper.medical_qs [prev-id=""]').each(function() {
								let dt_id = $(this).attr('data-id');
								if(dt_id < q_id){
									dtArr.push(dt_id);
								}
							});
							//console.log(dtArr);
							if(dtArr){
								let new_id = dtArr[dtArr.length-1];
								$(".welcome_content_wrapper.medical_qs [data-id="+new_id+"]").show();
							}							
						}
						
						$(this).hide();

						
					}						
				}
			});			
		}
		
		function goSubmit(){
			$("#SaveBtn").trigger('click');
		}
		
		$("#onlineVisitForm").submit(function(e) {
			e.preventDefault();

			$(".loader").css('display', 'flex');
			
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: $('#onlineVisitForm').serialize(),
				url: "{{ route('onlineVisitSubmit') }}",
				type: "POST",
				// dataType: 'json',
				success: function (response) {
					
					if(response['status'] == 'success'){

						setTimeout(function(){
							window.location = "{{ route('treatment_preference') }}";
							$(".loader").css('display', 'none');
						}, 1000);
					}else{
						$(".loader").css('display', 'none');
					}
				},
				error: function (data) {
					$(".loader").css('display', 'none');
					let errors = data.responseJSON.errors;
					
					$.each(errors, function(key, value) {
						errorAlert(value[0],5000,'bottom-left');
					});					
 					swal({
						title: 'Error Occured.',
						icon: 'error'
					})
				}
			});
		});		
    </script>	
@endsection