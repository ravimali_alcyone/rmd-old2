// GLOBAL VARIABLES
const authUserId = $("#user_id").val();
const baseUrl = window.location.origin;
const userRole = $("#user_role").val();

$(document).ready(function() {  
      Pusher.logToConsole = true;
	  var pusher_api_key = $("#pusher_api_key").val();
      var pusher_channel = $("#pusher_channel").val();
      var pusher_cluster = $("#pusher_cluster").val();
      var pusher = new Pusher(pusher_api_key, {
						cluster:pusher_cluster,
						forceTLS:true,
						encrypted: true
						});
      // Subscribe to the channel we specified in our Laravel Event
      var channel = pusher.subscribe(pusher_channel);
      // post like 
      channel.bind('AddAppointment', function(data) {
  
        var avatar = Math.floor(Math.random() * (71 - 20 + 1)) + 20;
		
		var user_id = authUserId;
		var message = data.message;
		
		if(message !=''){
			var followers_id = message.followers_id;
			
			if(followers_id==user_id){
				
				var notificationsCount = parseInt($('.notify-drop-title').find('.notice_count').attr('data-count'));
				notificationsCount +=1;
				
				if(notificationsCount==1){
				    $('.notice_count_spp').addClass('badge-danger');
				}
				$('.notif-count').text(notificationsCount);
				$('.notify-drop-title').find('.notice_count').attr('data-count',notificationsCount);
				$('.notice_count_spp').text(notificationsCount);
				
				var image =baseUrl+'/'+message.created_image;
				var url =baseUrl+'/provider/patient_visits';
				var noticeType="AddAppointment";
				
				var like_html ='<a href="javascript:void(0)" onclick="redirect_post('+message.id+')" class="dot list-group-item list-group-item-action pusher_notification_row" onclick="javascript: pusher_notification_row('+message.id+', '+noticeType+', this)" data-id="'+message.id+'" data-viewid=""><div class="d-flex w-100 justify-content-start align-items-start"><div class="noti_person_img"><img src="'+image+'" alt=""></div><div><p class="mb-1"> <b>'+message.created_name+'</b> new appointment</p><small class="badge badge-info">just ago</small></div></div></a>';
				
				$('.notif_main_row').prepend(like_html);
				getData();
				
			}
		}

      });
	  
	  channel.bind('VisitAllStatus', function(data) {
  
        var avatar = Math.floor(Math.random() * (71 - 20 + 1)) + 20;
		
		var user_id = authUserId;
		var message = data.message;
		if(message !=''){
			var followers_id = message.followers_id;
			
			if(followers_id==user_id){
				
				var notificationsCount = parseInt($('.notify-drop-title').find('.notice_count').attr('data-count'));
				notificationsCount +=1;
				
				if(notificationsCount==1){
				    $('.notice_count_spp').addClass('badge-danger');
				}
				
				$('.notif-count').text(notificationsCount);
				$('.notify-drop-title').find('.notice_count').attr('data-count',notificationsCount);
				$('.notice_count_spp').text(notificationsCount);
				
				var image =baseUrl+'/'+message.created_image;
				var url =baseUrl+'/provider/patient_visits';
				var noticeType =message.notice_type;
				var like_html='';
				if(noticeType=='paywall_feature_video_call'){
					var like_html ='<a href="javascript:void(0)" class="dot list-group-item list-group-item-action pusher_notification_row" onclick="javascript: pusher_notification_row('+message.id+', '+noticeType+', this)" data-id="'+message.id+'" data-viewid=""><div class="d-flex w-100 justify-content-start align-items-start"><div class="noti_person_img"><img src="'+image+'" alt=""></div><div><p class="mb-1"> <b>'+message.created_name+'</b> has paid for the video call feature.</p><small class="badge badge-info">just ago</small></div></div></a>';
				}
				
				if(noticeType=='paywall_feature_chat'){
					var like_html ='<a href="javascript:void(0)" onclick="redirect_post('+message.id+')" class="dot list-group-item list-group-item-action pusher_notification_row" data-id="'+message.id+'" onclick="javascript: pusher_notification_row('+message.id+', '+noticeType+', this)" data-viewid=""><div class="d-flex w-100 justify-content-start align-items-start"><div class="noti_person_img"><img src="'+image+'" alt=""></div><div><p class="mb-1"> <b>'+message.created_name+'</b> has paid for the chat feature</p><small class="badge badge-info">just ago</small></div></div></a>';
				}
				$('.notif_main_row').prepend(like_html);
				getData();
				
			}
		}

      });
	  
	// Follow request notification supporter request 
    channel.bind('FollowRequest', function(data) {
        //var avatar = Math.floor(Math.random() * (71 - 20 + 1)) + 20;
        var user_id = authUserId;
        var response = data.message;
        let follow_id = response.user_id;
				console.log('Provider FollowRequest');
				console.log(response);	
        if (response != '') {
            if (follow_id === user_id) {			
				requestIgnore();
				pusher_notification_row();
				requestAccept();
				follow();
                var notificationsCount = parseInt($('#pdNotifySu').find('.notice_count_sp').attr('data-count'));
                notificationsCount += 1;
                $('#pdNotifySu').find('.notif-count-sp').text(notificationsCount);
                $('.notice_count_su').text(notificationsCount);
				if(notificationsCount==1){
					$('.notice_count_su').addClass("notice_text");
					$('.notice_count_su').addClass("badge-danger");
				}
				var url = baseUrl+"/user/supporters";
				var rtype = "'supporter_accept'";

                var notification_html = '<a href="javascript:void(0)" class="dot list-group-item list-group-item-action pusher_notification_row" data-id="' + response.id + '" data-viewid="" data-viewtype="supporter_requests" onclick="javascript: pusher_notification_row('+response.id+', '+rtype+', this)">' +
                    '<div class="d-flex w-100 justify-content-start align-items-start">' +
                    '<div class="noti_person_img">' +
                    '<img src="/' + response.image + '" alt="" />' +
                    '</div>' +
                    '<div>' +
                    '<p class="mb-1"><b>' + response.name + ' </b> has been sent a friend request</p>' +
                    '<small class="badge badge-info">just ago</small>' +
                    '</div>' +
                    '</div>' +
                    '</a>';
                $('#pdNotifySu').find('.notif_main_row_sp').prepend(notification_html);

                let previous_requests_count = $("#requests_wrapper .request_box").length;
                let request_html = '<div class="request_box ' + (previous_requests_count == 0 ? 'mb-0' : '') + '">' +
                    '<div class="info_box">' +
                    '<div class="img_wrapper">' +
                    '<img src="/' + response.image + '" alt="request-0" class="w-100">' +
                    '</div>' +
                    '<div class="text_wrapper">' +
                    '<h6>' + response.name + '</h6>' +
                    '<p class="designation">' + (response.clinical_expertise != null ? response.clinical_expertise : '') + '</p>' +
                    response.follow_users +
                    '</div>' +
                    '</div>' +
                    '<div class="btns_box">' +
                    '<button class="btn btn-primary mr-1 requestAccept" onclick="javascript: requestAccept('+response.request_id+',this)" data-id='+ response.request_id +' >Confirm</button>' +
                    '<button class="btn btn-default requestIgnore" onclick="javascript: requestIgnore('+response.request_id+',this)" data-id=' + response.request_id + '>Ignore</button>';
				if(response.request_back_id==1){
					request_html+='</div>' +
                                  '</div>';
				}else{
					request_html+='<button class="btn btn-primary follow" onclick="javascript: follow('+response.request_id+',this,1)" data-id=' + response.request_id + '>Support Back</button>' +
                    '</div>' +
                    '</div>';				
				}	
                $("#requests_wrapper").prepend(request_html);
                $("#requests_wrapper>p").hide();
            }
        }
    });

    //Follow Accept request notification supporter Accept request
    channel.bind('AcceptRequest', function(data) {
        // var avatar = Math.floor(Math.random() * (71 - 20 + 1)) + 20;
        var user_id = authUserId;
        var response = data.message;
        let follower_id = response.request_id;
		
        if (response != '') {
			
            if (follower_id === user_id) {
		
				pusher_notification_row();
                var notificationsCount = parseInt($('#pdNotifySu').find('.notice_count_sp').attr('data-count'));
                notificationsCount += 1;
                $('#pdNotifySu').find('.notif-count-sp').text(notificationsCount);
				$('.notice_count_su').text(notificationsCount);
				if(notificationsCount==1){
					$('.notice_count_su').addClass("notice_text");
					$('.notice_count_su').addClass("badge-danger");
				}
				var url = baseUrl+"/user/supporters";
				var rtype = "'supporter_accept'";
                var notification_html = '<a href="javascript:void(0)" class="dot list-group-item list-group-item-action pusher_notification_row" data-id="' + response.id + '" onclick="javascript: pusher_notification_row('+response.id+', '+rtype+', this)" data-viewid="" data-viewtype="supporter_accept">' +
                    '<div class="d-flex w-100 justify-content-start align-items-start">' +
                    '<div class="noti_person_img">' +
                    '<img src="/' + response.image + '" alt="" />' +
                    '</div>' +
                    '<div>' +
                    '<p class="mb-1"><b>' + response.name + ' </b> accepted your request.</p>' +
                    '<small class="badge badge-info">'+ response.date_time +'</small>' +
                    '</div>' +
                    '</div>' +
                    '</a>';
                $('#pdNotifySu').find('.notif_main_row_sp').prepend(notification_html);
                //successAlert(response.name + ' accepted your request.', 5000, 'top-right');
				callProgressBar();
                $(".card.card_" + response.user_id).find('.text_wrapper>div>span').text('Supporting').addClass('text-success');
				$(".card.card_" + response.user_id).find('.text_wrapper>div>span').text('Supporting').addClass('text-success');
            }
        }
    });
	  
		
	$(document).on("click", function() {
		  $('.drop_collapse').collapse('hide');
	});
	

		
});	

function pusher_notification_row(notice_id='', viewType='', element=''){
		if(notice_id !=''){
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: {'notification_id':notice_id},
				url: baseUrl+"/provider/notification_view",
				type: "POST",
				// dataType: 'json',
				success: function (response) {
					
					if(response['status'] == 'success'){
						//$("#passModal").modal('hide');
						callProgressBar();
						if(viewType=='supporter_accept' || viewType=='supporter_requests'){
							var notificationsCount = parseInt($('.notify-drop-title').find('.notice_count_sp').attr('data-count'));
							if(notificationsCount>0){
								notificationsCount -= 1;
								$('.notify-drop-title').find('.notice_count_sp').attr('data-count',notificationsCount);
								$('.notif-count-sp').text(notificationsCount);
								$('.notice_count_su.notice_text').text(notificationsCount);
							}else{
								$('.notify-drop-title').find('.notice_count_sp').attr('data-count',0);
								$('.notice_count_su').removeClass('badge-danger');
								$('.notif-count-sp').text(' ');
								$('.notice_count_su.notice_text').text('');
							}
							setTimeout(function(){
								$(element).removeClass('dot');
								$(element).removeClass('pusher_notification_row');
								$(element).prop("onclick", null).off("click");
								//redirect to provider/supporters page
								
								window.location = baseUrl+'/provider/supporters';
							}, 2000);
						
						}else{
							var notificationsCount = parseInt($(element).parent().parent().parent().find('.notify-drop-title').find('.notice_count').attr('data-count'));
							if(notificationsCount>0){
								notificationsCount -= 1;
								$(element).parent().parent().parent().find('.notify-drop-title').find('.notif-count').text(notificationsCount);
								$(element).parent().parent().parent().find('.notify-drop-title').find('.notice_count').attr('data-count',notificationsCount);
								$('.n_t').text(notificationsCount);
							}else{
								$('.n_t').removeClass('badge-danger');
								$('.n_t').text(' ');
								$('.notif-count').text('');
								$(element).parent().parent().parent().find('.notify-drop-title').find('.notice_count').attr('data-count',0);
							}	
							setTimeout(function(){
								$(element).removeClass('dot');
								$(element).removeClass('pusher_notification_row');
								$(element).prop("onclick", null).off("click");
							}, 2000);
						}

					}
				},
				error: function (data) {
				
				}
			});
		}
		
	}
