<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MedicineFaq extends Model
{
	protected $table = 'medicine_faqs';

	protected $guarded = [];	
}

