<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

use Closure;

class UserLogin extends Middleware

{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    protected function redirectTo($request)
    {
        if (! $request->expectsJson()) {
            return route('sign_in');
			
        }
    }
	
    public function handle($request, Closure $next)
    {
        if(auth()->user() && auth()->user()->is_admin == 0 && (auth()->user()->user_role == 4) ){
            return $next($request);
        }
   
        return redirect('sign_in')->with('error',"You don't have user access.");
    }	
}
