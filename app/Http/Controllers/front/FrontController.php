<?php

namespace App\Http\Controllers\front;

use Illuminate\Http\Request;
use Stripe\SetupIntent;
use Laravel\Cashier\Cashier;
use \Softon\LaravelFaceDetect\Facades\FaceDetect;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Session;
use Auth;
use DB;
use Route;
use App\Library\Services\CommonService;
use App\User;
use App\Testimonial;
use App\Sponsor;
use App\ServicePricing;
use App\Faq;
use App\FaqCategory;
use App\ContactUs;
use App\ProviderCategory;
use App\Service;
use App\ProviderService;
use App\TreatmentMedicine;
use App\MedicineVariant;
use App\MedicineFaq;
use App\VisitForm;
use App\VisitAnswer;
use App\State;
use App\OnlineVisit;
use App\OnlineVisitPaywallFeature;
use App\OnlineVisitProviderAssign;
use App\Article;
use App\BlogCategory;
use App\CategoryTopic;
use App\PostType;
use App\BlogComment;
use App\SocialUserFollowConnection;
use App\BlogSubscription;
use App\ForumCategory;
use App\ForumTopic;
use App\ForumDetail;
use App\MasterSetting;
use App\ForumPaymentDetail;
use App\ForumTopicComment;
use App\ForumTopicLike;
use App\Notifications;
use App\EventAppointment;
use App\Events\Add_appointment;
use App\Events\Admin_notification;
use App\PaymentLogs;
use Mail;

class FrontController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {
        $data['page'] = $page = 'home';
		$data['meta_title'] = 'Home';
		$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';	

        $singal_section_data = DB::table('sections')->select('*')->where(array('page_name' => $page, 'language' => 'en'))->get();
        $pagesdata=array();
        foreach ($singal_section_data as $key => $value) {
            $pagesdata[$value->section_name] = json_decode($value->section_content);
        }
		
		$data['page_data'] = $pagesdata;
		
		$data['testimonials'] = Testimonial::select('*')->where('status',1)->latest()->get();
		$data['sponsors'] = Sponsor::select('*')->where('status',1)->latest()->get();
		
		$data['providers'] = array();
		$providers = User::select('users.*')->where('user_role',3)->where('status',1)->limit(5)->get();
		$gender = 'male';
		$data['services'] = Service::where('status',1)
								->where(function($query) use ($gender){
									$query->orWhere('available_for',$gender);
									$query->orWhere('available_for','both');
								})
								->get();
		
		if($providers && $providers->count() > 0){
			$i=0;
			foreach($providers as $row){
				
				$categoryString = '';
				$categories = explode(',', $row->category);
				$category_names = ProviderCategory::whereIn('id', $categories)->select('name')->get();
				
				
				if($category_names && $category_names->count() > 0){
					$categories_arr = array();
					
					foreach ($category_names as $key => $value) {
						array_push($categories_arr, $value->name);
					}
					$categoryString = implode(', ', $categories_arr);
				}			
				
				if($row->image == ''){
					$provider_image = '/dist/images/user_icon.png';
				}else{
					$provider_image = '/'.$row->image;
				}
						
				$data['providers'][$i]['id'] = $row->id;
				$data['providers'][$i]['name'] = $row->name;
				$data['providers'][$i]['image'] = $provider_image;
				$data['providers'][$i]['provider_categories'] = $categoryString;
				
				$i++;
			}
		}
		
        return view('front.index', $data);
    }
	
    public function plans() {
        $data['page'] = 'plans';
		$data['meta_title'] = 'Plans';
		$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';	
		
		$plans = array();
		
		$plan_data = ServicePricing::select('service_pricing.*','all_plan_features.name AS feature_name','all_plan_features.detail AS feature_detail')
						->leftJoin('selected_plan_features','selected_plan_features.plan_id','=','service_pricing.id')
						->leftJoin('all_plan_features','all_plan_features.id','=','selected_plan_features.plan_feature_id')
						->where('service_pricing.status',1)->get()->toArray();
				
		if($plan_data){
			$x = 0;
						
			foreach($plan_data as $key => $value){
				$plans[$value['id']]['id'] = $value['id'];
				$plans[$value['id']]['name'] = $value['name'];
				$plans[$value['id']]['is_monthly'] = $value['is_monthly'];
				$plans[$value['id']]['total_months'] = $value['total_months'];
				$plans[$value['id']]['monthly_price'] = $value['monthly_price'];
				$plans[$value['id']]['is_total'] = $value['is_total'];
				$plans[$value['id']]['total_price'] = $value['total_price'];
				$plans[$value['id']]['monthly_plan_text'] = $value['monthly_plan_text'];
				$plans[$value['id']]['total_plan_text'] = $value['total_plan_text'];
				$plans[$value['id']]['special'] = $value['special'];
				$plans[$value['id']]['detail'] = $value['detail'];
				$plans[$value['id']]['is_most_popular'] = $value['is_most_popular'];
				
				if($value['feature_name'] && $value['feature_detail']){
					$plans[$value['id']]['features'][$x]['feature_name'] = $value['feature_name'];
					$plans[$value['id']]['features'][$x]['feature_detail'] = $value['feature_detail'];					
				}
				
				$x++;
			}
		}
		//echo "<pre>"; print_r($plans);die();
		$data['plans'] = $plans;
						
        return view('front.plans', $data);
    }	

    public function how_it_works() {
        $data['page'] = 'how_it_works';
		$data['meta_title'] = 'How It Works';
		$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';			
        return view('front.how_it_works', $data);
    }
	
    public function about() {
        $data['page'] = 'about';
		$data['meta_title'] = 'About Us';
		$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';			
        return view('front.about', $data);
    }

    public function contact(Request $request, CommonService $common) {
		
		if ($request->ajax()) {
			$req = $request->all();
		
			$this->validate($request, [
				'name' => 'required|string|max:100',
				'email' => 'required|email|max:100',
				'phone' => 'string|max:20',
				'subject' => 'required|string|max:100',
				'message' => 'required|string',
			]);
			
			$input_data = [
				'name' => trim($req['name']),
				'email' => trim($req['email']),
				'phone' => trim($req['phone']),
				'user_category' => 1,
				'status'=> 1,
				'subject'=> trim($req['subject']),
				'comments' => trim($req['message']),
			];
			

			
			ContactUs::create($input_data);
			
			//Email Sending
			
			$input['name'] = 'ReplenishMD Admin';
			$input['email'] = env('ADMIN_EMAIL');
			$input['subject'] = 'Enquiry - '.$req['subject'];
			$input['template'] = 'front.email_template.contact_us';
			
			
			
			$input['visitor_name'] = $req['name'];
			$input['visitor_email'] = $req['email'];			
			$input['visitor_phone'] = $req['phone'];			
			$input['visitor_subject'] = $req['subject'];			
			$input['visitor_comments'] = $req['message'];			
				
			//$output = view($input['template'],$input)->render(); echo $output;	die;	
			
			$result = $common->sendMail($input);
				
			return response()->json(['status'=>'success', 'message'=>'Message Sent successfully.']);
			
		}else{		
			$data['page'] = 'contact';
			$data['meta_title'] = 'Contact Us';
			$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';				
			return view('front.contact', $data);
		}		
    }	
	
    public function faq() {

		$data['page'] = 'faq';
		$data['meta_title'] = 'FAQs';
		$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';			
		$data['categories'] = FaqCategory::where('parent_id', 0)->where('status', 1)->get();
		$data['faqs'] = Faq::select('faqs.id', 'faqs.category_id', 'c.name AS category','faqs.faq_data','faqs.status AS status')->join('faq_categories AS c','c.id','=','faqs.category_id')->orderBy('faqs.created_at','ASC')->get();		
		return view('front.faq', $data);
    }

    public function providers() {
        $data['page'] = 'providers';
		$data['meta_title'] = 'Our Providers';
		$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';			
		$data['providers'] = array();
		$providers = User::select('users.*')->where('user_role',3)->where('status',1)->get();
		
		if($providers && $providers->count() > 0){
			$i=0;
			foreach($providers as $row){
				
				$categoryString = '';
				$categories = explode(',', $row->category);
				$category_names = ProviderCategory::whereIn('id', $categories)->select('name')->get();
				
				
				if($category_names && $category_names->count() > 0){
					$categories_arr = array();
					
					foreach ($category_names as $key => $value) {
						array_push($categories_arr, $value->name);
					}
					$categoryString = implode(', ', $categories_arr);
				}			
				
				if($row->image == ''){
					$provider_image = '/dist/images/user_icon.png';
				}else{
					$provider_image = '/'.$row->image;
				}
						
				$data['providers'][$i]['id'] = $row->id;
				$data['providers'][$i]['name'] = $row->name;
				$data['providers'][$i]['image'] = $provider_image;
				$data['providers'][$i]['provider_categories'] = $categoryString;
				
				$i++;
			}
		}		
        return view('front.providers', $data);
    }

    public function provider_detail($id) {
        $data['page'] = 'provider_detail';
		$data['meta_title'] = 'Provider Detail';
		$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';			
		$data['provider'] = array();
		$provider = User::select('users.*')->where('users.id',$id)->where('user_role',3)->where('status',1)->first();
		
		if($provider && $provider->count() > 0){
				
			$categoryString = '';
			$categories = explode(',', $provider->category);
			$category_names = ProviderCategory::whereIn('id', $categories)->select('name')->get();
			
			
			if($category_names && $category_names->count() > 0){
				$categories_arr = array();
				
				foreach ($category_names as $key => $value) {
					array_push($categories_arr, $value->name);
				}
				$categoryString = implode(', ', $categories_arr);
			}			
			
			if($provider->image == ''){
				$provider_image = '/dist/images/user_icon.png';
			}else{
				$provider_image = '/'.$provider->image;
			}
					
			$data['provider']['id'] = $provider->id;
			$data['provider']['name'] = $provider->name;
			$data['provider']['short_info'] = $provider->short_info;
			$data['provider']['clinical_expertise'] = $provider->clinical_expertise;
			$data['provider']['credentials'] = $provider->credentials;
			$data['provider']['awards'] = $provider->awards;
			$data['provider']['image'] = $provider_image;
			$data['provider']['provider_categories'] = $categoryString;
			
			//Other Providers
			
			$data['other_providers'] = array();
			$providers = User::select('users.*')->where('user_role',3)->where('status',1)->where('id','!=',$id)->limit(4)->get();
			
			if($providers && $providers->count() > 0){
				$i=0;
				foreach($providers as $row){
					
					$categoryString2 = '';
					$categories2 = explode(',', $row->category);
					$category_names2 = ProviderCategory::whereIn('id', $categories2)->select('name')->get();
					
					
					if($category_names2 && $category_names2->count() > 0){
						$categories_arr2 = array();
						
						foreach ($category_names2 as $key => $value) {
							array_push($categories_arr2, $value->name);
						}
						$categoryString2 = implode(', ', $categories_arr2);
					}			
					
					if($row->image == ''){
						$provider_image = '/dist/images/user_icon.png';
					}else{
						$provider_image = '/'.$row->image;
					}
							
					$data['other_providers'][$i]['id'] = $row->id;
					$data['other_providers'][$i]['name'] = $row->name;
					$data['other_providers'][$i]['image'] = $provider_image;
					$data['other_providers'][$i]['provider_categories'] = $categoryString2;
					
					$i++;
				}		
			}
			return view('front.provider_detail', $data);
		}else{
			return redirect()->route('home')->withErrors('Invalid Provider Data');			
		}
	
        
    }
	
    public function privacy_policy() {
		$data['meta_title'] = 'Privacy Policy';
		$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';			
        $data['page'] = 'privacy_policy';
        return view('front.privacy_policy', $data);
    }

    public function terms_conditions() {
		$data['meta_title'] = 'Terms & Conditions';
		$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';			
        $data['page'] = 'terms_conditions';
        return view('front.terms_conditions', $data);
    }

    public function terms_of_use() {		
        $data['page'] = 'terms_of_use';
		$data['meta_title'] = 'Terms of Use';
		$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';			
        return view('front.terms_of_use', $data);
    }


	public function get_services(Request $request) {	
	
		$output = '';
		
		if ($request->isMethod('post')) {
			$req = $request->all();
			
			if(isset($req['gender']) && $req['gender'] != '') { 
				$gender = $req['gender'];
				$output = '<option value="">Select consult type...</option>';
				
				$services = Service::where('status',1)
							->where(function($query) use ($gender){
								$query->orWhere('available_for',$gender);
								$query->orWhere('available_for','both');
							})
							->get();
				if($services && $services->count() > 0){
					foreach($services as $row){
						$output .= '<option value="'.$row->id.'">'.$row->name.'</option>';
					}
				}
			}
		}
		
		echo $output;
	}
	
    public function treatment(Request $request) {
		
		if ($request->isMethod('get')) {
			
			$req = $request->all();
			
			if (isset($req['g']) && $req['g'] != '' && isset($req['t']) && $req['t'] != '') {
				
				$data['page'] = 'treatment';
				$gender = trim($req['g']);
				$id = trim($req['t']);
				
				session()->put('online_visit_gender', $gender);
				session()->put('online_visit_service_id', $id);
				
				$data['service_detail'] = Service::where('id',$id)->where('status',1)->first();
				
				if($data['service_detail'] && $data['service_detail']->count() > 0){
					
					if($data['service_detail']->available_for == $gender || $data['service_detail']->available_for == 'both'){
						
					}else{
						return redirect()->route('home')->withErrors('Unfortunately this treatment is currently not available to '.$gender.'.');
					}
					
					$data['meta_title'] = $data['service_detail']->name;
					$data['meta_description'] = $data['service_detail']->short_info;
					
					$data['medicines'] = TreatmentMedicine::where('service_id',$id)->where('status',1)->get();
					
					return view('front.treatment', $data);
					
				}else{
					return redirect()->route('home')->withErrors('Unfortunately this treatment is currently not available.');
				}			
			}else{
				return redirect()->route('home');
			}
			
		}else{
			return redirect()->route('home');
		}		

    }

	public function medicineDetail($id){
			
		$data['page'] = 'medicine_detail';
					
		$data['medicine'] = TreatmentMedicine::where('id',$id)->where('status',1)->first();
		
		if($data['medicine'] && $data['medicine']->count() > 0){
			$data['meta_title'] = $data['medicine']->name;
			$data['meta_description'] = $data['medicine']->name;

			$data['service_detail'] = Service::where('id',$data['medicine']->service_id)->where('status',1)->first();
			
				
			if($data['service_detail'] && $data['service_detail']->count() > 0){
				
				$data['gender'] = session('online_visit_gender') ? session('online_visit_gender') : 'male';
				
				$data['medicine_faqs'] = MedicineFaq::where('medicine_id',$id)->get();
				
				return view('front.medicine_detail',$data);
			}else{
				return redirect()->route('home')->withErrors('No treatment found.');
			}
			
						
		}else{
			return redirect()->route('home')->withErrors('No medicine found.');
		}
	}
	
	public function joinnow(Request $request)
	{
		if($request->route('temp_url')){
			$temp_url = $request->route('temp_url');
			
			if(Route::has($temp_url)){
				session()->put('temp_url', $temp_url);
			}else{
				session()->put('temp_url', '');
			}
			
		}else{
			session()->put('temp_url', '');
		}

		//Check if user is already logged in.
		
		if($this->checkAuth() != false){
			$redirect_path = $this->checkAuth();
			return redirect()->route($redirect_path);
		}

		$data['page'] = 'signup';
		$data['meta_title'] = 'Signup';
		$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';
		return view('front.signup_default', $data);
			
	}
	
	public function signup(Request $request) {
		
		if($request->route('temp_url')){
			$temp_url = $request->route('temp_url');
			
			if(Route::has($temp_url)){
				session()->put('temp_url', $temp_url);
			}else{
				session()->put('temp_url', '');
			}
			
		}else{
			session()->put('temp_url', '');
		}
		
		//Check if user is already logged in.
		
		if($this->checkAuth() != false){
			$redirect_path = $this->checkAuth();
			return redirect()->route($redirect_path);
		} 
		
		$data['page'] = 'signup';
		$data['meta_title'] = 'Signup';
		$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';
		
		if(!Session::has('online_visit_service_id')){
						
			return view('front.signup_default', $data);	
			
		}else{
			$service_id = session('online_visit_service_id');
			$data['service_detail'] = Service::where('id',$service_id)->where('status',1)->first();
			return view('front.signup', $data);			
		}	
	}
	
	public function sendEmailOtp(Request $request, CommonService $common)
	{
		if($request->ajax()) {
			$messages = [
				'first_name.regex' => 'First Name field accepts alphabetical characters only.',
				'last_name.regex' => 'Last Name field accepts alphabetical characters only.',
				'password.regex' => 'Password must contain at least 8 characters <br> at least one number <br> both lower and uppercase letters <br> special characters.',
			];
			$validator = Validator::make($request->all(), [
				'first_name' => 'required|string|regex:/^[A-Za-z. -]+$/|max:120',
				'last_name' => 'required|string|regex:/^[A-Za-z. -]+$/|max:120',
				'email' => 'required|email|max:120|unique:users,email',
				'password' => 'required|max:100|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^\w\s]).{8,}$/',
				'password_confirmation' => 'required_with:password|same:password',
				'agree' =>'accepted'
			], $messages)->validate();
			
			
			$otp_code = rand(1000,9999);
			$min = env('OTP_VALID_MIN','5');
			$otp_valid_at = date('Y-m-d H:i:s',strtotime("+$min minutes"));
			session()->put('email_otp', $otp_code);		
			session()->put('otp_name', $request->input('name'));	
			session()->put('otp_email', $request->input('email'));
			//Email Sending
			$input['otp_code'] = $otp_code;
			$input['name'] = $request->input('name');
			$input['email'] = $request->input('email');
			$input['subject'] = 'OTP for email verification';
			$input['template'] = 'front.email_template.email_verification';
				
			$result = $common->sendMail($input);
			
			if($result){
				$msg = 'OTP sent successfully. Please check your email inbox.';
				return response()->json(['status'=> 'success', 'redirect'=> '', 'otp'=>$otp_code, 'message' => $msg]);					
				
			}else{
				$msg = 'Error occured.Please try again.';
				return response()->json(['status'=> 'error', 'redirect'=> '', 'message' => $msg]);
				
			}
		}
		else{
			echo $msg = 'Error occured. Please try again.';
			//return response()->json(['status'=> 'error', 'redirect'=> '', 'message' => $msg]);				
		}
		
	}
	
	public function resendEmailOtp(Request $request, CommonService $common)
	{
		$otp_code = rand(1000,9999);
		$min = env('OTP_VALID_MIN','5');
		$otp_valid_at = date('Y-m-d H:i:s',strtotime("+$min minutes"));
		session()->put('email_otp', $otp_code);		
		
		//Email Sending
		$input['otp_code'] = $otp_code;
		$input['name'] = $request->session()->get('otp_name');
		$input['email'] = $request->session()->get('otp_email');
		$input['subject'] = 'OTP for email verification';
		$input['template'] = 'front.email_template.email_verification';
			
		$result = $common->sendMail($input);
		if($result){
			$msg = 'OTP sent successfully. Please check your email inbox.';
			return response()->json(['status'=> 'success', 'redirect'=> '', 'otp'=>$otp_code, 'message' => $msg]);					
		}else{
			$msg = 'Error occured.Please try again.';
			return response()->json(['status'=> 'error', 'redirect'=> '', 'message' => $msg]);
		}
	}
	
	public function signupSubmit(Request $request, CommonService $common) {	
	
		$messages = [
			'email_otp.regex' => 'OTP must be 4 digit long.',
		];
		$validator = Validator::make($request->all(), [
			'email_otp' => 'required|digits:4'
		], $messages)->validate();
		
		if($request->session()->get('email_otp')==$request->email_otp)
		{
			$data = array(
			'first_name' => trim($request->first_name),
			'last_name' => trim($request->last_name),
			'name' => trim($request->first_name).' '.trim($request->last_name),
			'email' => trim($request->email),
			'password' => Hash::make($request->password),
			'is_admin' => 0,
			'user_role' => 4,
			'image' => 'dist/images/user_icon.png',
			'dob' => '0000-00-00',
			);
			
			$user = User::create($data);
			
			if($user){
				###### Send welcome email to replenisher #######
				$input['name'] = trim($request->first_name).' '.trim($request->last_name) ;
				$input['email'] = trim($request->email);
				$input['subject'] = 'Welcome to ReplenishMD';
				$input['template'] = 'front.email_template.welcome_email';
				$result = $common->sendMail($input);
				########### End Here ############
				if(Session::has('online_visit_service_id')){
					$redirect = route('online_visit_welcome');
					Auth::login($user); //login	
					session()->put('online_visit_patient_id', $user->id);
					
				}elseif(Session::has('temp_url') && session('temp_url') != ''){
					$temp_url = session('temp_url');
					$redirect = route($temp_url);
					Auth::login($user);  //login	
					
				}else{
					$redirect = route('sign_in');
				}
				session()->put('email_otp', '');
				return response()->json(['status'=> 'success', 'message' => 'Registered successfully.', 'redirect' => $redirect]);
			}
			else{
				return response()->json(['status'=> 'error', 'message' => 'Signup Error']);
			}	
		}
		else
		{
			session()->put('otp_name', trim($request->first_name).' '.trim($request->last_name));
			session()->put('otp_email', trim($request->email));
			return response()->json(['status'=> 'error', 'message' => 'Sorry!! invalid OTP.']);	
		}	
	}


	public function sign_in(Request $request) {	
	
		//Check if user is already logged in.
		
		if($this->checkAuth() != false){
			$redirect_path = $this->checkAuth();
			return redirect()->route($redirect_path);
		} 
	
		$data['page'] = 'sign_in';
		$data['meta_title'] = 'Sign In';
		$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';
					
		return view('front.sign_in', $data);
	}
	
    public function signinSubmit(Request $request){   
        $input = $request->all();
   
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);
   
        if(auth()->attempt(array('email' => $input['email'], 'password' => $input['password']))){
			
			if(auth()->user()->status == 1){
				
				if (auth()->user()->is_admin == 0 && (auth()->user()->user_role == 4) ) { //Patient
					$msg = 'Login Successfully.';	
					
					if(Session::has('online_visit_service_id')){
						$redirect = route('online_visit_welcome');
						$user_id = auth()->user()->id;
						session()->put('online_visit_patient_id', $user_id);
					}
					elseif(Session::has('temp_url') && session('temp_url') != ''){
						$temp_url = session('temp_url');
						$redirect = route($temp_url);
					}
					#Using for when user comes from forum page
					elseif(Session::has('return_url') && session('return_url') !='')
					{
						$fid = session()->get('fid');
						session()->forget('fid');
						$redirect = route('forum.posts',[$fid,0]);
					}
					#Using for when user comes from forum post page to do comment
					elseif(Session::has('post_comment_check') && session('post_comment_check') !='')
					{
						$url = session()->get('post_comment_check');
						session()->forget('post_comment_check');
						$url_segment = explode('/',$url);
						$redirect = route($url_segment[0],$url_segment[1]);
						
					}
					else{
						$redirect = (auth()->user()->is_first_login==1) ? route('user.user_preference') : route('user.dashboard');
						#Update user login (is_availbale) status in table
						User::where('id',auth()->user()->id)->update(array("is_available"=>1));
					}
				
					return response()->json(['status'=> 'success', 'redirect'=> $redirect, 'message' => $msg]);
					
				}
				elseif (auth()->user()->is_admin == 0 && auth()->user()->user_role == 3) 
				{ //Provider
				    if(auth()->user()->provider_verified == 1){
    					$msg = 'Login Successfully.';
    					$redirect = (auth()->user()->is_first_login==1) ? route('provider.provider_preference') : route('provider.dashboard');
    					
    					#Update user login (is_availbale) status in table
    					User::where('id',auth()->user()->id)->update(array("is_available"=>1));					
    					return response()->json(['status'=> 'success', 'redirect'=> $redirect, 'message' => $msg]);
				    }
					elseif(Session::has('return_url') && session('return_url') !='')
					{
						$msg = 'Login Successfully.';
						$fid = session()->get('fid');
						session()->forget('fid');
						$redirect = route('forum.posts',[$fid,0]);
						return response()->json(['status'=> 'success', 'redirect'=> $redirect, 'message' => $msg]);
					}
					else{
				        $msg = 'This account has not been verified please contact to Admin.';	
					    return response()->json(['status'=> 'error', 'redirect'=> '', 'message' => $msg]);
				    }
					
				}
				else{
					$msg = 'You are not authorized to login.';	
					return response()->json(['status'=> 'error', 'redirect'=> '', 'message' => $msg]);
				}				
			}
			else{
				$msg = 'You must be active to login.';
				return response()->json(['status'=> 'error', 'redirect'=> '', 'message' => $msg]);
			}

        }else{
		    $msg = 'Invalid login credentials';
			return response()->json(['status'=> 'error', 'redirect'=> '', 'message' => $msg]);			
        }     
    }
	
	
	public function forgot_password(Request $request, CommonService $common) {

		if ($request->ajax()) {
	   
			$this->validate($request, [
				'email' => 'required|email|exists:users,email'
			]);	

			$user = User::where('status',1)->where('email',$request->email)->first();
			
			if($user && $user->count() > 0){
				
				$otp_code = rand(1000,9999);
				$min = env('OTP_VALID_MIN','30');
				$otp_valid_at = date('Y-m-d H:i:s',strtotime("+$min minutes"));
				
				$input = array(
					'otp_code' => $otp_code,
					'otp_valid_at' => $otp_valid_at,
					'is_pass_req' => 1,
				);
				
				User::where('id',$user->id)->update($input);
				
				session()->put('temp_fp_uid', $user->id);		
				
				//Email Sending
				
				$input['name'] = $user->name;
				$input['email'] = $user->email;
				$input['subject'] = 'OTP for password reset';
				$input['template'] = 'front.email_template.forgot_password';
					
				$result = $common->sendMail($input);
				
				if($result){
					$msg = 'OTP sent successfully. Please check your email inbox.';
					$redirect = route('reset_password');					
					return response()->json(['status'=> 'success', 'redirect'=> $redirect, 'message' => $msg]);					
				}else{
					$msg = 'Error occured.Please try again.';
					return response()->json(['status'=> 'error', 'redirect'=> '', 'message' => $msg]);						
				}
				
				
			}else{
				$msg = 'User is not activated.';
				return response()->json(['status'=> 'error', 'redirect'=> '', 'message' => $msg]);				
			}
		}else{
			$data['page'] = 'forgot_password';
			$data['meta_title'] = 'Forgot Password';
			$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';
			return view('front.forgot_password', $data);			
		}
	}
		
	public function reset_password(Request $request){
		//otp verfication and new password updation
		if ($request->ajax()) {
						
			$messages = [
				'password.regex' => 'Password must contain at least 8 characters <br> at least one number <br> both lower and uppercase letters <br> special characters.',
			];
			$validator = Validator::make($request->all(), [
				'otp' => 'required|digits:4|exists:users,otp_code',
				'password' => 'required|max:100|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^\w\s]).{8,}$/',
				'password_confirmation' => 'required_with:password|same:password'
			], $messages)->validate();	

			$input_data = array(
				'password' => Hash::make($request->password),
				'is_pass_req' => 0,
				'otp_code' => NULL,
				'otp_valid_at' => NULL,
			);
			
			$id = session('temp_fp_uid');
			
			User::where('id',$id)->update($input_data);
			
			session()->forget('temp_fp_uid');
			
			$msg = 'Password reset successfully';
			$redirect = route('sign_in');	

			return response()->json(['status'=> 'success', 'redirect'=> $redirect, 'message' => $msg]);			
		
		}else{
				
			if(Session::has('temp_fp_uid') && session('temp_fp_uid') != '' ){
				$id = session('temp_fp_uid');
				
				$user = User::where('status',1)->where('id',$id)->first();
				
				if($user && $user->count() > 0){
					$now = date('Y-m-d H:i:s');
					
					if($user->otp_valid_at > $now){
						$data['page'] = 'reset_password';
						$data['meta_title'] = 'Enter OTP and Update New Password';
						$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';
					
					return view('front.reset_password', $data);							
					}else{
						return redirect()->route('forgot_password')->withErrors('OTP expired.Please try again.');	
					}
			
				}else{
					return redirect()->route('forgot_password')->withErrors('Error occured. Please try again.');	
				}
				
			}else{
				return redirect()->route('forgot_password')->withErrors('Error occured. Please try again.');	
			}				
		}	
	}

	public function provider_signup(Request $request) {
		
		if($this->checkAuth() != false){
			$redirect_path = $this->checkAuth();
			return redirect()->route($redirect_path);
		} 
		
		$data['page'] = 'signup';
		$data['meta_title'] = 'Signup';
		$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';
		$data['services'] = Service::where('status',1)->get();
		return view('front.providersignup',$data);	
	}

	public function providersignupAccountInfo(Request $request)
	{
		if ($request->ajax()) {
						
			$messages = [
				'email_id.regex' => 'Email-id is required field.',
				'pwd.regex' => 'Password must contain at least 8 characters, at least one number and both lower and uppercase letters and special characters.',
				'cpwd.regex' => 'Confirm password must be same as password field.',
			];
			$validator = Validator::make($request->all(), [
			
				'email_id' => 'required|email|max:120|unique:users,email',
				'pwd' => 'required|max:100|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^\w\s]).{8,}$/',
				'cpwd' => 'required_with:pwd|same:pwd',
			], $messages)->validate();
		
			return response()->json(['status'=> 'success', 'redirect'=> '', 'message' => '']);
		}
		else{
			return response()->json(['status'=> 'error', 'message' => 'This request is not valid.', 'data' => '']);
		}
		
	}
	
	public function providersignupPernlInfo(Request $request)
	{
		if ($request->ajax()) {
						
			$messages = [
				'first_name.regex' => 'First name is required field.',
				'last_name.regex' => 'Last name is required field.',
				'phone_number.regex' => 'Phone number is required field.',
			];
			$validator = Validator::make($request->all(), [
				'first_name' => 'required',
				'last_name' => 'required',
				'phone_number' => 'required|digits:10',
			], $messages)->validate();
		
			return response()->json(['status'=> 'success', 'redirect'=> '', 'message' => '']);
		}
		else{
			return response()->json(['status'=> 'error', 'message' => 'This request is not valid.', 'data' => '']);
		}
	}
	
	public function resendProviderEmailOtp(Request $request, CommonService $common)
	{
		$otp_code = rand(1000,9999);
		$min = env('OTP_VALID_MIN','5');
		$otp_valid_at = date('Y-m-d H:i:s',strtotime("+$min minutes"));
		session()->put('email_otp', $otp_code);
		
		
		//Email Sending
		$input['name'] = $request->session()->get('otp_name');
		$input['email'] = $request->session()->get('otp_email');
		$input['subject'] = 'OTP for email verification';
		$input['template'] = 'front.email_template.email_verification';
			
		$result = $common->sendMail($input);
		if($result){
			$msg = 'OTP sent successfully. Please check your email inbox.';
			return response()->json(['status'=> 'success', 'redirect'=> '', 'otp'=>$otp_code, 'message' => $msg]);					
		}else{
			$msg = 'Error occured.Please try again.';
			return response()->json(['status'=> 'error', 'redirect'=> '', 'message' => $msg]);
		}
	}
	
	public function providersignupProfsInfo(Request $request, CommonService $common)
	{
		if ($request->ajax()) {
			$messages = [
				'service.regex' => 'Service is required filed.',
				/*'cred_img[].regex' => 'Credential is required filed.',*/
				'profile_img.regex' => 'Profile image is required filed.',
			];
			$validator = Validator::make($request->all(), [
				'service' => 'required',
				/*'cred_img[]' => 'required|image|mimes:jpeg,png,jpg,svg|max:2048',*/
				'profile_img' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
			], $messages)->validate();
			
			$otp_code = rand(1000,9999);
			$min = env('OTP_VALID_MIN','5');
			$otp_valid_at = date('Y-m-d H:i:s',strtotime("+$min minutes"));
			session()->put('provider_email_otp', $otp_code);
			session()->put('otp_name', $request->name);	
			session()->put('otp_email', $request->email);
			session()->put('service', $request->service);
			
			//Email Sending
			$input['otp_code'] = $otp_code;
			$input['name'] = $request->name;
			$input['email'] = $request->email;
			$input['subject'] = 'OTP for email verification';
			$input['template'] = 'front.email_template.email_verification';
			$result = $common->sendMail($input);
			if($result){
				$msg = 'OTP sent successfully. Please check your email inbox.';
				return response()->json(['status'=> 'success', 'redirect'=> '', 'otp'=>$otp_code, 'message' => $msg]);					
			}else{
				$msg = 'Error occured.Please try again.';
				return response()->json(['status'=> 'error', 'redirect'=> '', 'message' => $msg]);
			}			
		}
		else{
			return response()->json(['status'=> 'error', 'message' => 'This request is not valid.', 'data' => '']);
		}
	}
	
	
	public function providersignupSubmit(Request $request, CommonService $common) {	
		$messages = [
			'enteredOtp.regex' => 'OTP must be 4 digit long.',
		];
		$validator = Validator::make($request->all(), [
			'enteredOtp' => 'required|digits:4',
		], $messages)->validate();
		
		if($request->session()->get('provider_email_otp')==$request->enteredOtp)
		{
			/****Upload credential and profile pic****/
			$status = 'error';
			$message = 'Error occured';
			
			if (!empty($request->cred_img)) {
				$num = count($request->file('cred_img'));				
				$cred_images=array();
				for($i=0; $i<$num; $i++)
				{
					$file = $request->file('cred_img')[$i];
					if(strtolower($file->getClientOriginalExtension()) == 'jpg' || strtolower($file->getClientOriginalExtension()) == 'png' || strtolower($file->getClientOriginalExtension()) == 'jpeg' ||  strtolower($file->getClientOriginalExtension()) == 'pdf'){
						$name=time().'-'.rand(11111111,99999999).'.'.strtolower($file->getClientOriginalExtension());
						$destinationPath = 'uploads/users/';
						
						if($file->move($destinationPath, $name)){
							$image = 'uploads/users'.'/'.$name;
							$status = 'success';
							$message = 'Profile pic uploaded successfully.';
							$cred_images[]= $image;
						}
						
					}
					else{
						$status = 'error';
						$message = 'Only .jpg, .jpeg and .png files are allowable';					
					}	
				}
				
			}
			
			
			if (!empty($request->file('profile_img'))) {
				$file = $request->file('profile_img');
				
				if(strtolower($file->getClientOriginalExtension()) == 'jpg' || strtolower($file->getClientOriginalExtension()) == 'png' || strtolower($file->getClientOriginalExtension()) == 'jpeg'){
					$name=time().'-'.rand(11111111,99999999).'.'.strtolower($file->getClientOriginalExtension());
					$destinationPath = 'uploads/users/';
					
					if($file->move($destinationPath, $name)){
						$image = 'uploads/users'.'/'.$name;
						$status = 'success';
						$message = 'Profile pic uploaded successfully.';				
					}				
				}
				else{
					$status = 'error';
					$message = 'Only .jpg, .jpeg and .png files are allowable';					
				}
			}else{
				$image = 'dist/images/user_icon.png';
			}
			
			
			$data = array(
				'first_name' => trim(ucfirst($request->first_name)),
				'last_name' => trim(ucfirst($request->last_name)),
				'name' => trim(ucfirst($request->first_name)).' '.trim(ucfirst($request->last_name)),
				'email' => trim($request->email_id),
				'phone' => trim($request->phone),
				'password' => Hash::make($request->pwd),
				'is_admin' => 0,
				'status' => 0,
				'user_role' => 3,
				'dob' => '0000-00-00',
				'image' =>$image,
				'provider_licenses' =>json_encode($cred_images)
			);		
			
			$user = User::create($data);
			
			if($user){
			    
			    if(!empty($request->service)){
					$service = explode(",",$request->service);
					foreach($service as $val){
						
						ProviderService::create(['provider_id'=>$user->id, 'service_id'=>$val,'status'=>1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
					}
					
				}
				//Auth::login($user); //login	
				//$redirect = route('provider.dashboard');
				//$redirect = (auth()->user()->is_first_login==1) ? route('provider.provider_preference') : route('provider.dashboard');
				//$redirect = route('sign_in');
				$redirect = route('provider_message');
				//$redirect = route('provider.provider_preference');
        		
        		###### Send welcome email to replenisher #######
				$input1['name'] = trim(ucfirst($request->first_name)).' '.trim(ucfirst($request->last_name));
				$input1['email'] = trim($request->email_id);
				$input1['subject'] = 'Welcome to ReplenishMD';
				$input1['template'] = 'front.email_template.welcome_email';
				$result = $common->sendMail($input1);
				########### End Here ############
        		
				
				###### Send email to admin for account verification ######
        		$input['name'] = trim(ucfirst($request->first_name)).' '.trim(ucfirst($request->last_name));
                $input['email'] = env('ADMIN_EMAIL');
                $input['subject'] = 'request verified account';
                $input['template'] = 'front.email_template.verified_request';
                
                $input['provider_name'] = trim(ucfirst($request->first_name)).' '.trim(ucfirst($request->last_name));
                $input['admin_name'] = "Admin";
        		
        		$result = $common->sendMail($input);
        		############### End Here #################
        		
        		return response()->json(['status'=> 'success', 'message' => 'Provider registered successfully.', 'redirect' => $redirect]);
			}
			else{
				return response()->json(['status'=> 'error', 'message' => 'Signup Error']);
			}
		}
		else
		{
			session()->put('otp_name', trim($request->first_name).' '.trim($request->last_name));
			session()->put('otp_email', trim($request->email));
			return response()->json(['status'=> 'error', 'message' => 'Sorry!! invalid OTP.']);	
		}
			
	}

	public function online_visit_welcome(Request $request) {	
		$data['page'] = 'welcome';
		$data['meta_title'] = 'Online Visit Welcome';
		$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';
		

		if(!Session::has('online_visit_patient_id')){
			return redirect('home')->withErrors('Please choose treatment and signup first.');
		}
		
		$service_id = session('online_visit_service_id');
		$user_id = session('online_visit_patient_id');
		
		$data['service_detail'] = Service::where('id',$service_id)->where('status',1)->first();
		$data['user'] = User::where('id',$user_id)->first();
		
		if($data['service_detail']->count() > 0 && $data['user']->count() > 0){
			return view('front.online_visit_welcome', $data);
		}else{
			return redirect('home')->withErrors('Please choose treatment and signup first.');
		}				
	}

	public function online_visit_basics(Request $request) {	
		$data['page'] = 'basics';
		$data['meta_title'] = 'Online Visit Basics';
		$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';
		

		if(!Session::has('online_visit_patient_id')){
			return redirect('home')->withErrors('Please choose treatment and signup first.');
		}
		
		$service_id = session('online_visit_service_id');
		$user_id = session('online_visit_patient_id');
		
		$data['service_detail'] = Service::where('id',$service_id)->where('status',1)->first();
		$data['user'] = User::where('id',$user_id)->first();
		//echo date(env('DATE_FORMAT_PHP'),strtotime($data['user']->dob));die;
		
		if($data['service_detail']->count() > 0 && $data['user']->count() > 0){
			$data['max_date'] = date('Y-m-d', strtotime('-18 years'));
			return view('front.online_visit_basics', $data);
		}else{
			return redirect('home')->withErrors('Please choose treatment and signup first.');
		}				
	}
	
	public function basicsSubmit(Request $request) {
		
		$user_id = session('online_visit_patient_id');	
		$service_id = session('online_visit_service_id');
		$gender = $request->gender;
		
		$user = User::where('id',$user_id)->first();
		$service_detail = Service::where('id',$service_id)->where('status',1)->first();

		if($service_detail->count() > 0 && $user->count() > 0){
			

			if($service_detail->available_for == $gender || $service_detail->available_for == 'both'){
				$messages = [
					'zip_code.exists' => 'Only Texas zipcodes are allowable.',
				];
				$gender_rule = '';
			}else{
				if($gender == 'male'){
					$gender1 = 'female';
				}else{
					$gender1 = 'male';
				}
				$messages = [
					'gender.in' => 'Unfortunately this treatment is currently not available to '.$gender1.'.',
					'zip_code.exists' => 'Only Texas zipcodes are allowable.',
				];	
				$gender_rule = '|in:'.$gender1;
			}

			$validator = Validator::make($request->all(), [
				'gender' => 'required|string'.$gender_rule,
				'birth_date' => 'required|date',			
				'zip_code' => 'required|digits:5|exists:zip_codes,zipcode',
				'phone_number' => 'required|digits:10',
			], $messages)->validate();	

			$data = array(
				'gender' => trim($request->gender),
				'dob' => date('Y-m-d',strtotime(trim($request->birth_date))),
				'zip_code' => trim($request->zip_code),
				'phone' => trim($request->phone_number),
			);
			
			User::where('id',$user_id)->update($data);	
			
			return response()->json(['status'=> 'success', 'message' => 'Success']);			
		
		}else{
			return response()->json(['status'=> 'error', 'message' => 'Error Occured.']);
		}		
	}
		
	public function medical_questions(Request $request, CommonService $common) {	
		$data['page'] = 'medical_questions';
		$data['meta_title'] = 'Online Visit Medical Questions';
		$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';

		if(!Session::has('online_visit_patient_id')){
			return redirect('home')->withErrors('Please choose treatment and signup first.');
		}
		
		$service_id = session('online_visit_service_id');
		$user_id = session('online_visit_patient_id');
		
		$data['service_detail'] = Service::where('id',$service_id)->where('status',1)->first();
		$data['user'] = User::where('id',$user_id)->first();
		
		if($data['service_detail']->count() > 0 && $data['user']->count() > 0){
			
			$result = $common->getMedicalQuestions($service_id);

			$data['medical_questions'] = $result['questionData'];

			
			$data['visit_form'] = $result['visit_form'];
			
			if($data['visit_form'] && $data['visit_form']->count() > 0 && $data['medical_questions']){
				session()->put('online_visit_form_id', $data['visit_form']->id);					
				return view('front.medical_questions', $data);
			}else{
				return redirect('home')->withErrors('This online visit is currently not available.');
			}
			
			
		}else{
			return redirect('home')->withErrors('Please choose treatment and signup first.');
		}				
	}
	
	public function onlineVisitSubmit(Request $request) {
		
		$user_id = session('online_visit_patient_id');	
		$service_id = session('online_visit_service_id');
		$online_visit_form_id = session('online_visit_form_id');
		
		$user = User::where('id',$user_id)->first();
		$service_detail = Service::where('id',$service_id)->where('status',1)->first();
		$visit_form = VisitForm::where('id', $online_visit_form_id)->where('status',1)->first();

		if($service_detail->count() > 0 && $user->count() > 0 && $visit_form->count() > 0){
			
			$req = $request->all();

			//echo '<pre>'; print_R($req); die;
			
			//Option Answers
			if(isset($req['answers']) && count($req['answers']) > 0){

				if(Session::has('online_visit_id') && session('online_visit_id') != ''){
					$online_visit_id = session('online_visit_id');
				}else{
					$i_data2 = array(
						'visit_type' => $service_detail->visit_type,
						'patient_id' => $user->id,
						'service_id' => $service_detail->id,
						'visit_amount' => 0,
						'visit_status' => 'PENDING ACCEPTANCE',
						'payment_status' => 'null',
						'is_new' => 1,
						'is_submit' => 0,
					);
					
					$online_visit = OnlineVisit::create($i_data2);
					$online_visit_id = $online_visit->id;
					session()->put('online_visit_id', $online_visit_id);
				}
			

			//Delete Old Answer if updating
				VisitAnswer::where('online_visit_id',$online_visit_id)->delete();

				if(isset($req['answers']['option']) && count($req['answers']['option']) > 0){
					$option_answers = $req['answers']['option'];

					foreach($option_answers as $q => $options){

						if($options){
							foreach($options as $key => $val){

								if($val != ''){
									$question_id = $q;
									$answer_type = 'option';
									$answer_text = NULL;
									$answer_option_id = $val;

									$data = array(
										'user_id' => $user_id,
										'service_id' => $service_id,
										'online_visit_form_id' => $online_visit_form_id,
										'online_visit_id' => $online_visit_id,
										'question_id' => $question_id,
										'answer_type' => $answer_type,
										'answer_text' => $answer_text,
										'answer_option_id' => $answer_option_id,
									); 
									
									VisitAnswer::create($data);
								}
							}
						}
					}						
											
				}
				
				//Text Answers
				if(isset($req['answers']['text']) && count($req['answers']['text']) > 0){
					$text_answers = $req['answers']['text'];

					foreach($text_answers as $q => $val){

						if($val != ''){
							$question_id = $q;
							$answer_type = 'text';
							$answer_text = $val;
							$answer_option_id = NULL;

							$data = array(
								'user_id' => $user_id,
								'service_id' => $service_id,
								'online_visit_form_id' => $online_visit_form_id,
								'online_visit_id' => $online_visit_id,
								'question_id' => $question_id,
								'answer_type' => $answer_type,
								'answer_text' => $answer_text,
								'answer_option_id' => $answer_option_id,
							); 
							
							VisitAnswer::create($data);
						}						
					}					
				}				

			}	
			
			session()->put('online_visit_questions_done', true);

			return response()->json(['status'=> 'success', 'message' => 'Success']);			
		
		}else{
			return response()->json(['status'=> 'error', 'message' => 'Error Occured.']);
		}		
	}
	
	
	public function treatment_preference(Request $request) {	
		$data['page'] = 'treatment_preference';
		$data['meta_title'] = 'Online Visit - Treatment Preference';
		$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';
		
		session()->put('online_visit_questions_done', true);

		if(!Session::has('online_visit_patient_id')){
			return redirect('home')->withErrors('Please login first.');
		}

		if(!Session::has('online_visit_service_id')){
			return redirect('home')->withErrors('Please choose treatment first.');
		}
		
		if(!Session::has('online_visit_questions_done')){
			return redirect('medical_questions')->withErrors('Please complete online visit questions first.');
		}			
		
		$service_id = session('online_visit_service_id');
		$user_id = session('online_visit_patient_id');
		
		$data['service_detail'] = Service::where('id',$service_id)->where('status',1)->first();
		$data['user'] = User::where('id',$user_id)->first();
		
		if($data['service_detail']->count() > 0 && $data['user']->count() > 0){

			$data['drugs'] = TreatmentMedicine::where('service_id',$service_id)->where('status',1)->get();
			$data['states'] = State::get();
			
			return view('front.treatment_preference', $data);
		}else{
			return redirect('home')->withErrors('Some Error occured.');
		}				
	}
	
	public function getMedicineVariants(Request $request){
		$user_id = session('online_visit_patient_id');	
		$service_id = session('online_visit_service_id');
		$online_visit_form_id = session('online_visit_form_id');
		
		$user = User::where('id',$user_id)->first();
		$service_detail = Service::where('id',$service_id)->where('status',1)->first();
		$visit_form = VisitForm::where('id', $online_visit_form_id)->where('status',1)->first();

		if($service_detail->count() > 0 && $user->count() > 0 && $visit_form->count() > 0){
			
			$req = $request->all();

			if(isset($req['drug_id'])){

				$medicine = TreatmentMedicine::where('id',$req['drug_id'])->where('status',1)->first();
				
				if($medicine && $medicine->count() > 0){
					$variants = MedicineVariant::where('service_id',$service_id)
									->where('medicine_id',$req['drug_id'])
									->where('status',1)
									->orderBy('is_popular','DESC')
									->get();
					
					if($variants && $variants->count() > 0){
						
						$output = view('front.get_medicine_variants',array('variants'=>$variants, 'medicine' => $medicine))->render();
						return response()->json(['status'=> 'success', 'message' => 'Data found', 'data' => $output]);
					}else{
						return response()->json(['status'=> 'error', 'message' => 'No data found', 'data'=> '']);
					}					
				}else{
					return response()->json(['status'=> 'error', 'message' => 'No drug found', 'data'=> '']);
				}

								
			}else{
				return response()->json(['status'=> 'error', 'message' => 'Please select drug.', 'data'=> '']);
			}		
		}else{
			return response()->json(['status'=> 'error', 'message' => 'Error Occured.', 'data' => '']);
		}		
	}
	
	public function getTreatmentReview(Request $request){
		$user_id = session('online_visit_patient_id');	
		$service_id = session('online_visit_service_id');
		$online_visit_form_id = session('online_visit_form_id');
		
		$user = User::where('id',$user_id)->first();
		$service_detail = Service::where('id',$service_id)->where('status',1)->first();
		$visit_form = VisitForm::where('id', $online_visit_form_id)->where('status',1)->first();

		if($service_detail->count() > 0 && $user->count() > 0 && $visit_form->count() > 0){
			
			$req = $request->all();

			$check = TreatmentMedicine::where('service_id',$service_id)->where('status',1)->get();
			
			if($check && $check->count() > 0){

				if(isset($req['drug_id'])){

					$medicine = TreatmentMedicine::where('id',$req['drug_id'])->where('status',1)->first();
					
					if($medicine && $medicine->count() > 0){
						$variant = MedicineVariant::where('service_id',$service_id)
										->where('id',$req['variant_id'])
										->where('status',1)
										->first();
						
						if($variant && $variant->count() > 0){
							
							//echo '<Pre>';print_r($variant);die;
/* 							if($variant->is_recurring == 1 || $variant->is_recurring == 2){
								$type = 1;
							}else{
								$type = 0;
							} */
							session()->put('shipping_address', $req['shipping_address']); //array
							session()->put('medicine_variant_id', $variant->id); //string
							session()->put('medicine_id', $variant->medicine_id); //string
							session()->put('medicine_plan_interval', $variant->plan_interval); //string
							session()->put('medicine_qty', $req['quantity']); //string
							//session()->put('medicine_variant_payment_type', $type ); //string
							
							$data['quantity'] = $req['quantity'];
							$data['total_price'] = $data['quantity'] * $variant->price;
							$data['months'] = 1;													
							$data['amount'] = $data['months'] * $data['total_price'];						
							$data['final_amount'] = $data['amount'];							
							
							session()->put('final_amount', $data['final_amount']);
								
							$output = view('front.get_treatment_review',array('data' => $data, 'variant'=>$variant, 'medicine' => $medicine))->render();
													
							return response()->json(['status'=> 'success', 'message' => 'Data found', 'data' => $output]);
						}else{
							return response()->json(['status'=> 'error', 'message' => 'No data found', 'data'=> '']);
						}					
					}else{
						return response()->json(['status'=> 'error', 'message' => 'No drug found', 'data'=> '']);
					}
									
				}else{
					return response()->json(['status'=> 'error', 'message' => 'Please select drug.', 'data'=> '']);
				}
			
			}else{
				
				//treatment has no medicines
				session()->put('shipping_address', $req['shipping_address']); //array
				
				$data['amount'] = 50; //treatment amount only
				$data['promo_price'] = 15;
				$data['final_amount'] = $data['amount'] - $data['promo_price'];
				
				session()->put('final_amount', $data['final_amount']);
					
				$output = view('front.get_no_medicine_treatment_review',array('data' => $data, 'service' => $service_detail))->render();
				
				return response()->json(['status'=> 'success', 'message' => 'Data found', 'data' => $output]);
							
			}
				
		}else{
			return response()->json(['status'=> 'error', 'message' => 'Error Occured.', 'data' => '']);
		}		
	}
	
	public function payment_page($p_type,$intveral=null){
		if($p_type){
			session()->put('medicine_variant_payment_type', $p_type ); //string
			session()->put('total_interval', $intveral );
		}
		
        $intent = SetupIntent::create(
            [], Cashier::stripeOptions()
        );
		
		session()->put('is_being_payment', 1); //1 for yes, 0 for No, 2 for finished
		
		if(!empty(auth()->user()->id)){
			$user_id=auth()->user()->id;
			$card_info = DB::table('payment_card_info')->select('*')->where(array('user_id' => $user_id))->orderBy('card_id','DESC')->get();
			
			if(!empty($card_info)){
				$intent['card_info']= $card_info;
				
			}
			$card_primary = DB::table('payment_card_info')->select('*')->where(array('user_id' => $user_id,'is_primary'=>1))->orderBy('card_id','DESC')->get();
			if(!empty($card_primary) && !empty($card_primary[0]->card_id)){
				$intent['card_primary']= $card_primary[0]->card_id;
			}else{
				$intent['card_primary']="new";
			}
		}
        return view('front.payment_page', compact('intent'));
	}
	
	public function success_page(){

		if(Session::has('is_being_payment') && session('is_being_payment') == 2){
								
			$data['online_visit'] = $online_visit = OnlineVisit::where('id', session('online_visit_id'))->where('payment_status','done')->first();

			if($data['online_visit'] && $data['online_visit']->count() > 0){
				
				#Assign randomly provider id to assynchronus gig
				if($data['online_visit']->visit_type == 1)
				{
					$provider = $story = DB::select( DB::raw("select `provider_services`.`provider_id` FROM `provider_services` LEFT JOIN `users` ON `provider_services`.`provider_id`=`users`.`id` WHERE `provider_services`.`service_id`=".session('online_visit_service_id')." AND `users`.`status`=1 order by rand() limit 1"));
					
					$gigData = array(
					'online_visit_id' => session('online_visit_id'),
					'provider_id' => $provider[0]->provider_id
					);
					OnlineVisitProviderAssign::create($gigData);

					#Send notification to alloted provider
					if(!empty(auth()->user()->image)){
						$img =auth()->user()->image;
					}else{
						$img='dist/images/user_icon.png';
					}
					$noticeData = array(
						'created_id' => auth()->user()->id,
						'created_name' => auth()->user()->name,
						'created_image' => $img,
						'notification_type' => "add_appointment",
						'post_id' => session('online_visit_id'),
						'followers_id' => $provider[0]->provider_id,
						'status' => 1,
						'created_at' => date('Y-m-d H:i:s'),
						'updated_at' => date('Y-m-d H:i:s')
					);
					
					$notice = Notifications::create($noticeData);
					$noticeData['id']=$notice->id;
					event(new Add_appointment($noticeData));

					#Send payment notification to admin
					$payment_data = PaymentLogs::select("*")->where('online_visit_id',session('online_visit_id'))->where('userid',auth()->user()->id)->orderBy('id','DESC')->first();
					if(!empty(auth()->user()->image)){
						$img =auth()->user()->image;
					}
					else
					{
						$img='dist/images/user_icon.png';
					}

					$paymentData = array(
						'created_id' => auth()->user()->id,
						'created_name' => auth()->user()->name,
						'created_image' => $img,
						'notification_type' => "admin",
						'notice_type' => 'asynchronous_visit_payment',
						'post_id' => session('online_visit_id'),
						'followers_id' => $provider[0]->provider_id,
						'status' => 1,
						'created_at' => date('Y-m-d H:i:s'),
						'updated_at' => date('Y-m-d H:i:s')
					);
					
					$notice = Notifications::create($paymentData);
					$paymentData['id']=$notice->id;
					$paymentData['message_note']= 'Payment amount '.$payment_data['amount'].'$ paid by '.auth()->user()->name.' for Asynchronous Visit.';
					event(new Admin_notification($paymentData));

				}
				######## End Here ############################
				
				$input = array(
					'online_visit_id' => $online_visit->id,
					'patient_id' => auth()->user()->id,
					'provider_id' => 0,
					'status' => 0
					
				);	
		
				OnlineVisitPaywallFeature::create($input);

				session()->forget('online_visit_gender');
				session()->forget('online_visit_service_id');				
				session()->forget('online_visit_patient_id');
				session()->forget('online_visit_form_id');
				session()->forget('online_visit_questions_done');
				session()->forget('shipping_address');
				session()->forget('is_being_payment');
				session()->forget('medicine_variant_id');
				session()->forget('medical_id');			
				session()->forget('final_amount');		
				session()->forget('medicine_variant_payment_type');				
				session()->forget('medicine_qty');					
				session()->forget('online_visit_id');
				session()->forget('medicine_plan_interval');					
				session()->forget('total_interval');
				
				$data['page'] = 'payment_success';
					
				return view('front.success_page', $data);
			}else{
				session()->put('is_being_payment', 1); //1 for yes, 0 for No, 2 for finished
				redirect('payment_page')->withErrors('Payment failed');
			}
		}else{
			session()->put('is_being_payment', 1); //1 for yes, 0 for No, 2 for finished
			redirect('payment_page')->withErrors('Payment failed');
		}
	}

	public function checkAuth(){
		
		if(Auth::check()) {
			
			if(auth()->user()->user_role == 4){
				if(Session::has('online_visit_service_id')){
					$redirect_path = 'online_visit_welcome';
					$user_id = auth()->user()->id;
					session()->put('online_visit_patient_id', $user_id);
					
				}elseif(Session::has('temp_url') && session('temp_url') != ''){
					$temp_url = session('temp_url');
					$redirect_path = $temp_url;
					
				}else{
					$redirect_path = 'user.dashboard';
					
				}
			}elseif(auth()->user()->user_role == 3){
				$redirect_path = 'provider.dashboard';
			}else{
				$redirect_path = '/login';
			}
			
			return $redirect_path;			
		
		}else{
			return false;
		}		
	}
	
	public function profile_image(Request $request){
		
		$status = 'error';
		$message = 'Error occured';
		
		if (!empty($request->file('image'))) {
            $file = $request->file('image');
			
			if(strtolower($file->getClientOriginalExtension()) == 'jpg' || strtolower($file->getClientOriginalExtension()) == 'jpeg' ||  strtolower($file->getClientOriginalExtension()) == 'png'){
				$name=time().'-'.rand(11111111,99999999).'.'.strtolower($file->getClientOriginalExtension());
				$destinationPath = 'uploads/users/';
				
				if($file->move($destinationPath, $name)){
					$image = 'uploads/users'.'/'.$name;
					$imagefilepath = realpath($image);
					
				
					if(FaceDetect::extract($imagefilepath)->face_found){
						
						$input_data = [
							'image' => $image,
							'updated_at' => date('Y-m-d H:i:s')
						];	
						
						User::where('id',auth()->user()->id)->update($input_data);					
						
						$status = 'success';
						$message = 'Photo uploaded successfully.';
					}else{
						$status = 'error';
						$message = 'No face is detected in this photo.';					
					}				
				}				
			}else{
				$status = 'error';
				$message = 'Only .jpg and .png files are allowable';					
			}

        }
		
        return response()->json(['status'=>$status, 'message'=>$message]);		
	}	

	public function id_card_image(Request $request){
		
		$status = 'error';
		$message = 'Error occured';
		
		if (!empty($request->file('image'))) {
            $file = $request->file('image');
			
			//echo strtolower($file->getClientOriginalExtension());die;
			if(strtolower($file->getClientOriginalExtension()) == 'jpg' || strtolower($file->getClientOriginalExtension()) == 'png'){
				$name='ID-'.time().'-'.rand(11111111,99999999).'.'.strtolower($file->getClientOriginalExtension());
				$destinationPath = 'uploads/users/';
				
				if($file->move($destinationPath, $name)){
					$image = 'uploads/users'.'/'.$name;
						
					$input_data = [
						'id_card_image' => $image,
						'updated_at' => date('Y-m-d H:i:s')
					];	
					
					User::where('id',auth()->user()->id)->update($input_data);					
					
					$status = 'success';
					$message = 'ID card uploaded successfully.';
									
				}				
			}else{
				$status = 'error';
				$message = 'Only jpg and png files are allowable';					
			}

        }
		
        return response()->json(['status'=>$status, 'message'=>$message]);		
	}

	public function mychat($id){
		if($id != '')
		{
			$data['sender'] = auth()->user();
			$data['chat_user'] = User::where('id',$id)->where('status',1)->first();
			if($data['chat_user'] && $data['chat_user']->count() > 0){
				return view('front.chat',$data);
			}
			else
			{
				return redirect()->back();
			}
		}
		else
		{
			return redirect()->back();
		}
	}

 	public function myvideo($id){
		if($id != ''){
			$data['chat_user'] = User::where('id',$id)->where('status',1)->first();
			
			if($data['chat_user'] && $data['chat_user']->count() > 0 && $data['chat_user']->chat_id != NULL){
				
				return view('front.video',$data);
			}else{
				return redirect()->back();
			}
		}else{
			return redirect()->back();
		}
	}	
	
	public function setChatId(Request $request){
		$chat_id = $request->chat_id;
		$user_email = $request->user_email;
		
		User::where('email',$user_email)->update(array('chat_id' => $chat_id));
		
		return true;
		
	}
	
	public function gc_callback(Request $request){
		echo '<pre>';print_r($request);die;
	}
	
	public function provider_message(){
	    
	    return view('front.provider_message');
	    
	}

	#blog listing
	public function blogs(){
		
		$now = date('Y-m-d H:i:s');
		$data['page'] = 'blogs';
		$data['meta_title'] = 'Blogs';
		$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';	
		
		$data['categories_data'] = DB::table('articles')
                ->select('blog_categories.id','blog_categories.name', 'blog_categories.category_img', DB::raw('count(*) as total'))
				->join('blog_categories', function ($join) {
					$join->on('blog_categories.id', '=', 'articles.blog_category_id');
				})				
				->where(function($query) use ($now){
					$query->orWhere('articles.post_publish', '=', 1);
					$query->orWhere(function($query2) use ($now){
						$query2->where('articles.post_publish', '=', 2);
						$query2->where('articles.scheduled_at', '>=', $now);
					});
				})
				->where('articles.is_verify',1)				 
                ->groupBy('articles.blog_category_id')
                ->get();			
		
		$data['blog_list_top'] = Article::select('articles.*','users.name AS author', 'blog_categories.name as category_name')
						->join('users', function ($join) {
							$join->on('users.id', '=', 'articles.user_id');
						})
						->join('blog_categories', function ($join) {
							$join->on('blog_categories.id', '=', 'articles.blog_category_id');
						})
						->where(function($query) use ($now){
							$query->orWhere('articles.post_publish', '=', 1);
							$query->orWhere(function($query2) use ($now){
								$query2->where('articles.post_publish', '=', 2);
								$query2->where('articles.scheduled_at', '>=', $now);
							});
						})
						->where('articles.is_verify',1)	
						->limit(4)
						->groupBy('articles.id')
						->get();
						
		$data['blog_list_featured'] = Article::select('articles.*','users.name AS author', 'blog_categories.name as category_name')
						->join('users', function ($join) {
							$join->on('users.id', '=', 'articles.user_id');
						})
						->join('blog_categories', function ($join) {
							$join->on('blog_categories.id', '=', 'articles.blog_category_id');
						})
						->where('articles.is_featured','=',1)
						->where(function($query) use ($now){
							$query->orWhere('articles.post_publish', '=', 1);
							$query->orWhere(function($query2) use ($now){
								$query2->where('articles.post_publish', '=', 2);
								$query2->where('articles.scheduled_at', '>=', $now);
							});
						})
						->where('articles.is_verify',1)	
						->limit(6)
						->groupBy('articles.id')
						->get();

		$data['blog_list_editor_choice'] = Article::select('articles.*','users.name AS author', 'blog_categories.name as category_name')
						->join('users', function ($join) {
							$join->on('users.id', '=', 'articles.user_id');
						})
						->join('blog_categories', function ($join) {
							$join->on('blog_categories.id', '=', 'articles.blog_category_id');
						})
						->where('articles.is_monetize','=',1)
						->where(function($query) use ($now){
							$query->orWhere('articles.post_publish', '=', 1);
							$query->orWhere(function($query2) use ($now){
								$query2->where('articles.post_publish', '=', 2);
								$query2->where('articles.scheduled_at', '>=', $now);
							});
						})
						->where('articles.is_verify',1)	
						->limit(4)
						->groupBy('articles.id')
						->get();	

		$data['blog_list_latest_1'] = Article::select('articles.*','users.name AS author', 'blog_categories.name as category_name')
						->join('users', function ($join) {
							$join->on('users.id', '=', 'articles.user_id');
						})
						->join('blog_categories', function ($join) {
							$join->on('blog_categories.id', '=', 'articles.blog_category_id');
						})
						->where('articles.is_verify',1)	
						->where(function($query) use ($now){
							$query->orWhere('articles.post_publish', '=', 1);
							$query->orWhere(function($query2) use ($now){
								$query2->where('articles.post_publish', '=', 2);
								$query2->where('articles.scheduled_at', '>=', $now);
							});
						})						
						->limit(7)->offset(0)
						->latest()
						->groupBy('articles.id')
						->get();
						

		$data['blog_list_latest_2'] = Article::select('articles.*','users.name AS author', 'blog_categories.name as category_name')
						->join('users', function ($join) {
							$join->on('users.id', '=', 'articles.user_id');
						})
						->join('blog_categories', function ($join) {
							$join->on('blog_categories.id', '=', 'articles.blog_category_id');
						})
						->where(function($query) use ($now){
							$query->orWhere('articles.post_publish', '=', 1);
							$query->orWhere(function($query2) use ($now){
								$query2->where('articles.post_publish', '=', 2);
								$query2->where('articles.scheduled_at', '>=', $now);
							});
						})
						->where('articles.is_verify',1)	
						->limit(4)->offset(7)
						->latest()
						->groupBy('articles.id')
						->get();

		$data['blog_list_popular'] = Article::select('articles.*','users.name AS author', 'blog_categories.name as category_name')
						->join('users', function ($join) {
							$join->on('users.id', '=', 'articles.user_id');
						})
						->join('blog_categories', function ($join) {
							$join->on('blog_categories.id', '=', 'articles.blog_category_id');
						})
						->where('articles.is_monetize','=',1)
						->where(function($query) use ($now){
							$query->orWhere('articles.post_publish', '=', 1);
							$query->orWhere(function($query2) use ($now){
								$query2->where('articles.post_publish', '=', 2);
								$query2->where('articles.scheduled_at', '>=', $now);
							});
						})
						->where('articles.is_verify',1)	
						->limit(2)->offset(4)
						->groupBy('articles.id')
						->get();

		$data['blog_list_others'] = Article::select('articles.*','users.name AS author', 'blog_categories.name as category_name')
						->join('users', function ($join) {
							$join->on('users.id', '=', 'articles.user_id');
						})
						->join('blog_categories', function ($join) {
							$join->on('blog_categories.id', '=', 'articles.blog_category_id');
						})
						->where(function($query) use ($now){
							$query->orWhere('articles.post_publish', '=', 1);
							$query->orWhere(function($query2) use ($now){
								$query2->where('articles.post_publish', '=', 2);
								$query2->where('articles.scheduled_at', '>=', $now);
							});
						})
						->where('articles.is_verify',1)	
						->limit(4)->offset(4)
						->groupBy('articles.id')
						->get();						
		
        return view('front.blog.guest.blogs', $data);
		
	}
	
	#blog detail
	public function blog_detail($slug, Request $request){
		
		$now = date('Y-m-d H:i:s');
		$data['page'] = 'blogs';
		$data['meta_title'] = 'Blogs';
		$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';
		
		if($request->input('section') && $request->input('section') != ''){
			$data['section'] = $request->input('section');
		}else{
			$data['section'] = '';
		}
		
 		$data['blog'] = Article::select('articles.*','users.name AS author', 'blog_categories.name as category_name')
						->join('users', function ($join) {
							$join->on('users.id', '=', 'articles.user_id');
						})
						->join('blog_categories', function ($join) {
							$join->on('blog_categories.id', '=', 'articles.blog_category_id');
						})
						->where('articles.slug','=',$slug)
						->where(function($query) use ($now){
							$query->orWhere('articles.post_publish', '=', 1);
							$query->orWhere(function($query2) use ($now){
								$query2->where('articles.post_publish', '=', 2);
								$query2->where('articles.scheduled_at', '>=', $now);
							});
						})
						->first();
	
		//Post Visibility
		
		if($data['blog']){ //If blog is available.
			$data['meta_title'] = $data['blog']->title;
			$visiblity = $this->getPostVisibility($data['blog'], auth()->user());
		}
						
		//if($data['blog'] || ($data['blog']->post_visibility == 1 && $data['blog']->user_id != auth()->user()->id)){
		if($data['blog'] && $visiblity == true){
						
			$data['similar_blogs'] = Article::select('articles.*','blog_categories.name as category_name')
							->join('blog_categories', function ($join) {
								$join->on('blog_categories.id', '=', 'articles.blog_category_id');
							})
							->where('articles.id','!=',$data['blog']->id)
							->where('articles.blog_category_id','=',$data['blog']->blog_category_id)
							->where(function($query) use ($now){
								$query->orWhere('articles.post_publish', '=', 1);
								$query->orWhere(function($query2) use ($now){
									$query2->where('articles.post_publish', '=', 2);
									$query2->where('articles.scheduled_at', '>=', $now);
								});
							})
							//->where('articles.post_visibility','=',1)
							->limit(4)
							->get();
							
			$data['comments'] = BlogComment::select('blog_comments.*', 'users.name as author_name', 'users.id as author_id', 'users.user_role as author_role', 'users.image as author_image')
								->join('users', function ($join) {
									$join->on('users.id', '=', 'blog_comments.user_id');
								})
								->where('blog_comments.blog_id',$data['blog']->id)
								->where('blog_comments.status',1)
								->latest()
								->get();
					
			return view('front.blog.guest.blog_detail', $data);
		
		}else{
			return redirect()->route('front.blogs');
		}
				
					
	}
	
	public function getPostVisibility($data, $user){
		//$now = date('Y-m-d H:i:s');
		$visiblity = false;
		
		if($data->post_visibility == 0 ){ //Check if blog post visiblity is private
			$visiblity = false;
			
			if($user && $data->user_id == $user->id){ //Check if logged as author of post.
				$visiblity = true;
			}
			
			if($user && $user->user_role == 1){ //Check if logged as Admin
				$visiblity = true;
			}
			
			//Check if user is one of supporter of post author
			$supporter_data = SocialUserFollowConnection::where('user_id', $data->user_id)->first();
			
			if($supporter_data){
				$supporters = explode(',', $supporter_data->follower_id);
				
				if($user && in_array($user->id, $supporters)){
					$visiblity = true;
				}
			}
			
		}else{
			$visiblity = true; //Visiblity is public
		}
			
		return $visiblity;
	}
	
	
	#Blog category topic
	public function blog_catgeory_topic(Request $request){
		$now = date('Y-m-d H:i:s');		
		$data['page'] = 'blogs';
		$data['meta_title'] = 'Blogs';
		$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';
		
		$data['catname'] = BlogCategory::select('name')->where('id',$request->id)->first();
		
		if(!$data['catname']){
			return redirect()->route('front.blogs');
		}
		
		$topicId = ($request->tid != "") ? $request->tid : "";  
		
		$data['categories_data'] = DB::table('articles')
			->select('blog_categories.id','blog_categories.name', 'blog_categories.category_img', DB::raw('count(*) as total'))
			->join('blog_categories', function ($join) {
				$join->on('blog_categories.id', '=', 'articles.blog_category_id');
			})
			->where('blog_categories.id','!=',$request->id)				
			->where(function($query) use ($now){
				$query->orWhere('articles.post_publish', '=', 1);
				$query->orWhere(function($query2) use ($now){
					$query2->where('articles.post_publish', '=', 2);
					$query2->where('articles.scheduled_at', '>=', $now);
				});
			})
			//->where('articles.post_visibility','=',1)				 
			->groupBy('articles.blog_category_id')
			->get();
		
		$data['topic'] = DB::table('category_topic')
			->select('category_topic.id','category_topic.topic','category_topic.category_id')
			->where('category_topic.category_id',$request->id)				
			->where('category_topic.status','=',1)
			->get();
		
		$data['catname'] = BlogCategory::select('name')->where('id',$request->id)->first();
		$data['catId'] =   $request->id;

		#Check user is subscribe for this blog category or not.....
		if(isset(auth()->user()->email))
		{
			$SqlB = BlogSubscription::select(DB::raw('count(*) as total'))->where('subscriber_email',auth()->user()->email)->first();
			$data['subscribe'] = $SqlB->total;
		}
		
		else{
			$data['subscribe'] = 0;
		}
		
		
		if($topicId =="" || $topicId ==0)
		{
			$data['blog_list_top'] = Article::select('articles.*','users.name AS author', 'blog_categories.name as category_name')
				->join('users', function ($join) {
					$join->on('users.id', '=', 'articles.user_id');
				})
				->join('blog_categories', function ($join) {
					$join->on('blog_categories.id', '=', 'articles.blog_category_id');
				})
				->where('articles.blog_category_id','=',$request->id)
				->where(function($query) use ($now){
					$query->orWhere('articles.post_publish', '=', 1);
						$query->orWhere(function($query2) use ($now){
							$query2->where('articles.post_publish', '=', 2);
							$query2->where('articles.scheduled_at', '>=', $now);
					});
				})
				//->where('articles.post_visibility','=',1)
				->groupBy('articles.id')
				->latest('created_at')
				->paginate(10);
		}
		else
		{
			$data['blog_list_top'] = Article::select('articles.*','users.name AS author', 'blog_categories.name as category_name')
				->join('users', function ($join) {
					$join->on('users.id', '=', 'articles.user_id');
				})
				->join('blog_categories', function ($join) {
					$join->on('blog_categories.id', '=', 'articles.blog_category_id');
				})
				->where('articles.blog_category_id','=',$request->id)
				->whereIn('articles.category_topic_ids', [$topicId])
				->where(function($query) use ($now){
					$query->orWhere('articles.post_publish', '=', 1);
					$query->orWhere(function($query2) use ($now){
						$query2->where('articles.post_publish', '=', 2);
						$query2->where('articles.scheduled_at', '>=', $now);
					});
				})
				//->where('articles.post_visibility','=',1)
				->groupBy('articles.id')
				->latest('created_at')
				->paginate(10);
		}

		$data['topicId'] =   $topicId;

		return view('front.blog.guest.category_topic', $data);
		
	}

	#Blog Subscription 

	public function blog_subscription(Request $request){
		
		if ($request->ajax()) {
			$req = $request->all();
			
			$messages = [
				'email_subs.regex' => 'Email name is required field.',
			];
			
			$validator = Validator::make($request->all(), [
				'email_subs' => 'required|email|max:120|unique:blog_subscription,subscriber_email'
			], $messages)->validate();
			
			$input_data = [	
				'post_category_id' => $req['cateId'],			
				'subscriber_email' => trim($req['email_subs']),
			];

			$blog = BlogSubscription::create($input_data);
			
			if($blog){
				return response()->json(['status'=>'success', 'message'=>'Message Sent successfully.']);
			}else{
				return response()->json(['status'=>'Failed', 'message'=>'There is some problem. Please try again later']);
			}
			
		}
	}

	
	public function blogs_search(Request $request) {
		
		$now = date('Y-m-d H:i:s');
		$search_value = $request->search_value;
		
		if($search_value != ''){
			
			// category, topic, tags, title, author
			$blogs = Article::select('articles.id','articles.title', 'articles.slug')
						->join('users', function ($join) {
							$join->on('users.id', '=', 'articles.user_id');
						})
						->join('blog_categories', function ($join) {
							$join->on('blog_categories.id', '=', 'articles.blog_category_id');
						})
						->leftjoin("category_topic",\DB::raw("FIND_IN_SET(category_topic.id,articles.category_topic_ids)"),">",\DB::raw("'0'"))					
						->where(function($query) use ($now){
							$query->orWhere('articles.post_publish', '=', 1);
							$query->orWhere(function($query2) use ($now){
								$query2->where('articles.post_publish', '=', 2);
								$query2->where('articles.scheduled_at', '>=', $now);
							});
						})
						//->where('articles.post_visibility','=',1)
						//search
						->where(function($query) use ($search_value){
							 $query->orWhere('articles.title', 'LIKE', '%'.$search_value.'%'); //title
							 $query->orWhere('blog_categories.name', 'LIKE', '%'.$search_value.'%'); //category
							 $query->orWhere('users.name', 'LIKE', '%'.$search_value.'%'); //author
							 $query->orWhere('category_topic.topic', 'LIKE', '%'.$search_value.'%'); //topic
							 
						 })						
						->groupBy('articles.id')
						->get();		
			
			if($blogs && $blogs->count() > 0){
				$result = $blogs->toArray();
			}else{
				$result = array();
			}
			
			return response()->json(['status' => 'success', 'data' => $result]);			
		}else{
			return response()->json(['status' => 'error', 'data' => array()]);
		}
				
	}		
	
	
	public function blog_comments_add(Request $request) {
		
		$comment = new BlogComment;
		$comment->comments = strip_tags($request->comments); 
		$comment->blog_id = $request->blog_id; 
		$comment->user_id = auth()->user()->id; 
		$comment->status = 1; //auth()->user()->user_role == '1' ? 1 : 0
			
		if($comment->save()) {
			
			$result = BlogComment::select('blog_comments.*', 'users.name as author_name', 'users.id as author_id', 'users.user_role as author_role', 'users.image as author_image')
				->join('users', function ($join) {
					$join->on('users.id', '=', 'blog_comments.user_id');
				})
				->where('blog_comments.id',$comment->id)
				->where('blog_comments.status',1)
				->first();
							
			return response()->json(['status'=>'success', 'data' => $result]);
		}else{
			return response()->json(['status' => 'error', 'data' => array()]);
		}
				
	}	
	

	#forums listing
	
	public function forum_home()
	{
		
		$data['page'] = 'forum';
		$data['meta_title'] = 'Forum';
		$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';
		
		$where = array(
			'forum_details.status' => 1
		);

		$data['categories'] = ForumTopic::select('forum_details.*',DB::raw('COUNT(forum_topics.forum_category_id) as topic_count'))
							->rightJoin('forum_details', function ($join) {
									$join->on('forum_details.id', '=', 'forum_topics.forum_category_id');
							})
							->where($where)
							->groupBy('forum_details.id')->ORDERBY('forum_subject','ASC')
							->get();
		if(session()->get('paywall_forum_id'))
		{
			$data['paywall_forum_id'] = session()->get('paywall_forum_id');
			/********For render common paywall view page********/
			if(!empty(auth()->user()->id)){
				$user_id=auth()->user()->id;
				$card_info = DB::table('payment_card_info')->select('*')->where(array('user_id' => $user_id))->orderBy('card_id','DESC')->get();
				$forumDetail = ForumDetail::select('*')->where('id',session()->get('paywall_forum_id'))->first();
				
				if(!empty($card_info)){
					$intent['card_info']= $card_info;
					
				}
				$card_primary = DB::table('payment_card_info')->select('*')->where(array('user_id' => $user_id,'is_primary'=>1))->orderBy('card_id','DESC')->get();
				if(!empty($card_primary) && !empty($card_primary[0]->card_id)){
					$intent['card_primary']= $card_primary[0]->card_id;
				}else{
					$intent['card_primary']="new";
				}
				$intent['forum_amount'] = $forumDetail['access_amount'];
				$intent['payment_for'] = 'forum_access';
			}
			$data['paywall'] = view('front.common_paywall_page', compact('intent'))->render();
			/********End render common paywall view page********/
			
		}
		else{
			$data['paywall_forum_id'] ="";
		}
		
		return view('front.forum.guest.index', $data);
	}

	public function forum_post(Request $request)
	{
		$data['page'] = 'forum';
		$data['meta_title'] = 'Forum';
		$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';
		
		#First check forum is free or paid
		$check = $request->pid;
		if($check==0)                            					//1= free forum and 0=paid forum
		{  
			session()->put('return_url', 'forum');
			if(auth() && auth()->user())                            //Check user is login or not
			{ 
				#Now check for user paid to access this forum or not
				$paymentCheck = ForumPaymentDetail::select('*')->where('payment_for','forum_access')->where('user_id',auth()->user()->id)->where('forum_id',$request->id)->where('status',1)->orderby('id','DESC')->limit(1)->first();		
				
				if(!empty($paymentCheck))
				{
					$where = array(
						'forum_details.status' => 1
					);
					
					//popular topics
					$data['popular_topics'] = $this->getTopicList('popular',$where,$request->id);			
					
					//latest topics
					$data['latest_topics'] = $this->getTopicList('latest',$where,$request->id);
			
					$data['all_topics'] = $this->getTopicList('all',$where,$request->id);
							
					return view('front.forum.guest.post_list', $data);
				 
				}
				else
				{
					//echo "payment not done";	die;
					session()->put('paywall_forum_id', $request->id);
					if(Session::has('return_url') && session('return_url') !='')
					{
						//echo "condition-1";  die;
						return  redirect()->route('front.forum');
					}
					else
					{
						
						return redirect()->back()->with('success', 'your message,here');
					}
					
				}
			}
			else
			{
				session()->put('fid', $request->id);
				return redirect()->route('sign_in');
			}
		}
		else
		{
			$where = array(
				'forum_details.status' => 1
			);
			
			//popular topics
			$data['popular_topics'] = $this->getTopicList('popular',$where,$request->id);			
			
			//latest topics
			$data['latest_topics'] = $this->getTopicList('latest',$where,$request->id);
	
			$data['all_topics'] = $this->getTopicList('all',$where,$request->id);
					
			return view('front.forum.guest.post_list', $data);
		}
	}

	public function getTopicList($type,$where,$forum_id){
		$topicsArr = array();
		
		if($type == 'latest')
		{
			$topics = ForumTopic::select('forum_topics.id AS topic_id', 'forum_topics.title', 'forum_topics.slug', 'forum_details.forum_subject AS forum_category', 'forum_details.slug AS category_slug', 'forum_topic_comments.id AS comment_id', 'forum_topic_comments.user_id AS comment_user_id', 'forum_topic_comments.created_at AS comment_time');
		}
		elseif($type == 'all')
		{
			$topics = ForumTopic::select('forum_topics.id AS topic_id', 'forum_topics.title', 'forum_topics.slug', 'forum_details.forum_subject AS forum_category', 'forum_details.slug AS category_slug', 'forum_topic_comments.id AS comment_id', 'forum_topic_comments.user_id AS comment_user_id', 'forum_topic_comments.created_at AS comment_time');
		}
		else{		
			$topics = ForumTopic::select('forum_topics.id AS topic_id', 'forum_topics.title', 'forum_topics.slug', 'forum_details.forum_subject AS forum_category', 'forum_details.slug AS category_slug', 'forum_topic_comments.id AS comment_id', DB::raw('COUNT(forum_topic_likes.id) AS like_count'), 'forum_topic_comments.user_id AS comment_user_id', 'forum_topic_comments.created_at AS comment_time');
		}
		
		
		$topics = $topics->join('forum_details', function ($join) {
				$join->on('forum_details.id', '=', 'forum_topics.forum_category_id');
		})
		->leftJoin('forum_topic_comments', function ($join) {
				$join->on('forum_topics.id', '=', 'forum_topic_comments.forum_topic_id');
		})							
		->leftJoin('users', function ($join) {
				$join->on('forum_topic_comments.user_id', '=', 'users.id');
		});
							
		if($type == 'popular'){	
			$topics = $topics->leftJoin('forum_topic_likes', function ($join) {
					$join->on('forum_topics.id', '=', 'forum_topic_likes.forum_topic_id');
			});
		}

		$topics = $topics->where($where)
		->groupBy('forum_topic_comments.user_id')
		->groupBy('forum_topic_comments.id')
		->groupBy('forum_topics.id')
		->where('forum_topics.forum_category_id',$forum_id);

		if($type == 'latest'){
			$topics = $topics->orderBy('forum_topics.id','DESC');
			$topics = $topics->orderBy('forum_topic_comments.id','DESC');
		}
		elseif($type == 'all'){
			$topics = $topics->orderBy('forum_topics.id','DESC');
			$topics = $topics->orderBy('forum_topic_comments.id','DESC');
		}else{
			$topics = $topics->orderBy('like_count','DESC');
			$topics = $topics->orderBy('forum_topic_comments.id','DESC');							
		}

		if($type == 'latest')
		{
			$topics = $topics->limit(1);
		}
	
	$topics = 	$topics->get();
							
 		if($topics && $topics->count() > 0){
			
			//round 1
			$commentArr = array();
			$commentUserArr = array();
			foreach($topics as $k => $v){
				
				$topic = array(
                    'topic_id' => $v->topic_id,
                    'title' => $v->title,
                    'slug' => $v->slug,
                    'forum_category' => $v->forum_category,
                    'category_slug' => $v->category_slug,				
				);
				
				$topicsArr[$v->topic_id]['topic'] = $topic;
				
				if($v->comment_id != ''){
					$topicsArr[$v->topic_id]['comment'][] = $v->comment_id;
					$topicsArr[$v->topic_id]['comment_time'][] = $v->comment_time;
				}else{
					$topicsArr[$v->topic_id]['comment'] = array();
					$topicsArr[$v->topic_id]['comment_time'] = array();
				}
				
				if($v->comment_user_id != ''){
					$topicsArr[$v->topic_id]['comment_user'][] = $v->comment_user_id;
				}else{
					$topicsArr[$v->topic_id]['comment_user'] = array();
				}
			}
		}
									
		//round 2
		$topicList = $result = array();
		if($topicsArr){
			$j =0;
			foreach($topicsArr as $i => $val){
							
				if($val['comment_user']){
					$val['comment_user'] = array_values(array_unique($val['comment_user']));
					
					$user_data = User::select("image")->whereIn('id',$val['comment_user'])->get()->toArray();
					$val['comment_user_data'] = $user_data;
				}
				
				$topicList[$j] = $val;
				$j++;
			}
			
			$result = $topicList;
			//$temp = array_chunk($topicList,20);			
			//$result = $temp[0];
		}		
				
		return $result;
	}


	public function view_article(Request $request){
		
		$now = date('Y-m-d H:i:s');		
		$data['page'] = 'blogs';
		$data['meta_title'] = 'Blogs';
		$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';
		
		#Get post detail on the basis of slug
		$data['post_detail'] = ForumTopic::select('forum_topics.*','users.name','users.image','users.phone','users.city','users.education')
								->join('users', function ($join) {
									$join->on('users.id', '=', 'forum_topics.user_id');
								})
								->where('forum_topics.slug', $request->slug)->first();
		
		#Get comment details on the basis of post id
		$data['comment'] = ForumTopicComment::select('forum_topic_comments.*','users.name','users.image')
								->join('users', function ($join) {
									$join->on('users.id', '=', 'forum_topic_comments.user_id');
								})
								->where('forum_topic_comments.forum_topic_id', $data['post_detail']['id'])->get();
		
		#Get number of like on the basis of post_id
		$data['post_like'] = (auth() && auth()->user()) ? ForumTopicLike::select('*')->where('forum_topic_id',$data['post_detail']['id'])->where('user_id',auth()->user()->id)->count() : 0;
							
		return view('front.forum.guest.category_topic', $data);
		
	}

	#Forum post like details
	public function forum_post_like(Request $request)
	{

		if ($request->ajax()) 
		{
			$input_data = [
				"user_id" => auth()->user()->id,
				"forum_topic_id" => $request->post_id
			];
			$result = ForumTopicLike::create($input_data);
			if($result)
			{
				return response()->json(['status'=>'success', 'message'=>'Saved successfully.']);
			}
		}
	}

	public function comment_like(Request $request)
	{
		$post_id = $request->comment_id;
		$lastCount = $request->last_count;
		$insert_data = [
			"user_id" => auth()->user()->id,
			"forum_topic_id" =>$post_id,
			"status" =>1
		];

		return response()->json(['status'=> 'success']);
	}

	#Make comment on post
	public function post_comments(Request $request)
	{
		$post_id = $request->post_id;
		if($request->ajax()) {
			$messages = [
				'comment.regex' => 'Comment is required filed.',
			];
			$validator = Validator::make($request->all(), [
				'comment' => 'required'
			], $messages)->validate();
			
			$input_data = [
				'user_id' => auth()->user()->id,
				'forum_topic_id' => $post_id,
				'comment_content' => trim($request->comment),
				'status' => 1
			];
			
			$result = ForumTopicComment::create($input_data);
			if($result){
				return response()->json(['status'=> 'success']);
			}
		}
		
	}

	public function comman_paywall_payment(Request $request)
	{
		if(!empty(auth()->user()->id)){
			$user_id=auth()->user()->id;
			$card_info = DB::table('payment_card_info')->select('*')->where(array('user_id' => $user_id))->orderBy('card_id','DESC')->get();
			
			if(!empty($card_info)){
				$intent['card_info']= $card_info;
				
			}
			$card_primary = DB::table('payment_card_info')->select('*')->where(array('user_id' => $user_id,'is_primary'=>1))->orderBy('card_id','DESC')->get();
			if(!empty($card_primary) && !empty($card_primary[0]->card_id)){
				$intent['card_primary']= $card_primary[0]->card_id;
			}else{
				$intent['card_primary']="new";
			}
		}
		$data['paywall'] = view('front.common_paywall_page', compact('intent'))->render();
		return view('front.common_paywall_page', $data);
	}

	public function set_pin(Request $request)
	{
		$data['p_type'] = $request->p_type;
		$data['interval'] = ($request->interval !="") ? $request->interval : "";
		return view('front.set_pin',$data);
	}
}
