<?php

namespace App\Http\Controllers\front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Stripe\SetupIntent;	
use Laravel\Cashier\Cashier;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Session;
use App\Library\Services\CommonService;
use App\OnlineVisit;
use App\Service;
use App\User;
use App\ProviderCategory;
use App\ProviderReview;
use App\OnlineVisitProviderAssign;
use App\OnlineVisitStatusTimeline;
use App\OnlineVisitPaywallFeature;
use App\ProviderTimeSlot;
use App\EventAppointment;
use App\State;
use App\ShippingAddress;
use DB;
use App\Notifications;
use App\SocialUserFollowConnection;
use App\Events\Add_appointment;
use App\SupporterRequest;
use App\Article;
use App\PaymentLogs;
use App\Events\Admin_notification;

class PatientController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('user_auth');
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Contracts\Support\Renderable
	 */
	
	public function index(){
		$data['page'] = 'user_dashboard';
		$data['meta_title'] = 'Dashboard';
		$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';
		if(!empty(auth()->user()->chat_id)){
			//$this->unread_message();
		}
		return view('front.user.index', $data);
	}
	

	public function user_preference()
	{
		$data['page'] = 'user_preference';
		$data['meta_title'] = 'Dashboard';
		$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';
        $data['payment_pin'] = User::where('id',auth()->user()->id)->first();
		return view('front.user.user_preference', $data);
	}

	public function set_payment_pin(Request $request)
	{
		if ($request->ajax()) {
						
			if(empty($request->id)){
				$messages = [
					'payment_pin.regex' => 'Pin must contain 4 digit.',
				];
				$validator = Validator::make($request->all(), [
					'payment_pin' => 'required|digits:4'
				], $messages)->validate();	

				$input_data = array(
					'payment_confirmation_pin' => Hash::make($request->payment_pin),
					'payment_confirmation_pin_view' => mb_substr($request->payment_pin, 0, 1).'**'.substr($request->payment_pin,3),
					'is_first_login' => 0,
				);
			}else{
				$input_data = array(
					'is_first_login' => 0,
				);
			}
			User::where('id',auth()->user()->id)->update($input_data);
			
			$msg = 'Payment confirmation pin set successfully.';
			$redirect = route('user.dashboard');	

			return response()->json(['status'=> 'success', 'redirect'=> $redirect, 'message' => $msg]);
		}
		else{
			return response()->json(['status'=> 'error', 'message' => 'This request is not valid.', 'data' => '']);
		}
	}

	public function set_merchant_access(Request $request)
	{
		if ($request->ajax()) {
						
			$messages = [
				'merchant_login_id.regex' => 'Merchant Login Id is required filed.',
				'merchant_transaction_key.regex' => 'Merchant Transaction Key is required filed.',
			];
			$validator = Validator::make($request->all(), [
				'merchant_login_id' => 'required',
				'merchant_transaction_key' => 'required'
			], $messages)->validate();	

			$input_data = array(
				'MERCHANT_LOGIN_ID' => $request->merchant_login_id,
				'MERCHANT_TRANSACTION_KEY' => $request->merchant_transaction_key
			);

			User::where('id',auth()->user()->id)->update($input_data);
			
			$msg = 'Merchant account access set successfully.';
			$redirect = route('user.dashboard');	

			return response()->json(['status'=> 'success', 'redirect'=> $redirect, 'message' => $msg]);
		}
		else{
			return response()->json(['status'=> 'error', 'message' => 'This request is not valid.', 'data' => '']);
		}
	}

	public function selectProvider($online_visit_id){	
		
		$online_visit = OnlineVisit::select('online_visits.*')
			->where('id', $online_visit_id)
			->where('is_submit', 1)
			->where('payment_status', 'done')
			->first();
		
		if($online_visit && $online_visit->count() > 0){
		
				$provider_check = OnlineVisitProviderAssign::where('online_visit_id', $online_visit_id)->where('status', 1)->first();
				
				if($provider_check && $provider_check->count() > 0) {
					
					return redirect()->route('user.treatments');
					
				}else{
					$data['online_visit'] = $online_visit;
					$data['services'] = Service::where('status',1)->get();		
					$data['service'] = Service::where('id', $online_visit->service_id)->where('status', 1)->first();
							
					$data['page'] = 'user_treatments';
					$data['meta_title'] = 'My Treatments';
					$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';
				
					return view('front.user.select_provider', $data);					
				}			
		}
		else{
			return redirect()->route('user.treatments');
		}
	}

	public function get_provider_data(Request $request){
		
		if($request->ajax()) {	
			
			$online_visit_id = $request->online_visit_id;
			
			$online_visit = OnlineVisit::select('online_visits.*')
				->where('id', $online_visit_id)
				->where('is_submit', 1)
				->first();


			if ($online_visit && $online_visit->count() > 0) {

				$data['online_visit'] = $online_visit;

				$providers = User::select('users.*')
					->join('provider_services', function ($join) {
						$join->on('users.id', '=', 'provider_services.provider_id');
					})
					->where('provider_services.service_id', $online_visit->service_id)
					->where('users.user_role', 3)
					->where('users.status', 1)
					->where('provider_services.status', 1);
					
				if($request->provider_name != ''){
					$providers = $providers->where('users.name', 'like', '%' . $request->provider_name . '%');
				}
				
				$providers = $providers->groupBy('users.id')
					->orderBy('users.name', 'ASC')
					->get();

				//echo '<pre>'; print_r($providers->toArray()); die;

				$data['providers'] = array();

				$data['service'] = Service::where('id', $online_visit->service_id)->where('status', 1)->first();

				if ($providers && $providers->count() > 0) {
					$i = 0;
					foreach ($providers as $row) {

						$categoryString = '';
						$categories = explode(', ', $row->category);
						$category_names = ProviderCategory::whereIn('id', $categories)->select('name')->get();

						if ($category_names && $category_names->count() > 0) {
							$categories_arr = array();
							foreach ($category_names as $key => $value) {
								array_push($categories_arr, $value->name);
							}
							$categoryString = implode(', ', $categories_arr);
						}

						$reviews = ProviderReview::select(DB::raw('AVG(rating) as avg_rating'), DB::raw('COUNT(*) as review_count'))
							->where('provider_id', $row->id)
							->where('service_id', $online_visit->service_id)
							->where('status', 1)
							->first();

						if ($reviews && $reviews->count() > 0) {
							$rating = $reviews->avg_rating;
							$review_count = $reviews->review_count;
						} else {
							$rating = '';
							$review_count = 0;
						}

						if ($row->image == '') {
							$provider_image = '/dist/images/user_icon.png';
						} else {
							$provider_image = '/' . $row->image;
						}

						$data['providers'][$i]['id'] = $row->id;
						$data['providers'][$i]['name'] = $row->name;
						$data['providers'][$i]['image'] = $provider_image;
						$data['providers'][$i]['provider_categories'] = $categoryString;
						$data['providers'][$i]['review_count'] = $review_count;
						$data['providers'][$i]['rating'] = $rating;
						$data['providers'][$i]['is_available'] = $row->is_available;

						$i++;
					}
				}
				
	 			$output = '';

				if($data['providers']){
					$output = view('front.user.get_provider_data',$data)->render();
					return response()->json(['status'=> 'success', 'message' => 'Success', 'data' => $output]);
				}else{
					return response()->json(['status'=> 'error', 'message' => 'No provider found for this criteria.', 'data' => $output]);
				} 
				
			}else{
				return response()->json(['status'=> 'error', 'message' => 'This online visit is not valid.', 'data' => '']);
			}
			
		}else{
			return response()->json(['status'=> 'error', 'message' => 'This request is not valid.', 'data' => '']);
		}		
	}
	
	public function selectAppointment($online_visit_id)
	{

		$online_visit = OnlineVisit::select('online_visits.*')
			->where('id', $online_visit_id)
			->where('is_submit', 1)
			->first();

		if ($online_visit && $online_visit->count() > 0) {

			$provider_check = OnlineVisitProviderAssign::where('online_visit_id', $online_visit_id)->where('status', 1)->first();
			
			if ($provider_check && $provider_check->count() > 0) {
				
				//Check if already requested timeslot for this online_visit.
				
				$appointment_check = EventAppointment::where('patient_id',auth()->user()->id)
														->where('provider_id',$provider_check->provider_id)
														->where('online_visit_id',$online_visit_id)
														->where('is_delete',0)
														->first();
				
				if($appointment_check && $appointment_check->count() > 0){
					
					return redirect()->route('user.appointments')->withErrors('Already requested for appointment.');
					
				}else{
					$data['page'] = 'user_appointments';
					$data['meta_title'] = 'Select Appointment';
					$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';

					$data['online_visit'] = $online_visit;

					$data['provider'] = User::select('*')
						->where('users.id', $provider_check->provider_id)
						->first();

					$data['service'] = Service::select('*')
						->where('id', $online_visit->service_id)
						->first();

					//$str_date = date('Y-m-d');
					//$end_date = date("Y-m-d", strtotime("+7 days", strtotime($str_date)));
					$str_date = date('Y-m-d H:i');
					$end_date = date("Y-m-d H:i", strtotime("+7 days", strtotime($str_date)));
					
					$res = $this->getTimeSlots($provider_check->provider_id,$online_visit->visit_type, $str_date, $end_date);
					$data['timeslots'] = $res['timeslots'];
					
					$data['location'] = $res['location'];
					
				 	return view('front.user.select_appointment', $data);	
				
				}

			} else {
				//return redirect()->route('user.treatments');
				return redirect('/user/select_provider/'.$online_visit_id)->withErrors('Select provider first.');
			}
		} else {
			return redirect()->route('user.treatments')->withErrors('Error occured.');
		}
	}

	public function getTimeSlotsByDate(Request $request)
	{
		if ($request->ajax()) {
			$provider_id = $request->provider_id;
			$str_date = $request->start_date;
			$end_date = date("Y-m-d", strtotime("+7 days", strtotime($str_date)));
			$visit_type = $request->online_visit_type;
			//echo $end_date;die;
			$timeslots = $this->getTimeSlots($provider_id,$visit_type, $str_date, $end_date);

			if ($timeslots) {
				$data = json_encode($timeslots, true);
				return response()->json(['status' => 'success', 'message' => 'Data found.', 'data' => $data]);
			} else {
				$data = '';
				return response()->json(['status' => 'error', 'message' => 'Data not found.', 'data' => $data]);
			}
		} else {
			return response()->json(['status' => 'error', 'message' => 'Some Error Occured.']);
		}
	}

	public function addAppointment(Request $request, CommonService $common)
	{
		if ($request->ajax()) {

			$date = $request->appointment_date;
			$time = $request->appointment_time;
			$event_start = date('Y-m-d H:i:s', strtotime($date . ' ' . $time));
			
			//echo $event_start;die;

			$online_visit = OnlineVisit::select('online_visits.*')
				->where('id', $request->online_visit_id)
				->where('is_submit', 1)
				->first();

			if ($online_visit && $online_visit->count() > 0) {
				$timeslot = ProviderTimeSlot::where('provider_id', $request->provider_id)
					->where('event_start', $event_start)
					->first();

				if ($timeslot && $timeslot->count() > 0) {

					$service = Service::where('id',$online_visit->service_id)->first();
					
					if($service && $service->count() > 0){
						$service_name = $service->name;
					}else{
						$service_name = '';
					}
					
					if($online_visit->visit_type == 1){ $visit_type = 'virtual'; }
					if($online_visit->visit_type == 2){ $visit_type = 'virtual'; }
					if($online_visit->visit_type == 3){ $visit_type = 'concierge'; }
					
					$provider = User::where('id',$request->provider_id)->first();
					
					if($provider && $provider->count() > 0){
						$provider_name = $provider->name;
					}else{
						$provider_name = '';
					}
					
					$patient_name = auth()->user()->name;
					$provider_name = $provider->name;
					
					$event_title = $service_name." ".$visit_type." visit with ".$patient_name; //to display on provider's appointment calendar
					$event_title2 = $service_name." ".$visit_type." visit with ".$provider_name; //to display on patient's appointment calendar

					$data = array(
						'provider_id' => $request->provider_id,
						'online_visit_id' => $request->online_visit_id,
						'patient_id' => auth()->user()->id,
						'provider_timeslot_id' => $timeslot->id,
						'service_id' => $online_visit->service_id,
						'status' => 0,
						'title' => $event_title,
						'title2' => $event_title2,
						'start' => $timeslot->event_start,
						'end' => $timeslot->event_end
					);

					EventAppointment::create($data);

					OnlineVisit::where('id',$online_visit->id)->update(array('visit_status' => 'SCHEDULED' ));
					// send pusher Notifications
					if(!empty(auth()->user()->image)){
						$img =auth()->user()->image;
					}else{
						$img='dist/images/user_icon.png';
					}
					$noticeData = array(
						'created_id' => auth()->user()->id,
						'created_name' => auth()->user()->name,
						'created_image' => $img,
						'notification_type' => "add_appointment",
						'post_id' => $request->online_visit_id,
						'followers_id' =>$request->provider_id,
						'status' => 1,
						'created_at' => date('Y-m-d H:i:s'),
						'updated_at' => date('Y-m-d H:i:s')
					);
					
					$notice = Notifications::create($noticeData);
					$noticeData['id']=$notice->id;
					event(new Add_appointment($noticeData));
					
					$redirect = route('user.appointments');
					return response()->json(['status' => 'success', 'message' => 'Appointment Requested Successfully', 'redirect' => $redirect]);
					
					//Email Sending
					$user = User::find(auth()->user()->id);
					$provider = User::find($request->provider_id);
					$service = Service::find($online_visit->service_id);
					
					$input['name'] = $provider->name;
					$input['email'] = $provider->email;
					$input['subject'] = 'Appointment Request';
					$input['template'] = 'front.email_template.appointment_request';
					
					//Appointment Data

					if($service->visit_type == '1'){ $service_type = 'Asynchronous Telemedicine'; }
					if($service->visit_type == '2'){ $service_type = 'Synchronous Telemedicine'; }
					if($service->visit_type == '3'){ $service_type = 'Concierge'; }
					
					$input['provider_name'] = $provider->name;
					$input['patient_name'] = $user->name;
					$input['service_name'] = $service->name;
					$input['service_type'] = $service_type;
					$input['appointment_datetime'] = date(env('DATE_FORMAT_PHP_HA'),strtotime($timeslot->event_start));
						
					//$output = view($input['template'],$input)->render(); echo $output;	die;
					
					$result = $common->sendMail($input);	
					
					
				} else {
					return response()->json(['status' => 'error', 'message' => 'Time slot not found.']);
				}
			} else {
				return response()->json(['status' => 'error', 'message' => 'Online visit not valid.']);
			}
		} else {
			return response()->json(['status' => 'error', 'message' => 'Some Error Occured.']);
		}
	}

	public function getTimeSlots($provider_id, $visit_type, $str_date, $end_date)
	{
		$provider_timeslots = ProviderTimeSlot::select('provider_timeslots.id', 'provider_timeslots.location' ,DB::raw('DATE_FORMAT(provider_timeslots.event_start,"%H:%i") AS time_slot'), DB::raw('DATE_FORMAT(provider_timeslots.event_start,"%Y-%m-%d") AS event_date'))
			->leftJoin('events', function ($join) {
				$join->on('events.provider_timeslot_id', '=', 'provider_timeslots.id');
			})
			->where('provider_timeslots.provider_id', $provider_id)
			->where('provider_timeslots.visit_type', $visit_type)
			->where('provider_timeslots.status', 1)
			->whereBetween('provider_timeslots.event_start', [$str_date, $end_date])
			->whereNull('events.id')	
			->orderBy('provider_timeslots.event_start', 'ASC')
			->get();

		$data = array();
		$loc_data = array();
		$result = array();
		$loc_result = array();

		if ($provider_timeslots && $provider_timeslots->count() > 0) {
				
			foreach ($provider_timeslots->toArray() as $key => $value) {
				$data[$value['event_date']][] = $value['time_slot'];
				$loc_data[$value['event_date']][$value['time_slot']] = $value['location'];
			}
			/* echo "<pre>"; print_r($loc_data); die(); */
			$daysArr = $this->getNextSevenDays(7, 'Y-m-d');
			
			if ($data && $daysArr) {
				$x = 0;
				foreach ($daysArr as $key => $day) {
			
					if(isset($data[$day])){
						$result[$x] = $data[$day];
					}else{
						$result[$x]= array();
					}
					
					
					if(isset($loc_data[$day])){
						$loc_result[$x] = $loc_data[$day];
					}else{
						$loc_result[$x]= array();
					}
					
				$x++;
				}
			}
		

		/* echo '<pre>'; print_r($loc_result); die; */
			
		}
		$res = array(
			'timeslots'=> $result,
			'location'=> $loc_result,
		);
		return $res;
	}


	public function getNextSevenDays($days, $format = 'd/m'){
		$m = date("m"); $de= date("d"); $y= date("Y");
		$dateArray = array();
		for($i=0; $i<$days; $i++){
			$dateArray[] = date($format, mktime(0,0,0,$m,($de+$i),$y)); 
		}
		return $dateArray;
	}
	
	public function selectProviderSubmit(Request $request)
	{
		if ($request->ajax()) {
			$online_visit_id = $request->o_id;
			$provider_id = $request->p_id;

			#Get visit type
			$visitType = OnlineVisit::select('visit_type')->where('id',$online_visit_id)->first(); 
			
			#Check provider is available or this visit or not
			$checkFlag = ProviderTimeSlot::select('*')->whereRaw("event_start >= '".date('Y-m-d')."' ")->where('provider_id',$provider_id)->where('visit_type',$visitType['visit_type'])->count();
			
			if($checkFlag==0)
			{
				return response()->json(['status' => 'failed', 'message' => 'This provider not available. Please select another one.']);
			}
			else
			{
				//Provider Assign to Online Visit
				$data = array(
					'online_visit_id' => $online_visit_id,
					'provider_id' => $provider_id,
					'status' => 1
				);
				
				OnlineVisitProviderAssign::create($data);

				//Online Visit Status Timeline

				$data2 = array(
					'online_visit_id' => $online_visit_id,
					'old_visit_status' => NULL,
					'new_visit_status' => 'PENDING ACCEPTANCE',

				);

				OnlineVisitStatusTimeline::create($data2);

				$data3 = array(
					'provider_id' => $provider_id,
					'status' => 1
				);

				OnlineVisitPaywallFeature::where('online_visit_id', $online_visit_id)
					->where('patient_id', auth()->user()->id)
					->where('status', 0)
					->update($data3);

				return response()->json(['status' => 'success', 'message' => 'Provider Selected Successfully.']);
			}
		} 
		else {
			return response()->json(['status' => 'error', 'message' => 'Some Error Occured.']);
		}
	}

	public function profile()
	{
		
		$data['page'] = 'user_profile';
		$data['meta_title'] = 'My Profile';
		$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';
		$data['states'] = State::get();
		$data['shipping_data']  = ShippingAddress::where('user_id',auth()->user()->id)->first();
		$data['living_data']  = User::where('id',auth()->user()->id)->first();
		return view('front.user.profile', $data);
	}


	public function update_profile(Request $request){

		if($request->ajax()) {

			$user = User::find(auth()->user()->id);
			
			if($user && $user->count() > 0){
				
				$messages = [
					'first_name.regex' => 'First Name field accepts alphabetical characters only.',
					'last_name.regex' => 'Last Name field accepts alphabetical characters only.',
				];
				
				$validator = Validator::make($request->all(), [
					'first_name' => 'required|string|regex:/^[A-Za-z. -]+$/|max:120',
					'last_name' => 'required|string|regex:/^[A-Za-z. -]+$/|max:120',
					'phone' => 'required|string',
					'gender' => 'required|string',
					'dob' => 'required|string',
				], $messages)->validate();	
				
				$input_data = array(
					'first_name' => $request->first_name,
					'last_name' => $request->last_name,
					'name' => $request->first_name.' '.$request->last_name,
					'gender' => $request->gender,
					'phone' => $request->phone,
					'dob' => date('Y-m-d',strtotime($request->dob)),
					'short_info' => ($request->profile_summary != "") ? $request->profile_summary : null,
					'education'	=> ($request->eduction != "") ? $request->eduction : null,
					'goal'	=> ($request->goal != "") ? $request->goal : null,
					'interest'	=> ($request->interest != "") ? $request->interest : null,
					'relation_status' => ($request->rel_status != "") ? $request->rel_status : null,
					'updated_at' => date('Y-m-d H:i:s'),
				);
				
				User::where('id',$user->id)->update($input_data);
				
				$input_data['dob']=date(env('DATE_FORMAT_PHP'),strtotime($request->dob));
				$input_data['gender'] =ucfirst($request->gender);
				$msg = 'Profile updated successfully';
				return response()->json(['status'=> 'success', 'redirect'=> '', 'message' => $msg, 'user_data'=>$input_data]);					
				
				
			}else{
				$msg = 'Error occured. Please try again.';
				return response()->json(['status'=> 'error', 'redirect'=> '', 'message' => $msg]);				
			}						
		
		}else{
			$msg = 'Error occured. Please try again.';
			return response()->json(['status'=> 'error', 'redirect'=> '', 'message' => $msg]);				
		}	
	}
	
	public function shipping(Request $request)
	{
		if($request->ajax()){
			$address = $request->address_1.', '.$request->address_2.', '.$request->city.', '.$request->state.', '.$request->postal_code;
			$address = str_replace(", ,",",",$address);
			
			$data = array(
				'address' => $address,
				'address_line1' => $request->address_1,
				'address_line2' => $request->address_2,
				'city' => $request->city,
				'state' => $request->state,
				'postal_code' => $request->postal_code
			);

			$saddress = $request->copy_address_1.', '.$request->copy_address_2.', '.$request->copy_city.', '.$request->copy_state.', '.$request->copy_postal_code;
			$saddress = str_replace(", ,",",",$saddress);
			
			$data_2 = array(
				'user_id' => auth()->user()->id,				
				'address_line1' => $request->copy_address_1,
				'address_line2' => $request->copy_address_2,
				'city' => $request->copy_city,
				'state' => $request->copy_state,
				'postal_code' => $request->copy_postal_code,
				'is_primary' => 1,

			);

			#Update user address into users table
			User::where('id',auth()->user()->id)->update($data);
			

			#Update shipping address into shipping table
			$where['user_id'] = auth()->user()->id;
			ShippingAddress::updateOrCreate($where, $data_2);
			
			return response()->json(['status' => 'success', 'message' => 'Shipping address saved successfully.', 'ship_data'=>$saddress, 'living_data'=>$data]);
		}
		else
		{
			return response()->json(['status' => 'error', 'message' => 'Some Error Occured.']);
		}
		
	}
	
	public function change_password(Request $request){

		if($request->ajax()) {

			$user = User::find(auth()->user()->id);
			
			if($user && $user->count() > 0){
				
				$messages = [
					'password.regex' => 'Password must contain at least 8 characters, at least one number and both lower and uppercase letters and special characters',
				];
				
				$validator = Validator::make($request->all(), [
					'old_password' => ['required', function ($attribute, $value, $fail) use ($user) {
						if (!Hash::check($value, $user->password)) {
							return $fail(__('Old password is incorrect.'));
						}
					}],
					'password' => 'required|max:100|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^\w\s]).{8,}$/',
					'password_confirmation' => 'required_with:password|same:password'
				], $messages)->validate();	
					
				$input_data = array(
					'password' => Hash::make($request->password),
					'updated_at' => date('Y-m-d H:i:s'),
				);
				
				User::where('id',$user->id)->update($input_data);
				
				$msg = 'Password changed successfully';
				return response()->json(['status'=> 'success', 'redirect'=> '', 'message' => $msg]);					
				
				
			}else{
				$msg = 'Error occured. Please try again.';
				return response()->json(['status'=> 'error', 'redirect'=> '', 'message' => $msg]);				
			}						
		
		}else{
			$msg = 'Error occured. Please try again.';
			return response()->json(['status'=> 'error', 'redirect'=> '', 'message' => $msg]);				
		}	
	}
	
	public function treatments(Request $request){

		if($request->ajax()) {	

			$where = array();

			if($request->visit_type !=''){ $where['online_visits.visit_type'] = $request->visit_type; }
			 
			if($request->service !=''){ $where['online_visits.service_id'] = $request->service; }

			if($request->visit_status !=''){ $where['online_visits.visit_status'] = $request->visit_status; }

			$data['visits'] =  OnlineVisit::select('online_visits.*', 'providers.id AS provider_id', 'providers.name AS provider_name', 'providers.image AS provider_image', 'providers.chat_id AS provider_chat_id','services.id AS my_service_id','services.name AS service_name', 'services.visit_type AS service_visit_type', 'services.short_info AS service_info', 'orders.id AS order_id', 'orders.remarks AS orders_remarks', 'orders.amount AS orders_amount', 'subscriptions.id AS subscription_id', 'treatment_medicines.id AS medicine_id', 'treatment_medicines.name AS medicine_name', 'treatment_medicines.images AS medicine_images', 'medicine_variants.id AS variant_id', 'medicine_variants.variant_name', 'medicine_variants.pill_qty', 'medicine_variants.price AS variant_price','events.id AS event_id','events.start AS event_start','events.status AS event_status','events.paywall_feature_video_call AS event_video_call','events.paywall_feature_chat AS event_chat')	
				->join('services', function ($join) {
					$join->on('services.id', '=', 'online_visits.service_id');
				})
				->leftJoin('online_visit_provider_assign', function ($join) {
					$join->on('online_visit_provider_assign.online_visit_id', '=', 'online_visits.id');
				})
				->leftJoin('users as providers', function ($join) {
					$join->on('providers.id', '=', 'online_visit_provider_assign.provider_id');
				})
				->leftJoin('orders', function ($join) {
					$join->on('orders.online_visit_id', '=', 'online_visits.id')->on('orders.user_id', '=', 'online_visits.patient_id');
				})
				->leftJoin('subscriptions', function ($join) {
					$join->on('subscriptions.id', '=', 'orders.sub_id')->on('subscriptions.user_id', '=', 'orders.user_id');
				})
				->leftJoin('treatment_medicines', function ($join) {
					$join->on('treatment_medicines.id', '=', 'subscriptions.medicine_id');
				})
				->leftJoin('medicine_variants', function ($join) {
					$join->on('treatment_medicines.id', '=', 'medicine_variants.medicine_id');
				})
				->leftJoin('events', function($join) {
					$join->on('online_visits.id', '=', 'events.online_visit_id')->on('online_visit_provider_assign.provider_id', '=', 'events.provider_id')->on('services.id', '=', 'events.service_id')->on('events.patient_id', '=', 'online_visits.patient_id');
				})
				->where('online_visits.patient_id', auth()->user()->id);
			
			if($where){
				$data['visits'] = $data['visits']->where($where);
			}

			if($request->start_date != '' && $request->end_date != '')	{
				$data['visits'] = $data['visits']->whereDate('online_visits.created_at', '>=', $request->start_date)
				->whereDate('online_visits.created_at',   '<=', $request->end_date);
			}

			$data['visits'] =  $data['visits']->groupBy('online_visits.id')
								->orderBy('online_visits.created_at', 'DESC')
								->paginate(15);
				
			$output = '';
			
            
			if($data['visits'] && $data['visits']->count() > 0){
				$output = view('front.user.get_treatments',$data)->render();
				return response()->json(['status'=> 'success', 'message' => 'Success', 'data' => $output]);
			}
			else{
				return response()->json(['status'=> 'error', 'message' => 'No Data Found.', 'data' => $output]);
			}			
			
		}
		else{
			$data['page'] = 'user_treatments';
			$data['meta_title'] = 'My Treatments';
			$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';
			$data['services'] = Service::where('status',1)->get();
			return view('front.user.treatments', $data);
		}


	}


    public function appointments(Request $request) {
        $data['page'] = 'user_appointments';
		$data['meta_title'] = 'Appointments';
		$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';	
			
		$data['services'] = Service::where('status',1)->get();

/* 		// get all future events on a calendar
 		$events = Event::get();
		$data['appointments'] = array();
		
		if($events){
			
			$x=0;
			foreach($events as $key => $event){
				$data['appointments'][$x]['id'] = $event->id;
				$data['appointments'][$x]['name'] = $event->name;
				$data['appointments'][$x]['description'] = $event->description;
				$data['appointments'][$x]['startDateTime'] = $event->startDateTime;
				$data['appointments'][$x]['endDateTime'] = $event->endDateTime;
				$x++;
				
			}
		} 

		echo '<pre>';print_r($data['appointments']);die; */

        if($request->ajax()) {
			
			$where = array();

			if($request->service !=''){ $where['events.service_id'] = $request->service; }

			if($request->visit_type !=''){ $where['online_visits.visit_type'] = $request->visit_type; }
			 
			if($request->visit_status !=''){ $where['online_visits.visit_status'] = $request->visit_status; }

             $json_data = EventAppointment::select('events.id','events.start','events.end','events.status','events.remarks', 'events.provider_timeslot_id', 'title2 AS title', 'provider_timeslots.location', 'services.name AS service',DB::raw('(CASE 
			 WHEN online_visits.visit_type = "1" THEN "Asynchronous" 
			 WHEN online_visits.visit_type = "2" THEN "Synchronous" 
			 ELSE "Concierge" 
			 END) AS visit_type'))
							->join('online_visits', function($join) {
								$join->on('events.online_visit_id', '=', 'online_visits.id');
							})
							->join('users AS providers', function($join) {
								$join->on('events.provider_id', '=', 'providers.id');
							})
							->join('services', function($join) {
								$join->on('events.service_id', '=', 'services.id');
							})
							->join('provider_timeslots', function($join) {
								$join->on('events.provider_timeslot_id', '=', 'provider_timeslots.id');
							})								
							->where('online_visits.visit_status', '!=', 'PENDING ACCEPTANCE')
							->where('events.patient_id', '=', auth()->user()->id)
			 				->whereDate('events.start', '>=', $request->start_date)
                       		->whereDate('events.end',   '<=', $request->end_date);
			if($where){
				$json_data = $json_data->where($where);
			}
							
	   		$json_data = $json_data->get();
		
			if($json_data && $json_data->count() > 0){
				return response()->json($json_data);
			}else{
				return false;
			}
			  
            
        }
		
        return view('front.user.appointments', $data);
    }
	
	public function subscriptions()
	{
		$data['page'] = 'user_subscriptions';
		$data['meta_title'] = 'My Subscriptions';
		$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';
		return view('front.user.subscriptions', $data);
	}

	public function orders()
	{
		$data['page'] = 'user_orders';
		$data['meta_title'] = 'My Orders';
		$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';
		return view('front.user.orders', $data);
	}

	//Video Call Feature
	public function video_call($online_visit_id, $provider_id)
	{
		$online_visit = OnlineVisit::select('online_visits.*')
			->where('id', $online_visit_id)
			->where('is_submit', 1)
			->where('visit_status', '!=', 'PENDING ACCEPTANCE')
			->first();

		if ($online_visit && $online_visit->count() > 0) {

			$provider_assign = OnlineVisitProviderAssign::where('online_visit_id', $online_visit_id)->where('provider_id', $provider_id)->where('status', 1)->first();

			if ($provider_assign && $provider_assign->count() > 0) {
				$feature_check = OnlineVisitPaywallFeature::where('online_visit_id', $online_visit_id)
					->where('patient_id', auth()->user()->id)
					->where('provider_id', $provider_id)
					->where('status', 1)
					->first();

				if ($feature_check && $feature_check->count() > 0) {
					//Check Appointment Event
					$event = EventAppointment::where('online_visit_id', $online_visit_id)
						->where('patient_id', auth()->user()->id)
						->where('provider_id', $provider_id)
						->where('service_id', $online_visit->service_id)
						->first();

					if ($event && $event->count() > 0 && $event->status == 1) {
						echo 'Video Call Feature need to be added here.';
					} elseif ($event && $event->count() > 0 && $event->status == 0) {
						echo 'Wait for provider to accept appointment datetime';
					} else {
						return redirect('/user/select_appointment/' . $online_visit_id);
					}
				} else {
					echo 'Payment for Video Call Feature';
					//return redirect()->route('user.treatments');
				}
			} else {
				return redirect()->route('user.treatments');
			}
		} else {
			return redirect()->route('user.treatments');
		}
	}

	//Check for Video Call Feature
	public function check_video_call(Request $request){

		if ($request->ajax()) {
			$online_visit_id = $request->online_visit_id;
			$provider_id = $request->provider_id;

			$online_visit = OnlineVisit::select('online_visits.*')
				->where('id', $online_visit_id)
				->where('is_submit', 1)
				->where('visit_status', '!=', 'PENDING ACCEPTANCE')
				->first();

			if ($online_visit && $online_visit->count() > 0) {

				$provider_assign = OnlineVisitProviderAssign::where('online_visit_id', $online_visit_id)->where('provider_id', $provider_id)->where('status', 1)->first();

				if ($provider_assign && $provider_assign->count() > 0) {
					$feature_check = OnlineVisitPaywallFeature::where('online_visit_id', $online_visit_id)
						->where('patient_id', auth()->user()->id)
						->where('provider_id', $provider_id)
						->where('status', 1)
						->first();
					
					
					if (($feature_check && $feature_check->count() > 0) || $online_visit->visit_type != 1) {
						
						//Check Appointment Event
						$event = EventAppointment::where('online_visit_id', $online_visit_id)
						->where('patient_id', auth()->user()->id)
						->where('provider_id', $provider_id)
						->where('service_id', $online_visit->service_id)
						->where('status','!=',2)
						->first();
						
						if ($event && $event->count() > 0 && $event->status == 1) {
							$msg =  '';
							$redirect = env('APP_URL').'/myvideo/'.$provider_id;
							return response()->json(['status' => 'success', 'message' => $msg, 'redirect' => $redirect, 'type' => 'video_call']);
						} elseif ($event && $event->count() > 0 && $event->status == 0) {
							$msg =  'Please wait untill your provider don\'t accept your schedule appointment.';
							$redirect = '';
							return response()->json(['status' => 'error', 'message' => $msg, 'redirect' => $redirect]);
						} else {
							$msg = '';
							 $redirect = url('/user/select_appointment/'.$online_visit_id);
							return response()->json(['status' => 'success', 'message' => $msg, 'redirect' => $redirect]);
						}
					} else {
						
						//Check Appointment Event
						if($request->paywall_feature=='video_call'){
							$where_f='paywall_feature_video_call';
						}else{
							$where_f='paywall_feature_chat';
						}
						$event = EventAppointment::where('online_visit_id', $online_visit_id)
						->where('patient_id', auth()->user()->id)
						->where('provider_id', $provider_id)
						->where('service_id', $online_visit->service_id)
						->where('status','!=',2)
						->where($where_f,1)
						->first();
						if ($event && $event->count() > 0 && $event->status == 1) {
							$msg =  '';
							$redirect = env('APP_URL').'/myvideo/'.$provider_id;
							return response()->json(['status' => 'success', 'message' => $msg, 'redirect' => $redirect, 'type' => 'video_call']);
						} elseif ($event && $event->count() > 0 && $event->status == 0) {
							$msg =  'Please wait untill your provider don\'t accept your schedule appointment.';
							$redirect = '';
							return response()->json(['status' => 'error', 'message' => $msg, 'redirect' => $redirect]);
						} else {
							$msg =  'Payment for Video Call Feature not done.';
							//$redirect = route('payment_page');
							$redirect = route('user.paywall_payment_page');
							session()->put('paywall_feature_online_visit_id', $online_visit_id);
							session()->put('paywall_feature_provider_id', $provider_id);
							session()->put('paywall_feature_type', $request->paywall_feature);
							
							return response()->json(['status' => 'success', 'message' => $msg, 'redirect' => $redirect, 'type' => 'payment_page']);
						}
					}
				} else {
					$msg = 'Provider not assigned yet for this visit.';
					$redirect = '';
					return response()->json(['status' => 'error', 'message' => $msg, 'redirect' => $redirect]);
				}
			} else {
				$msg = 'Visit not found';
				$redirect = '';
				return response()->json(['status' => 'error', 'message' => $msg]);
			}
		}
	}
		
	public function provider_feedback(Request $request){
		
		if($request->ajax()){

			$messages = [
				'rating.required' => 'Rating is required.',
				'review_text.required' => 'Feedback is required.',
			];
			$validator = Validator::make($request->all(), [
				'review_text' => 'required|string',
				'rating' => 'required'				
			], $messages)->validate();	
				
			$data = array(
				'provider_id' => $request->provider_id,
				'patient_id' => auth()->user()->id,
				'service_id' => $request->service_id,
				'rating' => $request->rating,
				'review_text' => $request->feedback,
				'status' => 1,
			);

			ProviderReview::create($data);
			
			OnlineVisit::where('id',$request->online_visit_id)->update(array('is_feedback_done' => 1));
			
			return response()->json(['status' => 'success', 'message' => 'Feedback Submitted Successfully']);

		}else{
			return response()->json(['status' => 'error', 'message' => 'Some Error Occured.']);
		}
	}

	public function paywall_payment_page(){
         $intent = SetupIntent::create(
            [], Cashier::stripeOptions()
        );
		
		//Video Call Feature
		$online_visit_id = session('paywall_feature_online_visit_id');
		$provider_id = session('paywall_feature_provider_id');
		
		$online_visit = OnlineVisit::select('online_visits.*')
			->where('id', $online_visit_id)
			->where('is_submit', 1)
			->where('visit_status', '!=', 'PENDING ACCEPTANCE')
			->first();
				
				
		if($online_visit && $online_visit->count() > 0){

			$provider_assign = OnlineVisitProviderAssign::where('online_visit_id', $online_visit_id)->where('provider_id', $provider_id)->where('status', 1)->first();

			if($provider_assign && $provider_assign->count() > 0){
				
				$feature_check = OnlineVisitPaywallFeature::where('online_visit_id', $online_visit_id)
					->where('patient_id', auth()->user()->id)
					->where('provider_id', $provider_id)
					->where('status', 1)
					->first();

				if(($feature_check && $feature_check->count() > 0) || $online_visit->visit_type != 1){
					return redirect()->route('user.treatments');
				}else{
					
					session()->put('paywall_feature_title', 'Video Call Feature'); //paywall feature amount;
					session()->put('paywall_feature_amount', 50); //paywall feature amount;
					session()->put('final_amount', 50); //paywall feature amount;
					session()->put('is_being_payment', 1); //1 for yes, 0 for No, 2 for finished
					//session()->put('paywall_feature_type', 'video_call');
					session()->put('paywall_feature_service_id', $online_visit->service_id);
					session()->put('paywall_feature_visit_type', $online_visit->visit_type);
					
					if(!empty(auth()->user()->id)){
						$user_id=auth()->user()->id;
						$card_info = DB::table('payment_card_info')->select('*')->where(array('user_id' => $user_id))->orderBy('card_id','DESC')->get();
						
						if(!empty($card_info)){
							$intent['card_info']= $card_info;
							
						}
						$card_primary = DB::table('payment_card_info')->select('*')->where(array('user_id' => $user_id,'is_primary'=>1))->orderBy('card_id','DESC')->get();
						if(!empty($card_primary) && !empty($card_primary[0]->card_id)){
							$intent['card_primary']= $card_primary[0]->card_id;
						}else{
							$intent['card_primary']="new";
						}
					}
					
					return view('front.payment_page', compact('intent'));					
				}
			}
		}		 
		
	}
	
	public function paywall_success_page(){

		if(Session::has('is_being_payment') && session('is_being_payment') == 2){
			
			session()->forget('paywall_feature_amount');		
			session()->forget('paywall_feature_title');
			session()->forget('paywall_feature_provider_id');
			session()->forget('paywall_feature_online_visit_id');
			
			$data['page'] = 'payment_success';
					
			return view('front.user.paywall_success_page', $data);
		}else{
			session()->put('is_being_payment', 1); //1 for yes, 0 for No, 2 for finished
			redirect('user.paywall_payment_page')->withErrors('Payment failed');
		}
	}

	public function changeProfilePic(Request $request)
	{ 
	
		$status = 'error';
		$message = 'Error occured';
		
		if (!empty($request->file('file_input'))) {
            $file = $request->file('file_input');
			if(strtolower($file->getClientOriginalExtension()) == 'jpg' || strtolower($file->getClientOriginalExtension()) == 'png' || strtolower($file->getClientOriginalExtension()) == 'jpeg'){
				$name= time().'-'.rand(11111111,99999999).'.'.strtolower($file->getClientOriginalExtension());
				$destinationPath = 'uploads/users/';
				
				if($file->move($destinationPath, $name)){
					$image = 'uploads/users'.'/'.$name;
						
					$input_data = [
						'image' => $image,
						'updated_at' => date('Y-m-d H:i:s')
					];	
					
					User::where('id',auth()->user()->id)->update($input_data);					
					
					$status = 'success';
					$message = 'Profile pic updated successfully.';
									
				}				
			}else{
				$status = 'error';
				$message = 'Only jpg, jpeg and png files are allowable';					
			}

        }
		
        return response()->json(['status'=>$status, 'message'=>$message]);
	}
	
	function unread_message(){
		//check unread message
		$userLogin = auth()->user()->email;
		$userPassword =env('QUICKBLOX_PASSWORD');

		$body = [
			'application_id' =>env('QUICKBLOX_APPLICATION_ID'),
			'auth_key' =>env('QUICKBLOX_AUTH_KEY'),
			'nonce' => time(),
			'timestamp' => time(),
			'user' => ['login' => $userLogin, 'password' => $userPassword]
		];
		$built_query = urldecode(http_build_query($body));
		$signature = hash_hmac('sha1', $built_query ,env('QUICKBLOX_AUTH_SECRET'));
		$body['signature'] = $signature;
		$post_body = http_build_query($body);
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, 'https://api.quickblox.com/session.json');
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $post_body);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($curl);
		$response = json_decode($response, true);
		if(!empty($response)){
			$token=$response['session']['token'];
			$userId=$response['session']['user_id'];
			if(!empty($token)){
				$curl = curl_init();

				curl_setopt_array($curl, array(
				  CURLOPT_URL => 'https://api.quickblox.com/chat/Dialog.json?type=3&sort_desc=last_message_date_sent',
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => '',
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 0,
				  CURLOPT_FOLLOWLOCATION => true,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => 'GET',
				  CURLOPT_HTTPHEADER => array(
					'QB-Token:'.$token
				  ),
				));

				$response_dialog = curl_exec($curl);
				if(!empty($response_dialog)){
					$response_dialog = json_decode($response_dialog, true);
					$total_entries=$response_dialog['total_entries'];
					$items=$response_dialog['items'];
					
					foreach ($items as $val) {
						
						if($val['unread_messages_count']>0){
						    $chat_id =$val['last_message_user_id'];
						    $senderData = User::where('chat_id',$chat_id)->first();
							if(!empty($senderData) && $senderData->count()>0){
							    if(!empty($senderData->image)){
									$img =$senderData->image;
								}else{
									$img='dist/images/user_icon.png';
								}
								$noticeData = array(
									'created_id' => $senderData->id,
									'created_name' => $senderData->name,
									'created_image' => $img,
									'notification_type' => "unread_messages",
									'post_id' => $val['_id'],
									'followers_id' =>auth()->user()->id,
									'notice_type' =>$val['unread_messages_count'],
									'status' => 1,
									'created_at' => date('Y-m-d H:i:s'),
									'updated_at' => date('Y-m-d H:i:s')
								);
								$sender_notice = Notifications::where('followers_id',auth()->user()->id)->where('notification_type','unread_messages')->where('post_id',$val['_id'])->where('created_id',$senderData->id)->first();
								if(!empty($sender_notice) && $sender_notice->count()>0){
									Notifications::where('id',$sender_notice->id)->update($noticeData);
								}else{
									$notice = Notifications::create($noticeData);
								}
							}
						}
					}
				}
			}
		}
		
	}
	
	public function user_view($id){
		if($id != ''){
		$user = User::where('id',$id)->where('status',1)->first();
		$data=array();
		if(!empty($user) && $user->count() > 0){
		$followers = SocialUserFollowConnection::where('user_id', $id)->first();
		if(!empty($followers)){
		$followers_ids = explode(',', $followers['follower_id']);
		$data['followers_count'] = count($followers_ids);
		}else{
		$data['followers_count']=0;
		}
		$data['supporter'] = SupporterRequest::where(['user_id' => auth()->user()->id])->first();
		$data['supporting'] = SupporterRequest::where(['user_id' => $id])->first();
		$data['user']=$user;
		$data['id']=$id;
		$data['posts'] = $this->get_lates_post();
		$data['blogs'] = Article::select('*')
		->where('post_publish','=',1)
		->where('post_visibility','=',1)
		->where('is_verify',1)
		->limit(4)
		->latest()
		->get();
		return view('front.supporters.user_view',$data);
		}else{
		return redirect()->back();
		}
		}else{
		return redirect()->back();
		}
	}

	function get_lates_post(){
		#Stories detail
		$followerId = SocialUserFollowConnection::where('user_id', auth()->user()->id)->get();
		$followerId = (count($followerId)>0) ? $followerId[0]->follower_id : 0;
		
		if($followerId){
			$data =  DB::select( DB::raw("SELECT media_name from social_posts_media left join social_posts on
		social_posts_media.post_id = social_posts.post_id where social_posts.userid=".auth()->user()->id." OR (social_posts.userid IN (".$followerId.")) ORDER BY `social_posts_media`.media_id DESC LIMIT 16"));
		}else{
			$data = false;
		}
		return $data;	
	}

	function setting()
	{
		$data['page'] = 'provider_profile';
		$data['meta_title'] = 'My Profile';
		$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';	
		
		return view('front.user.setting', $data);
	}

	function setMerchantAccount(Request $request)
	{
		if ($request->ajax()) {
						
			if(empty($request->id)){
				$messages = [
					'login_id.regex' => 'Login Id is required field.',
					'transaction_key.regex' => 'Transaction Key is required field.',
				];
				$validator = Validator::make($request->all(), [
					'login_id' => 'required',
					'transaction_key' => 'required'
				], $messages)->validate();	

				$input_data = array(
					'MERCHANT_LOGIN_ID' => trim($request->login_id),
					'MERCHANT_TRANSACTION_KEY' => trim($request->transaction_key)
				);
			}

			User::where('id',auth()->user()->id)->update($input_data);
			
			$msg = 'Merchant detail saved successfully.';
			$redirect = route('user.dashboard');	

			return response()->json(['status'=> 'success', 'redirect'=> $redirect, 'message' => $msg, 'MERCHANT_LOGIN_ID'=>$request->login_id, 'MERCHANT_TRANSACTION_KEY'=>$request->transaction_key]);
		}
		else{
			return response()->json(['status'=> 'error', 'message' => 'This request is not valid.', 'data' => '']);
		}
	}
		
}


