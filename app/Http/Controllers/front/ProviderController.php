<?php

namespace App\Http\Controllers\front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Session;
use App\Library\Services\CommonService;
use App\User;
use App\ProviderSetting;
use App\ProviderLocation;
use App\Testimonial;
use App\Sponsor;
use App\ServicePricing;
use App\Faq;
use App\FaqCategory;
use App\ContactUs;
use App\ProviderCategory;
use App\Service;
use App\ProviderService;
use App\TreatmentMedicine;
use App\OnlineVisit;
use App\VisitQuestion;
use App\EventAppointment;
use App\OnlineVisitProviderAssign;
use App\ProviderTimeSlot;
use App\Race;
use App\Article;
use App\SupporterRequest;
use Spatie\GoogleCalendar\Event;
use DB;
use Carbon\Carbon;
use App\Notifications;
use App\SocialUserFollowConnection;
use App\Events\Accept_appointment;
use App\Events\Decline_appointment;
use App\Events\Visit_all_status;

class ProviderController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
	 
    public function __construct()
    {
		 $this->middleware('provider_auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
	 
	public function getLicenseExpireAttribute($date)
	{
		return Carbon::parse($date);
	}
	 
    public function index() {
			
        $data['page'] = 'provider_dashboard';
		$data['meta_title'] = 'Dashboard';
		$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';
		if(!empty(auth()->user()->chat_id)){
			//$this->unread_message();
		}
		
		//Blog Stories
		$data['blogs'] = Article::select('*')
					->where('post_publish','=',1)
					->where('post_visibility','=',1)
					->limit(4)
					->latest()
					->get();
						
        return view('front.provider.index', $data);
    }

	public function provider_preference()
	{
		$data['page'] = 'provider_dashboard';
		$data['meta_title'] = 'Provider Preference';
		$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';		
        
		$providerSetting = ProviderSetting::where('provider_id',auth()->user()->id)->first();
		
		$data['provider_location'] = ProviderLocation::where('provider_id',auth()->user()->id)->get();
		
		if($providerSetting && $providerSetting->count() > 0){	
			$data['office_start_end_time'] = json_decode($providerSetting->office_start_end_time);	
			$data['break_start_end_time'] = json_decode($providerSetting->break_start_end_time);	
		}else{	
			$data['office_start_end_time'] = array();	
			$data['break_start_end_time'] = array();				
		}

		if($providerSetting && $providerSetting->count() > 0){	
			$data['weekend'] = implode(', ',explode(',',$providerSetting->weekend_days));
			$data['weekendC'] = $providerSetting->weekend_days;
		}else{	
			$data['weekend'] = '';				
		}
		return view('front.provider.provider_preference', $data);
	}

	public function update_login_status(Request $request)
	{
		$input_data = array(
			'is_first_login' => 0,
			'updated_at' => date('Y-m-d H:i:s')
		);
		
		User::where('id',auth()->user()->id)->update($input_data);
		
		$msg = '';
		return response()->json(['status'=> 'success', 'redirect'=> '', 'message' => $msg]);
	}
	
    public function profile() {
        $data['page'] = 'provider_profile';
		$data['meta_title'] = 'My Profile';
		$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';	
		
		$providerSetting = ProviderSetting::where('provider_id',auth()->user()->id)->first();
		
		if($providerSetting && $providerSetting->count() > 0){	
			$data['office_start_end_time'] = json_decode($providerSetting->office_start_end_time);	
			$data['break_start_end_time'] = json_decode($providerSetting->break_start_end_time);	
		}
		else
		{	
			$data['office_start_end_time'] = array();	
			$data['break_start_end_time'] = array();				
		}
		
		$categories = explode(',', auth()->user()->category);
		$categories_arr = array();
		
		if($categories){
			$category_names = ProviderCategory::whereIn('id', $categories)->select('name')->get();
			
			if($category_names && $category_names->count() > 0){
				foreach ($category_names as $key => $value) {
					array_push($categories_arr, $value->name);
				}
			}
		}
		
		$data['provider_categories'] = $categories_arr;
		
		$data['provider_race'] = Race::where('id',auth()->user()->race)->first();
		$data['services'] = Service::where('status',1)->get();	
		$data['races'] = Race::where('status',1)->get();	
		
		$pro_services_arr = array();
		$pro_service_names = ProviderService::where('provider_id', auth()->user()->id)->where('status',1)->select('service_id')->get();
		
		if($pro_service_names && $pro_service_names->count() > 0){
			foreach ($pro_service_names as $key => $value) {
				array_push($pro_services_arr, $value->service_id);
			}
		}
		
		$data['provider_services'] = $pro_services_arr;
		
        return view('front.provider.profile', $data);
    }
	
	public function update_profile(Request $request){
		if($request->ajax()) {
			$user = User::find(auth()->user()->id);
			
			if($user && $user->count() > 0){
				
				$messages = [
					'first_name.regex' => 'First Name field accepts alphabetical characters only.',
					'last_name.regex' => 'Last Name field accepts alphabetical characters only.'
				];
				
				$validator = Validator::make($request->all(), [
					'first_name' => 'required|string|regex:/^[A-Za-z. -]+$/|max:120',
					'last_name' => 'required|string|regex:/^[A-Za-z. -]+$/|max:120',
					'gender' => 'required|string',
					'dob' => 'required|string',
					'race' => 'required|string',
				], $messages)->validate();	
					
				$input_data = array(
					'first_name' => $request->first_name,
					'last_name' => $request->last_name,
					'name' => $request->first_name.' '.$request->last_name,
					'gender' => $request->gender,
					'dob' => date('Y-m-d',strtotime($request->dob)),
					'race' => $request->race,
					'education' => $request->education,
					'goal' => $request->goal,
					'interest' => $request->interest,
					'relation_status' => $request->rel_status,
					'updated_at' => date('Y-m-d H:i:s'),
				);
				
				User::where('id',$user->id)->update($input_data);
				
				$msg = 'General information saved successfully';
				$input_data['gender'] =ucfirst($request->gender);
				$input_data['dob'] =date(env('DATE_FORMAT_PHP'),strtotime($request->dob));
				$race =Race::where('id',$request->race)->first();
				$input_data['race'] =$race->name;
				return response()->json(['status'=> 'success', 'redirect'=> '', 'user_data'=>$input_data, 'message' => $msg]);					
				
				
			}else{
				$msg = 'Error occured. Please try again.';
				return response()->json(['status'=> 'error', 'redirect'=> '', 'message' => $msg]);				
			}						
		
		}else{
			$msg = 'Error occured. Please try again.';
			return response()->json(['status'=> 'error', 'redirect'=> '', 'message' => $msg]);				
		}	
	}
	
	public function update_setting(Request $request)
	{
		if($request->ajax()) {
				/*$validator = Validator::make($request->all(), [
					'office_start_time' => 'required',
					'office_close_time' => 'required',
					'syn_slot_time' => 'required',
					'con_slot_time' => 'required',
				])->validate();*/
				
				$validator = Validator::make($request->all(), [
					'office_start_time' => 'required',
					'office_close_time' => 'required'
				])->validate();
					
				if (!empty($request->office_start_time)) {
					$off_stime = count($request->office_start_time);
					$off_start_end_timeArr = array();
					$office_location = array();
					for($i=0;$i<$off_stime;$i++)
					{
						$off_start_end_timeArr[] = array('st_time'=>$request->office_start_time[$i],'en_time'=>$request->office_close_time[$i],'location'=>$request->office_location[$i]);
					}
				}
				
				if (!empty($request->break_start_time)) {
					$break_stime = count($request->break_start_time);
					$break_start_end_timeArr = array();
					for($i=0;$i<$break_stime;$i++)
					{
						$break_start_end_timeArr[] = array('breakSt_time'=>$request->break_start_time[$i],'breakEn_time'=>$request->break_end_time[$i]);
					}
				}
				
				
				$input_data = array(
					'provider_id' => auth()->user()->id,
					'office_start_end_time' => json_encode($off_start_end_timeArr),
					'break_start_end_time' => json_encode($break_start_end_timeArr),
					'synchronus_slot_time' => '',
					'concierge_slot_time' => ''
				);
				
				ProviderSetting::updateOrCreate(['provider_id' => auth()->user()->id],$input_data);
				//User::where('id',$userid)->update($input_data);
				$msg = 'Profile setting updated successfully';
				
				$providerSetting = ProviderSetting::where('provider_id',auth()->user()->id)->first();
			
				if($providerSetting && $providerSetting->count() > 0){	
					$office_start_end_time = json_decode($providerSetting->office_start_end_time);	
					$break_start_end_time = json_decode($providerSetting->break_start_end_time);	
				}else{	
					$office_start_end_time = array();	
					$break_start_end_time = array();				
				}
				
				if($providerSetting && $providerSetting->count() > 0){	
					$weekend = $providerSetting->weekend_days;
				}else{	
					$weekend = '';				
				}
				
				$office_start ='<tr><td class="label-mute">Office Time</td><td>';
				if(count($office_start_end_time)>0){
					foreach($office_start_end_time as $val) {
						$office_start.=$val->st_time.' - '.$val->en_time.'<br>'; 
						} }
				$office_start.=	'</td></tr><tr><td class="label-mute">Break time</td><td>';
				if(count($break_start_end_time)>0){ 
					foreach($break_start_end_time as $val) { 
					$office_start.= $val->breakSt_time.' - '.$val->breakEn_time.'<br>';
					} }
				$office_start.='</td>
						</tr>';
						
				return response()->json(['status'=> 'success', 'redirect'=> '','user_data'=>$office_start,  'message' => $msg]);
		
		}
		else{
			$msg = 'Error occured. Please try again.';
			return response()->json(['status'=> 'error', 'redirect'=> '', 'message' => $msg]);				
		}
	}

	public function update_summary(Request $request)
	{
		if($request->ajax()) {
			$user = User::find(auth()->user()->id);
			if($user && $user->count() > 0){
				
				$messages = [
					'profile_summary.regex' => 'Please fill summary field.'
				];
				
				$validator = Validator::make($request->all(), [				
					'profile_summary' => 'required',
				], $messages)->validate();	
					
				$input_data = array(
					'short_info' => $request->profile_summary,					
					'updated_at' => date('Y-m-d H:i:s'),
				);
				
				User::where('id',$user->id)->update($input_data);
				
				$msg = 'Profile summary saved successfully';
				return response()->json(['status'=> 'success', 'redirect'=> '', 'message' => $msg, 'user_data'=>$input_data]);					
				
				
			}else{
				$msg = 'Error occured. Please try again.';
				return response()->json(['status'=> 'error', 'redirect'=> '', 'message' => $msg]);				
			}						
		
		}else{
			$msg = 'Error occured. Please try again.';
			return response()->json(['status'=> 'error', 'redirect'=> '', 'message' => $msg]);				
		}
	}

	public function update_contact(Request $request)
	{
		if($request->ajax()) {
			$user = User::find(auth()->user()->id);
			if($user && $user->count() > 0){
			$chCheck = 0;	
				$messages = [
					'phone_num.regex' => 'Please fill phone num field.',
					'email_id.regex' => 'Please fill email id field.'
				];
				
				$validator = Validator::make($request->all(), [				
					'phone_num' => 'required',
					'email_id' => 'required',
				], $messages)->validate();	
				
				if(strpos(',', $request->address) !== false ) {
					$chunk_address = explode(',',$request->address);
					$chCheck = 1;
				}
				
				
				
				$input_data = array(
					'phone' => $request->phone_num,
					'email' => $request->email_id,
					'address' => $request->address,	
					'address_line1' => 	($chCheck==1) ? $chunk_address[0] : $request->address,
					'address_line2' => 	($chCheck==1) ? $chunk_address[1] : '',
					'city' => 	($chCheck==1) ? $chunk_address[2] : '',			
					'updated_at' => date('Y-m-d H:i:s'),
				);
				
				User::where('id',$user->id)->update($input_data);
				
				$msg = 'Contact information saved successfully';
				return response()->json(['status'=> 'success', 'redirect'=> '','user_data'=>$input_data, 'message' => $msg]);					
				
				
			}else{
				$msg = 'Error occured. Please try again.';
				return response()->json(['status'=> 'error', 'redirect'=> '', 'message' => $msg]);				
			}						
		
		}else{
			$msg = 'Error occured. Please try again.';
			return response()->json(['status'=> 'error', 'redirect'=> '', 'message' => $msg]);				
		}
	}

	public function setting() {
        $data['page'] = 'provider_profile';
		$data['meta_title'] = 'My Profile';
		$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';	
		
		$providerSetting = ProviderSetting::where('provider_id',auth()->user()->id)->first();
		
		$data['provider_location'] = ProviderLocation::where('provider_id',auth()->user()->id)->get();
		
		if($providerSetting && $providerSetting->count() > 0){	
			$data['office_start_end_time'] = json_decode($providerSetting->office_start_end_time);	
			$data['break_start_end_time'] = json_decode($providerSetting->break_start_end_time);	
		}else{	
			$data['office_start_end_time'] = array();	
			$data['break_start_end_time'] = array();				
		}
		
		if($providerSetting && $providerSetting->count() > 0){	
			$data['weekend'] = $providerSetting->weekend_days;
		}else{	
			$data['weekend'] = '';				
		}
		
        return view('front.provider.setting', $data);
    }
	
	public function change_password(Request $request){

		if($request->ajax()) {

			$user = User::find(auth()->user()->id);
			
			if($user && $user->count() > 0){
				
				$messages = [
					'password.regex' => 'Password must contain at least 8 characters, at least one number and both lower and uppercase letters and special characters',
				];
				
				$validator = Validator::make($request->all(), [
					'old_password' => ['required', function ($attribute, $value, $fail) use ($user) {
						if (!Hash::check($value, $user->password)) {
							return $fail(__('Old password is incorrect.'));
						}
					}],
					'password' => 'required|max:100|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^\w\s]).{8,}$/',
					'password_confirmation' => 'required_with:password|same:password'
				], $messages)->validate();	
					
				$input_data = array(
					'password' => Hash::make($request->password),
					'updated_at' => date('Y-m-d H:i:s'),
				);
				
				User::where('id',$user->id)->update($input_data);
				
				$msg = 'Password changed successfully';
				return response()->json(['status'=> 'success', 'redirect'=> '', 'message' => $msg]);					
				
				
			}else{
				$msg = 'Error occured. Please try again.';
				return response()->json(['status'=> 'error', 'redirect'=> '', 'message' => $msg]);				
			}						
		
		}else{
			$msg = 'Error occured. Please try again.';
			return response()->json(['status'=> 'error', 'redirect'=> '', 'message' => $msg]);				
		}	
	}
	
	public function add_location(Request $request){
		if ($request->ajax()) {
			$messages = [];
			$validator = Validator::make($request->all(), [
				'add_type' => 'required',
				'address' => 'required'
			], $messages)->validate();	
	
			$data = array(
				'add_type' => trim($request->first_name),
				'address' => trim($request->last_name),
			);
			if (!empty($request->add_type)) {
				$count = count($request->add_type);
				for($i=0;$i<$count;$i++)
				{
					$input_data = array(
						'provider_id' => auth()->user()->id,
						'address_type' => $request->add_type[$i],
						'address' => $request->address[$i]
					);	
					ProviderLocation::updateOrCreate(['id' =>$request->location_id[$i]],$input_data);
					if($i==($count-1))
					{
						$msg = 'Location saved successfully.';
					}
				}
			}
			$provider_location = ProviderLocation::where('provider_id',auth()->user()->id)->get();
			$location_html="";
			$location_option="";
			$prefered_locations_box="";
			foreach($provider_location as $val){
			    $location_option.= '<option value="'.$val->id.'">'.$val->address_type.'</option>';
				$location_html.='<tr>
							<td class="label-mute">'.$val->address_type.'</td>
							<td>'.$val->address.'</td>
						</tr>';
				$prefered_locations_box.='<div class="prefered_location_text">'.$val->address_type.' - '.$val->address.'</div>';		
				
				}
		}
			
		return response()->json(['status'=> 'success', 'prefered_locations_box'=> $prefered_locations_box, 'user_data'=> $location_html, 'location_option'=> $location_option, 'message' => $msg]);
		
	}
	
	public function delete_location(Request $request){
		if($request->ajax()) {
			$id = $request->id;
			$location = ProviderLocation::where('id',$id)->delete();
			if($id)
			{
				$msg = 'Location removed successfully.';
				return response()->json(['status'=> 'success', 'redirect'=> '', 'message' => $msg]);
			}
		}
	}

	public function updateTreatCat(Request $request){
		
		if($request->ajax()) {
		
			$services = $request->treat_cat;
		
			if($services){
				foreach($services as $key=>$val){

					$where = array(
						'provider_id' => auth()->user()->id,
						'service_id' => $val
					);
					
					$input_data = array(
						'provider_id' => auth()->user()->id,
						'service_id' => $val,
						'status' => 1
					);
				
					ProviderService::updateOrCreate($where,$input_data);
				}

				$msg = 'Services saved successfully';
				$services_all = Service::where('status',1)->get();
				if($services_all){	
				$s_name='';
					foreach($services_all as $val){									
						if($services && in_array($val->id, $services)){
							$s_name .='<p>'.$val->name.'</p>'; 
						}
					}
				}
				return response()->json(['status'=> 'success', 'redirect'=> '','user_data'=>$s_name, 'message' => $msg]);
			}else{
				
			}				
			$msg = 'No service selected. Please try again.';
			return response()->json(['status'=> 'error', 'redirect'=> '', 'message' => $msg]);		
		}
		else{
			$msg = 'Error occured. Please try again.';
			return response()->json(['status'=> 'error', 'redirect'=> '', 'message' => $msg]);				
		}
	}
	
	public function deleteProviderService(Request $request){
		//status = 0 only
		if($request->ajax()) {
			
			$where = array(
				'provider_id' => auth()->user()->id,
				'service_id' => $request->service_id
			);
					
			ProviderService::where($where)->update(array('status' => 0));
			
			return true;
		}
	}

	public function add_weekend(Request $request)
	{
		if ($request->ajax()) 
		{
			if (!empty($request->weekend_day)) {
				$weekendDay = '';
				for($i=0;$i<count($request->weekend_day);$i++)
				{
					$weekendDay .= $request->weekend_day[$i].',';
				}
				$weekendDay = rtrim($weekendDay, ',');
				
				#Update provider weekend day selection
				$input_data = array(
					'weekend_days' => $weekendDay,
					'updated_at' => date('Y-m-d H:i:s')
				);	
				ProviderSetting::where('provider_id',auth()->user()->id)->update($input_data);
				
				#Update user first login status
				$user_data = array(
					'is_first_login' => 0,
					'updated_at' => date('Y-m-d H:i:s')
				);
				User::where('id',auth()->user()->id)->update($user_data);
				
				$msg = 'Weekend days saved successfully.';
				$weekendDay1 = implode(', ',explode(',',$weekendDay));
				
				return response()->json(['status'=> 'success', 'redirect'=> '', 'user_data'=>$input_data , 'newdata'=>$weekendDay1, 'message' => $msg]);
			}
		}
		else{
			$msg = 'Error occured. Please try again.';
			return response()->json(['status'=> 'error', 'redirect'=> '', 'newdata'=>'', 'message' => $msg]);				
		}	
		
	}
	
    public function patient_visits(Request $request) {
		
		if($request->ajax()) {	

			$where = array();

			if($request->visit_type !=''){ $where['online_visits.visit_type'] = $request->visit_type; }
			 
			if($request->service !=''){ $where['online_visits.service_id'] = $request->service; }

			if($request->visit_status !=''){ 
				
				if($request->visit_status == 'IMPORTANT'){
					$where['online_visits.is_imp'] = 1;
				}else{
					$where['online_visits.visit_status'] = $request->visit_status;
				}
			}

			$data['patient_visits'] =  OnlineVisit::select('online_visits.id AS online_visit_id','online_visits.visit_type','online_visits.visit_status','online_visits.created_at AS visit_time','online_visits.is_imp','users.id AS patient_id','users.name AS patient_name', 'users.image AS patient_image','users.gender', 'users.chat_id AS patient_chat_id','services.id AS service_id','services.name AS service_name','services.visit_type AS service_visit_type','services.short_info AS service_info','orders.id AS order_id','orders.remarks AS orders_remarks','orders.amount AS orders_amount','subscriptions.id AS subscription_id','treatment_medicines.id AS medicine_id','treatment_medicines.name AS medicine_name','treatment_medicines.images AS medicine_images','medicine_variants.id AS variant_id','medicine_variants.variant_name','medicine_variants.pill_qty','medicine_variants.price AS variant_price','events.id AS event_id','events.start AS event_start','events.status AS event_status','events.paywall_feature_video_call AS video_call_feature','events.paywall_feature_chat AS chat_feature')
						->join('users', function($join) {
							$join->on('users.id', '=', 'online_visits.patient_id');
						})
						->leftJoin('online_visit_provider_assign', function($join) {
							$join->on('online_visit_provider_assign.online_visit_id', '=', 'online_visits.id');
						})
						->join('services', function($join) {
							$join->on('services.id', '=', 'online_visits.service_id');
						})
						->leftJoin('orders', function($join) {
							$join->on('orders.online_visit_id', '=', 'online_visits.id')->on('orders.user_id', '=', 'online_visits.patient_id');
						})
						->leftJoin('subscriptions', function($join) {
							$join->on('subscriptions.id', '=', 'orders.sub_id')->on('subscriptions.user_id', '=', 'orders.user_id');
						})
						->leftJoin('treatment_medicines', function($join) {
							$join->on('treatment_medicines.id', '=', 'subscriptions.medicine_id');
						})
						->leftJoin('medicine_variants', function($join) {
							$join->on('treatment_medicines.id', '=', 'medicine_variants.medicine_id');
						})	
						->leftJoin('events', function($join) {
							$join->on('online_visits.id', '=', 'events.online_visit_id')->on('users.id', '=', 'events.patient_id')->on('online_visit_provider_assign.provider_id', '=', 'events.provider_id')->on('services.id', '=', 'events.service_id');
						})											
						->where('online_visit_provider_assign.provider_id',auth()->user()->id);
						

			if($where){
				$data['patient_visits'] = $data['patient_visits']->where($where);
			}

			if($request->start_date != '' && $request->end_date != '')	{
				$data['patient_visits'] = $data['patient_visits']->whereDate('online_visits.created_at', '>=', $request->start_date)->whereDate('online_visits.created_at',   '<=', $request->end_date);
			}
			
			$data['patient_visits'] = $data['patient_visits']->groupBy('online_visits.id')
						->orderBy('online_visits.created_at','DESC')
						->paginate(5);

			$output = '';

			if($data['patient_visits'] && $data['patient_visits']->count() > 0){
			    
				$output = view('front.provider.get_patient_visits',$data)->render();
				return response()->json(['status'=> 'success', 'message' => 'Success', 'data' => $output]);
			}else{
				return response()->json(['status'=> 'error', 'message' => 'No Data Found.', 'data' => $output]);
			}

		}else{
		    
			$data['page'] = 'provider_patient_visits';
			$data['meta_title'] = 'Patient Visits';
			$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';
			$data['services'] = Service::where('status',1)->get();
				
			return view('front.provider.patient_visits', $data);
		}

    }

	public function change_visit_status(Request $request){
		
		if ($request->ajax()) {
			$id = $request->id;
			$status = $request->status;
			
			$status =   ($status == 'accept') ? 'ACCEPTED' : (($status=='review') ? 'REVIEWED' : (($status=='complete') ? 'COMPLETE' : 'PENDING ACCEPTANCE'));
			
			$visit = OnlineVisit::where('id',$id)->first();
			
			if($visit && $visit->count() > 0){
				
				$flag = false;
				$msg = 'Success';
				
				if($visit->visit_status == 'PENDING ACCEPTANCE' && $status == 'ACCEPTED'){ 
					$flag = true; 
					$msg = 'Patient gigs accepted successfully.';
				}
				
				if($visit->visit_status == 'ACCEPTED' && $status == 'REVIEWED'){ 
					$flag = true;
					$msg = 'Patient gigs reviewed successfully.';
				}
				
				if($visit->visit_status == 'REVIEWED' && $status == 'COMPLETE'){ 
					$flag = true;
					$msg = 'Patient gigs completed successfully.';
				}
				
				if($visit->visit_status == 'ACCEPTED' && $status == 'COMPLETE'){ 
					$flag = true;
					$msg = 'Patient gigs completed successfully.';
				}
				
				if($visit->visit_type != '1'){ // if not an asynchronous telemedicine visit
				
					if($visit->visit_status == 'REVIEWED' && $status == 'SCHEDULED'){ 
						$flag = true;
						$msg = 'Patient gigs scheduled successfully.';
					}
					
					if($visit->visit_status == 'SCHEDULED' && $status == 'CANCELED'){ 
						$flag = true;
						$msg = 'Patient gigs canceled successfully.';
					}					
				}
							
				if($flag == true)
				{
					$data = array('visit_status' => $status);
					OnlineVisit::where('id',$id)->update($data);
					
					// send pusher Notifications
    				$follower_id =$visit->patient_id;
					if(!empty(auth()->user()->image)){
						$img =auth()->user()->image;
					}else{
						$img='dist/images/user_icon.png';
					}
					
    				$noticeData = array(
    					'created_id' => auth()->user()->id,
    					'created_name' => auth()->user()->name,
    					'created_image' => $img,
    					'notification_type' => "visit_all_status",
    					'post_id' => $request->id,
    					'followers_id' =>$follower_id,
    					'status' => 1,
    					'notice_type' =>$request->status,
    					'created_at' => date('Y-m-d H:i:s'),
    					'updated_at' => date('Y-m-d H:i:s')
    				);
    				
    				$notice = Notifications::create($noticeData);
					
					$noticeData['id']=$request->id;
					$noticeData['status_type']=$request->status;
					event(new Visit_all_status($noticeData));
    				//print_r($notice);
					
					return response()->json(['status'=> 'success', 'message' => $msg]);
				}
				else{
					return response()->json(['status'=> 'error', 'message' => 'Some error occured. please try again after sometime.']);
				}
				
			}else{
				return response()->json(['status'=> 'error', 'message' => 'Incorrect Visit']);
			}	
		}	
	}
	
	public function visit_detail($id){
		
        $data['page'] = 'provider_patient_visits';
		$data['meta_title'] = 'Visit Detail';
		$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';
		
		$data['patient_visit'] =  OnlineVisit::select('online_visits.id AS online_visit_id','online_visits.visit_type','online_visits.visit_status','online_visits.created_at AS visit_time','online_visits.provider_note','users.id AS patient_id','users.name AS patient_name', 'users.image AS patient_image','users.gender','users.dob','users.email','users.phone','services.name AS service_name','services.short_info AS service_info','orders.id AS order_id','orders.remarks AS orders_remarks','orders.amount AS orders_amount','subscriptions.id AS subscription_id','treatment_medicines.id AS medicine_id','treatment_medicines.name AS medicine_name','treatment_medicines.images AS medicine_images','medicine_variants.id AS variant_id','medicine_variants.variant_name','medicine_variants.pill_qty','medicine_variants.price AS variant_price')
				->join('users', function($join) {
					$join->on('users.id', '=', 'online_visits.patient_id');
				})
				->leftJoin('online_visit_provider_assign', function($join) {
					$join->on('online_visit_provider_assign.online_visit_id', '=', 'online_visits.id');
				})
				->join('services', function($join) {
					$join->on('services.id', '=', 'online_visits.service_id');
				})
				->leftJoin('orders', function($join) {
					$join->on('orders.online_visit_id', '=', 'online_visits.id')->on('orders.user_id', '=', 'online_visits.patient_id');
				})
				->leftJoin('subscriptions', function($join) {
					$join->on('subscriptions.id', '=', 'orders.sub_id')->on('subscriptions.user_id', '=', 'orders.user_id');
				})
				->leftJoin('treatment_medicines', function($join) {
					$join->on('treatment_medicines.id', '=', 'subscriptions.medicine_id');
				})
				->leftJoin('medicine_variants', function($join) {
					$join->on('treatment_medicines.id', '=', 'medicine_variants.medicine_id');
				})						
				->where('online_visits.id',$id)
				->first();
		
		if($data['patient_visit'] && $data['patient_visit']->count() > 0){
			
			$data['question_answers'] = $this->getVisitQuesAns($id);
			$data['last_visit'] = $this->getLastVisitDetail($data['patient_visit']->patient_id, $id);

			return view('front.provider.visit_detail', $data);
		}else{
			return redirect()->route('provider.patient_visits');
		}

	}
	
	public function add_appointment_timeslots(Request $request){
	
		$data['weekend_days_arr'] = $weekend_days = array();
		$data['weekend_days'] = '[]';
		
		$provider_setting = ProviderSetting::select('weekend_days')->where('provider_id',auth()->user()->id)->first();
		
		if($provider_setting && $provider_setting->count() > 0){
			
			if($provider_setting->weekend_days != ''){
				$data['weekend_days_arr'] = $weekend_days = explode(',',$provider_setting->weekend_days);
				$data['weekend_days'] = json_encode($weekend_days);
			}else{
    		    $weekend_days='';
    		    $data['weekend_days']='';
    		}
		}else{
		    $weekend_days='';
		    $data['weekend_days']='';
		}
		
		
		if ($request->ajax()) {
			
			$virtual_input_data = array();
			$concierge_input_data = array();
			$date = $request->appointment_date;
						
			
			if(isset($request->slots_data) && $request->slots_data){
				$slots_data = $request->slots_data;
				
				if(isset($slots_data['virtual']) && count($slots_data['virtual']) > 0){
					
					$virtual_slots_data = $slots_data['virtual'];
					
					if($virtual_slots_data['start_time'] && $virtual_slots_data['end_time']){
												
						foreach($virtual_slots_data['start_time'] as $key => $value){
							$virtual_input_data[$key]['start_time'] = $value;
						}
						
						foreach($virtual_slots_data['end_time'] as $key => $value){
							$virtual_input_data[$key]['end_time'] = $value;
						}
					}				
				}
				
				if(isset($slots_data['concierge']) && count($slots_data['concierge']) > 0){
					
					$concierge_slots_data = $slots_data['concierge'];
					
					if($concierge_slots_data['start_time'] && $concierge_slots_data['end_time']){
											
						foreach($concierge_slots_data['start_time'] as $key => $value){
							$concierge_input_data[$key]['start_time'] = $value;
						}
						
						foreach($concierge_slots_data['end_time'] as $key => $value){
							$concierge_input_data[$key]['end_time'] = $value;
						}
						
												
					}					
				}				
			}
			
			
			if(!empty($weekend_days)){
     			if($virtual_input_data){
    				$res1 = $this->copyWiseData($virtual_input_data,$request,$date,2,$weekend_days);
    			} 
    			
    			if($concierge_input_data){
    				$res2 = $this->copyWiseData($concierge_input_data,$request,$date,3,$weekend_days);
    			}
			}else{
			    if($virtual_input_data){
    				$res1 = $this->copyWiseData($virtual_input_data,$request,$date,2);
    			} 
    			
    			if($concierge_input_data){
    				$res2 = $this->copyWiseData($concierge_input_data,$request,$date,3);
    			}
			}
			
			if($res1==true || $res2==true)
			{
				return response()->json(['status'=> 'success', 'message' => 'Time slot saved successfully.']);
			}
			else{
				return response()->json(['status'=> 'error', 'message' => 'Some error occured. Try again later.']);
				//die("Failed");
			}
						

		}
		else{
			
			$data['page'] = 'provider_appointments';
			$data['meta_title'] = 'Add Appointment Availability Timeslots';
			$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';
			
			$providerSetting = ProviderSetting::select('office_start_end_time')->where('provider_id',auth()->user()->id)->first();

			if($providerSetting && $providerSetting->count() > 0){	
			
				$getVal = json_decode($providerSetting->office_start_end_time);
				
				$timeslot='';
				for($i=0;$i<count($getVal);$i++)
				{
					
					$start_time_ch = explode(' ',$getVal[$i]->st_time);
					$start_time_chnk = explode(':',$start_time_ch[0]);
					$start_time_t = ($start_time_ch[1]=='PM') ? ($start_time_chnk[0]+12) : $start_time_chnk[0];
					$start_time_a = $start_time_ch[1];

					$end_time_ch = explode(' ',$getVal[$i]->en_time);
					$end_time_chnk = explode(':',$end_time_ch[0]);
					$end_time_t = ($end_time_ch[1]=='PM') ? ($end_time_chnk[0]+12) : $end_time_chnk[0];
					$end_time_t = ($end_time_t - 1);
					$end_time_a = $end_time_ch[1];
					$timeslot .= implode(",",range($start_time_t,$end_time_t)).',';
				}
				$data['enable_time'] = rtrim($timeslot,',');
				
			}else{	
				$data['enable_time'] = '';	
			}
			
			return view('front.provider.add_appointment_timeslots', $data);	
		}		

	}

	public function copyWiseData($input_data,$request,$date,$type,$weekend_days=''){
		
		$result= false;
		if($request->setting_filter == 1){
			//This Date only
			foreach($input_data as $key =>$val){
				
				$input = array(
					'start_time' => date('Y-m-d H:i:s', strtotime($date.' '.$val['start_time'])),
					'end_time' => date('Y-m-d H:i:s', strtotime($date.' '.$val['end_time'])),
					'visit_type' => $type,
				);
				
				//if($type == 3){ 
				//	$input['location'] = isset($request->location) ? $request->location : null; 
				//}
				
				if(strtotime($input['end_time']) > strtotime($input['start_time']) ){
					$this->addEntry($input);
					$result =true;
				}
			}
		}
		elseif($request->setting_filter == 2){
			//This Week Only
			$dateArr = array();
			$start = $date;
			$end = date('Y-m-d',strtotime('next sunday',strtotime($start)));
			if(!empty($weekend_days)){
			    $dateArr = $this->getDatesFromRange($start,$end,$weekend_days);
			}else{
			    $dateArr = $this->getDatesFromRange($start,$end);
			}
								
			foreach($input_data as $key =>$val){
				foreach($dateArr as $k => $v){
					$input = array(
						'start_time' => date('Y-m-d H:i:s',strtotime($v.' '.$val['start_time'])),
						'end_time' => date('Y-m-d H:i:s',strtotime($v.' '.$val['end_time'])),
						'visit_type' => $type,
					);
					
					//if($type == 3){ 
					//	$input['location'] = isset($request->location) ? $request->location : null; 
					//}
				
					if(strtotime($input['end_time']) > strtotime($input['start_time']) ){
						//echo $input['start_time'].' - '.$input['end_time'];
						$this->addEntry($input);
						$result =true;
					}
				}
			}
			
			
		}
		elseif($request->setting_filter == 3){
			//This + Next Week
			$dateArr = array();
			
			$start = $date;
			$end = date('Y-m-d',strtotime('next sunday',strtotime($start)));
			if(!empty($weekend_days)){
			    $d1 = $this->getDatesFromRange($start,$end,$weekend_days);
			}else{
			    $d1 = $this->getDatesFromRange($start,$end);
			}
			
			
			$start = date('Y-m-d',strtotime('next monday',strtotime($date)));
			$end = date('Y-m-d',strtotime('next sunday',strtotime($start)));
			if(!empty($weekend_days)){
	             $d2 = $this->getDatesFromRange($start,$end,$weekend_days);
	        }else{
			    $d2 = $this->getDatesFromRange($start,$end);
			}		
			
			$dateArr = array_values(array_merge($d1,$d2));
													
			foreach($input_data as $key =>$val){	
				foreach($dateArr as $k => $v){
					$input = array(
						'start_time' => date('Y-m-d H:i:s', strtotime($v.' '.$val['start_time'])),
						'end_time' => date('Y-m-d H:i:s', strtotime($v.' '.$val['end_time'])),
						'visit_type' => $type,
					);
					
					//if($type == 3){ 
					//	$input['location'] = isset($request->location) ? $request->location : null; 
					//}
					
					if(strtotime($input['end_time']) > strtotime($input['start_time']) ){
						$this->addEntry($input);
						$result =true;
					}						
				}
			}						
		}
		elseif($request->setting_filter == 4){
			//This Month Only
			
			$dateArr = array();
			$start = $date;
			$end = date('Y-m-t',strtotime($start));
			if(!empty($weekend_days)){
			    $dateArr = $this->getDatesFromRange($start,$end,$weekend_days);
			}else{
			    $dateArr = $this->getDatesFromRange($start,$end);
			}	
			
			foreach($input_data as $key =>$val){
				foreach($dateArr as $k => $v){
					$input = array(
						'start_time' => date('Y-m-d H:i:s',strtotime($v.' '.$val['start_time'])),
						'end_time' => date('Y-m-d H:i:s',strtotime($v.' '.$val['end_time'])),
						'visit_type' => $type,
					);
					
					//if($type == 3){ 
					//	$input['location'] = isset($request->location) ? $request->location : null; 
					//}
				
					if(strtotime($input['end_time']) > strtotime($input['start_time']) ){
						$this->addEntry($input);
						$result =true;
					}						
				}
			}
		}				
		elseif($request->setting_filter == 5){
			//This + Next Month
			$dateArr = array();
			
			$start = $date;
			$end = date('Y-m-t',strtotime($start));
			if(!empty($weekend_days)){
			    $d1 = $this->getDatesFromRange($start,$end,$weekend_days);
			}else{
			    $d1 = $this->getDatesFromRange($start,$end);   
			}
			$start = date('Y-m-01',strtotime('next month',strtotime($date)));
			$end = date('Y-m-t',strtotime($start));
			
			if(!empty($weekend_days)){
			    $d2 = $this->getDatesFromRange($start,$end,$weekend_days);		
			}else{
			    $d2 = $this->getDatesFromRange($start,$end);	
			}
			
			$dateArr = array_values(array_merge($d1,$d2));
			
			foreach($input_data as $key =>$val){	
				foreach($dateArr as $k => $v){
					$input = array(
						'start_time' => date('Y-m-d H:i:s', strtotime($v.' '.$val['start_time'])),
						'end_time' => date('Y-m-d H:i:s', strtotime($v.' '.$val['end_time'])),
						'visit_type' => $type,
					);
					
					//if($type == 3){ 
					//	$input['location'] = $request->location; 
					//}
					
					if(strtotime($input['end_time']) > strtotime($input['start_time']) ){
						$this->addEntry($input);
						$result =true;
					}						
				}
			}						
		}
		elseif($request->setting_filter == 6){
			//Custom - Multiple Months
			$months = $request->custom_months; // array
						
			if($months){
				$start = date('Y-m-d',strtotime($months[0].'-01'));
				
				foreach($months as $x => $value){

					$op = explode('-',$value);
					$last_day = cal_days_in_month(CAL_GREGORIAN,$op[1],$op[0]); //Year,Month					
					$end = date('Y-m-d',strtotime($value.'-'.$last_day));
										
					$dateArr = array();
					if(!empty($weekend_days)){
					    $dateArr = $this->getDatesFromRange($start,$end,$weekend_days);
					}else{
					    $dateArr = $this->getDatesFromRange($start,$end);
					}
					
					foreach($input_data as $key =>$val){
						foreach($dateArr as $k => $v){
							$input = array(
								'start_time' => date('Y-m-d H:i:s',strtotime($v.' '.$val['start_time'])),
								'end_time' => date('Y-m-d H:i:s',strtotime($v.' '.$val['end_time'])),
								'visit_type' => $type,
							);
							
							//if($type == 3){ 
							//	$input['location'] = $request->location; 
							//}
					
							if(strtotime($input['end_time']) > strtotime($input['start_time']) ){
								$this->addEntry($input);
								$result =true;
							}						
						}
					}					
				}
			}						
		}
		return $result;
	}
	
	function getDatesFromRange($start,$end,$weekend_days=''){
		$result = array();
		$dates = array($start);
		//$weekend_days = array('Sat','Sun'); //hard coded, will be get from provider setting
				
		while(end($dates) < $end){			
			$dates[] = date('Y-m-d', strtotime(end($dates).' +1 day'));
		}
		
		if($dates){
			foreach($dates as $key => $value){
				$day = date('D',strtotime($value));
				
				if(!empty($weekend_days)){
					if( !in_array($day,$weekend_days) ){
						$result[] = $value;
					}
				}else{
					$result[] = $value;
				}
			}
		}
		return $result;
	}

	public function update_appointment_timeslot_single(Request $request){
		
		if ($request->ajax()) {	

			$event_start = date('Y-m-d H:i:s',strtotime($request->date_val.' '.$request->start_date));
			$event_end = date('Y-m-d H:i:s',strtotime($request->date_val.' '.$request->end_date));
			$location = isset($request->location) ? $request->location : null;
			$visit_type = $request->type_filter;
			
			if($visit_type == 3){
				$messages = [
					'type_filter.required' => 'Please select visit type field.',
					'start_date.required' => 'Please choose start time field.',
					'end_date.required' => 'Please choose end time field.',
					'location.required' => 'Please select location field.',
				];
				$validator = Validator::make($request->all(), [
					'type_filter' => 'required',
					'start_date' => 'required',
					'end_date' => 'required',
					'location' => 'required',
				], $messages)->validate();
			}
			else{
				$messages = [
					'type_filter.required' => 'Please select visit type field.',
					'start_date.required' => 'Please choose start time field.',
					'end_date.required' => 'Please choose end time field.',
				];
				$validator = Validator::make($request->all(), [
					'type_filter' => 'required',
					'start_date' => 'required',
					'end_date' => 'required',
				], $messages)->validate();
			}
	
			if(strtotime($event_start) >= strtotime($event_end)){
				return response()->json(['status'=> 'error', 'message' => 'End Time must be greater than Start Time.']);
			}else{
				$check = ProviderTimeSlot::where('provider_id',auth()->user()->id)
							->where('id',$request->id)
							->first();
							
				if($check && $check->count() > 0){
					$data = array(
						'provider_id' => auth()->user()->id,
						'event_start' => $event_start,
						'event_end' => $event_end,
						'status' => 1,
						'visit_type' => $visit_type,
						'location' =>$location
					);
					
					//echo '<pre>';print_r($data);die;
					ProviderTimeSlot::where('id',$check->id)->update($data);	
					
					return response()->json(['status'=> 'success', 'user_data'=>$data, 'message' => 'Time slot saved successfully.']);		
				}else{
					return response()->json(['status'=> 'error', 'message' => 'Timeslot not found.']);				
				}				
			}
		}		
	}
	
	
	public function addEntry($input){
		//echo '<pre>';
		//print_r($input);
		//die;
		$check = ProviderTimeSlot::where('provider_id',auth()->user()->id)
					->where('event_start',$input['start_time'])
					->where('event_end',$input['end_time'])
					->first();
					
		if($check && $check->count() > 0){
			//return response()->json(['status'=> 'error', 'message' => 'This time slot already added for this date.']);
		}else{
			
			$location = NULL;
			
			if($input['visit_type'] == 3){
				
				$location = $this->getLocationByTimeSlot($input['start_time'],$input['end_time']);			
								
			}
			
			
			$data = array(
				'provider_id' => auth()->user()->id,
				'event_start' => $input['start_time'],
				'event_end' => $input['end_time'],
				'status' => 1,
				'visit_type' => $input['visit_type'],
				'location' => $location
			);
			ProviderTimeSlot::create($data);
		}

	}
	
	#Get provider location by provider slot time
	public function getLocationByTimeSlot($stdate,$endate) {
		
		$st_time = strtotime(date("Y-m-d").' '.date('h:i A',strtotime($stdate)));//for comparison today date
		$en_time = strtotime(date("Y-m-d").' '.date('h:i A',strtotime($endate)));////for comparison today date
		
		//echo $st_time;die;
		
		$providerId = auth()->user()->id;
		$providerSetting = ProviderSetting::where('provider_id',auth()->user()->id)->first();
		$officetiming = json_decode($providerSetting->office_start_end_time);
		$location = '';
		//echo '<Pre>';print_r($officetiming);die;
		foreach($officetiming as $val){
			$c_st_time = strtotime(date("Y-m-d").' '.$val->st_time);
			$c_en_time = strtotime(date("Y-m-d").' '.$val->en_time);
			
			if( $st_time >= $c_st_time && $en_time <= $c_en_time ){
				$location_id =  $val->location;
				
				$location_data = ProviderLocation::where('id',$location_id)->where('provider_id',auth()->user()->id)->first(); 
				
				if($location_data){
					$location = $location_data->address_type.', '.$location_data->address;
				}
				break;
			}
		}
		
		//echo $location.'<br>Rk';die;
		return $location; 		
	}
	
	public function getTimeSlotsByDate(Request $request){
		
		if ($request->ajax()){	
			$date = $request->date_val; //string
			$output = '';

			$data['virtual_timeslots'] = ProviderTimeSlot::select('id','visit_type',DB::raw('DATE_FORMAT(event_start,"%h:%i %p") AS event_start'),DB::raw('DATE_FORMAT(event_end,"%h:%i %p") AS event_end'))
			->where('provider_id',auth()->user()->id)
			->where('status',1)
			->where('visit_type',2)
			->where(DB::raw("(STR_TO_DATE(event_start,'%Y-%m-%d'))"), "=", $date)
			->where(DB::raw("(STR_TO_DATE(event_end,'%Y-%m-%d'))"), "=", $date)
			->orderBy('event_start','ASC')
			->get();
			
			$data['concierge_timeslots'] = ProviderTimeSlot::select('id','visit_type',DB::raw('DATE_FORMAT(event_start,"%h:%i %p") AS event_start'),DB::raw('DATE_FORMAT(event_end,"%h:%i %p") AS event_end'))
			->where('provider_id',auth()->user()->id)
			->where('status',1)
			->where('visit_type',3)
			->where(DB::raw("(STR_TO_DATE(event_start,'%Y-%m-%d'))"), "=", $date)
			->where(DB::raw("(STR_TO_DATE(event_end,'%Y-%m-%d'))"), "=", $date)
			->orderBy('event_start','ASC')
			->get();			
				
			//if( ($data['virtual_timeslots'] && $data['virtual_timeslots']->count() > 0) || ($data['concierge_timeslots'] && $data['concierge_timeslots']->count() > 0)){
				
				$output = view('front.provider.timeslots',$data)->render();
				return response()->json(['status'=> 'success', 'message' => 'Data found', 'data' => $output , 'virtual_timeslots' => $data['virtual_timeslots']->toArray(), 'concierge_timeslots' => $data['concierge_timeslots']->toArray() ]);
			//}else{
			//	$output = '';
			//	return response()->json(['status'=> 'error', 'message' => 'No data found', 'data' => $output]);
			//}			
		}
	}	
		
    public function appointments(Request $request) {
		
        if($request->ajax()) {
			
			$where = array();

			if($request->service !=''){ $where['events.service_id'] = $request->service; }

			if($request->visit_type !=''){ $where['online_visits.visit_type'] = $request->visit_type; }
			 
			if($request->visit_status !=''){ $where['online_visits.visit_status'] = $request->visit_status; }

             $json_data = EventAppointment::select('events.id','events.start','events.status','events.remarks','title','services.name AS service',DB::raw('(CASE 
			 WHEN online_visits.visit_type = "1" THEN "Asynchronous" 
			 WHEN online_visits.visit_type = "2" THEN "Synchronous" 
			 ELSE "Concierge" 
			 END) AS visit_type'))
							->join('online_visits', function($join) {
								$join->on('events.online_visit_id', '=', 'online_visits.id');
							})
							->join('users AS patients', function($join) {
								$join->on('events.patient_id', '=', 'patients.id');
							})
							->join('services', function($join) {
								$join->on('events.service_id', '=', 'services.id');
							})	
							->where('online_visits.visit_status', '!=', 'PENDING ACCEPTANCE')
							->where('events.provider_id', '=', auth()->user()->id)
			 				->whereDate('events.start', '>=', $request->start_date)
                       		->whereDate('events.end',   '<=', $request->end_date);
			if($where){
				$json_data = $json_data->where($where);
			}
							
	   		$json_data = $json_data->get();

			if($json_data && $json_data->count() > 0){
				return response()->json($json_data);
			}else{
				return false;
			}	  
        }
		
        $data['page'] = 'provider_appointments';
		$data['meta_title'] = 'Appointments';
		$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';	
			
		$data['services'] = Service::where('status',1)->get();
/*
 		// get all future events on a calendar
 		$events = Event::get();
		$data['appointments'] = array();
		
		if($events){
			
			$x=0;
			foreach($events as $key => $event){
				$data['appointments'][$x]['id'] = $event->id;
				$data['appointments'][$x]['name'] = $event->name;
				$data['appointments'][$x]['description'] = $event->description;
				$data['appointments'][$x]['startDateTime'] = $event->startDateTime;
				$data['appointments'][$x]['endDateTime'] = $event->endDateTime;
				$x++;
				
			}
		} 

		echo '<pre>';print_r($data['appointments']);die; */
		
        return view('front.provider.appointments', $data);
    }	

    // public function appointmentsAjax(Request $request){
 
    //     switch ($request->type) {
    //        case 'add':
    //           $event = EventAppointment::create([
    //               'title' => $request->title,
    //               'start' => $request->start,
    //               'end' => $request->end,
    //           ]);
 
    //           return response()->json($event);
    //          break;
  
    //        case 'update':
    //           $event = EventAppointment::find($request->id)->update([
    //               'title' => $request->title,
    //               'start' => $request->start,
    //               'end' => $request->end,
    //           ]);
 
    //           return response()->json($event);
    //          break;
  
    //        case 'delete':
    //           $event = Event::find($request->id)->delete();
  
    //           return response()->json($event);
    //          break;
             
    //        default:
    //          # code...
    //          break;
    //     }
	// }
	
	public function getLastVisitDetail($user_id, $id){
		$output = array();

		$data = OnlineVisit::select('online_visits.id AS online_visit_id','online_visits.visit_type','online_visits.visit_status','online_visits.created_at AS visit_time','users.id AS patient_id','users.name AS patient_name', 'users.image AS patient_image','users.gender','users.dob','users.email','users.phone','providers.id AS provider_id','providers.name AS provider_name', 'providers.image AS provider_image','services.name AS service_name','services.short_info AS service_info','orders.id AS order_id','orders.remarks AS orders_remarks','orders.amount AS orders_amount','subscriptions.id AS subscription_id','treatment_medicines.id AS medicine_id','treatment_medicines.name AS medicine_name','treatment_medicines.images AS medicine_images','medicine_variants.id AS variant_id','medicine_variants.variant_name','medicine_variants.pill_qty','medicine_variants.price AS variant_price')
		->join('users', function($join) {
			$join->on('users.id', '=', 'online_visits.patient_id');
		})
		->leftJoin('online_visit_provider_assign', function($join) {
			$join->on('online_visit_provider_assign.online_visit_id', '=', 'online_visits.id');
		})
		->leftJoin('users as providers', function($join) {
			$join->on('providers.id', '=', 'online_visit_provider_assign.provider_id');
		})			
		->join('services', function($join) {
			$join->on('services.id', '=', 'online_visits.service_id');
		})
		->leftJoin('orders', function($join) {
			$join->on('orders.online_visit_id', '=', 'online_visits.id')->on('orders.user_id', '=', 'online_visits.patient_id');
		})
		->leftJoin('subscriptions', function($join) {
			$join->on('subscriptions.id', '=', 'orders.sub_id')->on('subscriptions.user_id', '=', 'orders.user_id');
		})
		->leftJoin('treatment_medicines', function($join) {
			$join->on('treatment_medicines.id', '=', 'subscriptions.medicine_id');
		})
		->leftJoin('medicine_variants', function($join) {
			$join->on('treatment_medicines.id', '=', 'medicine_variants.medicine_id');
		})						
		->where('online_visits.id', '!=', $id)
		->where('online_visits.patient_id', '=', $user_id)
		->where('online_visits.visit_status', '!=', 'PENDING ACCEPTANCE')
		->where('online_visits.visit_status', '!=', 'IN VISIT POOL')
		->orderBy('online_visits.created_at','DESC')
		->limit(1)
		->first();
		
		if($data && $data->count() > 0){
			$output = $data;
		}

		return $output;
	}

	public function getVisitQuesAns($id){
		
		$output = array();

		$data = OnlineVisit::select('visit_questions.question_order','visit_questions.id AS question_id','visit_questions.question_text','visit_answers.id AS answer_id','visit_answers.answer_type', 'visit_answers.answer_text','question_options.id AS option_id','question_options.option_text')
		->leftJoin('visit_answers', function ($join){
			$join->on('online_visits.id', '=', 'visit_answers.online_visit_id');
		})
		->leftJoin('visit_questions', function ($join){
			$join->on('visit_answers.question_id', '=', 'visit_questions.id');
		})				
		->leftJoin('question_options', function ($join){
			$join->on('question_options.question_id', '=', 'visit_questions.id')->on('question_options.visit_form_id', '=', 'visit_questions.visit_form_id');
		})		
		->where('online_visits.id',$id)
		->where('online_visits.is_submit',1)
		->orderBy('visit_questions.question_order','ASC')
		->orderBy('visit_questions.id','ASC')
		->orderBy('question_options.id','ASC')
		->get();

		if($data && $data->count() > 0){
			foreach($data->toArray() as $key => $value){
				$output[$value['question_order']] = $value;
			}
			//echo '<pre>'; print_r($output); die;
		}

		return $output;
	}

	function getTimes($min) {
		
		$formatter = function ($time) {
			return date('h:i A', $time);
		};
		
		$steps = range(0, 47*1800, 60 * $min);
		//echo '<pre>'; print_r($steps).'</pre>';
		return array_map($formatter, $steps);
	}
	
	public function change_appointment_status(Request $request, CommonService $common){

		if ($request->ajax()) {
			$id = $request->event_id;
			$status = $request->status;
			$remarks = $request->remarks;
			
			$appointment = EventAppointment::where('id',$id)->first();
			
			if($appointment && $appointment->count() > 0){	

				if($status == 'accept'){
					
					$data = array('status' => 1, 'remarks' => $remarks);
					EventAppointment::where('id',$id)->update($data);
					OnlineVisit::where('id',$appointment->online_visit_id)->update(array('visit_status' => 'ACCEPTED'));
					
					//Google Calendar Add Event
					$service = Service::where('id',$appointment->service_id)->first();
					$patient = User::where('id',$appointment->patient_id)->first();
					$provider = User::find(auth()->user()->id);
					
					if($service->visit_type == '1'){ $service_type = 'Virtual'; }
					if($service->visit_type == '2'){ $service_type = 'Virtual'; }
					if($service->visit_type == '3'){ $service_type = 'Concierge'; }
					
					
					if($remarks !=''){
						$description = $appointment->title.' - '.$remarks;
						$description2 = $appointment->title2.' - '.$remarks;
						
					}else{
						$description = $appointment->title;
						$description2 = $appointment->title2;
					}
					
					$patientData = [
						'email' => $patient->email,
						'name' => $patient->name,
						'comment' => $description2,
					];
							
					$providerData = [
						'email' => $provider->email,
						'name' => $provider->name,
						'comment' => $description,
					];	
					
					//create a new event
					$event_name = $service_type.' Visit with '.$provider->name.' and '.$patient->name;
					//echo $event_name;die;
					$event = new Event;
					$event->name = $event_name;
					$event->description = $description;
					$event->startDateTime = $this->getLicenseExpireAttribute($appointment->start);
					$event->endDateTime = $this->getLicenseExpireAttribute($appointment->end);
					$event->sendNotifications = true;
					$event->sendUpdates = 'all';	
 					$event->addAttendee($patientData);					
					$event->addAttendee($providerData);

					$event->save(); 	

					//Email to Patient
					$input['name'] = $patient->name;
					$input['email'] = $patient->email;
					$input['subject'] = 'Appointment Accepted';
					$input['template'] = 'front.email_template.appointment_accept';					
					$input['provider_name'] = $provider->name;
					$input['patient_name'] = $patient->name;
					$input['service_name'] = $service->name;
					$input['service_type'] = $service_type;
					$input['appointment_datetime'] = date('d M. Y h:i A',strtotime($appointment->start));
					//$output = view($input['template'],$input)->render(); echo $output;die;
					$result = $common->sendMail($input);
					//to Provider
					//mail send adding 
					
					// send pusher Notifications
    				if(!empty(auth()->user()->image)){
						$img =auth()->user()->image;
					}else{
						$img='dist/images/user_icon.png';
					}
    				$noticeData = array(
    					'created_id' => auth()->user()->id,
    					'created_name' => auth()->user()->name,
    					'created_image' => $img,
    					'notification_type' => "accept_appointment",
    					'post_id' => $appointment->online_visit_id,
    					'followers_id' =>$appointment->patient_id,
    					'status' => 1,
    					'created_at' => date('Y-m-d H:i:s'),
    					'updated_at' => date('Y-m-d H:i:s')
    				);
    				
    				$notice = Notifications::create($noticeData);
    				$noticeData['id']=$notice->id;
					event(new Accept_appointment($noticeData));
					
				}else{
					EventAppointment::where('id',$id)->delete();
					//$data = array('status' => 2, 'remarks' => $remarks);
					//EventAppointment::where('id',$id)->update($data);					
					OnlineVisit::where('id',$appointment->online_visit_id)->update(array('visit_status' => 'RESCHEDULED'));
					
					// send pusher Notifications
					if(!empty(auth()->user()->image)){
						$img =auth()->user()->image;
					}else{
						$img='dist/images/user_icon.png';
					}
    				$noticeData = array(
    					'created_id' => auth()->user()->id,
    					'created_name' => auth()->user()->name,
    					'created_image' =>$img,
    					'notification_type' => "decline_appointment",
    					'post_id' => $appointment->online_visit_id,
    					'followers_id' =>$appointment->patient_id,
    					'status' => 1,
    					'created_at' => date('Y-m-d H:i:s'),
    					'updated_at' => date('Y-m-d H:i:s')
    				);
    				
    				$notice = Notifications::create($noticeData);
    				$noticeData['id']=$notice->id;
					event(new Decline_appointment($noticeData));
    				
				}

				return response()->json(['status'=> 'success', 'message' => 'Success']);
				
				
				
			}else{
				return response()->json(['status'=> 'error', 'message' => 'Incorrect Visit']);
			}	
		}
	}
	
	public function visit_add_note(Request $request){
		
		if($request->ajax()){
			
			$data = array('provider_note' => $request->add_note);
			$result = OnlineVisit::where('id',$request->id)->update($data);			
			
			if($result){
				return response()->json(['status'=> 'success', 'message' => 'Success']);
			}else{
				return response()->json(['status'=> 'error', 'message' => 'Error']);
			}
		}
	}

	public function set_imp_visit(Request $request){

		if($request->ajax()){
			$id = $request->id;
			$is_imp = $request->is_imp;
			
			$online_visit = OnlineVisit::where('id',$id)->first();
			
			if($online_visit && $online_visit->count() > 0){	
			
				OnlineVisit::where('id',$id)->update(array('is_imp' => $is_imp));
				
				return response()->json(['status'=> 'success', 'message' => 'Success']);
			}else{
				return response()->json(['status'=> 'error', 'message' => 'Incorrect Visit']);
			}
					
		}else{
			return response()->json(['status'=> 'error', 'message' => 'Error Occured']);
		}		
	}	
	
    public function get_appointment_by_id(Request $request) {
		
        if($request->ajax()) {
			
             $json_data = EventAppointment::select('events.id','events.start','events.end','events.status','events.remarks','title','services.name AS service',DB::raw('(CASE 
			 WHEN online_visits.visit_type = "1" THEN "Asynchronous" 
			 WHEN online_visits.visit_type = "2" THEN "Synchronous" 
			 ELSE "Concierge" 
			 END) AS visit_type'))
							->join('online_visits', function($join) {
								$join->on('events.online_visit_id', '=', 'online_visits.id');
							})
							->join('users AS patients', function($join) {
								$join->on('events.patient_id', '=', 'patients.id');
							})
							->join('services', function($join) {
								$join->on('events.service_id', '=', 'services.id');
							})	
							->where('events.id', '=', $request->id)
							->first();

			if($json_data && $json_data->count() > 0){
				return response()->json($json_data);
			}else{
				return false;
			}	  
        }
    }

    public function get_timeslot_by_id(Request $request) {
		
        if($request->ajax()) {
			
             $json_data = ProviderTimeSlot::select('id',DB::raw('DATE_FORMAT(event_start,"%Y-%m-%d") AS date_val'),DB::raw('DATE_FORMAT(event_start,"%h:%i %p") AS event_start'),DB::raw('DATE_FORMAT(event_end,"%h:%i %p") AS event_end'),'visit_type','location')
					->where('provider_id',auth()->user()->id)
					->where('id',$request->id)
					->first();							

			if($json_data && $json_data->count() > 0){
				return response()->json($json_data);
			}else{
				return false;
			}	  
        }
    }
	
    public function deleteTimeSlot(Request $request) {
		
        if($request->ajax()) {
			
			try {
				$timeslot = ProviderTimeSlot::findOrFail($request->id);
			} catch(\Exception $exception){
				return response()->json(['status'=> 'error', 'message' => 'No timeslot found.']);
			}

			//echo '<Pre>';print_r($timeslot);die;
			$result = $timeslot->delete();
			
			if ($result) {
				return response()->json(['status'=> 'success', 'message' => 'Deleted.']);
			} else {
				return response()->json(['status'=> 'error', 'message' => 'Error Occured']);
			}
        }
    }	
	
	public function availability_timeslots(Request $request, CommonService $common){

		
        if($request->ajax()) {

			//Check if weekend_days found and delete it.
			$weekend_days = array();
			
			$provider_setting = ProviderSetting::select('weekend_days')->where('provider_id',auth()->user()->id)->first();
			//echo '<pre>';
			//print_r($provider_setting); die;
			if($provider_setting && $provider_setting->count() > 0){
				
				if($provider_setting->weekend_days != ''){
					$weekend_days = explode(',',$provider_setting->weekend_days);
				}else{
				    $weekend_days='';
				}
			}else{
			    $weekend_days='';
			}
		
			//$weekend_days = array('Sat','Sun');
			if(!empty($weekend_days)){
			    $common->check_weekend_days_timeslots(auth()->user()->id,$weekend_days);
			}
			//get data after delete.
			
            $json_data = ProviderTimeSlot::select('id','visit_type','event_start AS start','event_end AS end')
			->where('provider_id',auth()->user()->id)
			->where('status',1)
			//->where(DB::raw("(STR_TO_DATE(event_start,'%Y-%m-%d'))"), "=", $date)
			->whereDate('event_start', '>=', $request->start)
			->whereDate('event_start',   '<=', $request->end)	
			->orderBy('event_start','ASC')
			->get();

			if($json_data && $json_data->count() > 0){
				return response()->json($json_data);
			}else{
				return false;
			}	  
        }		
	}
	
	public function changeProfilePic(Request $request)
	{  
		$status = 'error';
		$message = 'Error occured';
		
		if (!empty($request->file('file_input'))) {
            $file = $request->file('file_input');
			if(strtolower($file->getClientOriginalExtension()) == 'jpg' || strtolower($file->getClientOriginalExtension()) == 'png' || strtolower($file->getClientOriginalExtension()) == 'jpeg'){
				$name= time().'-'.rand(11111111,99999999).'.'.strtolower($file->getClientOriginalExtension());
				$destinationPath = 'uploads/users/';
				
				if($file->move($destinationPath, $name)){
					$image = 'uploads/users'.'/'.$name;
						
					$input_data = [
						'image' => $image,
						'updated_at' => date('Y-m-d H:i:s')
					];	
					
					User::where('id',auth()->user()->id)->update($input_data);					
					
					$status = 'success';
					$message = 'Profile pic updated successfully.';
									
				}				
			}else{
				$status = 'error';
				$message = 'Only jpg, jpeg and png files are allowable';					
			}

        }
		
        return response()->json(['status'=>$status, 'message'=>$message]);
	}
	
	public function addMoreCertificate(Request $request)
	{
		$status = 'error';
		$message = 'Error occured';
		
		$credentials = User::select('users.provider_licenses')->where('id',auth()->user()->id)->get();

		if(!empty($credentials) && count($credentials)>0 && $credentials[0]->provider_licenses){
			$credentialsArry = json_decode($credentials);
					
			foreach($credentialsArry as $val)
			{
				$credentialsArry= json_decode($val->provider_licenses);
			}
		}else{
			$credentialsArry='';
		}
		
		if (!empty($request->file_inputs)) {
			$num = count($request->file('file_inputs'));				
			$cred_images=array();
			for($i=0; $i<$num; $i++)
			{
				$file = $request->file('file_inputs')[$i];
				
				if(strtolower($file->getClientOriginalExtension()) == 'jpg' || strtolower($file->getClientOriginalExtension()) == 'png' || strtolower($file->getClientOriginalExtension()) == 'jpeg' || strtolower($file->getClientOriginalExtension()) == 'pdf' || strtolower($file->getClientOriginalExtension()) == 'docx'){
					$name=time().'-'.rand(11111111,99999999).'.'.strtolower($file->getClientOriginalExtension());
					$destinationPath = 'uploads/users/';
					
					if($file->move($destinationPath, $name)){
						$image = 'uploads/users'.'/'.$name;
						$status = 'success';
						$message = 'Credential uploaded successfully.';
						$cred_images[]= $image;
					}
					
				}
				else{
					$status = 'error';
					$message = 'Only .jpg, .jpeg and .png files are allowable';					
				}	
			}
			
			if(!empty($credentialsArry) && count($credentialsArry)>0){
					$final = json_encode(array_merge($credentialsArry,$cred_images));
			}else{
				$final = json_encode($cred_images);
			}
			
			$input_data = [
				'provider_licenses' => $final,
				'updated_at' => date('Y-m-d H:i:s')
			];	
			
			User::where('id',auth()->user()->id)->update($input_data);					
			
			$status = 'success';
			$message = 'Credential added successfully.';
		}
		return response()->json(['status'=>$status, 'message'=>$message]);
		
	}
	
	public function get_timings(Request $request){
	        
	        $providerSetting = ProviderSetting::where('provider_id',auth()->user()->id)->first();
	    	$provider_location = ProviderLocation::where('provider_id',auth()->user()->id)->get();
		
    		if($providerSetting && $providerSetting->count() > 0){	
    			$office_start_end_time = json_decode($providerSetting->office_start_end_time);	
    			$break_start_end_time = json_decode($providerSetting->break_start_end_time);	
    		}else{	
    			$office_start_end_time = array();	
    			$break_start_end_time = array();				
    		}
    		
    		$rmd_office_start_html='';
    		if($request->type=='preference'){
        		if($office_start_end_time){	
    			    $i = 0;
    			  
    				foreach($office_start_end_time as $val){
    					$rmd_office_start_html.='<div class="form-row item_wrapper">
    						<div class="form-group col-md-3 has-feedback">';
    							if($i == 0){ $rmd_office_start_html.='<label for="first_name">Starting time <span
    									class="text-danger">*</span></label>';
    							}
    					$rmd_office_start_html.='<div class="input-group date mt-2">
    								<input type="text" class="form-control timepicker"
    									name="office_start_time[]" value="'.$val->st_time.'" />
    								<span class="input-group-text" id="basic-addon1">
    									<img src="'.asset('assets/images/clock.svg').'">
    								</span>
    							</div>
    							<span class="text-danger">
    								<strong class="error" id="dob-error"></strong>
    							</span>
    						</div>
    						<div class="form-group col-md-3 has-feedback">';
    							if($i == 0){ $rmd_office_start_html.='<label for="last_name">Closing time <span
    									class="text-danger">*</span></label>'; }
    						$rmd_office_start_html.='<div class="input-group date mt-2">
    								<input type="text" class="form-control timepicker"
    									name="office_close_time[]" value="'.$val->en_time.'" />
    								<span class="input-group-text" id="basic-addon1">
    									<img src="'.asset('assets/images/clock.svg').'">
    								</span>
    							</div>
    							<span class="text-danger">
    								<strong class="error" id="dob-error"></strong>
    							</span>
    						</div>
    						<div class="form-group col-md-4 has-feedback">';
    						if($i == 0){ $rmd_office_start_html.='<label for="last_name">Location <span
    									class="text-danger">*</span></label>';}
    						$rmd_office_start_html.='<div class="input-group date mt-2">
    								<select name="office_location[]" id="" class="form-control loc_opt">';
    									if($provider_location){
    										foreach($provider_location as $valu){
    											$cond = ($val->location == $valu->id) ? "selected=selected" : "";
    											$rmd_office_start_html.= '<option value="'.$valu->id.'" '.$cond.'>'.$valu->address_type.'</option>';
    										}
    									}
    						if($i == 0){$mt="mt-1"; }else{$mt="mt-3";}	
    						$rmd_office_start_html.='</select>
    							</div>
    							<span class="text-danger">
    								<strong class="error" id="dob-error"></strong>
    							</span>
    						</div>
    						<div class="form-group col-md-1 offset-md-1 has-feedback '.$mt.'">';
    							if($i == 0){$rmd_office_start_html.=' <a href="javascript:void(0)" title="Add more" class="add_item"><span style="color:green;"><i class="fa fa-plus-circle fa-2x" aria-hidden="true"></i></span></a>';
    							}else{
    							$rmd_office_start_html.='<a href="javascript:void(0)" title="Remove" class="remove_item"><span style="color:red;"><i class="fa fa-minus" aria-hidden="true"></i></span></a>';
    							}
    						$rmd_office_start_html.='</div>
    					</div>';
    				$i++; 
    				}
    			}else{
    				$rmd_office_start_html.='<div class="form-row">
    					<div class="form-group col-md-3 has-feedback">
    						<label for="first_name">Starting time <span
    								class="text-danger">*</span></label>
    						<div class="input-group date mt-2">
    							<input type="text" class="form-control timepicker"
    								name="office_start_time[]" value="'.date('h:i A').'" />
    							<span class="input-group-text" id="basic-addon1">
    								<img src="'.asset('assets/images/clock.svg').'">
    							</span>
    						</div>
    						<span class="text-danger">
    							<strong class="error" id="dob-error"></strong>
    						</span>
    					</div>
    					<div class="form-group col-md-3 has-feedback">
    						<label for="last_name">Closing time <span
    								class="text-danger">*</span></label>
    						<div class="input-group date mt-2">
    							<input type="text" class="form-control timepicker"
    								name="office_close_time[]" value="'.date('h:i A').'" />
    							<span class="input-group-text" id="basic-addon1">
    								<img src="'.asset('assets/images/clock.svg').'">
    							</span>
    						</div>
    						<span class="text-danger">
    							<strong class="error" id="dob-error"></strong>
    						</span>
    					</div>
    					<div class="form-group col-md-4 has-feedback">
    						<label for="last_name">Location <span
    								class="text-danger">*</span></label>
    						<div class="input-group date mt-2">
    							<select name="office_location[]" id="wthData" class="form-control loc_opt">';
    								 
    								foreach($provider_location as $val){
    									$rmd_office_start_html.='<option value="'.$val->id.'">'.$val->address_type.'</option>';
    								}
    						$rmd_office_start_html.='</select>
    						</div>
    						<span class="text-danger">
    							<strong class="error" id="dob-error"></strong>
    						</span>
    					</div>
    					<div class="form-group col-md-1 offset-md-1 has-feedback mt-1">
    						<a href="javascript:void(0)" title="Add more" class="add_item"><span
    								style="color:#2c99cb;"><i class="fa fa-plus-circle fa-2x"
    									aria-hidden="true"></i></span></a>
    					</div>
    				</div>';
    		}
    		
    		$rmd_office_start_html.='<div class="more_items_wrapper"></div>
    			<hr>';
    			
    			if($break_start_end_time){	
    			    $j = 0;
    			foreach($break_start_end_time as $val){				
    				$rmd_office_start_html.='<div class="form-row break_wrapper">
    					<div class="form-group col-md-5 has-feedback">';
    						if($j == 0){ $rmd_office_start_html.='<label for="gender">Break start time</label>'; }
    						$rmd_office_start_html.='<div class="input-group date mt-2">
    						<input type="text" class="form-control timepicker" name="break_start_time[]" value="'.$val->breakSt_time.'"/>
    						<span class="input-group-text" id="basic-addon1">
    							<img src="'.asset('assets/images/clock.svg').'">
    						</span>
    					</div>
    					<span class="text-danger">
    						<strong class="error" id="dob-error"></strong>
    					</span>
    					</div>
    					<div class="form-group col-md-5 has-feedback">';
    						if($j == 0){ $rmd_office_start_html.='<label for="email">Break end time</label>';}
    						if($j == 0){ $mt ="mt-1"; }else{ $mt ='mt-3'; }
    						$rmd_office_start_html.='<div class="input-group date mt-2">
    						<input type="text" class="form-control timepicker" name="break_end_time[]" value="'.$val->breakEn_time.'"/>
    						<span class="input-group-text" id="basic-addon1">
    							<img src="'.asset('assets/images/clock.svg').'">
    						</span>
    					</div>
    					<span class="text-danger">
    						<strong class="error" id="dob-error"></strong>
    					</span>	
    					</div>
    					<div class="form-group col-md-1 offset-md-1 has-feedback '.$mt.'">';
    						if($j == 0){ $rmd_office_start_html.='<a href="javascript:void(0)" title="Add more" class="add_break"><span style="color:green;"><i class="fa fa-plus-circle fa-2x" aria-hidden="true"></i></span></a>';
    						}else{
    						$rmd_office_start_html.='<a href="javascript:void(0)" title="Remove" class="remove_break"><span style="color:red;"><i class="fa fa-minus" aria-hidden="true"></i></span></a>';
    						}
    					$rmd_office_start_html.='</div>			  
    				</div>';
    				 $j++; 
    		    }
    		}else{
              $rmd_office_start_html.='<div class="form-row">
                    <div class="form-group col-md-5 has-feedback">
                        <label for="gender">Break start time</label>
                        <div class="input-group date mt-2">
                            <input type="text" class="form-control timepicker"
                                name="break_start_time[]" value="'.date('h:i A').'" />
                            <span class="input-group-text" id="basic-addon1">
                                <img src="'.asset('assets/images/clock.svg').'">
                            </span>
                        </div>
                        <span class="text-danger">
                            <strong class="error" id="dob-error"></strong>
                        </span>
                    </div>
                    <div class="form-group col-md-5 has-feedback">
                        <label for="gender">Break end time</label>
                        <div class="input-group date mt-2">
                            <input type="text" class="form-control timepicker"
                                name="break_end_time[]" value="'.date('h:i A').'" />
                            <span class="input-group-text" id="basic-addon1">
                                <img src="'.asset('assets/images/clock.svg').'">
                            </span>
                        </div>
                        <span class="text-danger">
                            <strong class="error" id="dob-error"></strong>
                        </span>
                    </div>
                    <div class="form-group col-md-1 offset-md-1 has-feedback mt-1">
                        <a href="javascript:void(0)" title="Add more"
                            class="add_break"><span style="color:#2c99cb;"><i
                                    class="fa fa-plus-circle fa-2x"
                                    aria-hidden="true"></i></span></a>
                    </div>
                </div>';
    		}
		}else{
		    
		    if($office_start_end_time){	
    		    $i = 0; 
        		foreach($office_start_end_time as $k => $val){	
        			$rmd_office_start_html.='<div class="form-row item_wrapper">
        			  <div class="form-group col-md-3 has-feedback">';
        				if($i == 0){ $rmd_office_start_html.='<label for="first_name">Starting time <span class="text-danger">*</span></label>';}
        					$rmd_office_start_html.='<div class="input-group date mt-2">
        						<input type="text" class="form-control timepicker" name="office_start_time[]" value="'.$val->st_time.'" />
        						<span class="input-group-text" id="basic-addon1">
        							<img src="'.asset('assets/images/clock.svg').'">
        						</span>
        					</div>		
        				<span class="text-danger">
        					<strong class="error" id="dob-error"></strong>
        				</span>	
        			  </div>
        			  <div class="form-group col-md-3 has-feedback">';
        				if($i == 0){ $rmd_office_start_html.='<label for="last_name">Closing time <span class="text-danger">*</span></label>';}
        					$rmd_office_start_html.='<div class="input-group date mt-2">
        						<input type="text" class="form-control timepicker" name="office_close_time[]" value="'.$val->en_time.'"/>
        						<span class="input-group-text" id="basic-addon1">
        							<img src="'.asset('assets/images/clock.svg').'">
        						</span>
        					</div>
        				<span class="text-danger">
        					<strong class="error" id="dob-error"> </strong>
        				</span>	
        			  </div>
        			  <div class="form-group col-md-4 has-feedback">';
        				if($i == 0){ $rmd_office_start_html.='<label for="last_name">Location <span class="text-danger">*</span></label>';}
        				$rmd_office_start_html.='<div class="input-group date mt-2">
        						<select name="office_location[]" id="" class="form-control loc_opt">';
        							 if($provider_location){
        								foreach($provider_location as $valu){
        									
        									if(isset($val->location) && $val->location == $valu->id){
        										$cond = "selected=selected";
        									}else{
        										$cond = "";
        									}
        									
        									$rmd_office_start_html.='<option value="'.$valu->id.'" '.$cond.'>'.$valu->address_type.'</option>';
        								}
        							}
        			        if($i == 0){ $mt ="mt-1"; }else{ $mt="mt-3"; }
        					$rmd_office_start_html.='</select>
        					</div>
        				<span class="text-danger">
        					<strong class="error" id="dob-error"></strong>
        				</span>	
        			  </div>
        			<div class="form-group col-md-1 offset-md-1 has-feedback '.$mt.'">';
        				if($i == 0){ $rmd_office_start_html.='<a href="javascript:void(0)" title="Add more" class="add_item"><span style="color:green;"><i class="fa fa-plus-circle fa-2x" aria-hidden="true"></i></span></a>'; }
        				else{
        				$rmd_office_start_html.='<a href="javascript:void(0)" title="Remove" class="remove_item"><span style="color:red;"><i class="fa fa-minus" aria-hidden="true"></i></span></a>';}				
        				//}
        			
        			  $rmd_office_start_html.='</div>
        			</div>';
        		 $i++; 
        		}
    		}else{
			$rmd_office_start_html.='<div class="form-row">
			  <div class="form-group col-md-3 has-feedback">
				<label for="first_name">Starting time <span class="text-danger">*</span></label>
					<div class="input-group date mt-2">
						<input type="text" class="form-control timepicker" name="office_start_time[]" value="'.date('h:i A').'" />
						<span class="input-group-text" id="basic-addon1">
							<img src="'.asset('assets/images/clock.svg').'">
						</span>
					</div>				
				<span class="text-danger">
					<strong class="error" id="dob-error"></strong>
				</span>	
			  </div>
			  <div class="form-group col-md-3 has-feedback">
				<label for="last_name">Closing time <span class="text-danger">*</span></label>				
					<div class="input-group date mt-2">
						<input type="text" class="form-control timepicker" name="office_close_time[]" value="'.date('h:i A').'"/>
						<span class="input-group-text" id="basic-addon1">
							<img src="'.asset('assets/images/clock.svg').'">
						</span>
					</div>
				<span class="text-danger">
					<strong class="error" id="dob-error"></strong>
				</span>	
			  </div>
			  <div class="form-group col-md-4 has-feedback">
				<label for="last_name">Location <span class="text-danger">*</span></label>
					<div class="input-group date mt-2">
						<select name="office_location[]" id="" class="form-control loc_opt">';
							
							foreach($provider_location as $val){
								$rmd_office_start_html.='<option value="'.$val->id.'">'.$val->address_type.'</option>';
							}
						
					$rmd_office_start_html.='</select>
					</div>
				<span class="text-danger">
					<strong class="error" id="dob-error"></strong>
				</span>	
			  </div>
			  <div class="form-group col-md-1 offset-md-1 has-feedback mt-1">
					<a href="javascript:void(0)" title="Add more" class="add_item"><span style="color:green;"><i class="fa fa-plus-circle fa-2x" aria-hidden="true"></i></span></a>
			 </div>
			</div>';
		}
		$rmd_office_start_html.='<div class="more_items_wrapper"></div>
		<div class="prefered_locations_box">';
    		if($provider_location && $provider_location->count()>0){
    	    $i=0; 
    		foreach($provider_location as $val){
    		$rmd_office_start_html.='<div class="prefered_location_text">
    			'.$val->address_type.' - '.$val->address.'
    		</div>';
    		$i++; 
    		}
		}
		$rmd_office_start_html.='</div>
		<br>';
			
	if($break_start_end_time){	
	$j = 0;
		foreach($break_start_end_time as $val){				
		$rmd_office_start_html.='<div class="form-row break_wrapper">
				<div class="form-group col-md-5 has-feedback">';
					if($j == 0){ $rmd_office_start_html.='<label for="gender">Break start time</label>';}
					$rmd_office_start_html.='<div class="input-group date mt-2">
					<input type="text" class="form-control timepicker" name="break_start_time[]" value="'.$val->breakSt_time.'"/>
					<span class="input-group-text" id="basic-addon1">
						<img src="'.asset('assets/images/clock.svg').'">
					</span>
				</div>
				<span class="text-danger">
					<strong class="error" id="dob-error"></strong>
				</span>
				</div>
				<div class="form-group col-md-5 has-feedback">';
					if($j == 0){ $rmd_office_start_html.='<label for="email">Break end time</label>';}
					if($j == 0){ $mt="mt-1";}else{ $mt="mt-3"; }
					$rmd_office_start_html.='<div class="input-group date mt-2">
					<input type="text" class="form-control timepicker" name="break_end_time[]" value="'.$val->breakEn_time.'"/>
					<span class="input-group-text" id="basic-addon1">
						<img src="'.asset('assets/images/clock.svg').'">
					</span>
				</div>
				<span class="text-danger">
					<strong class="error" id="dob-error"></strong>
				</span>	
				</div>
				<div class="form-group col-md-1 offset-md-1 has-feedback '.$mt.'">';
					if($j == 0){ $rmd_office_start_html.='<a href="javascript:void(0)" title="Add more" class="add_break"><span style="color:green;"><i class="fa fa-plus-circle fa-2x" aria-hidden="true"></i></span></a>';
					}else{
					$rmd_office_start_html.='<a href="javascript:void(0)" title="Remove" class="remove_break"><span style="color:red;"><i class="fa fa-minus" aria-hidden="true"></i></span></a>';
					}
				$rmd_office_start_html.='</div>			  
			</div>';
			 $j++;
		}
		
	}else{
		$rmd_office_start_html.='<div class="form-row">
				<div class="form-group col-md-5 has-feedback">
					<label for="gender">Break start time</label>
					<div class="input-group date mt-2">
					<input type="text" class="form-control timepicker" name="break_start_time[]" value="'.date('h:i A').'"/>
					<span class="input-group-text" id="basic-addon1">
						<img src="'.asset('assets/images/clock.svg').'">
					</span>
				</div>
				<span class="text-danger">
					<strong class="error" id="dob-error"></strong>
				</span>
				</div>
				<div class="form-group col-md-5 has-feedback">
					<label for="gender">Break end time</label>
					<div class="input-group date mt-2">
					<input type="text" class="form-control timepicker" name="break_end_time[]" value="'.date('h:i A').'"/>
					<span class="input-group-text" id="basic-addon1">
						<img src="'.asset('assets/images/clock.svg').'">
					</span>
				</div>
				<span class="text-danger">
					<strong class="error" id="dob-error"></strong>
				</span>	
				</div>
				<div class="form-group col-md-1 offset-md-1 has-feedback mt-1">
					<a href="javascript:void(0)" title="Add more" class="add_break"><span style="color:green;"><i class="fa fa-plus-circle fa-2x" aria-hidden="true"></i></span></a>
				</div>			  
			</div>';	
	        }
    		    
		}
		
		return response()->json(['status'=>'success', 'rmd_office_start_html'=>$rmd_office_start_html]);
	}
	
	public function check_appointment(Request $request){

		if($request->ajax()){
			$id = $request->id;
			
			$office_time = ProviderSetting::where('provider_id',$id)->first();
			
			if($office_time && $office_time->count() > 0){	
			    
				//return redirect()->route('provider.add_appointment_timeslots');
				return response()->json(['status'=>'success', 'redirect'=>route('provider.add_appointment_timeslots') ]);
				
			}else{
				return response()->json(['status'=> 'error', 'message' => "Provider can't add appointment without an office start time and end time."]);
			}
					
		}else{
			return response()->json(['status'=> 'error', 'message' => 'Error Occured']);
		}		
	}	
	
	public function notification_view(Request $request){
		
		if ($request->ajax()) {
			
			#Stories detail
			$viewData = Notifications::where('id', $request->notification_id)->first();
			
			if(!empty($viewData) && $viewData->count()>0){
				Notifications::where(['id' => $request->notification_id, 'followers_id'=>auth()->user()->id ])->update(['status'=>0]);
				if($viewData->notification_type=='unread_messages'){
					session()->put('user_unread_messages','yes');
				}
				return response()->json(['status'=> 'success', 'type'=>$viewData->notification_type,'patient_id'=>$viewData->created_id]);
			}else{
				return response()->json(['status'=> 'error', 'redirect'=> '', 'message' => '']);
			}
		}
		
	}
	
	function unread_message(){
		//check unread message
		$userLogin = auth()->user()->email;
		$userPassword =env('QUICKBLOX_PASSWORD');

		$body = [
			'application_id' =>env('QUICKBLOX_APPLICATION_ID'),
			'auth_key' =>env('QUICKBLOX_AUTH_KEY'),
			'nonce' => time(),
			'timestamp' => time(),
			'user' => ['login' => $userLogin, 'password' => $userPassword]
		];
		$built_query = urldecode(http_build_query($body));
		$signature = hash_hmac('sha1', $built_query ,env('QUICKBLOX_AUTH_SECRET'));
		$body['signature'] = $signature;
		$post_body = http_build_query($body);
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, 'https://api.quickblox.com/session.json');
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $post_body);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($curl);
		$response = json_decode($response, true);
		if(!empty($response)){
			$token=$response['session']['token'];
			$userId=$response['session']['user_id'];
			if(!empty($token)){
				$curl = curl_init();

				curl_setopt_array($curl, array(
				  CURLOPT_URL => 'https://api.quickblox.com/chat/Dialog.json?type=3&sort_desc=last_message_date_sent',
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => '',
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 0,
				  CURLOPT_FOLLOWLOCATION => true,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => 'GET',
				  CURLOPT_HTTPHEADER => array(
					'QB-Token:'.$token
				  ),
				));

				$response_dialog = curl_exec($curl);
				if(!empty($response_dialog)){
					$response_dialog = json_decode($response_dialog, true);
					$total_entries=$response_dialog['total_entries'];
					$items=$response_dialog['items'];
					
					foreach ($items as $val) {
						
						if($val['unread_messages_count']>0){
						    $chat_id =$val['last_message_user_id'];
						    $senderData = User::where('chat_id',$chat_id)->first();
							if(!empty($senderData) && $senderData->count()>0){
							    if(!empty($senderData->image)){
									$img =$senderData->image;
								}else{
									$img='dist/images/user_icon.png';
								}
								$noticeData = array(
									'created_id' => $senderData->id,
									'created_name' => $senderData->name,
									'created_image' => $img,
									'notification_type' => "unread_messages",
									'post_id' => $val['_id'],
									'followers_id' =>auth()->user()->id,
									'notice_type' =>$val['unread_messages_count'],
									'status' => 1,
									'created_at' => date('Y-m-d H:i:s'),
									'updated_at' => date('Y-m-d H:i:s')
								);
								$sender_notice = Notifications::where('followers_id',auth()->user()->id)->where('notification_type','unread_messages')->where('post_id',$val['_id'])->where('created_id',$senderData->id)->first();
								if(!empty($sender_notice) && $sender_notice->count()>0){
									Notifications::where('id',$sender_notice->id)->update($noticeData);
								}else{
									$notice = Notifications::create($noticeData);
								}
							}
						}
					}
				}
			}
		}
		
	}
	
	public function user_view($id){
		if($id != ''){
			$user = User::where('id',$id)->where('status',1)->first();
			$data=array();
			if(!empty($user) && $user->count() > 0){
				$followers = SocialUserFollowConnection::where('user_id', $id)->first();
				if(!empty($followers)){
					$followers_ids = explode(',', $followers['follower_id']);
					$data['followers_count'] = count($followers_ids);
				}else{
					$data['followers_count']=0;
				}

				$data['supporter'] = SupporterRequest::where(['user_id' => auth()->user()->id])->first();
				$data['supporting'] = SupporterRequest::where(['user_id' => $id])->first();
				
				$data['user']=$user;
				$data['id'] = $id;
				$data['posts'] = $this->get_lates_post();
				$data['blogs'] = Article::select('*')
					->where('post_publish','=',1)
					->where('post_visibility','=',1)
					->limit(4)
					->latest()
					->get();
				return view('front.supporters.user_view',$data);
				
			}else{
				return redirect()->back();
			}
		}else{
			return redirect()->back();
		}
	}
	
	function get_lates_post(){
		#Stories detail
		$followerId = SocialUserFollowConnection::where('user_id', auth()->user()->id)->get();
		$followerId = (count($followerId)>0) ? $followerId[0]->follower_id : 0;
		
		if($followerId){
			$data =  DB::select( DB::raw("SELECT media_name from social_posts_media left join social_posts on
		social_posts_media.post_id = social_posts.post_id where social_posts.userid=".auth()->user()->id." OR (social_posts.userid IN (".$followerId.")) ORDER BY `social_posts_media`.media_id DESC LIMIT 16"));
		}else{
			$data = false;
		}
		
		return $data;	
	}
	
	function setMerchantAccount(Request $request)
	{
		if ($request->ajax()) {
						
			if(empty($request->id)){
				$messages = [
					'login_id.regex' => 'Login Id is required field.',
					'transaction_key.regex' => 'Transaction Key is required field.',
				];
				$validator = Validator::make($request->all(), [
					'login_id' => 'required',
					'transaction_key' => 'required'
				], $messages)->validate();	

				$input_data = array(
					'MERCHANT_LOGIN_ID' => trim($request->login_id),
					'MERCHANT_TRANSACTION_KEY' => trim($request->transaction_key)
				);
			}

			User::where('id',auth()->user()->id)->update($input_data);
			
			$msg = 'Merchant detail saved successfully.';
			$redirect = route('provider.dashboard');	

			return response()->json(['status'=> 'success', 'redirect'=> $redirect, 'message' => $msg, 'MERCHANT_LOGIN_ID'=>$request->login_id, 'MERCHANT_TRANSACTION_KEY'=>$request->transaction_key]);
		}
		else{
			return response()->json(['status'=> 'error', 'message' => 'This request is not valid.', 'data' => '']);
		}
	}
	
}
