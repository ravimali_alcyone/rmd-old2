<?php
  
namespace App\Http\Controllers\Auth;
   
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Redis\Factory;
use Session;
use App\User;
   
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
  
    use AuthenticatesUsers;
  
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin/home';
   
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
   
    public function login(Request $request)
    {   
        $input = $request->all();
   
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);
   
        if(auth()->attempt(array('email' => $input['email'], 'password' => $input['password'])))
        {
			
			if(auth()->user()->status == 1){
				
				if (auth()->user()->is_admin == 1 && (auth()->user()->user_role == 1 || auth()->user()->user_role == 2) ) {
					return redirect()->route('admin.home');
				}else{
					Auth::logout();
					$msg = 'You are not authorized to login.';
					return redirect('/login')->withErrors([$msg]);	
				}				
			}else{
				Auth::logout();
				$msg = 'You must be active to login.';
				return redirect('/login')->withErrors([$msg]);
			}

        }else{
		    $msg = 'Invalid login credentials';
			return redirect('/login')->withErrors([$msg]);				
        }
          
    }
	
	public function logout(Request $request) {
		$route = '';
		
		if(auth()->user()->is_admin == 1 && (auth()->user()->user_role == 1 || auth()->user()->user_role == 2) ) {
			$route = '/login';
		}else{
			$route = '/sign_in';
            User::where("id",auth()->user()->id)->update(array("is_available"=>0));	
		}
				
		Auth::logout();
		Session::flush();
		return redirect($route);
	}
	
}