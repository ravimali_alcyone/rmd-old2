<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;
use App\Library\Services\CommonService;
use App\PaymentLogs;
use App\PaymentCardInfo;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Auth;
use DB;
use Route;
use App\TreatmentMedicine;
use App\MedicineVariant;
use App\Order;
use App\OnlineVisit;
use App\OnlineVisitProviderAssign;
use App\OnlineVisitStatusTimeline;
use App\OnlineVisitPaywallFeature;
use DateTime;
use App\Service;
use Session;
use App\EventAppointment;
use App\User;
use App\Events\Visit_all_status;
use App\Notifications;


class PaymentController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //$this->middleware('auth'); // later enable it when needed user login while payment
    }

    // start page form after start
    public function pay() {
        return view('pay');
    }
	
	#Card validation 
	public function payment_card_verification(Request $request)
	{
		
		if($request->cardSelect=='new'){
			$messages = [
				'card_owner.regex' => 'Name is required field.',
				'card_cvv.regex' => 'CVV must be 3 digit long and must be numeric.',
				'card_number.regex' => 'Card number must be 16 digit long and must be numeric.',
				'expirationdate.regex' => 'Invalid card expiration format.',
			];
			$validator = Validator::make($request->all(), [
				'card_owner' => 'required',
				'card_cvv' => 'required|digits:3',
				'card_number' => 'required|digits:16',
				'expirationdate' => 'required|digits:6',
			], $messages)->validate();
		}else{
			$messages = [
				'card_cvv.regex' => 'CVV must be 3 digit long and must be numeric.',
			];
			$validator = Validator::make($request->all(), [
				'card_cvv' => 'required|digits:3',
			], $messages)->validate();
		}
		#Check this user is come from direct front page visit means without registration first. If this, user have not any payment confirmation pin.
		$pinCheck = User::select('users.*')->where('id',auth()->user()->id)->first();
		$checkFlag = ($pinCheck->payment_confirmation_pin=="" || $pinCheck->payment_confirmation_pin==null) ? 1 : 0;	
		return response()->json(['status'=> 'success', 'redirect'=> '', 'message' => '','flag' =>$checkFlag ]);
	}

    public function handleonlinepay(Request $request, CommonService $common) {
        $input = $request->input();
		
		$messages = [
			'payment_pin.regex' => 'Payment Pin must be 4 digit long.',
		];
		$validator = Validator::make($request->all(), [
			'payment_pin' => 'required|digits:4'
		], $messages)->validate();
		
		$setPin = auth()->user()->payment_confirmation_pin;
		
		if(Hash::check($request->payment_pin, $setPin)) 							#verify input pin with set pin
		{
			
			if(!empty(session('paywall_feature_type')))
			{
			
				#Create a merchantAuthenticationType object with authentication details retrieved from the constants file
			
				$merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
				$merchantAuthentication->setName(env('MERCHANT_LOGIN_ID'));
				$merchantAuthentication->setTransactionKey(env('MERCHANT_TRANSACTION_KEY'));
			
				$card = $input['card_number'];
				$cardVaildity = explode('/',$input['expirationdate']);
				$nameoncard = explode(' ',$input['card_owner']);
				//$sh_add = session('shipping_address');

				// Set the transaction's refId
				$refId = 'ref' . time();
				$cardNumber = preg_replace('/\s+/', '', $card);
				$expiry_date = $cardVaildity[1] . "-" .$cardVaildity[0];
			
				#Create the payment data for a credit card
				$creditCard = new AnetAPI\CreditCardType();
				$creditCard->setCardNumber($cardNumber);
				$creditCard->setExpirationDate($expiry_date);
				$creditCard->setCardCode($input['card_cvv']);

				#Add the payment data to a paymentType object
				$paymentOne = new AnetAPI\PaymentType();
				$paymentOne->setCreditCard($creditCard);
				
				#Create order information
				$order = new AnetAPI\OrderType();
				$order->setInvoiceNumber(mt_rand(10000, 99999));   //generate random invoice number     
				$order->setDescription("Online Visit medicine prescription payment."); 
				
				#Set the customer's Bill To address
				$customerAddress = new AnetAPI\CustomerAddressType();
				$customerAddress->setFirstName($nameoncard[0]);
				$customerAddress->setLastName($nameoncard[1]);
				//$customerAddress->setAddress($sh_add['shipping_address1'].', '.$sh_add['shipping_address2']);
				//$customerAddress->setCity($sh_add['shipping_city']);
				//$customerAddress->setState($sh_add['shipping_state']);
				//$customerAddress->setZip($sh_add['shipping_zipcode']);
				//$customerAddress->setCountry($sh_add['shipping_country']);
				
				#Create a customer shipping address This is the object that I added
				$customerShippingAddress = new AnetAPI\CustomerAddressType();
				$customerShippingAddress->setFirstName($nameoncard[0]);
				$customerShippingAddress->setLastName($nameoncard[1]);
				//$customerShippingAddress->setAddress($sh_add['shipping_address1'].', '.$sh_add['shipping_address2']);
				//$customerShippingAddress->setCity($sh_add['shipping_city']);
				//$customerShippingAddress->setState($sh_add['shipping_state']);
				//$customerShippingAddress->setZip($sh_add['shipping_zipcode']);
				//$customerShippingAddress->setCountry($sh_add['shipping_country']);
				
				
				// Create a TransactionRequestType object and add the previous objects to it
				$transactionRequestType = new AnetAPI\TransactionRequestType();
				$transactionRequestType->setTransactionType("authCaptureTransaction");
				$transactionRequestType->setAmount($input['amount']);
				$transactionRequestType->setPayment($paymentOne);
				$transactionRequestType->setOrder($order);
				$transactionRequestType->setBillTo($customerAddress);
				$transactionRequestType->setShipTo($customerShippingAddress);
				

				// Assemble the complete transaction request
				$requests = new AnetAPI\CreateTransactionRequest();
				$requests->setMerchantAuthentication($merchantAuthentication);
				$requests->setRefId($refId);
				$requests->setTransactionRequest($transactionRequestType);

				// Create the controller and get the response
				$controller = new AnetController\CreateTransactionController($requests);
				$response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::SANDBOX);
			
				if ($response != null) 
				{
					// Check to see if the API request was successfully received and acted upon
					if ($response->getMessages()->getResultCode() == "Ok") 
					{
						// Since the API request was successful, look for a transaction response
						// and parse it to display the results of authorizing the card
						$tresponse = $response->getTransactionResponse();

						if ($tresponse != null && $tresponse->getMessages() != null) 
						{ 
							$message_text = $tresponse->getMessages()[0]->getDescription().", Transaction ID: " . $tresponse->getTransId();
							$msg_type = "success_msg";
							
							#Store data in database regrading for which visit payment
							$service_detail = Service::where('id',session('paywall_feature_service_id'))->where('status',1)->first();
							
							if($service_detail && $service_detail->count() > 0)
							{
								if(Session::has('paywall_feature_online_visit_id') && session('paywall_feature_online_visit_id') != '')
								{		
									
									$online_visit = OnlineVisit::where('id', session('paywall_feature_online_visit_id'))->first();
									
									if($online_visit && $online_visit->count() > 0)
									{
										$provider = User::where('id',session('paywall_feature_provider_id'))->first();
										
										$event_title = $service_detail->name."  virtual visit with ".auth()->user()->name; //to display on provider's appointment calendar
										$event_title2 = $service_detail->name." virtual visit with ".$provider->name; //to display on patient's appointment calendar
										
										$event_data = array(
											'provider_id' => session('paywall_feature_provider_id'),
											'online_visit_id' => session('paywall_feature_online_visit_id'),
											'patient_id' => auth()->user()->id,
											'service_id' => session('paywall_feature_service_id'),
											'status' => 1,
											'title' => $event_title,
											'title2' => $event_title2,
											'start' => date('Y-m-d H:i:s'),
											'end' => date('Y-m-d H:i:s'),
										);
										if(session('paywall_feature_type')=='video_call'){
											$event_data['paywall_feature_video_call']=1;
											$where_f="paywall_feature_video_call";
											
										}else{
											$event_data['paywall_feature_chat']=1;
											$where_f="paywall_feature_chat";
										}
										//Check Appointment Event
										$event = EventAppointment::where('online_visit_id', session('paywall_feature_online_visit_id'))
										->where('patient_id', auth()->user()->id)
										->where('provider_id', session('paywall_feature_provider_id'))
										->first();
										
										if(!empty($event) && $event->count() > 0){
											EventAppointment::where('id',$event->id)->update($event_data);
										}else{
											EventAppointment::create($event_data);
										}
										
										
										#Stored data in payment log table
										
										PaymentLogs::create([                                         
											'amount' => $input['amount'],
											'userid' => auth()->user()->id, 
											'visit_type' => $online_visit->visit_type,
											'paywall_feature_type' => session('paywall_feature_type'),
											'online_visit_id' => $online_visit->id,
											'payment_mode' =>1,						
											'response_code' => $tresponse->getResponseCode(),
											'transaction_id' => $tresponse->getTransId(),
											'auth_id' => $tresponse->getAuthCode(),
											'message_code' => $tresponse->getMessages()[0]->getCode(),
											'name_on_card' => trim($input['card_owner']),
											'quantity'=>1
										]);
										
										$noticeData = array(
											'created_id' => auth()->user()->id,
											'created_name' => auth()->user()->name,
											'created_image' => auth()->user()->image,
											'notification_type' => "visit_all_status",
											'notice_type' => $where_f,
											'post_id' => $online_visit->id,
											'followers_id' =>session('paywall_feature_provider_id'),
											'status' => 1,
											'created_at' => date('Y-m-d H:i:s'),
											'updated_at' => date('Y-m-d H:i:s')
										);
										
										$notice = Notifications::create($noticeData);
										$noticeData['id']=$notice->id;
										event(new Visit_all_status($noticeData));
															
									}
								}
							}
							//add payent card details
							if($input['cardSelect']=='new'){
								$online_visit_data = array(
											'is_primary' => 0, 
											'active_status' => 0,
										);
										
								PaymentCardInfo::where('user_id',auth()->user()->id)->update($online_visit_data);
								
								PaymentCardInfo::create([                                         
												'user_id' => auth()->user()->id, 
												'card_number' => trim($input['card_number']),
												'card_name' => trim($input['card_owner']),
												'expire_date' => trim($input['expirationdate']),
												'is_primary' =>1,						
												'active_status' => 1,
											]);
							}else{ 
							
								$online_visit_data = array(
											'is_primary' => 0, 
											'active_status' => 0,
										);
										
								PaymentCardInfo::where('user_id',auth()->user()->id)->update($online_visit_data);
								
								$online_visit_data = array(
											'is_primary' => 1, 
											'active_status' => 1,
										);
								PaymentCardInfo::where('card_id',$input['cardSelect'])->update($online_visit_data); 
								
							}
							
							session()->put('is_being_payment', 2); //1 for yes, 0 for No, 2 for finished
							session()->forget('final_amount');
							session()->forget('shipping_address');
							session()->forget('paywall_feature_type');
							session()->forget('paywall_feature_provider_id');
							session()->forget('paywall_feature_online_visit_id');
							session()->forget('paywall_feature_service_id');
							
							#Send email notification to user regarding payment..
								$input['invoice'] = mt_rand(10000, 99999);
								$input['description'] = 'Paywall feature payment.';
								$input['amount'] = number_format($input['amount'], 2);
								$input['transaction_type'] = 'Authorization and Capture';
								$input['auth_code'] = $tresponse->getAuthCode();
								$input['transaction_id'] = $tresponse->getTransId();
								$input['name'] = auth()->user()->name;
								$input['email'] = auth()->user()->email;
								$input['subject'] = 'Payment Information';
								$input['template'] = 'front.email_template.payment_information';
								$result = $common->sendMail($input);
							######     End Here   ########
							
							$message_text = 'Congratulation!! your payment done successfully.';
							$msg_status = "success";
							$paywall_feature = "paywall_feature";
						} 
						else 
						{
							$message_text = 'There were some issue with the payment. Please try again later.';
							$msg_type = "error_msg"; 
							
							#Send failed payment notification to user regarding payment..
								$input['message'] = 'There were some issue with the payment. Please try again later.';
								$input['name'] = auth()->user()->name;
								$input['email'] = auth()->user()->email;
								$input['subject'] = 'Payment Information';
								$input['template'] = 'front.email_template.payment_failed_information';
								$result = $common->sendMail($input);
							######     End Here   ########	

							if ($tresponse->getErrors() != null) {
								$message_text = $tresponse->getErrors()[0]->getErrorText();
								$msg_status = "error";                                    
							}
						}
					} 
					else 
					{
						$message_text = 'There were some issue with the payment. Please try again later.';
						$msg_status = "error";
							
						#Send failed payment notification to user regarding payment..
							$input['message'] = 'There were some issue with the payment. Please try again later.';
							$input['name'] = auth()->user()->name;
							$input['email'] = auth()->user()->email;
							$input['subject'] = 'Payment Information';
							$input['template'] = 'front.email_template.payment_failed_information';
							$result = $common->sendMail($input);
						######     End Here   ########

						$tresponse = $response->getTransactionResponse();

						if ($tresponse != null && $tresponse->getErrors() != null) {
							$message_text = $tresponse->getErrors()[0]->getErrorText();
							$msg_status = "error";                    
						} else {
							$message_text = $response->getMessages()->getMessage()[0]->getText();
							$msg_status = "error";
						}                
					}
				} 
				else 
				{
					#Send failed payment notification to user regarding payment..
						$input['message'] = 'There were some issue with the payment. Please try again later.';
						$input['name'] = auth()->user()->name;
						$input['email'] = auth()->user()->email;
						$input['subject'] = 'Payment Information';
						$input['template'] = 'front.email_template.payment_failed_information';
						$result = $common->sendMail($input);
					######     End Here   ########
					$message_text = "No response returned";
					$msg_status = "error";
					
				}
				
				return response()->json(['status'=> $msg_status, 'message' => $message_text, 'paywall_feature' => "paywall_feature"]);
				
			}
			
			#One time payment script
			else if(session('medicine_variant_payment_type')==0)
			{
				#Create a merchantAuthenticationType object with authentication details retrieved from the constants file
			
				$merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
				$merchantAuthentication->setName(env('MERCHANT_LOGIN_ID'));
				$merchantAuthentication->setTransactionKey(env('MERCHANT_TRANSACTION_KEY'));
			
				$card = $input['card_number'];
				$cardVaildity = explode('/',$input['expirationdate']);
				$nameoncard = explode(' ',$input['card_owner']);
				$sh_add = session('shipping_address');

				// Set the transaction's refId
				$refId = 'ref' . time();
				$cardNumber = preg_replace('/\s+/', '', $card);
				$expiry_date = $cardVaildity[1] . "-" .$cardVaildity[0];
			
				#Create the payment data for a credit card
				$creditCard = new AnetAPI\CreditCardType();
				$creditCard->setCardNumber($cardNumber);
				$creditCard->setExpirationDate($expiry_date);
				$creditCard->setCardCode($input['card_cvv']);

				#Add the payment data to a paymentType object
				$paymentOne = new AnetAPI\PaymentType();
				$paymentOne->setCreditCard($creditCard);
				
				#Create order information
				$order = new AnetAPI\OrderType();
				$order->setInvoiceNumber(mt_rand(10000, 99999));   //generate random invoice number     
				$order->setDescription("Online Visit medicine prescription payment."); 
				
				#Set the customer's Bill To address
				$customerAddress = new AnetAPI\CustomerAddressType();
				$customerAddress->setFirstName($nameoncard[0]);
				$customerAddress->setLastName($nameoncard[1]);
				$customerAddress->setAddress($sh_add['shipping_address1'].', '.$sh_add['shipping_address2']);
				$customerAddress->setCity($sh_add['shipping_city']);
				$customerAddress->setState($sh_add['shipping_state']);
				$customerAddress->setZip($sh_add['shipping_zipcode']);
				$customerAddress->setCountry($sh_add['shipping_country']);
				
				#Create a customer shipping address This is the object that I added
				$customerShippingAddress = new AnetAPI\CustomerAddressType();
				$customerShippingAddress->setFirstName($nameoncard[0]);
				$customerShippingAddress->setLastName($nameoncard[1]);
				$customerShippingAddress->setAddress($sh_add['shipping_address1'].', '.$sh_add['shipping_address2']);
				$customerShippingAddress->setCity($sh_add['shipping_city']);
				$customerShippingAddress->setState($sh_add['shipping_state']);
				$customerShippingAddress->setZip($sh_add['shipping_zipcode']);
				$customerShippingAddress->setCountry($sh_add['shipping_country']);
				
				
				// Create a TransactionRequestType object and add the previous objects to it
				$transactionRequestType = new AnetAPI\TransactionRequestType();
				$transactionRequestType->setTransactionType("authCaptureTransaction");
				$transactionRequestType->setAmount($input['amount']);
				$transactionRequestType->setPayment($paymentOne);
				$transactionRequestType->setOrder($order);
				$transactionRequestType->setBillTo($customerAddress);
				$transactionRequestType->setShipTo($customerShippingAddress);
				

				// Assemble the complete transaction request
				$requests = new AnetAPI\CreateTransactionRequest();
				$requests->setMerchantAuthentication($merchantAuthentication);
				$requests->setRefId($refId);
				$requests->setTransactionRequest($transactionRequestType);

				// Create the controller and get the response
				$controller = new AnetController\CreateTransactionController($requests);
				$response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::SANDBOX);
			
				if ($response != null) 
				{
					// Check to see if the API request was successfully received and acted upon
					if ($response->getMessages()->getResultCode() == "Ok") 
					{
						// Since the API request was successful, look for a transaction response
						// and parse it to display the results of authorizing the card
						$tresponse = $response->getTransactionResponse();

						if ($tresponse != null && $tresponse->getMessages() != null) 
						{ 
							$message_text = $tresponse->getMessages()[0]->getDescription().", Transaction ID: " . $tresponse->getTransId();
							$msg_type = "success_msg";
							
							#Store data in database regrading for which visit payment
							$service_detail = Service::where('id',session('online_visit_service_id'))->where('status',1)->first();
							
							if($service_detail && $service_detail->count() > 0)
							{
								if(Session::has('online_visit_id') && session('online_visit_id') != '')
								{		
									
									$online_visit = OnlineVisit::where('id', session('online_visit_id'))->where('is_submit',0)->first();
									
									if($online_visit && $online_visit->count() > 0)
									{
										$online_visit_data = array(
											'visit_amount' => $input['amount'], // services table
											'payment_status' => 'done',
											'is_submit' => 1,
										);
										
										OnlineVisit::where('id',$online_visit->id)->update($online_visit_data); //Update Online Visit 

										$order_data = array(
											'user_id' => auth()->user()->id,
											'online_visit_id' => $online_visit->id,					
											'sub_id' => '',
											'remarks' => 'Online Visit medicine prescription payment',
											'amount' => session('final_amount'),
											'status' => 1 // Processing
										);
										
										Order::create($order_data); //Order Create
										
										#Stored data in payment log table
										PaymentLogs::create([                                         
											'amount' => $input['amount'],
											'userid' => auth()->user()->id, 
											'visit_type' => $online_visit->visit_type,
											'online_visit_id' => $online_visit->id,
											'payment_mode' =>1,						
											'response_code' => $tresponse->getResponseCode(),
											'transaction_id' => $tresponse->getTransId(),
											'auth_id' => $tresponse->getAuthCode(),
											'message_code' => $tresponse->getMessages()[0]->getCode(),
											'name_on_card' => trim($input['card_owner']),
											'quantity'=>1
										]);
										
										//If Visit type is Asynchronous
										if($service_detail->visit_type == '1')
										{
											//Check providers to assign
											$provider = $common->getProviderForOnlineVisit();
											if($provider)
											{
												//Provider Assign to Online Visit
												$i_data3 = array(
													'online_visit_id' => $online_visit->id,
													'provider_id' => $provider,
													'status' => 1
												);
												OnlineVisitProviderAssign::create($i_data3);
												//Online Visit Status Timeline
												$i_data4 = array(
													'online_visit_id' => $online_visit->id,
													'old_visit_status' => NULL,
													'new_visit_status' => 'PENDING ACCEPTANCE',
												);
												OnlineVisitStatusTimeline::create($i_data4);
											}
										}						
									}
								}
							}
							//add payent card details
							if($input['cardSelect']=='new'){
								$online_visit_data = array(
											'is_primary' => 0, 
											'active_status' => 0,
										);
										
								PaymentCardInfo::where('user_id',auth()->user()->id)->update($online_visit_data);
								
								PaymentCardInfo::create([                                         
												'user_id' => auth()->user()->id, 
												'card_number' => trim($input['card_number']),
												'card_name' => trim($input['card_owner']),
												'expire_date' => trim($input['expirationdate']),
												'is_primary' =>1,						
												'active_status' => 1,
											]);
							}
							else
							{ 
							
								$online_visit_data = array(
											'is_primary' => 0, 
											'active_status' => 0,
										);
										
								PaymentCardInfo::where('user_id',auth()->user()->id)->update($online_visit_data);
								
								$online_visit_data = array(
											'is_primary' => 1, 
											'active_status' => 1,
										);
								PaymentCardInfo::where('card_id',$input['cardSelect'])->update($online_visit_data); 
								
							}
							
							#Send email notification to user regarding payment..
								$input['invoice'] = mt_rand(10000, 99999);
								$input['description'] = 'Online Visit medicine prescription payment.';
								$input['amount'] = number_format($input['amount'], 2);
								$input['transaction_type'] = 'Authorization and Capture';
								$input['auth_code'] = $tresponse->getAuthCode();
								$input['transaction_id'] = $tresponse->getTransId();
								$input['name'] = auth()->user()->name;
								$input['email'] = auth()->user()->email;
								$input['subject'] = 'Payment Information';
								$input['template'] = 'front.email_template.payment_information';
								$result = $common->sendMail($input);
							######     End Here   ########
							
							session()->put('is_being_payment', 2); //1 for yes, 0 for No, 2 for finished
							session()->forget('final_amount');
							session()->forget('shipping_address');
							$message_text = 'Congratulation!! your payment done successfully.';
							$msg_status = "success";
						} 
						else 
						{
							$message_text = 'There were some issue with the payment. Please try again later.';
							$msg_type = "error_msg";                                    

							if ($tresponse->getErrors() != null) {
								$message_text = $tresponse->getErrors()[0]->getErrorText();
								$msg_status = "error";                                    
							}
						}
					} 
					else 
					{
						#Send failed payment notification to user regarding payment..
							$input['message'] = 'There were some issue with the payment. Please try again later.';
							$input['name'] = auth()->user()->name;
							$input['email'] = auth()->user()->email;
							$input['subject'] = 'Payment Information';
							$input['template'] = 'front.email_template.payment_failed_information';
							$result = $common->sendMail($input);
						######     End Here   ########
						$message_text = 'There were some issue with the payment. Please try again later.';
						$msg_status = "error";                                    

						$tresponse = $response->getTransactionResponse();

						if ($tresponse != null && $tresponse->getErrors() != null) {
							$message_text = $tresponse->getErrors()[0]->getErrorText();
							$msg_status = "error";                    
						} else {
							$message_text = $response->getMessages()->getMessage()[0]->getText();
							$msg_status = "error";
						}                
					}
				} 
				else 
				{
					#Send failed payment notification to user regarding payment..
						$input['message'] = 'Sorry it seems you payment went fail due to some technical reason. Please try again later.';
						$input['name'] = auth()->user()->name;
						$input['email'] = auth()->user()->email;
						$input['subject'] = 'Payment Information';
						$input['template'] = 'front.email_template.payment_failed_information';
						$result = $common->sendMail($input);
					######     End Here   ########
							
					$message_text = "No response returned";
					$msg_status = "error";
					
				}
				
				return response()->json(['status'=> $msg_status, 'message' => $message_text, 'redirect' => ""]);			
			}
			
			#Recurring payment script
			else
			{
				$interval = session('medicine_plan_interval');
				$sh_add = session('shipping_address');
				$start_date = date('Y-m-d');
				$intervalLength = ($interval=='month') ? "30" : (($interval=='week') ? "7": (($interval=='day') ? "1" : (($interval=='year') ? "365" : "" )));
				$totalcycles=session('total_interval');
				
				//extract($arr_subscription);
    			/* Create a merchantAuthenticationType object with authentication details retrieved from the constants file */
				$merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
				$merchantAuthentication->setName(env('MERCHANT_LOGIN_ID'));
				$merchantAuthentication->setTransactionKey(env('MERCHANT_TRANSACTION_KEY'));
     
				$card = $input['card_number'];
				$cardVaildity = explode('/',$input['expirationdate']);
				// Set the transaction's refId
				$refId = 'ref' . time();
				$cardNumber = preg_replace('/\s+/', '', $card);
				$expiry_date = $cardVaildity[1] . "-" .$cardVaildity[0];
				$nameoncard = explode(' ',$input['card_owner']);
				
				// Subscription Type Info
				$subscription = new AnetAPI\ARBSubscriptionType();
				$subscription->setName("Sample Subscription");
				$interval = new AnetAPI\PaymentScheduleType\IntervalAType();
				$interval->setLength($intervalLength);
				$interval->setUnit("days");
				$paymentSchedule = new AnetAPI\PaymentScheduleType();
				$paymentSchedule->setInterval($interval);
				$paymentSchedule->setStartDate(new DateTime($start_date));
				$paymentSchedule->setTotalOccurrences($totalcycles);
				$subscription->setPaymentSchedule($paymentSchedule);
				$subscription->setAmount($input['amount']);
				//$subscription->setTrialAmount("0.00");
				 
				$creditCard = new AnetAPI\CreditCardType();
				$creditCard->setCardNumber($cardNumber);
				$creditCard->setExpirationDate($expiry_date);
				$payment = new AnetAPI\PaymentType();
				$payment->setCreditCard($creditCard);
				$subscription->setPayment($payment);
				$order = new AnetAPI\OrderType();
				$order->setInvoiceNumber(mt_rand(10000, 99999));   //generate random invoice number     
				$order->setDescription("Online Visit medicine prescription payment."); 
				$subscription->setOrder($order); 
				
				#Billing and Shipping Info
				$billTo = new AnetAPI\NameAndAddressType();
				$billTo->setFirstName($nameoncard[0]);
				$billTo->setLastName($nameoncard[1]);
				$billTo->setAddress($sh_add['shipping_address1'].', '.$sh_add['shipping_address2']);
				$billTo->setCity($sh_add['shipping_city']);
				$billTo->setState($sh_add['shipping_state']);
				$billTo->setZip($sh_add['shipping_zipcode']);
				$billTo->setCountry($sh_add['shipping_country']);
				$subscription->setBillTo($billTo);
				
				#Create a customer shipping address This is the object that I added
				$customerShippingAddress = new AnetAPI\NameAndAddressType();
				$customerShippingAddress->setFirstName($nameoncard[0]);
				$customerShippingAddress->setLastName($nameoncard[1]);
				$customerShippingAddress->setAddress($sh_add['shipping_address1'].', '.$sh_add['shipping_address2']);
				$customerShippingAddress->setCity($sh_add['shipping_city']);
				$customerShippingAddress->setState($sh_add['shipping_state']);
				$customerShippingAddress->setZip($sh_add['shipping_zipcode']);
				$customerShippingAddress->setCountry($sh_add['shipping_country']);
				$subscription->setShipTo($customerShippingAddress);
				
				$request = new AnetAPI\ARBCreateSubscriptionRequest();
				$request->setmerchantAuthentication($merchantAuthentication);
				$request->setRefId($refId);
				$request->setSubscription($subscription);
				$controller = new AnetController\ARBCreateSubscriptionController($request);
				$response = $controller->executeWithApiResponse( \net\authorize\api\constants\ANetEnvironment::SANDBOX);
				//echo '<pre>';
				//print_r($response); die;
				if ($response != null) 
				{
					// Check to see if the API request was successfully received and acted upon
					if ($response->getMessages()->getResultCode() == "Ok") 
					{
						// Since the API request was successful, look for a transaction response
						// and parse it to display the results of authorizing the card
						//$tresponse = $response->getTransactionResponse();

						//if ($tresponse != null && $tresponse->getMessages() != null) 
						//{ 
							//$message_text = $tresponse->getMessages()[0]->getDescription().", Transaction ID: " . $tresponse->getTransId();
							//$msg_type = "success_msg";
							
							#Store data in database regrading for which visit payment
							$service_detail = Service::where('id',session('online_visit_service_id'))->where('status',1)->first();
							
							if($service_detail && $service_detail->count() > 0)
							{
								if(Session::has('online_visit_id') && session('online_visit_id') != '')
								{		
									
									$online_visit = OnlineVisit::where('id', session('online_visit_id'))->where('is_submit',0)->first();
									
									if($online_visit && $online_visit->count() > 0)
									{
										$online_visit_data = array(
											'visit_amount' => $input['amount'], // services table
											'payment_status' => 'done',
											'is_submit' => 1,
										);
										
										OnlineVisit::where('id',$online_visit->id)->update($online_visit_data); //Update Online Visit 

										$order_data = array(
											'user_id' => auth()->user()->id,
											'online_visit_id' => $online_visit->id,					
											'sub_id' => '',
											'remarks' => 'Online Visit medicine prescription payment',
											'amount' => session('final_amount'),
											'status' => 1 // Processing
										);
										
										Order::create($order_data); //Order Create
										
										#Stored data in payment log table
										PaymentLogs::create([                                         
											'amount' => $input['amount'],
											'userid' => auth()->user()->id, 
											'visit_type' => $online_visit->visit_type,
											'online_visit_id' => $online_visit->id,
											'payment_mode' =>1,						
											'response_code' => 1,
											'transaction_id' => $response->getSubscriptionId(),
											'auth_id' => $response->getMessages()->getResultCode(),
											'message_code' => $response->getMessages()->getResultCode(),
											'name_on_card' => trim($input['card_owner']),
											'quantity'=>1
										]);
										
										//If Visit type is Asynchronous
										if($service_detail->visit_type == '1')
										{
											//Check providers to assign
											$provider = $common->getProviderForOnlineVisit();
											if($provider)
											{
												//Provider Assign to Online Visit
												$i_data3 = array(
													'online_visit_id' => $online_visit->id,
													'provider_id' => $provider,
													'status' => 1
												);
												OnlineVisitProviderAssign::create($i_data3);
												//Online Visit Status Timeline
												$i_data4 = array(
													'online_visit_id' => $online_visit->id,
													'old_visit_status' => NULL,
													'new_visit_status' => 'PENDING ACCEPTANCE',
												);
												OnlineVisitStatusTimeline::create($i_data4);
											}
										}						
									}
								}
							}
							
						//add payent card details
						if($input['cardSelect']=='new'){
							$online_visit_data = array(
										'is_primary' => 0, 
										'active_status' => 0,
									);
									
							PaymentCardInfo::where('user_id',auth()->user()->id)->update($online_visit_data);
							
							PaymentCardInfo::create([                                         
											'user_id' => auth()->user()->id, 
											'card_number' => trim($input['card_number']),
											'card_name' => trim($input['card_owner']),
											'expire_date' => trim($input['expirationdate']),
											'is_primary' =>1,						
											'active_status' => 1,
										]);
						}else{ 
						
							$online_visit_data = array(
										'is_primary' => 0, 
										'active_status' => 0,
									);
									
							PaymentCardInfo::where('user_id',auth()->user()->id)->update($online_visit_data);
							
							$online_visit_data = array(
										'is_primary' => 1, 
										'active_status' => 1,
									);
							PaymentCardInfo::where('card_id',$input['cardSelect'])->update($online_visit_data); 
							
						}
						
						#Send email notification to user regarding payment..
							$input['invoice'] = mt_rand(10000, 99999);
							$input['description'] = 'Online Visit medicine prescription payment.';
							$input['amount'] = number_format($input['amount'], 2);
							$input['transaction_type'] = 'Authorization and Capture';
							$input['auth_code'] = $response->getMessages()->getResultCode();
							$input['transaction_id'] = $response->getSubscriptionId();
							$input['name'] = auth()->user()->name;
							$input['email'] = auth()->user()->email;
							$input['subject'] = 'Payment Information';
							$input['template'] = 'front.email_template.payment_information';
							$result = $common->sendMail($input);
						######     End Here   ########
						 
						 
						session()->put('is_being_payment', 2); //1 for yes, 0 for No, 2 for finished
						session()->forget('final_amount');
						session()->forget('shipping_address');
						$message_text = 'Congratulation!! your payment done successfully.';
						$msg_status = "success";
					} 
					else 
					{
						#Send failed payment notification to user regarding payment..
							$input['message'] = 'There were some issue with the payment. Please try again later.';
							$input['name'] = auth()->user()->name;
							$input['email'] = auth()->user()->email;
							$input['subject'] = 'Payment Information';
							$input['template'] = 'front.email_template.payment_failed_information';
							$result = $common->sendMail($input);
						######     End Here   ########
						$message_text = 'There were some issue with the payment. Please try again later.';
						$msg_status = "error";
					}
				} 
				else 
				{
					#Send failed payment notification to user regarding payment..
						$input['message'] = 'Sorry payment failed due to some technical issue. Please try again later.';
						$input['name'] = auth()->user()->name;
						$input['email'] = auth()->user()->email;
						$input['subject'] = 'Payment Information';
						$input['template'] = 'front.email_template.payment_failed_information';
						$result = $common->sendMail($input);
					######     End Here   ########
					$message_text = "Sorry payment failed due to some technical issue.";
					$msg_status = "error";
				}
				
				return response()->json(['status'=> $msg_status, 'message' => $message_text, 'redirect' => ""]);			
				
			}
		}
		else
		{
			return response()->json(['status'=> 'error', 'message' => 'Sorry!! invalid pin. Try again later.', 'redirect' => ""]);	
		}
    }
	
	

}