<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller {

    # Dashboard Pages & Sections View
    public function PagesSection($page,$section=null) {
		//echo $section;die;
		$lang = 'en';
        if(Auth::user()) {
            $data=array();
            $data['user'] = Auth::user();
            $single_sec_data =  DB::table('sections')->select('*')->where(array('page_name' => $page,'section_name' => $section, 'language' => $lang))->first();
			
			//echo '<pre>';print_r($single_sec_data);die; 
            $page_data =  DB::table('sections')->select('*')->where(array('page_name' => $page, 'language' => $lang))->first();
			
			//echo '<Pre>';print_r($single_sec_data);die;

            if ($single_sec_data) {
	            $section != null ? $data['section_name'] = $single_sec_data->section_name : '';
				$data['parent_page'] = 'website_content';
	            $data['page'] = $data['page_name'] =  $single_sec_data->page_name;
	            $data['language'] =  $single_sec_data->language;
	            $data['section_content'] = json_decode($single_sec_data->section_content);
            } else {
				$data['parent_page'] = 'website_content';
            	$section != null ? $data['section_name'] = $section : '';
	            $data['page'] = $data['page_name'] =  $page;
	            $data['language'] =  $lang;
				
	           // $data['section_content'] = (!empty($page_data) ? json_decode($page_data->section_content) : '');
	            $data['section_content'] = array();
            }

            if ($section == null) {
				$data['parent_page'] = 'website_content';
				$data['page'] = $data['page_name'] =  $page;
                return view("admin.website_content.pages.$page", $data);
            } else {

                if($page == 'home'){
					$data['parent_page'] = 'website_content';
					$data['page'] = $data['page_name'] =  $page;					
            	   return view("admin.website_content.pages.home_sections.$section", $data);
                }
				
				elseif($page == 'about'){		
					$data['parent_page'] = 'website_content';
					$data['page'] = $data['page_name'] =  $page;				
					return view("admin.website_content.pages.about_sections.$section", $data);
				}				
				
            }
        } else {
            return redirect('/');
        }
    }
}
