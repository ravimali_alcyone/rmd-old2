<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Route::group(['middleware' => ['guest']], function () {
	Route::get('/', function () {
        return redirect('/login');
    });
}); */



Route::post('logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::get('admin', 'admin\HomeController@index')->name('admin')->middleware('is_admin');
Route::get('admin/home', 'admin\HomeController@index')->name('admin.home')->middleware('is_admin');
Route::get('admin/profile', 'admin\HomeController@profile')->name('admin.profile')->middleware('is_admin');
Route::post('admin/change_password', 'admin\HomeController@change_password')->name('admin.change_password')->middleware('is_admin');
Route::post('admin/contact_details', 'admin\HomeController@contact_details')->name('admin.contact_details')->middleware('is_admin');
Route::post('admin/profile_image', 'admin\HomeController@profile_image')->name('admin.profile_image')->middleware('is_admin');

#04/01/2022
Route::post('admin/setting/update/{flag1}/{flag2}', 'admin\HomeController@setting_update')->name('admin.setting.update')->middleware('is_admin');
Route::post('admin/forum/setting', 'admin\HomeController@update_forum_setting')->name('admin.forum.setting')->middleware('is_admin');
Route::post('admin/chat_paywall/setting', 'admin\HomeController@update_chat_paywall_setting')->name('admin.chat_paywall.setting')->middleware('is_admin');

//Sub-Admins
Route::any('admin/subadmins','admin\AdminController@index')->name('admin.subadmins');
Route::get('admin/subadmins/add','admin\AdminController@add')->name('admin.subadmins.add');
Route::get('admin/subadmins/add/{id}','admin\AdminController@edit')->name('admin.subadmins.edit');
Route::post('admin/subadmins/store', 'admin\AdminController@store')->name('admin.subadmins.store');
Route::delete('admin/subadmins/destroy/{id}','admin\AdminController@destroy')->name('admin.subadmins.destroy');
Route::post('admin/subadmins/change_status','admin\AdminController@change_status');

//Faqs
Route::any('admin/faqs','admin\FaqController@index')->name('admin.faqs');
Route::get('admin/faqs/add','admin\FaqController@add')->name('admin.faqs.add');
Route::get('admin/faqs/add/{id}','admin\FaqController@edit')->name('admin.faqs.edit');
Route::post('admin/faqs/store', 'admin\FaqController@store')->name('admin.faqs.store');
Route::delete('admin/faqs/destroy/{id}','admin\FaqController@destroy')->name('admin.faqs.destroy');
Route::post('admin/faqs/change_status', 'admin\FaqController@change_status')->name('admin.faqs.change_status');

// Providers
Route::any('admin/providers/{cat_id?}','admin\ProviderController@index')->name('admin.providers');
Route::get('admin/providers/add','admin\ProviderController@add')->name('admin.providers.add');
Route::get('admin/providers/add/{id}','admin\ProviderController@edit')->name('admin.providers.edit');
Route::post('admin/providers/store', 'admin\ProviderController@store')->name('admin.providers.store');
Route::delete('admin/providers/destroy/{id}','admin\ProviderController@destroy')->name('admin.providers.destroy');
Route::post('admin_provider_status','admin\ProviderController@change_status')->name('provider.status');
Route::get('admin/providers/view/{id}','admin\ProviderController@view');
Route::post('/admin_providers_verified','admin\ProviderController@verified')->name('provider.verified');

// Patients
Route::any('admin/patients','admin\PatientController@index')->name('admin.patients');
Route::get('admin/patients/add','admin\PatientController@add')->name('admin.patients.add');
Route::get('admin/patients/add/{id}','admin\PatientController@edit')->name('admin.patients.edit');
Route::post('admin/patients/store', 'admin\PatientController@store')->name('admin.patients.store');
Route::delete('admin/patients/destroy/{id}','admin\PatientController@destroy')->name('admin.patients.destroy');
Route::post('admin/patients/change_status','admin\PatientController@change_status');
Route::get('admin/patients/view/{id}','admin\PatientController@view');

// Products
Route::any('admin/products','admin\ProductController@index')->name('admin.products');
Route::get('admin/products/add','admin\ProductController@add')->name('admin.products.add');
Route::get('admin/products/edit/{id}','admin\ProductController@edit')->name('admin.products.edit');
Route::post('admin/products/store', 'admin\ProductController@store')->name('admin.products.store');
Route::delete('admin/products/destroy/{id}','admin\ProductController@destroy')->name('admin.products.destroy');
Route::post('admin/products/change_status','admin\ProductController@change_status');
Route::post('admin/products/get_sub_category', 'admin\ProductController@get_sub_category')->name('admin.products.get_sub_category');
Route::post('admin/products/get_sub_sub_category', 'admin\ProductController@get_sub_sub_category')->name('admin.products.get_sub_sub_category');
Route::get('admin/products/view/{id}','admin\ProductController@view');


//Service Pricing
Route::any('admin/service_pricing','admin\ServicePricingController@index')->name('admin.service_pricing');
Route::get('admin/service_pricing/add','admin\ServicePricingController@add')->name('admin.service_pricing.add');
Route::get('admin/service_pricing/add/{id}','admin\ServicePricingController@edit')->name('admin.service_pricing.edit');
Route::post('admin/service_pricing/store', 'admin\ServicePricingController@store')->name('admin.service_pricing.store');
Route::delete('admin/service_pricing/destroy/{id}','admin\ServicePricingController@destroy')->name('admin.service_pricing.destroy');
Route::post('admin/service_pricing/change_status','admin\ServicePricingController@change_status');

//Plan Features
Route::any('admin/plan_features','admin\PlanFeaturesController@index')->name('admin.plan_features');
Route::get('admin/plan_features/add','admin\PlanFeaturesController@add')->name('admin.plan_features.add');
Route::get('admin/plan_features/add/{id}','admin\PlanFeaturesController@edit')->name('admin.plan_features.edit');
Route::post('admin/plan_features/store', 'admin\PlanFeaturesController@store')->name('admin.plan_features.store');
Route::delete('admin/plan_features/destroy/{id}','admin\PlanFeaturesController@destroy')->name('admin.plan_features.destroy');
Route::post('admin/plan_features/change_status','admin\PlanFeaturesController@change_status');

//Contact Us
Route::any('admin/contact_us','admin\ContactUsController@index')->name('admin.contact_us');
Route::delete('admin/contact_us/destroy/{id}','admin\ContactUsController@destroy')->name('admin.contact_us.destroy');
Route::post('admin/contact_us/change_status','admin\ContactUsController@change_status');


// Visit Pool

//Route::delete('admin/visits/destroy/{id}','admin\OnlineVisitController@destroy');
//Route::post('admin/visits/change_status','admin\OnlineVisitController@change_status');

Route::any('admin/asynchronous_visits','admin\OnlineVisitController@index')->name('admin.asynchronous_visits');
Route::any('admin/synchronous_visits','admin\OnlineVisitController@index2')->name('admin.synchronous_visits');
Route::any('admin/concierge_visits','admin\OnlineVisitController@index3')->name('admin.concierge_visits');

Route::get('admin/asynchronous_visits/view/{id}','admin\OnlineVisitController@view');
Route::get('admin/synchronous_visits/view/{id}','admin\OnlineVisitController@view2');
Route::get('admin/concierge_visits/view/{id}','admin\OnlineVisitController@view3');

//Services/Treatments
Route::any('admin/services','admin\ServiceController@index')->name('admin.services');
Route::get('admin/services/add','admin\ServiceController@add')->name('admin.services.add');
Route::get('admin/services/add/{id}','admin\ServiceController@edit')->name('admin.services.edit');
Route::post('admin/services/store', 'admin\ServiceController@store')->name('admin.services.store');
Route::delete('admin/services/destroy/{id}','admin\ServiceController@destroy')->name('admin.services.destroy');
Route::post('admin/services/change_status','admin\ServiceController@change_status');


//Visit Forms
Route::any('admin/visit_forms','admin\VisitFormController@index')->name('admin.visit_forms');
Route::get('admin/visit_forms/add','admin\VisitFormController@add')->name('admin.visit_forms.add');
Route::get('admin/visit_forms/add/{id}','admin\VisitFormController@edit')->name('admin.visit_forms.edit');
Route::post('admin/visit_forms/store', 'admin\VisitFormController@store')->name('admin.visit_forms.store');
Route::delete('admin/visit_forms/destroy/{id}','admin\VisitFormController@destroy')->name('admin.visit_forms.destroy');

Route::get('admin/visit_form_questions/add/{formid}','admin\VisitFormController@addQuestion')->name('admin.visit_form_questions.add');
Route::post('admin/visit_form_questions/store', 'admin\VisitFormController@storeQuestion')->name('admin.visit_form_questions.store');

Route::delete('admin/visit_forms/destroy_field/{id}','admin\VisitFormController@destroy_field')->name('admin.visit_forms.destroy_field');
Route::post('admin/visit_forms/get_services', 'admin\VisitFormController@get_services')->name('admin.visit_forms.get_services');

Route::delete('admin/visit_forms/destroy_question/{id}','admin\VisitFormController@destroy_question')->name('admin.visit_forms.destroy_question');
Route::delete('admin/visit_forms/destroy_option/{id}','admin\VisitFormController@destroy_option')->name('admin.visit_forms.destroy_option');


//Testimonials
Route::any('admin/testimonials','admin\TestimonialController@index')->name('admin.testimonials');
Route::get('admin/testimonials/add','admin\TestimonialController@add')->name('admin.testimonials.add');
Route::get('admin/testimonials/add/{id}','admin\TestimonialController@edit')->name('admin.testimonials.edit');
Route::post('admin/testimonials/store', 'admin\TestimonialController@store')->name('admin.testimonials.store');
Route::delete('admin/testimonials/destroy/{id}','admin\TestimonialController@destroy')->name('admin.testimonials.destroy');
Route::post('admin/testimonials/change_status','admin\TestimonialController@change_status');

//Sponsors
Route::any('admin/sponsors','admin\SponsorController@index')->name('admin.sponsors');
Route::get('admin/sponsors/add','admin\SponsorController@add')->name('admin.sponsors.add');
Route::get('admin/sponsors/add/{id}','admin\SponsorController@edit')->name('admin.sponsors.edit');
Route::post('admin/sponsors/store', 'admin\SponsorController@store')->name('admin.sponsors.store');
Route::delete('admin/sponsors/destroy/{id}','admin\SponsorController@destroy')->name('admin.sponsors.destroy');
Route::post('admin/sponsors/change_status','admin\SponsorController@change_status');


// Blogs
Route::any('admin/blogs','admin\BlogController@index')->name('admin.blogs');
Route::get('admin/blogs/add','admin\BlogController@add')->name('admin.blogs.add');
Route::get('admin/blogs/edit/{id}','admin\BlogController@edit')->name('admin.blogs.edit');
Route::post('admin/blogs/store', 'admin\BlogController@store')->name('admin.blogs.store');
Route::delete('admin/blogs/destroy/{id}','admin\BlogController@destroy')->name('admin.blogs.destroy');
Route::post('admin/blogs/change_status','admin\BlogController@change_status');
Route::post('admin/blogs/get_topics', 'admin\BlogController@get_topics')->name('admin.blogs.get_topics');
Route::get('admin/blogs/view/{id}','admin\BlogController@view');
Route::any('admin/blogs/blog_category','admin\BlogController@blog_category')->name('admin.blogs.blog_category');
Route::post('admin/blogs/change_category_status','admin\BlogController@change_category_status');
Route::get('admin/blogs/add_category/{id}','admin\BlogController@add_category');
Route::POST('admin/blogs/store_category','admin\BlogController@store_category')->name('admin.blogs.store_category');
Route::delete('admin/category/destroy/{id}','admin\BlogController@destroy_category')->name('admin.category.destroy');
Route::delete('admin/category/destroy_topic/{id}','admin\BlogController@destroy_topic')->name('admin.category.destroy_topic');
#06/12/2021 By Sudhir
Route::delete('admin/category/verify/{id}','admin\BlogController@verify_category')->name('admin.category.verify');
Route::delete('admin/category_topic/verify/{id}','admin\BlogController@verify_category_topic')->name('admin.category_topic.verify');
Route::delete('admin/blog_post/verify/{id}','admin\BlogController@verify_blog_post')->name('admin.blog_post.verify');
#12/11/2021
Route::get('admin/blogs/category_topic','admin\BlogController@category_topic')->name('admin.blogs.category_topic');
Route::get('admin/blogs/blog_post_type','admin\BlogController@blog_post_type')->name('admin.blogs.blog_post_type');
Route::get('admin/blogs/add_category_topic/{id}','admin\BlogController@add_category_topic');
Route::POST('admin/blogs/store_category_topic','admin\BlogController@store_category_topic')->name('admin.blogs.store_category_topic');
Route::get('admin/blogs/add_post_type/{id}','admin\BlogController@add_post_type');
Route::POST('admin/blogs/store_post_type','admin\BlogController@store_post_type')->name('admin.blogs.store_post_type');



/* Ravi K changes */
//ADMIN PANEL
//forum topics

Route::get('admin/forums/topics/edit/{id}','admin\ForumsController@editTopic')->name('admin.forums.topics.edit');	
Route::post('admin/forums/topics/comment_on','admin\ForumsController@comment_on')->name('admin.forums.topics.comment_on');
Route::post('admin/forums/topics/is_paid','admin\ForumsController@is_paid')->name('admin.forums.topics.is_paid');
Route::delete('admin/forums/topics/verify/{id}','admin\ForumsController@verify_topic')->name('admin.forums.topics.verify');
Route::post('admin/forums/topics/is_paid','admin\ForumsController@topic_paid_status')->name('admin.forums.topics.is_paid');

//forum categories
Route::any('admin/forums/categories','admin\ForumsController@categories_list')->name('admin.forums.categories.list');
Route::get('admin/forums/categories/edit/{id}','admin\ForumsController@edit')->name('admin.forums.categories.edit');	
Route::post('admin/forums/categories/store','admin\ForumsController@store')->name('admin.forums.categories.store');	
Route::post('admin/forums/categories/status','admin\ForumsController@status')->name('admin.forums.categories.status');
Route::post('admin/forums/categories/approve','admin\ForumsController@approve')->name('admin.forums.categories.approve');
Route::delete('admin/forums/categories/destroy/{id}','admin\ForumsController@destroy_category')->name('admin.forums.categories.delete');
Route::delete('admin/forum/category/verify/{id}','admin\ForumsController@verify_category')->name('admin.forum.category.verify');
Route::post('admin/forums/categories/change_status','admin\ForumsController@change_status');

#07/01/2022
Route::any('admin/forums','admin\ForumsController@categories_list')->name('admin.forums');
Route::get('admin/forums/addForum/{id}','admin\ForumsController@addForum')->name('admin.forums.addForum');
Route::post('admin/forums/store','admin\ForumsController@storeForum')->name('admin.forums.store');
Route::any('admin/forums/posts','admin\ForumsController@index')->name('admin.forums.posts');
Route::get('admin/forums/post/add','admin\ForumsController@addPost')->name('admin.forums.post.add');
Route::get('admin/forums/post/edit/{id}','admin\ForumsController@editPost')->name('admin.forums.post.add');
Route::delete('admin/forums/post/destroy/{id}','admin\ForumsController@destroy')->name('admin.forums.post.delete');
Route::post('admin/forums/post/status','admin\ForumsController@topicStatus')->name('admin.forums.post.status');
Route::post('admin/forums/topics/store','admin\ForumsController@storePost')->name('admin.forums.topics.store');


//FRONT
Route::get('forum','front\FrontController@forum_home')->name('front.forum');
Route::get('forum/posts/{id}/{pid}','front\FrontController@forum_post')->name('forum.posts');
Route::get('view-article/{slug}','front\FrontController@view_article')->name('view-article');
Route::post('post/comment','front\FrontController@post_comments')->name('post.comment');
Route::post('forums/post/like','front\FrontController@forum_post_like')->name('forums.post.like');

Route::post('forum/category/{slug}','front\FrontController@forum_topics_by_category')->name('front.forum.category');
Route::post('forum/topic/{slug}','front\FrontController@forum_topic_by_id')->name('front.forum.topic');
/* Ravi K changes */

// Treatment Medicines
Route::any('admin/treatment_medicines','admin\TreatmentMedicineController@index')->name('admin.treatment_medicines');
Route::get('admin/treatment_medicines/add','admin\TreatmentMedicineController@add')->name('admin.treatment_medicines.add');
Route::get('admin/treatment_medicines/edit/{id}','admin\TreatmentMedicineController@edit')->name('admin.treatment_medicines.edit');
Route::post('admin/treatment_medicines/store', 'admin\TreatmentMedicineController@store')->name('admin.treatment_medicines.store');
Route::delete('admin/treatment_medicines/destroy/{id}','admin\TreatmentMedicineController@destroy')->name('admin.treatment_medicines.destroy');
Route::delete('admin/treatment_medicines/variant_destroy/{id}','admin\TreatmentMedicineController@variant_destroy')->name('admin.treatment_medicines.variant_destroy');
Route::post('admin/treatment_medicines/change_status','admin\TreatmentMedicineController@change_status');
Route::post('admin/treatment_medicines/get_services', 'admin\TreatmentMedicineController@get_services')->name('admin.treatment_medicines.get_services');
Route::post('image-upload', 'admin\TreatmentMedicineController@imageUpload')->name('image-upload');
Route::get('admin/treatment_medicines/view/{id}','admin\TreatmentMedicineController@view');

# Dashboard pages & sections
Route::get('/admin/content/{pagename}', 'admin\DashboardController@PagesSection');
Route::get('/admin/content/{pagename}/{sectionname}', 'admin\DashboardController@PagesSection');
Route::post('/admin/insertAndUpdateSection', 'admin\DataStoreController@insertAndUpdateSection');
# Files upload
Route::post('/admin/FilesUpload', 'admin\DataStoreController@FilesUpload');
Route::post('/admin/MultipalFilesUpload', 'admin\DataStoreController@MultipalFilesUpload');
Route::post('/admin/multiSectionImageUpload', 'admin\DataStoreController@multiSectionImageUpload');

Auth::routes();

//Reports	
Route::get('admin/reports/prescription_products', 'admin\ReportController@prescription_products')->name('admin.reports.prescription_products');	
Route::get('admin/reports/providers', 'admin\ReportController@providers')->name('admin.reports.providers');	
Route::get('admin/reports/patients', 'admin\ReportController@patients')->name('admin.reports.patients');

//Website Content
Route::get('admin/website_content', 'admin\WebsiteContentController@index')->name('admin.website_content');
Route::get('admin/website_content/contact', 'admin\WebsiteContentController@contact')->name('admin.website_content.contact');

# Dashboard pages & sections
Route::get('/admin/{lang}/{pagename}', 'admin\DashboardController@PagesSection');
Route::get('/admin/{lang}/{pagename}/{sectionname}', 'admin\DashboardController@PagesSection');
Route::post('/admin/insertAndUpdateSection', 'admin\DataStoreController@insertAndUpdateSection');

# Files upload
Route::post('/admin/FilesUpload', 'admin\DataStoreController@FilesUpload');
Route::post('/admin/MultipalFilesUpload', 'admin\DataStoreController@MultipalFilesUpload');
Route::post('/admin/multiSectionImageUpload', 'admin\DataStoreController@multiSectionImageUpload');


# Front Routes
Route::get('/', 'front\FrontController@index')->name('home');
Route::get('/home', 'front\FrontController@index')->name('home');
Route::get('/plans', 'front\FrontController@plans')->name('plans');
Route::get('/how_it_works', 'front\FrontController@how_it_works');
Route::get('/blogs', 'front\FrontController@blogs');
Route::get('/about', 'front\FrontController@about');
Route::get('/contact', 'front\FrontController@contact');
Route::post('/contactSubmit', 'front\FrontController@contact')->name('front.contactSubmit');
Route::get('/faq', 'front\FrontController@faq');
Route::get('/providers', 'front\FrontController@providers');
Route::get('/providers/{id}', 'front\FrontController@provider_detail');
Route::get('/privacy-policy', 'front\FrontController@privacy_policy');
Route::get('/terms-conditions', 'front\FrontController@terms_conditions');
Route::get('/terms-of-use', 'front\FrontController@terms_of_use');

Route::post('/get_services', 'front\FrontController@get_services')->name('get_services');
Route::get('/treatment', 'front\FrontController@treatment');
Route::get('/medicine/{id}', 'front\FrontController@medicineDetail');
Route::get('/signup', 'front\FrontController@signup')->name('signup');
Route::get('/joinnow', 'front\FrontController@joinnow')->name('joinnow');
Route::get('/signup/{temp_url?}', 'front\FrontController@signup')->name('signup');
Route::post('/sendEmailOtp', 'front\FrontController@sendEmailOtp')->name('send_email_otp');
Route::post('/resendEmailOtp', 'front\FrontController@resendEmailOtp')->name('resend_email_otp');
Route::post('/signupSubmit', 'front\FrontController@signupSubmit')->name('signup_submission');
Route::get('/online-visit/welcome', 'front\FrontController@online_visit_welcome')->name('online_visit_welcome');
Route::get('/online-visit/basics', 'front\FrontController@online_visit_basics')->name('online_visit_basics');
Route::post('/basicsSubmit', 'front\FrontController@basicsSubmit')->name('basics_submission');
Route::post('/onlineVisitSubmit', 'front\FrontController@onlineVisitSubmit')->name('onlineVisitSubmit');
Route::get('/online-visit/medical_questions', 'front\FrontController@medical_questions')->name('medical_questions');
Route::get('/online-visit/treatment_preference', 'front\FrontController@treatment_preference')->name('treatment_preference');
Route::post('/online-visit/get_medicine_variants', 'front\FrontController@getMedicineVariants')->name('get_medicine_variants');
Route::post('/online-visit/get_treatment_review', 'front\FrontController@getTreatmentReview')->name('get_treatment_review');
Route::post('online-visit/profile_image', 'front\FrontController@profile_image')->name('online_visit.profile_image');
Route::post('online-visit/id_card_image', 'front\FrontController@id_card_image')->name('online_visit.id_card_image');

Route::get('/sign_in', 'front\FrontController@sign_in')->name('sign_in');
Route::post('/signinSubmit', 'front\FrontController@signinSubmit')->name('signin_submission');
Route::any('/forgot_password', 'front\FrontController@forgot_password')->name('forgot_password');
Route::any('/reset_password', 'front\FrontController@reset_password')->name('reset_password');
Route::get('/providersignup', 'front\FrontController@provider_signup')->name('provider_signup');
Route::post('/providersignupAccountInfo', 'front\FrontController@providersignupAccountInfo')->name('providersignup_account_info');
Route::post('/providersignupPernlInfo', 'front\FrontController@providersignupPernlInfo')->name('providersignup_pernl_info');
Route::post('/providersignupProfsInfo', 'front\FrontController@providersignupProfsInfo')->name('providersignup_profs_info');
Route::post('/providersignupSubmit', 'front\FrontController@providersignupSubmit')->name('providersignup_submission');


/* By Ravi K */

// User(Patient) Blogs
Route::any('user/blogs','front\BlogController@index')->name('user.blogs');
Route::get('user/blogs/add','front\BlogController@add')->name('user.blogs.add');
Route::get('user/blogs/view/{id}','front\BlogController@view');
Route::get('user/blogs/edit/{id}','front\BlogController@edit')->name('user.blogs.edit');
Route::post('user/blogs/store', 'front\BlogController@store')->name('user.blogs.store');
Route::delete('user/blogs/destroy/{id}','front\BlogController@destroy')->name('user.blogs.destroy');
Route::post('user/blogs/change_status','front\BlogController@change_status');
Route::post('user/blogs/get_topics', 'front\BlogController@get_topics')->name('user.blogs.get_topics');

// Provider(Doctor) Blogs
Route::any('provider/blogs','front\BlogController@index')->name('provider.blogs');
Route::get('provider/blogs/add','front\BlogController@add')->name('provider.blogb  s.add');
Route::get('provider/blogs/view/{id}','front\BlogController@view');
Route::get('provider/blogs/edit/{id}','front\BlogController@edit')->name('provider.blogs.edit');
Route::post('provider/blogs/store', 'front\BlogController@store')->name('provider.blogs.store');
Route::delete('provider/blogs/destroy/{id}','front\BlogController@destroy')->name('provider.blogs.destroy');
Route::post('provider/blogs/change_status','front\BlogController@change_status');
Route::post('provider/blogs/get_topics', 'front\BlogController@get_topics')->name('provider.blogs.get_topics');

// Front(Guest) Blogs
Route::get('blogs','front\FrontController@blogs')->name('front.blogs');
Route::post('blogs/search','front\FrontController@blogs_search')->name('front.blogs.search');
Route::post('blogs/comments','front\FrontController@blog_comments_add')->name('front.blogs.comments');
Route::get('blogs/post/{slug}','front\FrontController@blog_detail')->name('front.blogs.post');
/* By Ravi K */

#25/11/2021 By sudhir
Route::get('/blogs/category/{id}', 'front\FrontController@blog_catgeory_topic');
Route::any('/blogs/category/{id}/{tid}', 'front\FrontController@blog_catgeory_topic');
Route::post('/blogs/subscribe', 'front\FrontController@blog_subscription')->name('blogs.subscribe');


#Route::get('/user/dashboard', 'front\PatientController@index')->name('user.dashboard');
Route::get('/user/user_preference', 'front\PatientController@user_preference')->name('user.user_preference');
Route::post('/user/set_payment_pin', 'front\PatientController@set_payment_pin')->name('user.set_payment_pin');
Route::get('/user/profile', 'front\PatientController@profile')->name('user.profile');
Route::post('/user/shipping', 'front\PatientController@shipping')->name('user.shipping');
Route::post('/user/update_profile', 'front\PatientController@update_profile')->name('user.update_profile');
Route::any('/user/change_password', 'front\PatientController@change_password')->name('user.change_password');
Route::any('/user/treatments', 'front\PatientController@treatments')->name('user.treatments');
Route::any('/user/appointments', 'front\PatientController@appointments')->name('user.appointments');
Route::get('/user/subscriptions', 'front\PatientController@subscriptions')->name('user.subscriptions');
Route::get('/user/orders', 'front\PatientController@orders')->name('user.orders');
Route::get('/user/select_provider/{online_visit_id}', 'front\PatientController@selectProvider');
Route::post('/user/get_provider_data', 'front\PatientController@get_provider_data')->name('user.get_provider_data');
Route::post('/user/selectProviderSubmit', 'front\PatientController@selectProviderSubmit')->name('user.selectProviderSubmit');
Route::get('/user/select_appointment/{online_visit_id}', 'front\PatientController@selectAppointment');
Route::post('/user/get_time_slots', 'front\PatientController@getTimeSlotsByDate')->name('user.get_time_slots');
Route::post('/user/addAppointment', 'front\PatientController@addAppointment')->name('user.addAppointment');

Route::post('/user/check_video_call', 'front\PatientController@check_video_call')->name('user.check_video_call');
Route::get('/user/video_call/{online_visit_id}/{provider_id}', 'front\PatientController@video_call');
Route::post('/user/provider_feedback', 'front\PatientController@provider_feedback')->name('user.provider_feedback');
Route::post('/user/change_profile_pic', 'front\PatientController@changeProfilePic')->name('user.change_profile_pic');

#19/01/2022
Route::post('/user/set_merchant_account', 'front\PatientController@setMerchantAccount')->name('user.set_merchant_account');

#Sudhir 24/11/2021
Route::get('/user/set_pin/{p_type}', 'front\FrontController@set_pin');
Route::get('/user/set_pin/{p_type}/{interval}', 'front\FrontController@set_pin');

//Billing for paywall features for patient
Route::get('/user/paywall_payment_page', 'front\PatientController@paywall_payment_page')->name('user.paywall_payment_page');
Route::get('/user/paywall_success_page', 'front\PatientController@paywall_success_page')->name('user.paywall_success_page');
Route::post('/user/paywall_billing', 'PayWallBillingController@index')->name('user.paywall_billing');
Route::get('/user/paywall_reprocess', 'PayWallBillingController@reprocess')->name('user.paywall_reprocess');
Route::get('/user/view/{id}', 'front\PatientController@user_view');

Route::any('gc/callback}', 'front\FrontController@gc_callback');

//Provider Routes

Route::get('/provider/dashboard', 'front\ProviderController@index')->name('provider.dashboard');
Route::get('/provider/provider_preference', 'front\ProviderController@provider_preference')->name('provider.provider_preference');
Route::post('/provider/update_login_status', 'front\ProviderController@update_login_status')->name('provider.update_login_status');
Route::post('/provider/deleteProviderService', 'front\ProviderController@deleteProviderService')->name('provider.deleteProviderService');
Route::get('/provider/profile', 'front\ProviderController@profile')->name('provider.profile');
Route::post('/provider/update_profile', 'front\ProviderController@update_profile')->name('provider.update_profile');
Route::post('/provider/update_setting', 'front\ProviderController@update_setting')->name('provider.update_setting');
Route::post('/provider/profileSummary', 'front\ProviderController@update_summary')->name('provider.update_summary');
Route::post('/provider/update_contact', 'front\ProviderController@update_contact')->name('provider.update_contact');
Route::get('/provider/setting', 'front\ProviderController@setting')->name('provider.setting');
Route::any('/provider/change_password', 'front\ProviderController@change_password')->name('provider.change_password');
Route::post('/provider/add_location', 'front\ProviderController@add_location')->name('provider.add_location');
Route::post('/provider/delete_location', 'front\ProviderController@delete_location')->name('provider.delete_location');
Route::post('/provider/updateTreatCat', 'front\ProviderController@updateTreatCat')->name('provider.updateTreatCat');
Route::post('/provider/add_weekend', 'front\ProviderController@add_weekend')->name('provider.add_weekend');
Route::any('/provider/patient_visits', 'front\ProviderController@patient_visits')->name('provider.patient_visits');
//Route::get('/provider/patient_visits/{id}', 'front\ProviderController@patient_visits');
Route::get('/provider/visit_detail/{id}', 'front\ProviderController@visit_detail');
Route::post('/provider/change_visit_status', 'front\ProviderController@change_visit_status')->name('provider.change_visit_status');
Route::any('/provider/appointments', 'front\ProviderController@appointments')->name('provider.appointments');
//Route::post('/provider/appointments_ajax', 'front\ProviderController@appointmentsAjax')->name('provider.appointments_ajax');
Route::any('/provider/add_appointment_timeslots', 'front\ProviderController@add_appointment_timeslots')->name('provider.add_appointment_timeslots');
Route::any('/provider/update_appointment_timeslot_single', 'front\ProviderController@update_appointment_timeslot_single')->name('provider.update_appointment_timeslot_single');
Route::post('/provider/getTimeSlotsByDate', 'front\ProviderController@getTimeSlotsByDate')->name('provider.getTimeSlotsByDate');
Route::post('/provider/change_appointment_status', 'front\ProviderController@change_appointment_status')->name('provider.change_appointment_status');
Route::post('/provider/visit_add_note', 'front\ProviderController@visit_add_note')->name('provider.visit_add_note');
Route::post('/provider/set_imp_visit', 'front\ProviderController@set_imp_visit')->name('provider.set_imp_visit');
Route::post('/provider/get_appointment_by_id', 'front\ProviderController@get_appointment_by_id')->name('provider.get_appointment_by_id');
Route::post('/provider/deleteTimeSlot', 'front\ProviderController@deleteTimeSlot')->name('provider.deleteTimeSlot');
Route::post('/provider/get_timeslot_by_id', 'front\ProviderController@get_timeslot_by_id')->name('provider.get_timeslot_by_id');
Route::post('/provider/availability_timeslots', 'front\ProviderController@availability_timeslots')->name('provider.availability_timeslots');
Route::post('/provider/change_profile_pic', 'front\ProviderController@changeProfilePic')->name('provider.change_profile_pic');
Route::post('/provider/add_more_certificate', 'front\ProviderController@addMoreCertificate')->name('provider.add_more_certificate');
//new
Route::post('/provider/get_timings', 'front\ProviderController@get_timings')->name('provider.get_timings');
Route::post('/provider/check_appointment', 'front\ProviderController@check_appointment')->name('provider.check_appointment');
Route::any('/provider/notification_view', 'front\ProviderController@notification_view')->name('provider.notification_view');
Route::get('/provider/view/{id}', 'front\ProviderController@user_view');
#19/01/2022
Route::post('/provider/set_merchant_account', 'front\ProviderController@setMerchantAccount')->name('provider.set_merchant_account');

//Common for Patient/Provider
Route::get('mychat/{id}', 'front\FrontController@mychat');
Route::get('myvideo/{id}', 'front\FrontController@myvideo');
Route::post('setChatId', 'front\FrontController@setChatId')->name('setChatId');


//Billing for online visit flow
Route::get('/payment_page', 'front\FrontController@payment_page')->name('payment_page');
Route::get('/payment_page/{p_type}', 'front\FrontController@payment_page');
Route::get('/payment_page/{p_type}/{interval}', 'front\FrontController@payment_page');
Route::get('/success_page', 'front\FrontController@success_page')->name('success_page');
Route::post('billing', 'BillingController@index')->name('billing');
Route::get('reprocess', 'BillingController@reprocess')->name('reprocess');

#04/01/2022
Route::get('/user/paywall/payment', 'front\FrontController@comman_paywall_payment')->name('user.paywall.payment');


Route::get('/pay','PaymentController@pay')->name('pay');
Route::post('/dopay/online', 'PaymentController@handleonlinepay')->name('dopay.online');
Route::post('/payment_card_verification', 'PaymentController@payment_card_verification')->name('payment_card_verification');
Route::post('/payment_verification_pin', 'front\FrontController@payment_verification_pin')->name('payment_verification_pin');

#Social dashboard
Route::get('/user/dashboard', 'front\SocialPostController@index')->name('user.dashboard');
Route::get('/load_data', 'front\SocialPostController@load_data')->name('load_data');

Route::get('/get_social_story', 'front\SocialPostController@get_social_story')->name('get_social_story');
Route::post('/social_post', 'front\SocialPostController@add_post')->name('social_post');
Route::post('/add_post_comment', 'front\SocialPostController@add_post_comment')->name('add_post_comment');
Route::post('/add_post_like', 'front\SocialPostController@add_post_like')->name('add_post_like');
Route::get('/get_post_like/{id}', 'front\SocialPostController@get_post_like')->name('get_post_like');
Route::get('/user/create_story', 'front\SocialPostController@create_story')->name('create_story');
Route::get('/user/text_story', 'front\SocialPostController@text_story')->name('text_story');
Route::get('/user/photo_story', 'front\SocialPostController@photo_story')->name('photo_story');
Route::post('/user/share_to_story', 'front\SocialPostController@share_to_story')->name('share_to_story');
Route::get('/post/{id}/{flag}', 'front\SocialPostController@post_param_link')->name('post');

Route::any('/user/notification_view', 'front\SocialPostController@notification_view')->name('user.notification_view');
Route::get('/provider_message', 'front\FrontController@provider_message')->name('provider_message');

#provider Supporter screen
Route::any('/provider/supporters', 'front\SupporterController@supporters');
Route::any('/provider/request_for_follow', 'front\SupporterController@request_for_follow');
Route::any('/provider/all_supporters', 'front\SupporterController@all_supporters');
Route::any('/provider/request_for_unfollow', 'front\SupporterController@request_for_unfollow');
Route::any('/provider/all_known_persons', 'front\SupporterController@all_known_persons');

#Patient Supporter screen
Route::any('/user/supporters', 'front\SupporterController@supporters')->name('user.supporters');
Route::any('/user/request_for_follow', 'front\SupporterController@request_for_follow');
Route::any('/user/request_accept', 'front\SupporterController@request_accept');
Route::any('/user/request_ignore', 'front\SupporterController@request_ignore');
Route::any('/user/all_supporters', 'front\SupporterController@all_supporters');
Route::any('/user/request_for_unfollow', 'front\SupporterController@request_for_unfollow');
Route::any('/user/all_known_persons', 'front\SupporterController@all_known_persons');

//Route::any('/facebook_feed', 'front\SocialPostController@facebook_feed');
#facebook
Route::get('/facebook_login', 'front\SocialPostController@redirectToFacebookLogin');
Route::get('/facebook_callback', 'front\SocialPostController@facebookCallback');

Route::get('/user/retrieve_profile', 'front\FacebookGraphController@retrieveUserProfile');
Route::post('/user/publish_post', 'front\FacebookGraphController@publishToPage')->name('user_publish_post_text');
Route::post('/user/publish_post_img', 'front\FacebookGraphController@publishToPageimg')->name('user_publish_post_img');
Route::post('/user/publish_post_images', 'front\FacebookGraphController@publishImages')->name('user_publish_post_images');


#cron script routes
Route::get('notification-cron', 'CronController@index')->name('notification-cron');
Route::get('subscription-cron', 'CronController@send_post_detail_to_subscriber')->name('subscription-cron');
Route::get('disable-story-time', 'CronController@disable_story_time')->name('disable-story-time');

#10/12/2021  Sudhir
Route::post('/get_post_images', 'front\SocialPostController@get_post_images')->name('get_post_images');
Route::post('/store_media_comment', 'front\SocialPostController@store_media_comment')->name('store_media_comment');
Route::post('/add_media_like', 'front\SocialPostController@add_media_like')->name('add_media_like');

#29/12/2021  Forum at replnisher and provider side
Route::any('user/forums','front\ForumsController@index')->name('user.forums');
Route::get('user/forums/add/{id}','front\ForumsController@addTopic')->name('user.forums.add');
Route::get('user/forums/addForum/{id}','front\ForumsController@addForum')->name('user.forums.addForum');
Route::post('user/forums/store','front\ForumsController@storeTopic')->name('user.forums.store');
Route::post('user/forums/storeForum','front\ForumsController@storeForum')->name('user.forums.storeForum');
Route::delete('user/forums/destroy/{id}','front\ForumsController@destroy')->name('user.forums.destroy');
Route::post('user/forums/change_status','front\ForumsController@change_status');
Route::get('user/forums/post','front\ForumsController@post_details')->name('user.forums.post');

Route::any('provider/forums','front\ForumsController@index')->name('provider.forums');
Route::get('provider/forums/add/{id}','front\ForumsController@addTopic')->name('provider.forums.add');
Route::get('provider/forums/addForum/{id}','front\ForumsController@addForum')->name('provider.forums.addForum');
Route::post('provider/forums/store','front\ForumsController@storeTopic')->name('provider.forums.store');
Route::post('provider/forums/storeForum','front\ForumsController@storeForum')->name('provider.forums.storeForum');
Route::delete('provider/forums/destroy/{id}','front\ForumsController@destroy')->name('provider.forums.destroy');
Route::post('provider/forums/change_status','front\ForumsController@change_status');
Route::get('provider/forums/post','front\ForumsController@post_details')->name('provider.forums.post');


#05/01/2022
Route::post('/paywall_card_verification', 'CommonPaymentController@payment_card_verification')->name('paywall_card_verification');
Route::post('/payment_verification_pin', 'front\FrontController@payment_verification_pin')->name('payment_verification_pin');
Route::post('/dopay/payment', 'CommonPaymentController@handleonlinepay')->name('dopay.payment');
Route::get('/user/setting', 'front\PatientController@setting')->name('user.setting');
Route::post('user/set_merchant_access', 'front\PatientController@set_merchant_access')->name('user.set_merchant_access');





Route::group(['prefix' => 'filemanager', 'middleware' => ['web']], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
});